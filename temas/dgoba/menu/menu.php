<?php
function cadena_replace_m(&$replace1,&$replace2){
	$replace1=array(' ','.',',','(',')','/','"','á','é','í','ó','ú','&aacute;','&eacute;','&iacute;','&oacute;','&uacute;','Á','É','Í','Ó','Ú','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;','ñ','Ñ','&ntilde;','&Ntilde;','&','amp;');
	$replace2=array('-','-','-','-','-','-','-','a','e','i','o','u','a','e','i','o','u','A','E','I','O','U','A','E','I','O','U','n','N','n','N','','');
}

function title(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo,$title;	
echo ucfirst($mod).' | '.$title;
}

function login(&$login_se,$username,$ID_login){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo,$dboard;
 if(isset($_SESSION['username'])){
	$login_se='<a href="'.$page_url.'index.php?mod='.$dboard.'"><i class="fa fa-sign-in"></i> '.$username.'</a> <a href="'.logout($ID_login).'" title="Salir"><i class="fa fa-power-off"></i></a>';
 }else{
	$login_se='<a href="'.$page_url.'admin/"><i class="fa fa-sign-in"></i> Login</a>';
 }
}

function top_bg(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."menu_web WHERE modulo='{$mod}' AND ext='{$ext}' AND ima_top!='';") or print mysqli_error($mysqli); 
    if($reg=mysqli_fetch_array($sql)){$ima_top=$reg['ima_top'];}else{$ima_top='gris.png';}
    echo'<style>
    .banner_mod {
         background: url('.$page_url.'modulos/Home/media/top/'.$ima_top.')no-repeat 0px 0px;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        -ms-background-size: cover;
        background-size: cover;
        background-position: center;
     	min-height: 380px;
		border-bottom: 5px solid #c8102e;
     }
     </style>';	
}

function tit_seccion(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;
$cond=($ext!='' && $ext!='index')?" AND ext='{$ext}'":"";
//echo $cond;
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."menu_web WHERE modulo='{$mod}'".$cond.";") or print mysqli_error($mysqli); 
 if($reg=mysqli_fetch_array($sql)){
    $menu=$reg['menu'];$tit_sec=$reg['tit_sec'];
	if($tit_sec!=''){echo $tit_sec;}else{echo $menu;}
 }	
}

function des_seccion(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;
$cond=($ext!='' && $ext!='index')?" AND ext='{$ext}'":"";
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."menu_web WHERE modulo='{$mod}'".$cond.";") or print mysqli_error($mysqli); 
 if($reg=mysqli_fetch_array($sql)){
	 $des_tit=$reg['des_sec'];
	 echo $des_tit;
 }	
}

function nom_cate(&$cate){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$idp;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='{$idp}';") or print mysqli_error($mysqli); 
 if($reg=mysqli_fetch_array($sql)){
 	$cate=$reg['categoria'];
 }
}

function link_url(&$link_url,$url_m,$mod_menu,$ext_menu){
global $mysqli,$DBprefix,$url,$URL,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo;
sql_opciones('link_var',$valor);
 if($valor==1){
	if($mod_menu!=''){$mod_menu='?mod='.$mod_menu;}
	if($ext_menu!=''){$ext_menu='&ext='.$ext_menu;}
	$link_url='index.php'.$mod_menu.$ext_menu;
 }else{
 	if($tema_previo!='' && $tema_previo!=NULL){
		$url_m2=(substr($url_m,0,1)=='#')?$url_m:'';
		if($mod_menu!=''){$mod_menu='&mod='.$mod_menu;}
		if($ext_menu!=''){$ext_menu='&ext='.$ext_menu;}
		$link_url=$page_url.'index.php?tema_previo='.$tema_previo.$mod_menu.$ext_menu.$url_m2;	
 	}else{
		$link_url=(substr($url_m,0,1)=='#')?$URL.$url_m:$page_url.$url_m;
 	}
 }
}

function menu_web(){
global $mysqli,$DBprefix,$url,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo,$path_jsonDB;
$tabla='menu_web';
$api_url=$page_url.'bloques/ws/t/?t='.$tabla;
$urlexists = url_exists($api_url);
$path_JSON=($urlexists!=1)?$path_jsonDB.$tabla.'.json':$api_url;	
if($path_JSON){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
usort($Data, function($a, $b){return strnatcmp($a['ord'], $b['ord']);});//Orden del menu
$i=0;
 if($_SESSION['level']!=-1){echo '<!-- '.$tabla.'.json -->'."\n\r";}else{echo '<!-- '.$tabla.'.json URL:('.$path_JSON.')-->'."\n\r";}
	foreach ($Data as $rowm){$i++;
		$ID_menu=$rowm['ID'];
		$nom_menu=$rowm['menu'];
		$url_m=$rowm['url'];
		$mod_menu=$rowm['modulo'];
		$ext_menu=$rowm['ext'];
		$orden=$rowm['ord'];
		$sub_menu=$rowm['subm'];
		$visible=$rowm['visible'];
		
		if($visible==1 && $sub_menu==NULL){
			$link_css1=($mod==$rowm['modulo'])?'azul':'';
			$k=0;$link_css2=$link_css3='';$a_datos='';
			link_url($link_url,$url_m,$mod_menu,$ext_menu);//Reubicado aqui para agregar "#" en la url cuando hay submenu 						
			foreach ($Data as $row){
				$ID_menu1=$row['ID'];
				$nom_menu1=$row['menu'];
				//$url_m1=$row['url'];
				$sub_menu1=$row['subm'];
				$visible1=$row['visible'];
				if($visible1!=0 && $sub_menu1!='' && $sub_menu1==$ID_menu){$k++;
					$link_css2='mega-menu-item-has-children';
					$link_css3='aria-haspopup="true"';
					$link_url='#';
					$a_datos='';					
					if($_SESSION['level']!=-1){echo '<!-- k:'.$k.' -->'."\n\r";}else{echo '<!--1 k:'.$k.' ID:'.$sub_menu1.' ID_menu:'.$ID_menu1.' CSS:'.$link_css2.' -->'."\n\r";}
				}				
			}			
			echo '<!--'.$i.'-'.$mod_menu.'-->
			<li class="mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-align-bottom-left mega-menu-flyout '.$link_css2.' mega-menu-item-'.$rowm['ID'].'" id="mega-menu-item-'.$rowm['ID'].'">
            	<a class="mega-menu-link '.$link_css1.'" '.$link_css3.' href="'.$link_url.'" tabindex="'.$ID_menu.'">'.$nom_menu.'</a>';
			//if($mod_menu=='Home'){echo '<span class="sr-only">(current)</span>';}
			$n=0;$j=0;
			foreach ($Data as $rowm){if($rowm['visible']==1 && $rowm['subm']==$ID_menu){$n++;}}
			echo '<!--'.$n.'-->';
			foreach ($Data as $rowm){
				$nom_menu2=$rowm['menu'];
				$url_m2=$rowm['url'];
				$mod_menu2=$rowm['modulo'];
				$ext_menu2=$rowm['ext'];
				$sub_menu2=$rowm['subm'];
				$visible2=$rowm['visible'];
				if($visible2==1 && $sub_menu2==$ID_menu){$j++;
					if($j==1){echo "\n\r".'<ul class="mega-sub-menu" style="background-color:transparent !important;">
												<li class="mega-menu-item mega-menu-columns-1 mega-menu-item-text-10" id="mega-menu-item-text-10">
                                                	<div class="textwidget">
                                                    	<ul class="sub-mega-menu">'."\n\r";}
					//link_url($link_url2,$url_m2,$mod_menu2,$ext_menu2);
					//echo '<!--j:'.$j.'--><a class="dropdown-item" href="'.$link_url2.'">'.$nom_menu2.'</a>';
						$menu_json2='categorias.json';
						$path_JSON2='modulos/productos/'.$menu_json2;
						if(!file_exists($path_JSON2)){$path_JSON2=$page_url.'bloques/ws/t/?t=productos_cate';}		
						if($_SESSION['level']!=-1){echo '<!-- '.$menu_json2.' -->'."\n\r";}else{echo '<!--'.$menu_json2.' URL:('.$path_JSON2.')-->'."\n\r";}

						if($path_JSON2){			
							$objData2=file_get_contents($path_JSON2);
							$Data2=json_decode($objData2,true);$l=0;
							foreach ($Data2 as $row){$l++;
								$ID_cate=$row['ID_cate'];
								$categoria=$row['categoria'];
								$visible3=$row['visible'];
								cadena_replace_m($replace1,$replace2);
								$cate=str_replace($replace1,$replace2,$categoria);								
								$link_url3=$page_url.'productos/categoria/'.$ID_cate.'-'.$cate;
								echo '<li id="bm'.$ID_cate.'"><!--j:'.$l.'--><i class="fa fa-circle" style="font-size:9px;"></i> <a href="'.$link_url3.'">'.$categoria.'</a>';
								
								$menu_json3='subcategorias.json';
								$path_JSON3='modulos/productos/'.$menu_json3;
								if(!file_exists($path_JSON3)){$path_JSON3=$page_url.'bloques/ws/t/?t=productos_sub_cate';}		
								if($_SESSION['level']!=-1){echo '<!-- '.$menu_json3.' -->'."\n\r";}else{echo '<!-- '.$menu_json3.' URL:('.$path_JSON3.')-->'."\n\r";}
								if($path_JSON3){			
									//echo '<div id="sm'.$ID_cate.'" class="submenu1"><ul>';
									$objData3=file_get_contents($path_JSON3);
									$Data3=json_decode($objData3,true);$k=0;$li='';
									foreach ($Data3 as $row){$k++;
										$ID_sub_cate=$row['ID_sub_cate'];
										$subcategoria=$row['subcategoria'];
										$ID_cate2=$row['ID_cate'];
										$visible4=$row['visible'];
										cadena_replace_m($replace1,$replace2);
										$subcate=str_replace($replace1,$replace2,$subcategoria);
										if($ID_cate==$ID_cate2 && $visible4==1){
											$li.='<li><!--j:'.$k.'--><a href="'.$page_url.'productos/subcategoria/'.$ID_sub_cate.'-'.$subcate.'">'.$subcategoria.'</a></li>';
											//echo '<li><!--j:'.$k.'--><a href="'.$page_url.'productos/subcategoria/'.$ID_sub_cate.'-'.$subcate.'">'.$subcategoria.'</a></li>';
										}
									}
									if($li!=''){
										echo '<div id="sm'.$ID_cate.'" class="submenu1"><ul>';
										echo $li;								
										echo '</ul></div>';
									}
								}								
								echo '</li>'."\n\r";
							}
							//echo'<li><a target="_blank" href="'.$path_JSON_cate.'">Existe archivo</a></li>';
						}else{echo'<li><a target="_blank" href="'.$path_JSON_cate.'">NO existe archivo</a></li>';}										
					if($n==$j){echo '</ul>
                                                    </div>
												</li>
        									</ul>'."\n\r";}
				}
			}
			echo '</li>';
		}
	}
 	echo '<!-- /menu.json -->
	';
 }
}

function menu_responsivo(){
global $mysqli,$DBprefix,$url,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo,$path_jsonDB;
$tabla='menu_web';
$api_url=$page_url.'bloques/ws/t/?t='.$tabla;
$urlexists = url_exists($api_url);
$path_JSON=($urlexists!=1)?$path_jsonDB.$tabla.'.json':$api_url;	
if($path_JSON){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
usort($Data, function($a, $b){return strnatcmp($a['ord'], $b['ord']);});//Orden del menu
$i=0;
 if($_SESSION['level']!=-1){echo '<!-- '.$tabla.'.json -->'."\n\r";}else{echo '<!-- '.$tabla.'.json URL:('.$path_JSON.')-->'."\n\r";}
	foreach ($Data as $rowm){$i++;
		$ID_menu=$rowm['ID'];
		$nom_menu=$rowm['menu'];
		$url_m=$rowm['url'];
		$mod_menu=$rowm['modulo'];
		$ext_menu=$rowm['ext'];
		$orden=$rowm['ord'];
		$sub_menu=$rowm['subm'];
		$visible=$rowm['visible'];
		
		if($visible==1 && $sub_menu==NULL){
			$link_css1=($mod==$rowm['modulo'])?'azul':'';
			$k=0;$link_css2=$link_css3='';$a_datos='';
			link_url($link_url,$url_m,$mod_menu,$ext_menu);//Reubicado aqui para agregar "#" en la url cuando hay submenu 						
			foreach ($Data as $row){
				$ID_menu1=$row['ID'];
				$nom_menu1=$row['menu'];
				//$url_m1=$row['url'];
				$sub_menu1=$row['subm'];
				$visible1=$row['visible'];
				if($visible1!=0 && $sub_menu1!='' && $sub_menu1==$ID_menu){$k++;
					$link_css2='mega-menu-item-has-children';
					$link_css3='aria-haspopup="true"';
					$link_url='#';
					$a_datos='';					
					if($_SESSION['level']!=-1){echo '<!-- k:'.$k.' -->'."\n\r";}else{echo '<!--1 k:'.$k.' ID:'.$sub_menu1.' ID_menu:'.$ID_menu1.' CSS:'.$link_css2.' -->'."\n\r";}
				}				
			}			
			echo '<!--'.$i.'-'.$mod_menu.'-->
			<!--li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-6 current_page_item menu-item-20"-->
			<li class="mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-align-bottom-left mega-menu-flyout '.$link_css2.' mega-menu-item-'.$rowm['ID'].'" id="mega-menu-item-'.$rowm['ID'].'">
            	<a class="mega-menu-link '.$link_css1.'" '.$link_css3.' href="'.$link_url.'" tabindex="'.$ID_menu.'">'.$nom_menu.'</a>';
			//if($mod_menu=='Home'){echo '<span class="sr-only">(current)</span>';}
			$n=0;$j=0;
			foreach ($Data as $rowm){if($rowm['visible']==1 && $rowm['subm']==$ID_menu){$n++;}}
			echo '<!--'.$n.'-->';
			foreach ($Data as $rowm){
				$nom_menu2=$rowm['menu'];
				$url_m2=$rowm['url'];
				$mod_menu2=$rowm['modulo'];
				$ext_menu2=$rowm['ext'];
				$sub_menu2=$rowm['subm'];
				$visible2=$rowm['visible'];
				if($visible2==1 && $sub_menu2==$ID_menu){$j++;
					if($j==1){echo "\n\r".'<ul class="mega-sub-menu" style="background-color:transparent !important;">
												<li class="mega-menu-item mega-menu-columns-1 mega-menu-item-text-10" id="mega-menu-item-text-10">
                                                	<div class="textwidget">
                                                    	<ul class="sub-mega-menu">'."\n\r";}
					//link_url($link_url2,$url_m2,$mod_menu2,$ext_menu2);
					//echo '<!--j:'.$j.'--><a class="dropdown-item" href="'.$link_url2.'">'.$nom_menu2.'</a>';
						$menu_json2='categorias.json';
						$path_JSON2='modulos/productos/'.$menu_json2;
						if(!file_exists($path_JSON2)){$path_JSON2=$page_url.'bloques/ws/t/?t=productos_cate';}		
						if($_SESSION['level']!=-1){echo '<!-- '.$menu_json2.' -->'."\n\r";}else{echo '<!--'.$menu_json2.' URL:('.$path_JSON2.')-->'."\n\r";}

						if($path_JSON2){			
							$objData2=file_get_contents($path_JSON2);
							$Data2=json_decode($objData2,true);$l=0;
							foreach ($Data2 as $row){$l++;
								$ID_cate=$row['ID_cate'];
								$categoria=$row['categoria'];
								$visible3=$row['visible'];
								cadena_replace_m($replace1,$replace2);
								$cate=str_replace($replace1,$replace2,$categoria);								
								$link_url3=$page_url.'productos/categoria/'.$ID_cate.'-'.$cate;
								echo '<li id="bm'.$ID_cate.'"><!--j:'.$l.'--><i class="fa fa-circle" style="font-size:9px;"></i> <a href="'.$link_url3.'">'.$categoria.'</a>';
								
								$menu_json3='subcategorias.json';
								$path_JSON3='modulos/productos/'.$menu_json3;
								if(!file_exists($path_JSON3)){$path_JSON3=$page_url.'bloques/ws/t/?t=productos_sub_cate';}		
								if($_SESSION['level']!=-1){echo '<!-- '.$menu_json3.' -->'."\n\r";}else{echo '<!-- '.$menu_json3.' URL:('.$path_JSON3.')-->'."\n\r";}
								if($path_JSON3){			
									//echo '<div id="sm'.$ID_cate.'" class="submenu1"><ul>';
									$objData3=file_get_contents($path_JSON3);
									$Data3=json_decode($objData3,true);$k=0;$li='';
									foreach ($Data3 as $row){$k++;
										$ID_sub_cate=$row['ID_sub_cate'];
										$subcategoria=$row['subcategoria'];
										$ID_cate2=$row['ID_cate'];
										$visible4=$row['visible'];
										cadena_replace_m($replace1,$replace2);
										$subcate=str_replace($replace1,$replace2,$subcategoria);
										if($ID_cate==$ID_cate2 && $visible4==1){
											$li.='<li><!--j:'.$k.'--><a href="'.$page_url.'productos/subcategoria/'.$ID_sub_cate.'-'.$subcate.'">'.$subcategoria.'</a></li>';
											//echo '<li><!--j:'.$k.'--><a href="'.$page_url.'productos/subcategoria/'.$ID_sub_cate.'-'.$subcate.'">'.$subcategoria.'</a></li>';
										}
									}
									if($li!=''){
										echo '<div id="sm'.$ID_cate.'" class="submenu1"><ul>';
										echo $li;								
										echo '</ul></div>';
									}
								}								
								echo '</li>'."\n\r";
							}
							//echo'<li><a target="_blank" href="'.$path_JSON_cate.'">Existe archivo</a></li>';
						}else{echo'<li><a target="_blank" href="'.$path_JSON_cate.'">NO existe archivo</a></li>';}										
					if($n==$j){echo '</ul>
                                                    </div>
												</li>
        									</ul>'."\n\r";}
				}
			}
			echo '</li>';
		}
	}
 	echo '<!-- /menu.json -->
	';
 }
}

function slider(){
global $mysqli,$DBprefix,$tema,$page_url,$path_tema,$mod,$ext,$opc,$path_jsonDB;
$tabla='slider';
$api_url=$page_url.'bloques/ws/t/?t='.$tabla;
$urlexists = url_exists($api_url);
$path_JSON=($urlexists!=1)?$path_jsonDB.$tabla.'.json':$api_url;	

 if($path_JSON){
 $objData=file_get_contents($path_JSON);
 $Data=json_decode($objData,true);
 $i=-1;
	$op_f=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
	foreach ($Data as $rowm){$i++;
		$id=$rowm['ID'];
		$cover=$rowm['ima'];
		$tit1=$rowm['tit1'];
		$tit2=$rowm['tit2'];
		$des=$rowm['des_slide'];
		$btn=$rowm['btn_nom'];
		$url_s=$rowm['url'];
		$tema_slider=$rowm['tema_slider'];
		$act=($i!=0)?'':' active';
		$visible=$rowm['visible'];
		if($visible==1 && $tema_slider==$tema){
			$indicador.='
			<li data-target="#myCarousel" data-slide-to="'.$i.'" class="'.$act.'"></li>';

			$tit1_1=($tit1!='')?''.$tit1.'':'';
			$tit2_1=($tit2!='')?''.$tit2.'':'';
			$des=($des!='')?''.$des.'':'';
			$img=($id==4)?'<img id="im_btn" src="'.$page_url.$path_tema.'img/marca.png">':'';
			$btn_1=($btn!='')?'<div class="ma-a"><a id="btn-'.$id.$i.'" href="'.$url_s.'" style="float:left;">'.$btn.'</a>'.$img.'</div>':'';

			$diapositivas.='
						<!--Id:'.$id.' - Item[i]:'.$i.'-->
                        <li data-index="rs-40" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="'.$page_url.'modulos/Home/media/slide/'.$cover.'" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="'.$page_url.'modulos/Home/media/slide/'.$cover.'" alt="" title="Banner_Lumify_Morado_" width="1600" height="600" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <div id="layer-content">
                            	<div id="layer-space">
                                    <div class="tit28">'.$tit1_1.'</div>
                                	<div class="tit52">'.$tit2_1.'</div>
                                    <div class="txt18">'.$des.'</div>
                                    '.$btn_1.'
                                </div>
                            </div>
                        </li>
			';
		}//if
	}//foreach
		echo '
<!-- slider -->
        <div class="page-slider header-slider">
			<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:#2b2d35;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.4.5.1 fullwidth mode -->
                <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.5.1">
                    <ul>
                        <!-- SLIDES  -->
						'.$diapositivas.'
                        <!-- SLIDES  -->
                    </ul>
                    <script>
                        var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                        var htmlDivCss = "";
                        if (htmlDiv) {
                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                            var htmlDiv = document.createElement("div");
                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
                <script>
                    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                    var htmlDivCss = "";
                    if (htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                    } else {
                        var htmlDiv = document.createElement("div");
                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                    }
                </script>
                <script type="text/javascript">
                    setREVStartSize({
                        c: jQuery(\'#rev_slider_1_1\'),
                        gridwidth: [1170],
                        gridheight: [410],
                        sliderLayout: \'fullwidth\'
                    });

                    var revapi1,
                        tpj = jQuery;
                    tpj.noConflict();
                    tpj(document).ready(function() {
                        if (tpj("#rev_slider_1_1").revolution == undefined) {
                            revslider_showDoubleJqueryError("#rev_slider_1_1");
                        } else {
                            revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                sliderType: "standard",
                                //jsFileLocation: "//www.manprec.com/wp-content/plugins/revslider/public/assets/js/",
                                jsFileLocation: "'.$page_url.'temas/goba/js/",
                                sliderLayout: "fullwidth",
                                dottedOverlay: "none",
                                delay: 9000,
                                navigation: {
                                    keyboardNavigation: "on",
                                    keyboard_direction: "horizontal",
                                    mouseScrollNavigation: "off",
                                    mouseScrollReverse: "default",
                                    onHoverStop: "off",
                                    touch: {
                                        touchenabled: "on",
                                        touchOnDesktop: "off",
                                        swipe_threshold: 75,
                                        swipe_min_touches: 1,
                                        swipe_direction: "horizontal",
                                        drag_block_vertical: false
                                    },
									/*
									bullets: {
										enable: true,
										style: "hesperiden"
									},*/
									/*	
									tabs: {
        								enable: true,
        								style: "hesperiden",
        								width: 180,
        								height: 65,
        								min_width: 180,
        								wrapper_padding: 0,
        								wrapper_color: "#FFF",
        								wrapper_opacity: "0.65",
        								tmp: \'<div class="tp-tab-content"><span class="tp-tab-date">{{param1}}</span><span class="tp-tab-title">{{title}}</span></div><div class="tp-tab-image"></div>\'
 									},*/
                                    arrows: {
										enable:true,
                                        style: "hesperiden",
                                        hide_onmobile: false,
                                        hide_onleave: false,
                                        tmp: \'\',
										left: {
                                            h_align: "left",
                                            v_align: "center",
                                            h_offset: 20,
                                            v_offset: 10
                                        },
                                        right: {
                                            h_align: "right",
                                            v_align: "center",
                                            h_offset: 20,
                                            v_offset: 10
                                        }
                                    }
                                },
                                visibilityLevels: [1240, 1024, 778, 480],
                                gridwidth: 1170,
                                gridheight: 410,
                                lazyType: "none",
                                shadow: 0,
                                spinner: "spinner0",
                                stopLoop: "off",
                                stopAfterLoops: -1,
                                stopAtSlide: -1,
                                shuffle: "off",
                                autoHeight: "on",
                                disableProgressBar: "on",
                                hideThumbsOnMobile: "off",
                                hideSliderAtLimit: 0,
                                hideCaptionAtLimit: 0,
                                hideAllCaptionAtLilmit: 0,
                                debugMode: false,
                                fallbacks: {
                                    simplifyAll: "off",
                                    nextSlideOnWindowFocus: "off",
                                    disableFocusListener: false,
                                }
                            });
                        }

                    }); /*ready*/
                </script>
                <script>
                    var htmlDivCss = unescape(".hesperiden.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%09width%3A40px%3B%0A%09height%3A40px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%20%20%20%20border-radius%3A%2050%25%3B%0A%7D%0A.hesperiden.tparrows%3Ahover%20%7B%0A%09background%3Argba%280%2C%200%2C%200%2C%201%29%3B%0A%7D%0A.hesperiden.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A20px%3B%0A%09color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%2040px%3B%0A%09text-align%3A%20center%3B%0A%7D%0A.hesperiden.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce82c%22%3B%0A%20%20%20%20margin-left%3A-3px%3B%0A%7D%0A.hesperiden.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce82d%22%3B%0A%20%20%20%20margin-right%3A-3px%3B%0A%7D%0A");
                    var htmlDiv = document.getElementById(\'rs-plugin-settings-inline-css\');
                    if (htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                    } else {
                        var htmlDiv = document.createElement(\'div\');
                        htmlDiv.innerHTML = \'<style>\' + htmlDivCss + \'</style>\';
                        document.getElementsByTagName(\'head\')[0].appendChild(htmlDiv.childNodes[0]);
                    }
                </script>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>
<!-- /slider -->
		';	
 }
}
?>