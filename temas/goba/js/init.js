(function( $ ) {

"use strict";
/*global Modernizr, jQuery */
/*jslint bitwise: true */

var LOLLUM = {};

Modernizr.addTest('firefox', function () {
	return !!navigator.userAgent.match(/firefox/i);
});

/*-----------------------------------------------------------------------------------*/
/*	Navigation Menu
/*-----------------------------------------------------------------------------------*/
LOLLUM.submenu = function() {
	$( '#nav-menu .sf-menu' ).find( '.megamenu_wrap' ).find( 'ul' ).addClass( 'sub-mega' );
	$( '#nav-menu .sf-menu' ).superfish({
		popUpSelector: 'ul, .sf-mega, .megamenu_wrap',
		autoArrows: false,
		delay: 500,
		speed: 200,
		speedOut: 200,
		useClick: false
	});
};
/*-----------------------------------------------------------------------------------*/
/*	Sticky Header
/*-----------------------------------------------------------------------------------*/
LOLLUM.sticky = function() {
	var t = 50,
		body = $( document.documentElement ),
		header = $( '#branding' );
	resizeHeader();
	$( window ).scroll( resizeHeader );
	$( window ).resize(function(){
		resizeHeader();
	});
	function resizeHeader() {
		if ( Modernizr.mq( 'only screen and (min-width: 992px)' ) || body.hasClass( 'no-responsive' ) ) {
			var st = $( window ).scrollTop();
			if ( st > t ) {
				body.addClass( 'fixed-yes' );
				if ( body.hasClass( 'header-transparent' ) ) {
					header.removeClass().addClass( header.data( 'style-end' ) );
				}
			} else {
				body.removeClass( 'fixed-yes' );
				if ( body.hasClass( 'header-transparent' ) ) {
					header.removeClass().addClass( header.data( 'style-start' ) );
				}
			}
		} else {
			body.removeClass( 'fixed-yes' );
			if ( body.hasClass( 'header-transparent' ) ) {
				header.removeClass().addClass( header.data( 'style-start' ) );
			}
		}
	}
};
/*-----------------------------------------------------------------------------------*/
/*	Mobile Menu
/*-----------------------------------------------------------------------------------*/
LOLLUM.mobileMenu = function() {
	var mobileOpen = false,
		mobileMenu = $( '#mobile-menu' ),
		mobileMenuLink = $( '#mobile-menu-link' ),
		body = $( document.documentElement );
	mobileMenuLink.on( 'click', function( e ) {
		e.preventDefault();
		if ( mobileOpen === false ) {
			openMobileMenu();
		} else {
			closeMobileMenu();
		}
	});
	function openMobileMenu() {
		mobileOpen = true;
		body.addClass( 'mobile-menu-open' );
		mobileMenu.slideDown();
	}
	function closeMobileMenu() {
		if ( mobileOpen === true ) {
			mobileOpen = false;
			mobileMenu.slideUp();
			mobileMenu.find( '.sub-menu' ).slideUp();
			body.removeClass( 'mobile-menu-open' );
		}
	}
	$( window ).resize( function() {
		if ( mobileOpen === true ) {
			closeMobileMenu();
		}
	});
	$( '#mobile-menu .menu-parent-item' ).on( 'click', function( e ) {
		e.preventDefault();
		$( this ).addClass( 'active' );
		$( this ).children( '.sub-menu' ).slideToggle();
		$( this ).find( '.sub-menu .sub-menu' ).slideUp();
	});
	$( '#mobile-menu .sub-menu' ).not( 'menu-parent-item a' ).on( 'click', function( e ) {
		e.stopPropagation();
	});
};
/*-----------------------------------------------------------------------------------*/
/*	Cart Header
/*-----------------------------------------------------------------------------------*/
LOLLUM.cartHeader = function() {
	var offOpen = false,
		clickable = false,
		cartLink = $( '#icon-cart' ),
		body = $( document.documentElement );
	cartLink.on( 'click', function( e ) {
		e.preventDefault();
		if ( offOpen === false ) {
			openSidebarCart();
		} else {
			closeSidebarCart();
		}
	});
	function openSidebarCart() {
		if ( ! body.hasClass( 'mobile-menu-open' ) ) {
			body.addClass( 'move-left' );
			offOpen = true;
			setTimeout( function () {
				body.addClass( 'off-open' );
				clickable = true;
			}, 1);
		}
	}
	function closeSidebarCart() {
		if ( offOpen === true ) {
			offOpen = false;
			clickable = false;
			body.removeClass( 'move-left' );
			setTimeout( function () {
				body.removeClass( 'off-open' );
			}, 1);
		}
	}
	$( window ).resize( function() {
		if ( offOpen === true ) {
			closeSidebarCart();
		}
	});
	$( '#wrap' ).on( 'click', function() {
		if ( offOpen === true && clickable === true ) {
			closeSidebarCart();
		}
	});
};
/*-----------------------------------------------------------------------------------*/
/*	Search Header
/*-----------------------------------------------------------------------------------*/
LOLLUM.searchHeader = function() {
	var searchButton = $( '#icon-search-btn ' ),
		branding = $( '#branding ' ),
		input = $( '#branding .searchbox input' );
	searchButton.on( 'click', function(e) {
		branding.toggleClass( 'search-active' );
		input.select();
		e.preventDefault();
	});
};
/*-----------------------------------------------------------------------------------*/
/*	fitVids
/*-----------------------------------------------------------------------------------*/
LOLLUM.rVideos = function() {
	$( '.entry-video, .video-widget, .entry-content' ).fitVids();
};
/*-----------------------------------------------------------------------------------*/
/*	Flexslider
/*-----------------------------------------------------------------------------------*/
LOLLUM.flex = function() {
	if ( jQuery().flexslider ) {
		$( '.flex-gallery' ).flexslider({
			controlNav: false,
			slideshowSpeed: 4000,
			animation: "fade",
			animationSpeed: 600,
			start: function( slider ) {
				slider.find( '.preloader' ).hide();
			}
		});
		$( '.flex-testimonial' ).flexslider({
			controlNav: false,
			directionNav: false,
			slideshowSpeed: 8000,
			animation: "slide",
			smoothHeight:true,
			animationSpeed: 600,
			start: function( slider ) {
				slider.find( '.preloader' ).hide();
			}
		});
		$( '.flex-product' ).flexslider({
			manualControls: ".thumbnails-nav img",
			slideshow: false,
			animation: "fade",
			animationSpeed: 600,
			start: function( slider ) {
				slider.find( '.preloader' ).hide();
			}
		});
	}
};
/*-----------------------------------------------------------------------------------*/
/*	Toggles
/*-----------------------------------------------------------------------------------*/
LOLLUM.toggle = function() {
	var lolToggle = $( '.lol-toggle' ),
		toggleContents = $( '.lol-toggle' ).find( '.lol-toggle-content' );
	toggleContents.not( '[data-toggle="open"]' ).css( 'display', 'none' );
	lolToggle.on( 'click', '.lol-toggle-header', function() {
		var toggleContent = $( this ).next( '.lol-toggle-content' );
		toggleContent.slideToggle();
	});
};
/*-----------------------------------------------------------------------------------*/
/*	FAQs
/*-----------------------------------------------------------------------------------*/
LOLLUM.faq = function() {
	var lolFaq = $( '.lol-faq' ),
		faqContents = $( '.lol-faq' ).find( '.lol-faq-content' ),
		sortFaq = $( '.faqs-select select' );
	faqContents.css( 'display', 'none' );
	lolFaq.on( 'click', '.lol-faq-header', function(){
		var faqContent = $( this ).next( '.lol-faq-content' );
		faqContent.slideToggle();
	});
	sortFaq.change( function() {
		var selector = $( this ).val();
		if ( selector !== '*' ) {
			$( '.lol-faq-wrap .lol-faq' ).not( '[data-filter~="' + selector + '"]' ).slideUp( 'normal', function(){
				$( '.lol-faq-wrap .lol-faq' ).filter( '[data-filter~="' + selector + '"]' ).slideDown();
			});
		} else {
			$( '.lol-faq-wrap .lol-faq' ).slideDown();
		}
	});
	$( '.faqs-tabs' ).find( 'a' ).on( 'click', function( e ) {
		e.preventDefault();
		var selector = $( this ).attr( 'data-filter' );
		if ( selector !== '*' ) {
			$( '.lol-faq-wrap .lol-faq' ).not( '[data-filter="' + selector + ' "]' ).slideUp( 'normal', function(){
				$( '.lol-faq-wrap .lol-faq' ).filter( '[data-filter="' + selector + ' "]' ).slideDown();
			});
		} else {
			$( '.lol-faq-wrap .lol-faq' ).slideDown();
		}
		$( this ).parents( 'ul' ).find( 'li' ).removeClass( 'active' );
		$( this ).parent().addClass( 'active' );
	});
	$( '#lol-faq-topics li a, .back-faqs' ).on( 'click', function( e ) {
		e.preventDefault();
		var target = this.hash,
		$target = $( target );
		$( 'html, body' ).stop().animate({
		'scrollTop': $target.offset().top
		}, 600, function () {
			window.location.hash = target;
		});
	});
};
/*-----------------------------------------------------------------------------------*/
/*	Placeholder Support
/*-----------------------------------------------------------------------------------*/
LOLLUM.placeholder = function() {
	if ( ! Modernizr.input.placeholder ) {
		$( '[placeholder]' ).focus(function() {
			var input = $( this );
			if ( input.val() === input.attr( 'placeholder' ) ) {
				input.val( '' );
				input.removeClass( 'placeholder' );
			}
		}).blur( function() {
			var input = $( this );
			if ( input.val() === '' || input.val() === input.attr( 'placeholder' ) ) {
				input.addClass( 'placeholder' );
				input.val(input.attr( 'placeholder' ) );
			}
		}).blur();
		$( '[placeholder]' ).parents( 'form' ).submit( function() {
			$( this ).find( '[placeholder]' ).each( function() {
				var input = $( this );
				if ( input.val() === input.attr( 'placeholder' ) ) {
					input.val( '' );
				}
			});
		});
	}
};
/*-----------------------------------------------------------------------------------*/
/*	Filterable Portfolio
/*-----------------------------------------------------------------------------------*/
LOLLUM.isotope = function() {
	if ( jQuery().isotope ) {
		var itemsContainer = $( '.has-isotope' ),
			portfolioFilter = $( '.portfolio-filter select' ),
			layoutMode = 'fitRows';
		if ( itemsContainer.hasClass( 'masonry' ) ) {
			layoutMode = 'masonry';
		}
		itemsContainer.isotope({
			itemSelector: '.isotope-block-item',
			layoutMode: layoutMode
		});
		itemsContainer.isotope('reloadItems').isotope();
		portfolioFilter.change(function() {
			var selector = $( this ).val();
			itemsContainer.isotope({ filter: selector });
		});
		$( '.portfolio-tabs' ).find( 'a' ).on('click', function( e ) {
			e.preventDefault();
			var selector = $( this ).attr( 'data-filter' );
			itemsContainer.isotope({ filter: selector });
			$( this ).parents( 'ul' ).find( 'li' ).removeClass( 'active' );
			$( this ).parent().addClass( 'active' );
		});
	}
};
/*-----------------------------------------------------------------------------------*/
/*	Skills
/*-----------------------------------------------------------------------------------*/
LOLLUM.animations = function() {
	var skills = $( '.lol-skill' ).find( '.lol-bar' );
	skills.appear( function() {
		var currentSkill = $( this );
		var w = currentSkill.data( 'skill' );
		$( this ).animate({ width: w + "%" }, 600 );
	});
	if( jQuery().countTo ) {
		var progressNumbers = $( '.progress-number' );
		progressNumbers.appear(function() {
			$( '.timer' ).countTo();
		});
	}
};
/*-----------------------------------------------------------------------------------*/
/*	Parallax
/*-----------------------------------------------------------------------------------*/
LOLLUM.bgParallax = function() {
	if ( jQuery().parallax ) {
		var parallaxSections = $( '.parallax-yes' );
		parallaxSections.each(function() {
			$( this ).parallax( "49%", 0.3 );
		});
		$( window ).resize(function() {
			parallaxSections.each(function() {
				$( this ).parallax( "49%", 0.3 );
			});
		});
	}
};
/*-----------------------------------------------------------------------------------*/
/*	prettyPhoto
/*-----------------------------------------------------------------------------------*/
LOLLUM.pretty = function() {
	if ( jQuery().prettyPhoto ) {
		$( '.lol-item-image, .portfolio-item' ).find( 'a[data-rel^="prettyPhoto"]' ).prettyPhoto({
			deeplinking: false,
			social_tools: false,
			show_title: false,
			theme: 'pp_default'
		});
	}
};
/*-----------------------------------------------------------------------------------*/
/*	Checkout Height
/*-----------------------------------------------------------------------------------*/
LOLLUM.checkoutHeight = function() {
	if ( $( document.body ).hasClass( 'woocommerce-checkout' ) ) {
		var h = $( '#review-col' ).height() + 80;
		$( 'form.checkout' ).css( 'min-height', h );
	}
};
/*-----------------------------------------------------------------------------------*/
/*	Carousel Blocks
/*-----------------------------------------------------------------------------------*/
LOLLUM.carouselBlocks = function() {
	if ( jQuery().owlCarousel ) {
		$( '.carousel-items' ).owlCarousel({
			items : 4,
			loop: true,
			nav: true,
			dots: false,
			merge:true
		});
	}
};
/*-----------------------------------------------------------------------------------*/
/*	Carousel Images
/*-----------------------------------------------------------------------------------*/
LOLLUM.carouselImages = function() {
	if ( jQuery().owlCarousel ) {
		$( '.carousel-images' ).each( function(){
			var carouselItem = $( this ),
				columns = carouselItem.attr( 'data-columns' ),
				loop = carouselItem.attr( 'data-loop' );
			loop = ( loop == 'yes' ) ? true : false;
			carouselItem.owlCarousel({
				items : columns,
				loop: loop,
				nav: true,
				dots: false
			});
		});
	}
};
/*-----------------------------------------------------------------------------------*/
/*	Init Functions
/*-----------------------------------------------------------------------------------*/
$( document ).ready(function() {
	if ( Modernizr.touch ) {
		$( document.documentElement ).removeClass( 'lol-sticky-header-yes' );
	}
	LOLLUM.submenu();
	LOLLUM.mobileMenu();
	LOLLUM.cartHeader();
	LOLLUM.searchHeader();
	LOLLUM.rVideos();
	LOLLUM.flex();
	LOLLUM.toggle();
	LOLLUM.faq();
	LOLLUM.placeholder();
	LOLLUM.pretty();
	LOLLUM.animations();
	LOLLUM.checkoutHeight();
	LOLLUM.carouselBlocks();
	LOLLUM.carouselImages();
	if ( ( $( document.documentElement ).hasClass( 'lol-sticky-header-yes' ) ) && ( ! Modernizr.touch ) ) {
		LOLLUM.sticky();
	}

	$( 'body' ).on('change', 'input[name=payment_method]:radio', function() {
		setTimeout(function () {
			LOLLUM.checkoutHeight();
		}, 500);
	});
});

$( window ).load( function() {
	if (!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch)) {
		LOLLUM.bgParallax();
	}
	LOLLUM.isotope();
});

})( jQuery );
