<!DOCTYPE html>
<html class="no-js yes-responsive lol-sticky-header-yes header-no-transparent  blog-classic " lang="es-MX" prefix="og: http://ogp.me/ns#">
<head>
<!--Carateres-->
<meta charset="iso-8859-1">
<!--meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /-->
<title>Home | DGoba - La m&aacute;s grande red en el baj&iacute;&shy;o</title>
<!--Responsive Meta-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- META-TAGS generadas por http://metatags.miarroba.es -->
<META NAME="DC.Language" SCHEME="RFC1766" CONTENT="Spanish">
<META NAME="AUTHOR" CONTENT="Guillermo Jimenez">
<META NAME="REPLY-TO" CONTENT="multiportal@outlook.com">
<LINK REV="made" href="mailto:multiportal@outlook.com">
<META NAME="DESCRIPTION" CONTENT="Venta de equipo de rayos-x.">
<META NAME="KEYWORDS" CONTENT="samsung-healthcare,salud,equipo medico,rayos-x">
<!-- LINK-ICON -->
<link rel="shortcut icon" type="image/x-icon" href="http://dgoba.com/temas/dgoba/images/icon/favicon.ico">
<link rel="icon" type="image/x-icon" href="http://dgoba.com/temas/dgoba/images/icon/favicon.ico">
<link rel="apple-touch-icon" sizes="57x57" href="http://dgoba.com/temas/dgoba/images/icon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="http://dgoba.com/temas/dgoba/images/icon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="http://dgoba.com/temas/dgoba/images/icon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="http://dgoba.com/temas/dgoba/images/icon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="http://dgoba.com/temas/dgoba/images/icon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="http://dgoba.com/temas/dgoba/images/icon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="http://dgoba.com/temas/dgoba/images/icon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="http://dgoba.com/temas/dgoba/images/icon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="http://dgoba.com/temas/dgoba/images/icon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="http://dgoba.com/temas/dgoba/images/icon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="http://dgoba.com/temas/dgoba/images/icon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="http://dgoba.com/temas/dgoba/images/icon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="http://dgoba.com/temas/dgoba/images/icon/favicon-16x16.png">
<link rel="manifest" href="http://dgoba.com/temas/dgoba/images/icon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="http://dgoba.com/temas/dgoba/images/icon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- LINK-STYLE -->
<!--Se sustituyen todos los estilos anterior: < ?php echo $style;? > -->
<!-- ?php style();?-->
<!-- JAVASCRIPT -->
<script src="http://dgoba.com/assets/js/main.js" type="text/javascript" language="javascript"></script>
<!--GOOGLE ANALITYCS-->
<!--FIN GOOGLE ANALITYCS-->
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <link rel='stylesheet' id='contact-form-7-css' href='http://dgoba.com/temas/dgoba/css/styles.css?ver=4.7' type='text/css' media='all' />
        <link rel='stylesheet' id='essential-grid-plugin-settings-css' href='http://dgoba.com/temas/dgoba/css/settings.css?ver=2.1.0.2' type='text/css' media='all' />
        <link rel='stylesheet' id='tp-open-sans-css' href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='tp-raleway-css' href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='tp-droid-serif-css' href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='rs-plugin-settings-css' href='http://dgoba.com/temas/dgoba/css/settings1.css?ver=5.4.5.1' type='text/css' media='all' />
        <style id='rs-plugin-settings-inline-css' type='text/css'>
            #rs-demo-id {}
        </style>
        <link rel='stylesheet' id='megamenu-css' href='http://dgoba.com/temas/dgoba/css/style_1.css?ver=277a93' type='text/css' media='all' />
        <link rel='stylesheet' id='dashicons-css' href='http://dgoba.com/temas/dgoba/css/dashicons.min.css?ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='f-primary-400-css' href='https://fonts.googleapis.com/css?family=Raleway%3A400&#038;ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='f-primary-400i-css' href='https://fonts.googleapis.com/css?family=Raleway%3A400italic&#038;ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='f-primary-700-css' href='https://fonts.googleapis.com/css?family=Raleway%3A700&#038;ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='grid-css-css' href='http://dgoba.com/temas/dgoba/css/grid.css?ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='fonts-css' href='http://dgoba.com/temas/dgoba/css/fonts.css?ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='nantes-default-css' href='http://dgoba.com/temas/dgoba/css/style.css?ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='nantes-css-css' href='http://dgoba.com/temas/dgoba/css/base.css?ver=4.7.9' type='text/css' media='all' />
        <link rel='stylesheet' id='woocommerce-css-css' href='http://dgoba.com/temas/dgoba/css/woocommerce.css?ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='nantes-custom-css' href='http://dgoba.com/temas/dgoba/css/custom.css?ver=4.7.8' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='ie8-css-css'  href='https://www.dgoba.com/wp-content/themes/nantes/css/ie8.css?ver=4.7.8' type='text/css' media='all' />
<![endif]-->
        <link rel='stylesheet' id='lolfmk-prettyPhoto-css-css' href='http://dgoba.com/temas/dgoba/css/prettyPhoto.css?ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='mediaelement-css' href='http://dgoba.com/temas/dgoba/css/mediaelementplayer.min.css?ver=2.22.0' type='text/css' media='all' />
        <link rel='stylesheet' id='wp-mediaelement-css' href='http://dgoba.com/temas/dgoba/css/wp-mediaelement.min.css?ver=4.7.8' type='text/css' media='all' />
        <link rel='stylesheet' id='views-pagination-style-css' href='http://dgoba.com/temas/dgoba/css/wpv-pagination.css?ver=2.5.1' type='text/css' media='all' />
        <style id='views-pagination-style-inline-css' type='text/css'>
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-default > span.wpv-sort-list,
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-default .wpv-sort-list-item {
                border-color: #cdcdcd;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-default .wpv-sort-list-item a {
                color: #444;
                background-color: #fff;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-default a:hover,
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-default a:focus {
                color: #000;
                background-color: #eee;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-default .wpv-sort-list-item.wpv-sort-list-current a {
                color: #000;
                background-color: #eee;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-grey > span.wpv-sort-list,
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-grey .wpv-sort-list-item {
                border-color: #cdcdcd;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-grey .wpv-sort-list-item a {
                color: #444;
                background-color: #eeeeee;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-grey a:hover,
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-grey a:focus {
                color: #000;
                background-color: #e5e5e5;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-grey .wpv-sort-list-item.wpv-sort-list-current a {
                color: #000;
                background-color: #e5e5e5;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-blue > span.wpv-sort-list,
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-blue .wpv-sort-list-item {
                border-color: #0099cc;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-blue .wpv-sort-list-item a {
                color: #444;
                background-color: #cbddeb;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-blue a:hover,
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-blue a:focus {
                color: #000;
                background-color: #95bedd;
            }
            
            .wpv-sort-list-dropdown.wpv-sort-list-dropdown-style-blue .wpv-sort-list-item.wpv-sort-list-current a {
                color: #000;
                background-color: #95bedd;
            }
        </style>
        <!--
      <script>
      if (document.location.protocol != "https:") {
          document.location = document.URL.replace(/^http:/i, "https:");
      }
      </script>
-->
        <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.js?ver=1.12.4'></script>
        <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery-migrate.min.js?ver=1.4.1'></script>
        <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/lightbox.js?ver=2.1.0.2'></script>
        <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.themepunch.tools.min.js?ver=2.1.0.2'></script>
        <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.themepunch.revolution.min.js?ver=5.4.5.1'></script>
        <!--script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/public.js?ver=5.0.7'></script-->
        <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/modernizr.js?ver=4.7.8'></script>
        <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.colorbox.js?ver=4.7.8'></script>
        <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.tooltip.js?ver=4.7.8'></script>
        <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/lightbox-gallery.js?ver=4.7.8'></script>

        <link rel="stylesheet" type="text/css" href="http://dgoba.com/temas/dgoba/css/lightbox-gallery.css" />
        <script type="text/javascript">
            var ajaxRevslider;

            jQuery(document).ready(function() {
                // CUSTOM AJAX CONTENT LOADING FUNCTION
                ajaxRevslider = function(obj) {

                    // obj.type : Post Type
                    // obj.id : ID of Content to Load
                    // obj.aspectratio : The Aspect Ratio of the Container / Media
                    // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content

                    var content = "";

                    data = {};

                    data.action = 'revslider_ajax_call_front';
                    data.client_action = 'get_slider_html';
                    data.token = '66e3c2e76d';
                    data.type = obj.type;
                    data.id = obj.id;
                    data.aspectratio = obj.aspectratio;

                    // SYNC AJAX REQUEST
                    jQuery.ajax({
                        type: "post",
                        url: "https://www.dgoba.com/wp-admin/admin-ajax.php",
                        dataType: 'json',
                        data: data,
                        async: false,
                        success: function(ret, textStatus, XMLHttpRequest) {
                            if (ret.success == true)
                                content = ret.data;
                        },
                        error: function(e) {
                            console.log(e);
                        }
                    });

                    // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
                    return content;
                };

                // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
                var ajaxRemoveRevslider = function(obj) {
                    return jQuery(obj.selector + " .rev_slider").revkill();
                };

                // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
                var extendessential = setInterval(function() {
                    if (jQuery.fn.tpessential != undefined) {
                        clearInterval(extendessential);
                        if (typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
                            jQuery.fn.tpessential.defaults.ajaxTypes.push({
                                type: "revslider",
                                func: ajaxRevslider,
                                killfunc: ajaxRemoveRevslider,
                                openAnimationSpeed: 0.3
                            });
                            // type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
                            // func: the Function Name which is Called once the Item with the Post Type has been clicked
                            // killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
                            // openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
                        }
                    }
                }, 30);
            });
        </script>

        <!-- Facebook Pixel code is added on this page by PixelYourSite FREE v5.0.7 plugin. You can test it with Pixel Helper Chrome Extension. -->

        <!--link href="http://dgoba.com/temas/dgoba/css/samsung2018.css" type="text/css"-->

        <style type="text/css">
            /* accent color */
            
            #mobile-menu a:hover,
            #mobile-menu ul.mobile-menu li.current_page_item > a,
            #mobile-menu ul.mobile-menu li.current_page_parent > a,
            #mobile-menu ul.mobile-menu li.current-menu-parent > a,
            #mobile-menu ul.mobile-menu li.current-menu-ancestor > a,
            #mobile-menu ul.mobile-menu li a:hover,
            #mobile-menu .company-info a.tel,
            article.post.format-status .entry-avatar .fa,
            article.post.format-quote .entry-quote .fa,
            article.post.format-quote .quote-caption a,
            .blog-listed .blog article.hentry h3.entry-title a:hover,
            .blog-listed .category article.hentry h3.entry-title a:hover,
            .blog-listed .tag article.hentry h3.entry-title a:hover,
            .blog-listed .author article.hentry h3.entry-title a:hover,
            .blog-listed .date article.hentry h3.entry-title a:hover,
            .search article.hentry h3.entry-title a:hover,
            .blog-listed .blog article.hentry .post-meta a[rel="author"]:hover,
            .blog-listed .category article.hentry .post-meta a[rel="author"]:hover,
            .blog-listed .tag article.hentry .post-meta a[rel="author"]:hover,
            .blog-listed .author article.hentry .post-meta a[rel="author"]:hover,
            .blog-listed .date article.hentry .post-meta a[rel="author"]:hover,
            .search article.hentry .post-meta a[rel="author"]:hover,
            article.post .entry-header h1 a:hover,
            article.post .entry-header h3 a:hover,
            article.post .entry-header .categories-wrap,
            article.post .entry-header .categories-wrap a,
            a:hover,
            #respond .comment-must-logged a,
            .lol-item-service-column h3 a:hover,
            .lol-item-mini-service-column h3 a:hover,
            .lol-item-portfolio-list .entry-meta .lol-love.loved,
            .lol-item-portfolio-list .entry-meta .lol-love:hover,
            .lol-toggle .lol-toggle-header:hover,
            .lol-faq-wrap .lol-faq-header:hover,
            .full-section.light #lol-faq-topics a:hover,
            .single-lolfmk-portfolio .projects-navigation-wrap .lol-love-wrap a:hover,
            .single-lolfmk-portfolio .projects-navigation-wrap .lol-love-wrap a.loved,
            .widget_rss li cite,
            .widget_calendar table tbody tr td a,
            .lol-projects-widget .entry-meta .lol-love.loved,
            .lol-projects-widget .entry-meta .lol-love:hover,
            .full-footer .footer-widget .lol-projects-widget .lol-love.loved,
            .full-footer .footer-widget .lol-projects-widget .lol-love:hover,
            #footer.dark .lol-projects-widget .lol-love.loved,
            #footer.dark .lol-projects-widget .lol-love:hover,
            .lol-item-section-title h2 span {
                color: #b6a189;
            }
            
            #mobile-menu .social-links li a,
            #comment-nav a:hover,
            .full-section.light button:hover,
            .full-section.light input[type="button"]:hover,
            .full-section.light input[type="reset"]:hover,
            .full-section.light input[type="submit"]:hover,
            .full-section.light .lol-button:hover,
            .full-section.light .lol-item-text-banner-alt a.btn-block:hover,
            .full-section.light .post-item .more-link:hover,
            .lol-item-member .member-links li a:hover,
            .full-section.light .lol-item-member .member-links li a:hover,
            .full-section.light .back-faqs:hover,
            .lol-social-widget li a,
            #footer.dark button:hover,
            #footer.dark input[type="button"]:hover,
            #footer.dark input[type="reset"]:hover,
            #footer.dark input[type="submit"]:hover,
            #footer.dark .lol-button:hover,
            #footer.dark .lol-social-widget li a:hover,
            .lol-skill .lol-bar,
            html div.wpcf7-response-output,
            html div.wpcf7-mail-sent-ok {
                background-color: #b6a189;
            }
            
            article.post.sticky,
            .blog-listed article.post.sticky {
                border-bottom: 3px solid #b6a189;
            }
            
            article.post .post-tags a:hover,
            .widget_tag_cloud a:hover {
                background: #b6a189;
                border-color: #b6a189;
            }
            /* primary font family */
            
            body,
            .price-item .price-plan,
            .sf-menu ul {
                font-family: Raleway;
            }
            /* secondary font family */
            
            label,
            select,
            button,
            input[type="button"],
            input[type="reset"],
            input[type="submit"],
            .lol-button,
            #site-title,
            #mobile-menu ul.mobile-menu,
            #mobile-menu .company-info a.tel,
            #page-title-wrap .meta-wrap,
            .crumbs,
            article.post .entry-header .categories-wrap a,
            article.post .entry-header .post-meta,
            article.post .more-link,
            article.post .post-tags a,
            article.post .pagelink a,
            article.post.not-found span,
            article.post.no-results span,
            article.post.not-found p,
            article.post.no-results p,
            article.error404 h1,
            .blog-listed .blog article.hentry .post-meta,
            .blog-listed .category article.hentry .post-meta,
            .blog-listed .tag article.hentry .post-meta,
            .blog-listed .author article.hentry .post-meta,
            .blog-listed .date article.hentry .post-meta,
            .search article.hentry .post-meta,
            .about-author .bio-label,
            h1,
            h2,
            h3,
            h4,
            h5,
            h6,
            blockquote,
            blockquote cite,
            cite,
            #comments .commentlist .comment-meta,
            #comments .commentlist .comment-meta .comment-reply-link,
            #comments .comment-awaiting-moderation,
            #comments .nocomments,
            #comments .cancel-comment-reply a,
            #comment-nav a,
            #respond .comment-must-logged,
            .block-woocommerce-required,
            .lol-item-heading h2,
            .lol-item-heading-small h2,
            .lol-item-image-banner .lol-item-image-banner-title,
            .lol-item-text-banner-alt a.btn-block,
            .full-section.light .lol-item-text-banner-alt a.btn-block,
            .post-item .meta-wrap,
            .post-item .more-link,
            .full-section.light .post-item .more-link,
            .lol-item-blog-list .entry-meta .entry-title a,
            .lol-item-portfolio-list .entry-meta .entry-title a,
            .lol-item-member .member-name h3,
            .lol-item-member .member-name span,
            .progress-circle .chart span,
            .lol-item-testimonials .testimonial-title,
            .lol-toggle .lol-toggle-header,
            .lol-faq-wrap .lol-faq-header,
            .back-faqs,
            .job-list .entry-job a,
            .lol-item-call-to-action.light .lol-button-block,
            .lol-item-call-to-action.dark .lol-button-block,
            .portfolio-item .project-categories span,
            .single-lolfmk-portfolio .projects-navigation-wrap .projects-navigation a,
            .single-lolfmk-portfolio .projects-navigation-wrap .lol-love-wrap,
            .single-lolfmk-portfolio .project-details .project-detail span,
            .pagination a,
            .pagination .current,
            .widget_nav_menu a,
            .widget_pages a,
            .widget_categories a,
            .widget_archive li,
            .widget_recent_comments li:before,
            .widget_recent_comments a,
            .widget_recent_entries a,
            .widget_rss li .rsswidget,
            .widget_tag_cloud a,
            .widget_calendar table caption,
            .widget_calendar table thead tr th,
            .widget_calendar table tfoot tr td,
            .widget_meta li:before,
            .widget_meta a,
            .lol-posts-widget .entry-meta .entry-title a,
            .lol-projects-widget .entry-meta .entry-title a,
            .lol-jobs-widget .entry-job a,
            .lol-skill-name,
            .price-item .price-name,
            .price-item .price-cost,
            .lol-dropcap,
            .lolfmk-job .job-location,
            .lolfmk-job .job-tags li,
            .single-attachment #image-navigation a,
            .single-attachment .entry-caption,
            div.pp_default .pp_description,
            #nav-menu,
            .sf-menu .megamenu_wrap > ul > li,
            .sf-menu .megamenu_wrap > ul > li > a,
            .portfolio-tabs li a,
            .faqs-tabs li a {
                font-family: Raleway;
            }
        </style>

        <style type="text/css">
                    .term-35 .sidebar-left .side.lm-col-3 {
                display: none;
            }
            
            .term-35 .sidebar-left .cont.lm-col-9 {
                width: 100% !important;
            }
            
            .noticias-abajocategoria .product-item a:first-of-type {
                height: 270px;
                overflow: hidden;
                display: block;
            }
            
            .term-concentradores-de-oxigeno .noticias-abajocategoria {
                display: block;
            }
            
            .noticias-abajocategoria {
                display: none;
            }
            
            .page-id-28 #footer {
                margin-top: 100px;
            }
            
            .noticias-abajocategoria {
                float: right;
                width: 100%;
            }
            
            .noticias-abajocategoria .product-item img {
                width: 100%;
            }
            
            .noticias-abajocategoria .product-item a {
                font-weight: 600;
                color: #8d65ac;
            }
            
            .noticias-abajocategoria .product-item {
                height: 300px;
            }
            
            .woocommerce-tabs.wc-tabs-wrapper {
                display: none;
            }
            
            li#tab-title-reviews {
                display: none;
            }
            
            .single-product #reviews {
                border-top: 2px solid #8c64aa;
                padding-top: 30px;
            }
            
            .review-helpful-text {
                margin-bottom: 5px;
                font-size: 12px;
                display: none;
            }
            
            #histogram-table .histogram-row-label {
                width: 80px;
                display: inline-block;
                font-weight: 700;
                color: #aaa;
            }
            
            .lol-review-rating {
                display: none;
            }
            
            #sidebar-cart .view-checkout:hover {
                color: #ffffff !important;
                background-color: #8c63ab;
            }
            
            .summary .product_meta .sku,
            .summary .product_meta a,
            .summary .product_meta p {
                font-weight: 700;
                color: #303030;
                text-decoration: none;
                text-transform: uppercase;
                font-size: 1.2em;
            }
            
            div.quantity input[type=number] {
                margin-bottom: 0;
                padding: 8px 0;
                width: 50px;
                font-family: Montserrat;
                font-size: 20px;
                text-align: center;
                border-color: #303030;
                margin-bottom: 10px;
            }
            
            .summary .product_meta .posted_in,
            .summary .product_meta .sku_wrapper,
            .summary .product_meta .tagged_as {
                display: block;
                font-size: 1.3em;
                line-height: 1.9em;
            }
            
            div#boton-mas-info {
                text-align: center;
                float: left;
                margin-top: 50px;
                margin-bottom: 30px;
            }
            
            div#boton-mas-info a {
                background: #D9251A;
                color: #fff;
                padding: 15px;
                border-radius: 10px;
                text-decoration: none;
            }
            
            div#boton-mas-info a:hover {
                background: #522d6f;
                color: #fff !important;
                text-decoration: none;
            }
            
            a.buttonera:hover {
                background: #522d6f;
                color: #fff !important;
                text-decoration: none;
            }
            
            .woocommerce-NoticeGroup.woocommerce-NoticeGroup-checkout {
                float: left;
                width: 100%;
            }
            
            a.button-mas:hover {
                background: #5d2e82;
                color: #fff !important;
                text-decoration: none;
                font-weight: 500;
            }
            
            li.cat-item.cat-item-22,
            li.cat-item.cat-item-31.cat-parent &gt;
            a,
            li.cat-item.cat-item-38,
            li.cat-item.cat-item-27 {
                display: none;
            }
            
            li.cat-item.cat-item-22.current-cat,
            li.cat-item.cat-item-27,
            li.cat-item.cat-item-31.cat-parent a,
            li.cat-item.cat-item-41,
            li.cat-item.cat-item-28,
            li.cat-item.cat-item-29,
            li.cat-item.cat-item-30.cat-parent a {
                display: none;
            }
            
            li.cat-item.cat-item-31.cat-parent .children a,
            li.cat-item.cat-item-30.cat-parent .children a {
                display: inline;
            }
            
            footer .pull-right.support {
                display: none !important;
            }
            
            .woocommerce-checkout #review-col #place_order:hover {
                color: #fff !important;
                background: #000 !important;
            }
            
            .gateway-icon {
                max-width: 190px;
            }
            
            .term-31 .product-category .product-category-mask:after,
            .term-30 .product-category .product-category-mask:after {
                top: 0 !important;
            }
            
            .page-id-245 li#mega-menu-item-1126258,
            .woocommerce li#mega-menu-item-1126258,
            .page-id-411 li#mega-menu-item-1126258,
            .page-id-245 li#mega-menu-item-1126258 {
                display: none !important;
            }
            
            .term-31 .product-category .product-category-mask:after,
            .term-30 .product-category .product-category-mask:after {
                bottom: 0px !important;
            }
            
            .term-31 .product-category .product-category-mask,
            .term-30 .product-category .product-category-mask {
                position: relative;
                display: block;
                text-align: center;
                overflow: hidden;
                max-height: 210px;
            }
            
            .term-31 .products .product-category,
            .term-30 .products .product-category {
                padding: 10px;
            }
            
            img.aligncenter.size-full.wp-image-1126256 {
                float: left;
                width: 40%;
                margin: 0;
            }
            
            body #wrap #branding .container .lm-col-12 .cart-yes #nav-menu #mega-menu-wrap-primary .mega-menu-item-1126258 a:focus,
            body #wrap #branding .container .lm-col-12 .cart-yes #nav-menu #mega-menu-wrap-primary .mega-menu-item-1126258 a:active {
                background: #D9251A !important;
                color: #fff !important;
            }
            
            body #wrap #branding .container .lm-col-12 .cart-yes #nav-menu #mega-menu-wrap-primary .mega-menu-item-1126258 a {
                line-height: 20px !important;
                /* margin-top: 30px; */
                background: #D9251A;
                color: #fff !important;
                padding: 10px 20px !important;
                border-radius: 10px;
                position: relative;
                top: 20px;
                font-size: 1em !important;
            }
            
            body #wrap #branding .container .lm-col-12 .cart-yes #nav-menu #mega-menu-wrap-primary .mega-menu-item-1126258 a:hover {
                background: #613f7b !important;
            }
            
            img.alignleft.size-full.wp-image-1126257 {
                float: left;
                width: 50%;
                margin: 0;
                padding-top: 20px;
            }
            
            #bloque-interior img {
                margin: 0 auto;
            }
            ul.sub-mega-menu{margin:1px 0px !important;}
            ul.sub-mega-menu li {
                text-align: left;
				padding:0px 4px;
				background-color:transparent;
            }
            ul.sub-mega-menu li:hover {
				background-color:#D9251A;
            }
            
            .products .row {
                display: table;
                width: 100%;
            }
            
            .product-wrap {
                height: 200px;
                display: inline-grid;
                overflow: hidden;
            }
            
            p.woocommerce-result-count {
                display: none !important;
            }
            
            nav.woocommerce-pagination {
                padding-bottom: 50px;
            }
            
            li#menu-item-257 input#searchsubmit {
                float: right;
                position: relative;
                top: -26px;
                padding: 8px;
            }
            
            .home #heads p {
                text-shadow: 2px 1px 4px transparent !important;
                /*color: #000!important;*/
                background: transparent;
            }
            section#comments {display:inherit;}
            .page-id-12 div#heads p {
                font-weight: 400;
                color: #ffffff;
                font-size: 18px;
                max-width: 600px;
            }
            
            li#menu-item-257 {
                display: none;
            }
            
            li#menu-item-257 input#searchsubmit {
                width: 100%;
            }
            
            li#menu-item-257 input#searchsubmit {
                width: 100%;
                background: #dadada;
                color: black;
                font-size: 12px;
                font-weight: 700;
                letter-spacing: 0px;
            }
            
            li#menu-item-254 {
                display: none;
            }
            
            .page-id-245 li#menu-item-257 {
                display: inline-block;
                min-width: 320px;
                max-width: 100%;
            }
            
            .page-id-245 li#menu-item-254 {
                display: inline-block;
            }
            
            .page-id-245 li#menu-item-20 {
                display: none;
            }
            
            .page-id-245 li#menu-item-95 {
                display: none;
            }
            
            .page-id-245 li#menu-item-18 {
                display: none;
            }
            
            .page-id-245 li#menu-item-17 {
                display: none;
            }
            
            .page-id-245 li#menu-item-19 {
                display: none;
            }
            
            .page-id-245 li#menu-item-252 {
                display: none;
            }
            
            .page-id-167 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .page-id-192 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            #headsdos h2 {
                text-transform: capitalize;
                font-weight: bold;
                font-size: 35px;
                text-shadow: none;
                color: #303030;
            }
            
            .page-id-165 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .page-id-131 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .page-id-143 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .page-id-298 div#heads {
                margin-bottom: 60px;
            }
            
            .page-id-178 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .page-id-137 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .merivaara,
            .berman {
                line-height: 26px;
                margin: 0 0 5px 0;
                color: black;
                font-size: 16px;
                letter-spacing: normal;
                font-family: sans-serif;
                font-weight: 100;
            }
            
            .backy {
                background: rgba(255, 255, 255, 0.46);
                padding: 10px!important;
            }
            
            ul.merivaara {
                margin-left: 22px;
            }
            
            li#menu-item-64 {
                background: #dadada;
                border-radius: 20px;
                height: 35px;
                margin-right: 14px;
            }
            
            div#bloque-interior .lol-item-column p {
                margin-top: 10px;
                text-align: justify;
            }
            
            div#bloque-interior h3 {
                font-weight: 900;
                font-size: 20px;
                color: #D9251A;
                margin-bottom: 0;
                padding: 0;
                min-height: 59px;
            }
            
            .page-id-113 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .page-id-113 #content {
                padding: 0 0 75px 0;
            }
            
            .uju .fa {
                margin-right: 8px;
            }
            
            div#boxes {
                background: rgba(255, 255, 255, 0.32);
                padding: 40px;
            }
            
            .social-meta li a {
                background: gainsboro!important;
            }
            
            div#formulario-servicios h3 {
                text-transform: capitalize;
                font-weight: bold;
                font-size: 35px;
                margin-bottom: 40px;
            }
            
            #formulario-servicios {
                max-width: 600px;
                margin: 0 auto;
            }
            
            .opciones .fa-twitter {
                margin: 4px 5px;
                font-size: 20px;
            }
            
            .opciones .fa-facebook {
                margin: 4px 5px;
                font-size: 20px;
            }
            
            .opciones .fa-linkedin {
                margin: 4px 5px;
                font-size: 20px;
            }
            
            div#heads {
                margin-bottom: 0;
                padding-bottom: 50px;
            }
            
            p.opciones, p.opciones2 {
                margin: 0;
            }
            .opciones2 a{
				font-weight:300;
				color:#fff !important;
            }
            .opciones2 span{
				font-weight:bold;
				color:#D9251A !important;
            }

            
            #content {
                padding: 75px 0 0;
            }
            
            a.button-mas {
                border-radius: 6px;
                background: #D9251A;
                display: block;
                color: white;
                padding: 10px;
                text-align: center;
                max-width: 160px;
                font-size: 12px;
                margin-top: 30px;
            }
            
            div#sin-bottom {
                margin-bottom: 0;
            }
            
            div#xox p {
                font-size: 18px;
                text-transform: capitalize;
                font-weight: 300;
            }
            
            div#xox {
                color: white;
                font-size: 20px;
            }
            
            .page-id-8 #content {
                padding: 0 0 0 0;
            }
            
            .page-id-12 #content {
                padding: 0 0 75px 0;
            }
            
            .page-id-93 #content {
                padding: 0 0 75px 0;
            }
            
            div#xox h3 {
                color: white;
                font-size: 30px;
                font-weight: bold;
            }
            
            div#boxes h3 {
                margin-bottom: 10px;
                font-size: 25px;
                font-weight: 500;
                color: #D9251A;
            }
            
            div#boxes p {
                font-size: 15px;
            }
            
            .page-id-93 .lol-item-heading h2 {
                color: #D9251A;
            }
            
            div#shd-text1 {
                text-align: justify;
                font-size: 15px;
            }
            
            div#logos {
                opacity: 0.7;
            }
            
            aside#search {
                display: none;
            }
            
            div#shd-text p {
                text-align: justify;
                font-size: 15px;
            }
            
            div#shd-text h3 {
                font-size: 16px;
                font-weight: 600;
                margin-bottom: 0;
                padding: 0;
            }
            
            nav.crumbs {
                display: none;
            }
            
            div#spc h3 {
                text-transform: capitalize;
                font-weight: bold;
                font-size: 35px;
                color: #D9251A;
            }
            
            .page-id-8 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .page-id-12 #heads {
                padding: 120px;
                background-size: 100%!important;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .page-id-93 #heads {
                padding: 120px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .lol-item-service-column h3 {
                font-weight: 500;
                color: #D9251A;
            }
            
            .fa {
                color: #D9251A;
            }
            
            .lol-item-service-column .service-icon {
                background-color: #f7edff;
            }
            
            .home i.service-icon {
                display: none!important;
            }
            
            div#hacemos-realidad {
                color: black;
                letter-spacing: normal;
                font-family: sans-serif;
                font-weight: 100;
                font-size: 16px;
                line-height: 24px;
            }
            
            #footer .widget-header {
                margin-bottom: 20px;
            }
            
            h3.widget-title {
                text-transform: none!important;
                font-size: 18px!important;
            }
            
            .divider {
                margin-bottom: 20px;
            }
            
            #footer-bottom .lm-col-12 {
                padding: 0;
            }
            
            #footer-bottom.dark {
                color: #fff;
                background-color: #D9251A;
            }
            
            .page-row .divider h3 {
                border-bottom: none;
            }
            
            p.footer-bottom-copy {
                text-align: center;
                font-size:16px;
            }
			
			p.footer-bottom-copy span{
				color:#fff;
	            font-weight: 700;
			}
            
            .footer-widgets .lm-col-6 {
                float: right;
                width: 30%;
            }
            
            div#hacemos-realidad h3 {
                font-family: sans-serif;
                text-transform: none;
                font-size: 24px;
                font-weight: 100;
                text-align: left;
                padding: 0;
            }
            
            .textwidget p {
                font-weight: 300;
                font-size: 14px;
                line-height: 21px;
                color: #fff;
            }
            
            .lol-item-service-column p {
                font-size: 15px;
            }
            
            a.buttonera {
                background: #D9251A;
                color: white;
                padding: 20px 70px!important;
                border-radius: 7px;
                text-transform: uppercase;
            }
            
            div#heads p {
                font-weight: 200;
                color: #444;
                font-size: 15px;
                max-width: 750px;
                background: rgba(140, 100, 171, 0.63);
            }
            
            .esg-grid .mainul li.eg-washington-wrapper {
                background-color: transparent!important;
            }
            
            li#menu-item-63 {
                background: #D9251A;
                border-radius: 20px;
                height: 35px;
                color: white;
            }
            
            div#columnados span {
                color: #D9251A;
                font-size: 14px;
                letter-spacing: normal;
                font-family: sans-serif;
                font-weight: 100;
            }
            
            div#columnados p {
                line-height: 30px;
                margin: 0 0 5px 0;
                color: black;
                font-size: 16px;
                letter-spacing: normal;
                font-family: sans-serif;
                font-weight: 100;
            }
            
            #page-title-wrap h1,
            #page-title-wrap h3 {
                margin-bottom: 0;
                line-height: 38.832px;
                text-transform: capitalize;
                font-weight: bold;
                font-size: 35px;
                text-shadow: 2px 2px 4px black;
                color: #f9f9f9;
            }
            
            div#heads h2 {
                text-transform: uppercase;
                font-weight: bold;
                font-size: 28px;
                text-shadow: 2px 2px 4px black;
                color: #f9f9f9;
            }
            
            .home div#heads h2 {
                text-transform: uppercase;
                font-weight: bold;
                font-size: 24px;
                text-shadow: none;
                color: #111;
            }
            
            li#menu-item-63 a {
                color: white;
            }
            
            .sf-menu li a {
                padding: 5px 0;
            }
            
            .page-id-103 h3 {
                font-weight: 500;
                color: #D9251A;
                font-size: 2em;
            }
            
            .page-id-103 .full-section {
                padding: 0px 0 70px;
            }
            
            .page-id-103 input.wpcf7-form-control.wpcf7-submit {
                width: 100%;
                background: #D9251A;
                font-size: 1.2em;
            }
            
            .page-id-103 #heads {
                padding: 100px;
                margin-bottom: 60px;
                margin-top: 0;
            }
            
            .page-id-103 #content {
                padding: 0 0;
            }
            
            ul.marcas {
                text-align: center;
            }
            
            .marcas li {
                display: inline-block;
                margin: 5px 25px;
            }
            
            footer label {
                color: #fff;
                margin-bottom: 0px;
            }


            div#heads p {
                font-weight: 400 !important;
                text-shadow: 2px 1px 4px black !important;
                color: #444 !important;
            }

            .post-meta .meta-wrap:first-child,
            .post-meta .meta-wrap:nth-child(3) {
                display: none !important;
            }
            
            #menu-item-323,
            #menu-item-324,
            #menu-item-325 {
                display: none;
            }
            
            #mega-menu-wrap-primary {
                background-color: transparent !important;
            }
            
            .page-id-245 #mega-menu-item-20,
            .page-id-245 #mega-menu-item-64,
            .page-id-245 #mega-menu-item-63,
            .page-id-245 #mega-menu-item-95,
            .page-id-245 #mega-menu-item-17,
            .page-id-245 #mega-menu-item-18,
            .page-id-245 #mega-menu-item-19,
            .page-id-245 #icon-search-btn {
                display: none !important;
            }
            
            .page-id-245 #mega-menu-item-254 {
                float: right !important;
            }
            
            .page-id-245 #mega-menu-item-257 {
                display: block !important;
            }
            
            .text_input {
                width: 300px !important;
            }
            
            .menu-item.menu-item-search {
                display: none;
            }
            
            .page-id-245 .menu-item.menu-item-search {
                display: block !important;
            }
            
            @media all and (max-width: 1200px) and (min-width: 992px) {
                {
                    .page-id-245 .lm-col-12 #nav-menu {
                        width: 80% !important;
                    }
                    #mega-menu-wrap-primary #mega-menu-primary input,
                    #mega-menu-wrap-primary #mega-menu-primary img {
                        max-width: 30% !important;
                    }
                }
                @media (max-width: 1200px) {
                    {
                        #mega-menu-wrap-primary #mega-menu-primary input,
                        #mega-menu-wrap-primary #mega-menu-primary img {
                            max-width: 30% !important;
                        }
                    }
                    @media (max-width: 992px) {
                        .woocommerce-page #searchform,
                        .page-id-245 #searchform {
                            display: block;
                        }
                    }
                    @media (min-width: 992px) {
                        .page-id-245 .sf-menu&gt;
                        li {
                            height: 60px !important;
                        }
                        .page-id-245 .search-yes #nav-menu {
                            margin-left: 100px !important;
                            /* margin: 0 auto; */
                        }
                        .page-id-245 .lm-col-12 #nav-menu {
                            float: none !important;
                            text-align: center;
                            width: 80% !important;
                        }
                        .page-id-245 header {
                            height: 130px !important;
                        }
                    }
                    #mega-menu-wrap-primary #mega-menu-primary li.mega-menu-item a.mega-menu-link {
                        padding: 7px 5px !important;
                    }
                    #mega-menu-wrap-primary #mega-menu-primary {
                        text-align: center !important;
                    }
                    #mega-menu-wrap-primary #mega-menu-primary input,
                    #mega-menu-wrap-primary #mega-menu-primary img {
                        max-width: 30% !important;
                    }
                    #mega-menu-wrap-primary #mega-menu-primary li.mega-menu-item.mega-toggle-on a.mega-menu-link,
                    #mega-menu-wrap-primary #mega-menu-primary li.mega-menu-item a.mega-menu-link:hover,
                    #mega-menu-wrap-primary #mega-menu-primary li.mega-menu-item a.mega-menu-link:focus {
                        background-color: transparent !important;
                        color: #000 !important;
                    }
                    .mega-sub-menu {
                        list-style: none !important;
                        margin-top: 42px !important;
                    }
                    #mega-menu-wrap-primary #mega-menu-primary li.mega-menu-item a.mega-menu-link {
                        color: #000 !important;
                        margin: 0 5px;
                        position: relative;
                        display: block;
                        line-height: 25px;
                        -webkit-transition: all .2s ease;
                        transition: all .2s ease;
                        font-size: 12px !important;
                        font-weight: 700 !important;
                        line-height: 70px !important;
                        padding: 7px 2px !important;
                    }
                    #mega-menu-item-323,
                    #mega-menu-item-324,
                    #mega-menu-item-325 {
                        display: none !important;
                    }
                    .page-id-245 #mega-menu-item-323,
                    .page-id-245 #mega-menu-item-324,
                    .page-id-245 #mega-menu-item-325 {
                        display: inline-block !important;
                    }
                    .my-wp-search {
                        background: #D9251A !important;
                        color: #fff !important;
                    }
                    .page-id-245 #nav-menu {
                        width: 80% !important
                    }
        </style>

        <style type="text/css">
            /* accent color */
            
            .shop_table td.product-name dl.variation,
            .shop_table .backorder_notification,
            .woocommerce-message .wc-forward,
            .product-item .yith-wcwl-add-to-wishlist a:hover,
            .summary .product_meta a:hover,
            .summary .product_meta .sku:hover,
            #histogram-table .histogram-row-label a:hover,
            #comments-loaded-wrap #comments-close-btn:hover,
            .summary .stock,
            .woocommerce-checkout .woocommerce-info-login a,
            .woocommerce-checkout .woocommerce-info-coupon a,
            .woocommerce-checkout #review-col a,
            .woocommerce-checkout #review-col .shop_table thead tr,
            .woocommerce-checkout #review-col #payment .payment_method_paypal .gateway-icon a:hover,
            .sidebar-account a,
            .woocommerce-account .address h4,
            .woocommerce-account .lm-col-8 .digital-downloads a,
            #sidebar-cart a:hover,
            #sidebar-cart .username,
            #sidebar-cart .cart-product .cart-product-title:hover {
                color: #b6a189;
            }
            
            .demo_store,
            .product-item .soldout,
            .summary .variations_form.cart .reset_variations,
            .summary .yith-wcwl-add-button a:hover,
            .summary .yith-wcwl-wishlistaddedbrowse a:hover,
            .summary .yith-wcwl-wishlistexistsbrowse a:hover,
            .woocommerce-cart .shipping_calculator .button,
            .woocommerce-checkout #review-col #place_order,
            .has-not-bg #page.register-page .myaccount-login .my-account-tabs a:hover,
            .has-not-bg #page.register-page .myaccount-login .my-account-tabs li.active a,
            .woocommerce-account .address .edit:hover,
            .dark .widget_shopping_cart_content .wc-forward,
            .dark .widget_shopping_cart_content .wc-forward.checkout:hover,
            .dark .widget_price_filter .ui-slider .ui-slider-range,
            #sidebar-cart .view-checkout,
            .widget_layered_nav li.chosen a,
            .widget_layered_nav_filters li.chosen a {
                background-color: #b6a189;
            }
            
            div.quantity input[type="number"]:focus {
                outline: none;
                border-color: #b6a189;
            }
            
            .full-section.light .product-item .added_to_cart:hover {
                color: #fff;
                border: 2px solid #b6a189;
                background: #b6a189;
            }
            
            .woocommerce-account .lm-col-8 .shop_table tbody td.order-status span,
            .woocommerce-wishlist .wishlist_table tr td.product-stock-status span.wishlist-in-stock,
            .woocommerce-wishlist .wishlist_table tr td.product-stock-status span.wishlist-out-of-stock {
                color: #b6a189;
                border: 2px solid #b6a189;
            }
            
            .widget_product_tag_cloud a:hover {
                background: #b6a189;
                border-color: #b6a189;
            }
            
            #sidebar-cart .cart-count .count {
                border: 2px solid #b6a189;
            }
            
            .summary .stock {
                border-color: #b6a189;
            }
            /* primary font family */
            
            .woocommerce-cart .cart-totals-calculator .input-text,
            .woocommerce-cart .cart-totals-calculator input[type="text"],
            .woocommerce-cart .cart-totals-calculator input[type="password"],
            .woocommerce-cart .cart-totals-calculator input[type="datetime"],
            .woocommerce-cart .cart-totals-calculator input[type="datetime-local"],
            .woocommerce-cart .cart-totals-calculator input[type="date"],
            .woocommerce-cart .cart-totals-calculator input[type="month"],
            .woocommerce-cart .cart-totals-calculator input[type="time"],
            .woocommerce-cart .cart-totals-calculator input[type="week"],
            .woocommerce-cart .cart-totals-calculator input[type="number"],
            .woocommerce-cart .cart-totals-calculator input[type="email"],
            .woocommerce-cart .cart-totals-calculator input[type="url"],
            .woocommerce-cart .cart-totals-calculator input[type="search"],
            .woocommerce-cart .cart-totals-calculator input[type="tel"],
            .woocommerce-cart .cart-totals-calculator input[type="color"],
            .woocommerce-cart .cart-totals-calculator textarea,
            .woocommerce-checkout #review-col input[type="text"],
            .woocommerce-checkout #review-col input[type="password"],
            .woocommerce-checkout #review-col input[type="datetime"],
            .woocommerce-checkout #review-col input[type="datetime-local"],
            .woocommerce-checkout #review-col input[type="date"],
            .woocommerce-checkout #review-col input[type="month"],
            .woocommerce-checkout #review-col input[type="time"],
            .woocommerce-checkout #review-col input[type="week"],
            .woocommerce-checkout #review-col input[type="number"],
            .woocommerce-checkout #review-col input[type="email"],
            .woocommerce-checkout #review-col input[type="url"],
            .woocommerce-checkout #review-col input[type="search"],
            .woocommerce-checkout #review-col input[type="tel"],
            .woocommerce-checkout #review-col input[type="color"],
            .woocommerce-checkout #review-col textarea,
            .sidebar-account input[type="text"],
            .sidebar-account input[type="password"],
            .sidebar-account input[type="datetime"],
            .sidebar-account input[type="datetime-local"],
            .sidebar-account input[type="date"],
            .sidebar-account input[type="month"],
            .sidebar-account input[type="time"],
            .sidebar-account input[type="week"],
            .sidebar-account input[type="number"],
            .sidebar-account input[type="email"],
            .sidebar-account input[type="url"],
            .sidebar-account input[type="search"],
            .sidebar-account input[type="tel"],
            .sidebar-account input[type="color"],
            .sidebar-account textarea,
            .woocommerce-account .lm-col-8 .shop_table tbody td.order-status span {
                font-family: Raleway;
            }
            /* secondary font family */
            
            div.quantity input[type="number"],
            .shop_table,
            .woocommerce-info,
            .woocommerce-message,
            .woocommerce-error,
            .archive.woocommerce .page-description,
            .archive.woocommerce .term-description,
            .products-filter .products-page-filter a,
            .woocommerce-pagination a,
            .woocommerce-pagination .dots,
            .woocommerce-pagination .current,
            .product-item .product-categories span,
            .product-item .price,
            .product-item .button,
            .product-item .added_to_cart,
            .product-category h3 span,
            .summary .price,
            .summary .variations_form.cart .reset_variations,
            .summary .product_meta,
            .summary .yith-wcwl-add-button a,
            .summary .yith-wcwl-wishlistaddedbrowse a,
            .summary .yith-wcwl-wishlistexistsbrowse a,
            .summary .yith-wcwl-add-to-wishlist .feedback,
            .product-type-grouped .summary form.cart table .button,
            .product-type-external .single_add_to_cart_button,
            .woocommerce-tabs .tabs a,
            .woocommerce-tabs #comments p.meta,
            .woocommerce-tabs #comments .woocommerce-noreviews,
            .woocommerce-tabs #respond .must-log-in,
            .woocommerce-tabs .comment-tabs,
            .woocommerce-tabs .view-all-ratings,
            #histogram-table,
            .review-helpful-text span,
            .lol-review-rating,
            #comments-loaded-wrap p.meta,
            .woocommerce-cart .cart-totals-calculator,
            .cart-empty-wrap .cart-empty,
            .cart-empty-wrap .wc-backward,
            .woocommerce-checkout .checkout-must-be-logged-in,
            .woocommerce-checkout .woocommerce-info-login,
            .woocommerce-checkout .woocommerce-info-coupon,
            .woocommerce-checkout form.login .lost_password,
            .woocommerce-checkout .thankyou-wrap .thankyou_text,
            .woocommerce-checkout .thankyou-wrap .order_details_wrap .order_details li,
            .woocommerce-checkout .thankyou-wrap .bacs_details li,
            .woocommerce-checkout .thankyou-wrap .thankyou-btns a,
            #page.register-page .myaccount-login .my-account-tabs li.user-login-tab-default,
            #page.register-page .myaccount-login .my-account-tabs a,
            #page.register-page .myaccount-login form.login p.lost_password,
            #page.register-page .myaccount-lost-password .left-panel p,
            #page.register-page .lost_reset_password p.lost-reset-password-message,
            .sidebar-account .notes .meta,
            .woocommerce-account .address .edit,
            .order-again .button,
            .no-orders-myaccount .orders-empty,
            .no-orders-myaccount .wc-backward,
            .track-order-wrap .left-panel p,
            .track_order p.track-order-message,
            .widget_product_categories a,
            .product_list_widget,
            .widget_product_tag_cloud a,
            .widget_layered_nav li,
            .widget_layered_nav_filters li,
            .widget_shopping_cart_content li.empty,
            .widget_shopping_cart_content .total,
            .widget_shopping_cart_content .wc-forward,
            .widget_price_filter .price_label,
            #sidebar-cart .cart-count,
            #sidebar-cart .cart-count .count,
            #sidebar-cart .login-logout,
            #sidebar-cart h4,
            #sidebar-cart .cart-product .cart-product-meta,
            #sidebar-cart .cart-product .cart-product-title,
            #sidebar-cart .cart-subtotal,
            #sidebar-cart .view-checkout,
            #sidebar-cart .no-products,
            .woocommerce-wishlist h2,
            .woocommerce-wishlist .wishlist_table tr td .add_to_cart {
                font-family: Raleway;
            }
</style>
        <script type="text/javascript">
            function setREVStartSize(e) {
                try {
                    var i = jQuery(window).width(),
                        t = 9999,
                        r = 0,
                        n = 0,
                        l = 0,
                        f = 0,
                        s = 0,
                        h = 0;
                    if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
                            f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                        }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                        var u = (e.c.width(), jQuery(window).height());
                        if (void 0 != e.fullScreenOffsetContainer) {
                            var c = e.fullScreenOffsetContainer.split(",");
                            if (c) jQuery.each(c, function(e, i) {
                                u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                            }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                        }
                        f = u
                    } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                    e.c.closest(".rev_slider_wrapper").css({
                        height: f
                    })
                } catch (d) {
                    console.log("Failure at Presize of Slider:" + d)
                }
            };
        </script>
<style>
            .page-id-332 #branding {
                display: none;
            }
            
            .page-id-332 .container {
                min-width: 100vw;
                padding: 0px;
            }
            
            .page-id-332 #wrap {
                padding-top: 0px;
            }
            
            .page-id-332 .entry-content {
                margin-top: -50px;
            }
            
            .page-id-332 #footer {
                display: none;
            }
            
            .page-id-332 .footer-bottom-copy {
                text-align: center;
            }
            
            .page-id-422 .container {
                min-width: 100vw;
                padding: 0px;
            }
            
            .page-id-422 .entry-content {
                margin-top: -143px;
            }
            
            .page-id-422 #logo {
                margin-left: 50px;
            }
            
            .page-id-422 .footer-widgets #text-2 {
                padding-left: 30px;
            }
            
            .page-id-422 .footer-bottom-copy {
                margin-left: 50px;
            }
        </style>

<link rel='stylesheet' href='http://dgoba.com/temas/dgoba/css/style_original.css' type='text/css' media='all' />

        <!--form role="search" method="get" id="searchform" class="searchform" action="http://dgoba.com/">
            <label class="screen-reader-text" for="s">Search for:</label>
            <input type="text" value="" name="s" id="s" />
            <input type="hidden" value="product" name="post_type" id="post_type">
            <input type="submit" id="searchsubmit" value="Buscar" />
        </form-->
        <!-- END WP -->

<!--Google Analytics-->
</head>
<body data-rsssl=1 class="home page-template-default page page-id-6 mega-menu-primary">
    <nav id="mobile-menu" class="light" role="navigation">
        <div class="container">
            <!-- BEGIN row -->
            <div class="row">
                <!-- BEGIN col-12 -->
                <div class="lm-col-12">
                    <div class="m-col-6">
                        <div class="menu-main-menu-container">
                            <ul id="menu-main-menu" class="mobile-menu">
                                <form method="get" class="menu-search-form" action="http://dgoba.com/index.php">
                                    <p>
                                        <input class="text_input" type="text" value="Buscar en la tienda" name="s" id="s" onfocus="if (this.value == 'Buscar en la tienda') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Buscar en la tienda';}" />
                                        <input type="hidden" value="product" name="post_type" id="post_type" />
                                        <input type="submit" class="my-wp-search fa fa-search" id="searchsubmit" value="BUSCAR" />
                                    </p>
                                </form>
                                </li>
                                <div class="pull-right support">
                                    <h4><i class="fa fa-phone" aria-hidden="true"></i> Soporte:  <a href="tel:525553126374">(55) 5312 6374 </a></h4>
                                </div>
                                <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-6 current_page_item menu-item-20"><a href="http://dgoba.com/">HOME</a></li>
								<!--?php menu_responsivo();--?>
                            </ul>
                        </div>
                    </div>

                    <div class="m-col-6 last">
                        <div class="company-info">
                        </div>
                    </div>

                </div>
                <!-- END col-12 -->
            </div>
            <!-- END row -->
        </div>

    </nav>

    <div id="sidebar-cart">

        <div class="sidebar-content">

            <div class="sidebar-links">

                <div id="cart-count-sidebar">

                    <span class="cart-count">Artículos<span class="count">0</span></span>

                </div>

                <a href="http://dgoba.com/mi-cuenta/" class="login-logout"><i class="fa fa-user"></i>Iniciar Sesión</a>

            </div>

            <h4><a href="http://dgoba.com/carrito/">Mi Carrito</a></h4>

            <div id="sidebar-cart-items">

                <div class="no-products">
                    <span>No products in the shopping bag.</span>
                </div>

            </div>

        </div>

    </div>

    <!-- BEGIN wrap -->
    <div id="wrap">

        <!-- BEGIN branding -->
        <header id="branding" class="light" data-style-start="light" data-style-end="light" data-border="" role="banner">
            <div class="container">
                <!-- BEGIN row -->
                <div class="row">
                    <!-- BEGIN col-12 -->
                    <div class="lm-col-12">

                        <!-- BEGIN #logo -->
                        <div id="logo">
                            <a href="http://dgoba.com/" title="">

                                <img src="http://dgoba.com/temas/dgoba/images/logo.min.png" class="" alt="logo">

                            </a>
                        </div>
                        <!-- END #logo -->
                        <div class="cart-yes search-no">

                            <a href="#" id="mobile-menu-link">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="44px" height="34px" viewBox="0 0 44 34" enable-background="new 0 0 44 34" xml:space="preserve">
                                    <rect x="11" y="16" width="22" height="2" class="style0" />
                                    <rect x="11" y="10" width="22" height="2" class="style0" />
                                    <rect x="11" y="22" width="22" height="2" class="style0" />
                                </svg>
                            </a>

                            <!-- BEGIN nav-menu -->
                            <nav id="nav-menu" role="navigation">
                                <h3 class="assistive-text">Main menu</h3>
                                <div class="skip-link">
                                    <a class="assistive-text" href="#content" title="Skip to primary content">Skip to primary content</a>
                                </div>
                                <div class="skip-link">
                                    <a class="assistive-text" href="#sidebar" title="Skip to secondary content">Skip to secondary content</a>
                                </div>
                                <div id="mega-menu-wrap-primary" class="mega-menu-wrap">
                                    <div class="mega-menu-toggle" tabindex="0">
                                        <div class='mega-toggle-block mega-menu-toggle-block mega-toggle-block-right mega-toggle-block-1' id='mega-toggle-block-1'></div>
                                    </div>

                                    <ul id="mega-menu-primary" class="mega-menu mega-menu-horizontal mega-no-js" data-event="click" data-effect="fade_up" data-effect-speed="200" data-second-click="close" data-document-click="collapse" data-vertical-behaviour="accordion" data-breakpoint="600" data-unbind="true">
                                        <li class="menu-item menu-item-search">
                                            <form method="get" class="menu-search-form" action="http://dgoba.com/">
                                                <p>
                                                    <input class="text_input" type="text" value="Buscar en la tienda" name="s" id="s" onfocus="if (this.value == 'Buscar en la tienda') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Buscar en la tienda';}" />
                                                    <input type="hidden" value="product" name="post_type" id="post_type" />
                                                    <input type="submit" class="my-wp-search fa fa-search" id="searchsubmit" value="BUSCAR" />
                                                </p>
                                            </form>
                                        </li>
                                        <div class="pull-right support">
                                            <h4><i class="fa fa-phone" aria-hidden="true"></i> Soporte:  <a href="tel:525553126374">(55) 5312 6374 </a></h4>
                                        </div>

										<!-- menu_web.json URL:(http://dgoba.com/bloques/ws/t/?t=menu_web)-->

<!--1-Home-->
			<li class="mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-align-bottom-left mega-menu-flyout  mega-menu-item-1" id="mega-menu-item-1">
            	<a class="mega-menu-link azul"  href="http://dgoba.com/index.php" tabindex="1">HOME</a><!--0--></li><!--1 k:1 ID:2 ID_menu:6 CSS:mega-menu-item-has-children -->

<!--3-productos-->
			<li class="mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-align-bottom-left mega-menu-flyout mega-menu-item-has-children mega-menu-item-2" id="mega-menu-item-2">
            	<a class="mega-menu-link " aria-haspopup="true" href="#" tabindex="2">PRODUCTOS</a><!--1-->

<ul class="mega-sub-menu" style="background-color:transparent !important;">
												<li class="mega-menu-item mega-menu-columns-1 mega-menu-item-text-10" id="mega-menu-item-text-10">
                                                	<div class="textwidget">
                                                    	<ul class="sub-mega-menu">

<!--categorias.json URL:(modulos/productos/categorias.json)-->

<li id="bm1"><!--j:1--><i class="fa fa-circle" style="font-size:9px;"></i> <a href="http://dgoba.com/productos/categoria/1-COLCHONES">COLCHONES</a><!-- subcategorias.json URL:(modulos/productos/subcategorias.json)-->

<div id="sm1" class="submenu1"><ul><li><!--j:1--><a href="http://dgoba.com/productos/subcategoria/1-HOSPITALARIOS">HOSPITALARIOS</a></li><li><!--j:13--><a href="http://dgoba.com/productos/subcategoria/3-DE-AIRE-ANITIDECUBITO">DE AIRE ANITIDECUBITO</a></li></ul></div></li>

<li id="bm2"><!--j:2--><i class="fa fa-circle" style="font-size:9px;"></i> <a href="http://dgoba.com/productos/categoria/2-CAMAS">CAMAS</a><!-- subcategorias.json URL:(modulos/productos/subcategorias.json)-->

<div id="sm2" class="submenu1"><ul><li><!--j:5--><a href="http://dgoba.com/productos/subcategoria/5-HOSPITALARIAS">HOSPITALARIAS</a></li><li><!--j:7--><a href="http://dgoba.com/productos/subcategoria/6-GINECOLOGICAS">GINECOL&Oacute;GICAS</a></li><li><!--j:12--><a href="http://dgoba.com/productos/subcategoria/7-PEDIATRICAS">PEDI&Aacute;TRICAS</a></li><li><!--j:15--><a href="http://dgoba.com/productos/subcategoria/23-GERIATRICAS">GERIATRICAS</a></li></ul></div></li>

<li id="bm3"><!--j:3--><i class="fa fa-circle" style="font-size:9px;"></i> <a href="http://dgoba.com/productos/categoria/3-RAYOS-X">RAYOS X</a><!-- subcategorias.json URL:(modulos/productos/subcategorias.json)-->

<div id="sm3" class="submenu1"><ul><li><!--j:4--><a href="http://dgoba.com/productos/subcategoria/8-RADIOGRAFIA-DIRECTA">RADIOGRAFIA DIRECTA</a></li><li><!--j:8--><a href="http://dgoba.com/productos/subcategoria/9-RADIOGRAFIA-COMPUTARIZADA">RADIOGRAFIA COMPUTARIZADA</a></li><li><!--j:14--><a href="http://dgoba.com/productos/subcategoria/10-IMPRESORAS">IMPRESORAS</a></li><li><!--j:17--><a href="http://dgoba.com/productos/subcategoria/11-CONSUMIBLES">CONSUMIBLES</a></li><li><!--j:20--><a href="http://dgoba.com/productos/subcategoria/29-DETECTORES">DETECTORES</a></li></ul></div></li>

<li id="bm4"><!--j:4--><i class="fa fa-circle" style="font-size:9px;"></i> <a href="http://dgoba.com/productos/categoria/4-QUIROFANO-Y-SOPORTE-DE-VIDA">QUIROFANO Y SOPORTE DE VIDA</a><!-- subcategorias.json URL:(modulos/productos/subcategorias.json)-->

<div id="sm4" class="submenu1"><ul><li><!--j:2--><a href="http://dgoba.com/productos/subcategoria/12-MAQUINA-DE-ANESTESIA">MAQUINA DE ANESTESIA</a></li><li><!--j:9--><a href="http://dgoba.com/productos/subcategoria/13-VENTILADORES">VENTILADORES</a></li><li><!--j:10--><a href="http://dgoba.com/productos/subcategoria/14-MESAS-DE-QUIROFANO">MESAS DE QUIROFANO</a></li><li><!--j:18--><a href="http://dgoba.com/productos/subcategoria/15-MONITOR-DE-SIGNOS-VITALES">MONITOR DE SIGNOS VITALES</a></li><li><!--j:19--><a href="http://dgoba.com/productos/subcategoria/16-LAMPARAS-DE-QUIROFANO">LAMPARAS DE QUIROFANO</a></li><li><!--j:21--><a href="http://dgoba.com/productos/subcategoria/17-DESFIBRILADORES">DESFIBRILADORES</a></li><li><!--j:22--><a href="http://dgoba.com/productos/subcategoria/28-ENDOSCOPIA">ENDOSCOPIA</a></li></ul></div></li>

<li id="bm5"><!--j:5--><i class="fa fa-circle" style="font-size:9px;"></i> <a href="http://dgoba.com/productos/categoria/5-OPTICA">OPTICA</a><!-- subcategorias.json URL:(modulos/productos/subcategorias.json)-->

<div id="sm5" class="submenu1"><ul><li><!--j:3--><a href="http://dgoba.com/productos/subcategoria/18-BAUSH+LOMB">BAUSH+LOMB</a></li><li><!--j:6--><a href="http://dgoba.com/productos/subcategoria/19-JOHNSON--JOHNSON">JOHNSON &amp; JOHNSON</a></li><li><!--j:11--><a href="http://dgoba.com/productos/subcategoria/20-COOPERVISION">COOPERVISION</a></li><li><!--j:16--><a href="http://dgoba.com/productos/subcategoria/21-ALCON">ALCON</a></li></ul></div></li>

<li id="bm6"><!--j:6--><i class="fa fa-circle" style="font-size:9px;"></i> <a href="http://dgoba.com/productos/categoria/6-SEDANTES">SEDANTES</a><!-- subcategorias.json URL:(modulos/productos/subcategorias.json)-->

</li>

</ul>
                                                    </div>
												</li>
        									</ul>

</li><!--7-contacto-->
			<li class="mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-align-bottom-left mega-menu-flyout  mega-menu-item-5" id="mega-menu-item-5">
            	<a class="mega-menu-link "  href="http://dgoba.com/contacto/" tabindex="5">CONTACTO</a><!--0--></li><!-- /menu.json -->
	                                    </ul>
                                </div>
                            </nav>

                            <div id="icon-cart">
<!--
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" class="cart-icon" width="28px" height="32px" viewBox="0 0 28 32" enable-background="new 0 0 28 32" xml:space="preserve">
                                    <path class="header-cart-svg" d="M25.996 8.91C25.949 8.4 25.5 8 25 8h-5V6c0-3.309-2.691-6-6-6c-3.309 0-6 2.691-6 6v2H3 C2.482 8 2.1 8.4 2 8.91l-2 22c-0.025 0.3 0.1 0.6 0.3 0.764C0.451 31.9 0.7 32 1 32h26 c0.281 0 0.549-0.118 0.738-0.326c0.188-0.207 0.283-0.484 0.258-0.764L25.996 8.91z M10 6c0-2.206 1.795-4 4-4s4 1.8 4 4v2h-8V6z M24.087 10l1.817 20H2.096l1.817-20" />
                                </svg>
-->
                                <div id="cart-count-header">
                                    <span class="cart-count">0</span>
                                </div>

                            </div>

                        </div>

                    </div>
                    <!-- END col-12 -->
                </div>
                <!-- END row -->
            </div>
        </header>
        <!-- END branding -->

<!--Modulos-->

<!-- slider -->
        <div class="page-slider header-slider">
			<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:#2b2d35;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.4.5.1 fullwidth mode -->
                <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.5.1">
                    <ul>
                        <!-- SLIDE  -->
						
                        <li data-index="rs-40" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://dgoba.com/temas/dgoba/images/BANNER1.png" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="http://dgoba.com/modulos/Home/media/slide/BANNER1.png" alt="" title="Banner_Lumify_Morado_" width="1600" height="600" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <div id="layer-content">
                            	<div id="layer-space">
                                    <div class="tit28">La Sala de Rayos X Digital</div>
                                	<div class="tit52">AUTOMATIZADA</div>
                                    <div class="txt18"></div>
                                    <div class="ma-a"><a href="http://dgoba.com/productos/categoria/3-RAYOS-X" class="btn-azul" style="float:left;">Ver m&aacute;s</a></div>
                                </div>
                            </div>
                        </li>
			
                        <li data-index="rs-40" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://dgoba.com/temas/dgoba/images/ANACONDA_BANNER-b.png" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="http://dgoba.com/modulos/Home/media/slide/ANACONDA_BANNER-b.png" alt="" title="Banner_Lumify_Morado_" width="1600" height="600" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <div id="layer-content">
                            	<div id="layer-space">
                                    <div class="tit28"></div>
                                	<div class="tit52"></div>
                                    <div class="txt18"></div>
                                    <div class="ma-a"><a href="http://dgoba.com/productos/categoria/6-SEDANTES" class="btn-azul" style="float:left;">Ver m&aacute;s</a></div>
                                </div>
                            </div>
                        </li>
			
                        <li data-index="rs-40" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://dgoba.com/temas/dgoba/images/MEDIMAT_BANNER-b.png" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="http://dgoba.com/modulos/Home/media/slide/MEDIMAT_BANNER-b.png" alt="" title="Banner_Lumify_Morado_" width="1600" height="600" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <div id="layer-content">
                            	<div id="layer-space">
                                    <div class="tit28"></div>
                                	<div class="tit52"></div>
                                    <div class="txt18"></div>
                                    <div class="ma-a"><a href="http://dgoba.com/productos/item/121-MEDIMAT" class="btn-azul" style="float:left;">Ver m&aacute;s</a></div>
                                </div>
                            </div>
                        </li>
			
                        <!-- SLIDE  -->
                        <!--li data-index="rs-32" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?php echo $page_url.$path_tema;?>images/BANNER2.png" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <img src=".$page_url.$path_tema.images/BANNER2.png" alt="" title="principal-everflo" width="1600" height="600" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <div id="layer-content">
                            	<div id="layer-space">
                                    <div class="tit28">CURSO INTERNACIONAL DE</div>
                                	<div class="tit72">Radiolog&iacute;a e Imagen</div>
                                    <div class="txt18">del 17 de febrero en el Hotel Marquis Reforma, Ciudad de M&eacute;xico.</div>
                                    <div class="ma-a"><a href="http://www.servimed.com.mx/congresos-/index.php/radiologia2018" class="btn-azul">Ver m&aacute;s</a></div>
                                </div>
                            </div>
                        </li-->
                    </ul>
                    <script>
                        var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                        var htmlDivCss = "";
                        if (htmlDiv) {
                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                            var htmlDiv = document.createElement("div");
                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
                <script>
                    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                    var htmlDivCss = "";
                    if (htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                    } else {
                        var htmlDiv = document.createElement("div");
                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                    }
                </script>
                <script type="text/javascript">
                    setREVStartSize({
                        c: jQuery('#rev_slider_1_1'),
                        gridwidth: [1170],
                        gridheight: [410],
                        sliderLayout: 'fullwidth'
                    });

                    var revapi1,
                        tpj = jQuery;
                    tpj.noConflict();
                    tpj(document).ready(function() {
                        if (tpj("#rev_slider_1_1").revolution == undefined) {
                            revslider_showDoubleJqueryError("#rev_slider_1_1");
                        } else {
                            revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                sliderType: "standard",
                                //jsFileLocation: "//www.manprec.com/wp-content/plugins/revslider/public/assets/js/",
                                jsFileLocation: "http://dgoba.com/temas/goba/js/",
                                sliderLayout: "fullwidth",
                                dottedOverlay: "none",
                                delay: 9000,
                                navigation: {
                                    keyboardNavigation: "off",
                                    keyboard_direction: "horizontal",
                                    mouseScrollNavigation: "off",
                                    mouseScrollReverse: "default",
                                    onHoverStop: "off",
                                    touch: {
                                        touchenabled: "on",
                                        touchOnDesktop: "off",
                                        swipe_threshold: 75,
                                        swipe_min_touches: 1,
                                        swipe_direction: "horizontal",
                                        drag_block_vertical: false
                                    },
                                    arrows: {
                                        style: "hesperiden",
                                        enable: true,
                                        hide_onmobile: false,
                                        hide_onleave: false,
                                        tmp: '',
                                        left: {
                                            h_align: "left",
                                            v_align: "center",
                                            h_offset: 20,
                                            v_offset: 0
                                        },
                                        right: {
                                            h_align: "right",
                                            v_align: "center",
                                            h_offset: 20,
                                            v_offset: 0
                                        }
                                    }
                                },
                                visibilityLevels: [1240, 1024, 778, 480],
                                gridwidth: 1170,
                                gridheight: 410,
                                lazyType: "none",
                                shadow: 0,
                                spinner: "spinner0",
                                stopLoop: "off",
                                stopAfterLoops: -1,
                                stopAtSlide: -1,
                                shuffle: "off",
                                autoHeight: "on",
                                disableProgressBar: "on",
                                hideThumbsOnMobile: "off",
                                hideSliderAtLimit: 0,
                                hideCaptionAtLimit: 0,
                                hideAllCaptionAtLilmit: 0,
                                debugMode: false,
                                fallbacks: {
                                    simplifyAll: "off",
                                    nextSlideOnWindowFocus: "off",
                                    disableFocusListener: false,
                                }
                            });
                        }

                    }); /*ready*/
                </script>
                <script>
                    var htmlDivCss = unescape(".hesperiden.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%09width%3A40px%3B%0A%09height%3A40px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%20%20%20%20border-radius%3A%2050%25%3B%0A%7D%0A.hesperiden.tparrows%3Ahover%20%7B%0A%09background%3Argba%280%2C%200%2C%200%2C%201%29%3B%0A%7D%0A.hesperiden.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A20px%3B%0A%09color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%2040px%3B%0A%09text-align%3A%20center%3B%0A%7D%0A.hesperiden.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce82c%22%3B%0A%20%20%20%20margin-left%3A-3px%3B%0A%7D%0A.hesperiden.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce82d%22%3B%0A%20%20%20%20margin-right%3A-3px%3B%0A%7D%0A");
                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                    if (htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                    } else {
                        var htmlDiv = document.createElement('div');
                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                    }
                </script>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>
<!-- /slider -->
		        <!-- BEGIN #page -->
        <div id="page" class="hfeed ">

            <!-- BEGIN #main -->
            <div id="main">

                <!-- BEGIN #content -->
                <div id="content" class="" role="main">

                    <!-- BEGIN #post -->
                    <article id="post-6" class="post-6 page type-page status-publish hentry">

                        <div class="entry-page-items">
                            <!-- BEGIN full-section -->
                            <div class="page-row full-img parallax-yes margin-yes padding-top-yes padding-bottom-yes dark repeat-no" style="background-color:#ffffff;" id="heads">
                                <!-- BEGIN page-row -->
                                <div class="page-row ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="lm-col-12 lol-page-item">
                                                <div class="lol-item-heading">

                                                    <h2>NUESTRA TECNOLOG&Iacute;A</h2>

                                                    <p>Proveemos servicios integrales en el &aacute;rea de la salud.<br><sup><i class="fa fa-circle" style="font-size:6px; color:#666;"></i></sup> Material de curaci&oacute;n y osteosintesis <sup><i class="fa fa-circle" style="font-size:6px; color:#666;"></i></sup> Equipamiento hospitalario del &aacute;rea de imagen <br><sup><i class="fa fa-circle" style="font-size:6px; color:#666;"></i></sup> Software <sup><i class="fa fa-circle" style="font-size:6px; color:#666;"></i></sup> Consumibles <sup><i class="fa fa-circle" style="font-size:6px; color:#666;"></i></sup> Soporte t&eacute;cnico y capacitaci&oacute;n <sup><i class="fa fa-circle" style="font-size:6px; color:#666;"></i></sup> &Oacute;ptica<br><sup><i class="fa fa-circle" style="font-size:6px; color:#666;"></i></sup> Sedantes/AnaConDa</p>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END page-row -->

                                <!-- BEGIN page-row -->
                                <div class="page-row ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="lm-col-12 lol-page-item">

                                                <div class="lol-item-column">
<link href="http://dgoba.com/temas/dgoba/css/home1.css" type="text/css">                                                    
                                                    <!-- CACHE CREATED FOR: 5 -->
<link href="http://dgoba.com/temas/dgoba/css/home2.css" type="text/css">                                                    
                                                    <!-- THE ESSENTIAL GRID 2.1.0.2 POST -->

                                                    <article class="myportfolio-container minimal-light" id="esg-grid-5-2-wrap">

                                                        <div id="esg-grid-5-2" class="esg-grid" style="background-color: transparent;padding: 0px 0px 0px 0px ; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;">
                                                            <ul>
                                                            	<!-- categorias.json --><!--[1] -1-->
        <li id="eg-5-post-id-79" class="filterall filter-salud eg-harding-wrapper eg-post-id-79 item-ima1" data-date="1584072041">
        	<div class="esg-media-cover-wrapper">
            	<a href="http://dgoba.com/productos/categoria/1-COLCHONES" target="_self">
                	<div class="esg-entry-media esg-mediazoom" data-delay="0"><img src="http://dgoba.com/modulos/productos/fotos/colchones.jpg" alt="" width="1600" height="1200"></div>
               	</a>
            	<div class="esg-overlay esg-fade eg-harding-container" data-delay="0"></div>
                <div class="esg-entry-content eg-harding-content">
                	<div class="esg-content eg-post-79 eg-harding-element-0-a" style="text-align:center;"><a class="eg-harding-element-0 eg-post-79 tit-azul" href="http://dgoba.com/productos/categoria/1-COLCHONES" target="_self">COLCHONES</a></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 tit-black"><strong>Hospitalarios, para Camilla y de Aire</strong></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 desc-txt">Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</div>
                    <div class="esg-content eg-post-79 eg-harding-element-5" style="text-align:center"><a href="http://dgoba.com/productos/categoria/1-COLCHONES" class="btn-azul">Ver m&aacute;s</a></div>
                    <div class="esg-content eg-harding-element-6 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
        		</div>
        	</div>
        </li>
			<!--[2] -2-->
        <li id="eg-5-post-id-79" class="filterall filter-salud eg-harding-wrapper eg-post-id-79 item-ima2" data-date="1584072041">
        	<div class="esg-media-cover-wrapper">
            	<a href="http://dgoba.com/productos/categoria/2-CAMAS" target="_self">
                	<div class="esg-entry-media esg-mediazoom" data-delay="0"><img src="http://dgoba.com/modulos/productos/fotos/camas.jpg" alt="" width="1600" height="1200"></div>
               	</a>
            	<div class="esg-overlay esg-fade eg-harding-container" data-delay="0"></div>
                <div class="esg-entry-content eg-harding-content">
                	<div class="esg-content eg-post-79 eg-harding-element-0-a" style="text-align:center;"><a class="eg-harding-element-0 eg-post-79 tit-azul" href="http://dgoba.com/productos/categoria/2-CAMAS" target="_self">CAMAS</a></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 tit-black"><strong>Hospitalarias, Ginecol&oacute;gicas y Pedri&aacute;ticas</strong></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 desc-txt">Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</div>
                    <div class="esg-content eg-post-79 eg-harding-element-5" style="text-align:center"><a href="http://dgoba.com/productos/categoria/2-CAMAS" class="btn-azul">Ver m&aacute;s</a></div>
                    <div class="esg-content eg-harding-element-6 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
        		</div>
        	</div>
        </li>
			<!--[3] -3-->
        <li id="eg-5-post-id-79" class="filterall filter-salud eg-harding-wrapper eg-post-id-79 item-ima3" data-date="1584072041">
        	<div class="esg-media-cover-wrapper">
            	<a href="http://dgoba.com/productos/categoria/3-RAYOS-X" target="_self">
                	<div class="esg-entry-media esg-mediazoom" data-delay="0"><img src="http://dgoba.com/modulos/productos/fotos/menu-home-rayosx.jpg" alt="" width="1600" height="1200"></div>
               	</a>
            	<div class="esg-overlay esg-fade eg-harding-container" data-delay="0"></div>
                <div class="esg-entry-content eg-harding-content">
                	<div class="esg-content eg-post-79 eg-harding-element-0-a" style="text-align:center;"><a class="eg-harding-element-0 eg-post-79 tit-azul" href="http://dgoba.com/productos/categoria/3-RAYOS-X" target="_self">RAYOS X</a></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 tit-black"><strong>Radiograf&iacute;a Directa y Computarizada</strong></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 desc-txt">Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</div>
                    <div class="esg-content eg-post-79 eg-harding-element-5" style="text-align:center"><a href="http://dgoba.com/productos/categoria/3-RAYOS-X" class="btn-azul">Ver m&aacute;s</a></div>
                    <div class="esg-content eg-harding-element-6 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
        		</div>
        	</div>
        </li>
			<!--[4] -4-->
        <li id="eg-5-post-id-79" class="filterall filter-salud eg-harding-wrapper eg-post-id-79 item-ima4" data-date="1584072041">
        	<div class="esg-media-cover-wrapper">
            	<a href="http://dgoba.com/productos/categoria/4-QUIROFANO-Y-SOPORTE-DE-VIDA" target="_self">
                	<div class="esg-entry-media esg-mediazoom" data-delay="0"><img src="http://dgoba.com/modulos/productos/fotos/QUIROFANO_MENU.jpg" alt="" width="1600" height="1200"></div>
               	</a>
            	<div class="esg-overlay esg-fade eg-harding-container" data-delay="0"></div>
                <div class="esg-entry-content eg-harding-content">
                	<div class="esg-content eg-post-79 eg-harding-element-0-a" style="text-align:center;"><a class="eg-harding-element-0 eg-post-79 tit-azul" href="http://dgoba.com/productos/categoria/4-QUIROFANO-Y-SOPORTE-DE-VIDA" target="_self">QUIROFANO Y SOPORTE DE VIDA</a></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 tit-black"><strong>Hospitalarios, para Camilla y de Aire</strong></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 desc-txt">Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</div>
                    <div class="esg-content eg-post-79 eg-harding-element-5" style="text-align:center"><a href="http://dgoba.com/productos/categoria/4-QUIROFANO-Y-SOPORTE-DE-VIDA" class="btn-azul">Ver m&aacute;s</a></div>
                    <div class="esg-content eg-harding-element-6 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
        		</div>
        	</div>
        </li>
			<!--[5] -5-->
        <li id="eg-5-post-id-79" class="filterall filter-salud eg-harding-wrapper eg-post-id-79 item-ima5" data-date="1584072041">
        	<div class="esg-media-cover-wrapper">
            	<a href="http://dgoba.com/productos/categoria/5-OPTICA" target="_self">
                	<div class="esg-entry-media esg-mediazoom" data-delay="0"><img src="http://dgoba.com/modulos/productos/fotos/menu-home-optica.jpg" alt="" width="1600" height="1200"></div>
               	</a>
            	<div class="esg-overlay esg-fade eg-harding-container" data-delay="0"></div>
                <div class="esg-entry-content eg-harding-content">
                	<div class="esg-content eg-post-79 eg-harding-element-0-a" style="text-align:center;"><a class="eg-harding-element-0 eg-post-79 tit-azul" href="http://dgoba.com/productos/categoria/5-OPTICA" target="_self">OPTICA</a></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 tit-black"><strong>Lentes de Contacto y Cuidado</strong></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 desc-txt">Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</div>
                    <div class="esg-content eg-post-79 eg-harding-element-5" style="text-align:center"><a href="http://dgoba.com/productos/categoria/5-OPTICA" class="btn-azul">Ver m&aacute;s</a></div>
                    <div class="esg-content eg-harding-element-6 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
        		</div>
        	</div>
        </li>
			<!--[6] -6-->
        <li id="eg-5-post-id-79" class="filterall filter-salud eg-harding-wrapper eg-post-id-79 item-ima6" data-date="1584072041">
        	<div class="esg-media-cover-wrapper">
            	<a href="http://dgoba.com/productos/categoria/6-SEDANTES" target="_self">
                	<div class="esg-entry-media esg-mediazoom" data-delay="0"><img src="http://dgoba.com/modulos/productos/fotos/nodisponible.jpg" alt="" width="1600" height="1200"></div>
               	</a>
            	<div class="esg-overlay esg-fade eg-harding-container" data-delay="0"></div>
                <div class="esg-entry-content eg-harding-content">
                	<div class="esg-content eg-post-79 eg-harding-element-0-a" style="text-align:center;"><a class="eg-harding-element-0 eg-post-79 tit-azul" href="http://dgoba.com/productos/categoria/6-SEDANTES" target="_self">SEDANTES</a></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 tit-black"><strong><span class="TextRun  BCX0 SCXP1746169" lang="ES-ES" xml:lang="ES-ES" data-scheme-color="@595959,1,18:65000,19:35000" data-usefontface="true" data-contrast="none"><span class="NormalTextRun  BCX0 SCXP1746169">Innovaci&oacute;n dise&ntilde;ada para facilitar la&nbsp;</span></span><span class="TextRun  BCX0 SCXP1746169" lang="ES-ES" xml:lang="ES-ES" data-scheme-color="@595959,1,18:65000,19:35000" data-usefontface="true" data-contrast="none"><span class="NormalTextRun  BCX0 SCXP1746169">administraci&oacute;n de anest&eacute;sicos inhalados</span></span></strong></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5 desc-txt"><span class="TextRun SCXP267490768 BCX0" lang="ES-ES" xml:lang="ES-ES" data-scheme-color="@7F7F7F,0,18:50000" data-usefontface="true" data-contrast="none"><span class="NormalTextRun SCXP267490768 BCX0">El dispositivo AnaConDa&reg; ha sido desarrollado para&nbsp;</span></span><span class="TextRun SCXP267490768 BCX0" lang="ES-ES" xml:lang="ES-ES" data-scheme-color="@7F7F7F,0,18:50000" data-usefontface="true" data-contrast="none"><span class="NormalTextRun SCXP267490768 BCX0">reemplazar las costosas y complejas m&aacute;quinas de&nbsp;</span></span><span class="TextRun SCXP267490768 BCX0" lang="ES-ES" xml:lang="ES-ES" data-scheme-color="@7F7F7F,0,18:50000" data-usefontface="true" data-contrast="none"><span class="NormalTextRun SCXP267490768 BCX0">anestesia. Su dise&ntilde;o &uacute;nico permite la&nbsp;</span></span><span class="TextRun SCXP267490768 BCX0" lang="ES-ES" xml:lang="ES-ES" data-scheme-color="@7F7F7F,0,18:50000" data-usefontface="true" data-contrast="none"><span class="NormalTextRun SCXP267490768 BCX0">administraci&oacute;n sencilla de Isoflurano y Sevoflurano.</span></span></div>
                    <div class="esg-content eg-post-79 eg-harding-element-5" style="text-align:center"><a href="http://dgoba.com/productos/categoria/6-SEDANTES" class="btn-azul">Ver m&aacute;s</a></div>
                    <div class="esg-content eg-harding-element-6 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
        		</div>
        	</div>
        </li>
			<!-- /categorias.json -->                                                            </ul>
                                                        </div>

                                                    </article>
                                                    <div class="clear"></div>
                                                    <script type="text/javascript">
                                                        function eggbfc(winw, resultoption) {
                                                            var lasttop = winw,
                                                                lastbottom = 0,
                                                                smallest = 9999,
                                                                largest = 0,
                                                                samount = 0,
                                                                lamoung = 0,
                                                                lastamount = 0,
                                                                resultid = 0,
                                                                resultidb = 0,
                                                                responsiveEntries = [{
                                                                    width: 1400,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 1170,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 1024,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 960,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 778,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 640,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 480,
                                                                    amount: 1,
                                                                    mmheight: 0
                                                                }];
                                                            if (responsiveEntries != undefined && responsiveEntries.length > 0)
                                                                jQuery.each(responsiveEntries, function(index, obj) {
                                                                    var curw = obj.width != undefined ? obj.width : 0,
                                                                        cura = obj.amount != undefined ? obj.amount : 0;
                                                                    if (smallest > curw) {
                                                                        smallest = curw;
                                                                        samount = cura;
                                                                        resultidb = index;
                                                                    }
                                                                    if (largest < curw) {
                                                                        largest = curw;
                                                                        lamount = cura;
                                                                    }
                                                                    if (curw > lastbottom && curw <= lasttop) {
                                                                        lastbottom = curw;
                                                                        lastamount = cura;
                                                                        resultid = index;
                                                                    }
                                                                });
                                                            if (smallest > winw) {
                                                                lastamount = samount;
                                                                resultid = resultidb;
                                                            }
                                                            var obj = new Object;
                                                            obj.index = resultid;
                                                            obj.column = lastamount;
                                                            if (resultoption == "id")
                                                                return obj;
                                                            else
                                                                return lastamount;
                                                        }
                                                        if ("masonry" == "even") {
                                                            var coh = 0,
                                                                container = jQuery("#esg-grid-5-2");
                                                            var cwidth = container.width(),
                                                                ar = "4:2",
                                                                gbfc = eggbfc(jQuery(window).width(), "id"),
                                                                row = 6;
                                                            ar = ar.split(":");
                                                            aratio = parseInt(ar[0], 0) / parseInt(ar[1], 0);
                                                            coh = cwidth / aratio;
                                                            coh = coh / gbfc.column * row;
                                                            var ul = container.find("ul").first();
                                                            ul.css({
                                                                display: "block",
                                                                height: coh + "px"
                                                            });
                                                        }
                                                        var essapi_5;
                                                        jQuery(document).ready(function() {
                                                            essapi_5 = jQuery("#esg-grid-5-2").tpessential({
                                                                gridID: 5,
                                                                layout: "masonry",
                                                                forceFullWidth: "off",
                                                                lazyLoad: "off",
                                                                row: 6,
                                                                loadMoreAjaxToken: "644dd52a1f",
                                                                //loadMoreAjaxUrl: "https://www.manprec.com/wp-admin/admin-ajax.php",
                                                                loadMoreAjaxAction: "Essential_Grid_Front_request_ajax",
                                                                ajaxContentTarget: "ess-grid-ajax-container-",
                                                                ajaxScrollToOffset: "0",
                                                                ajaxCloseButton: "off",
                                                                ajaxContentSliding: "on",
                                                                ajaxScrollToOnLoad: "on",
                                                                ajaxNavButton: "off",
                                                                ajaxCloseType: "type1",
                                                                ajaxCloseInner: "false",
                                                                ajaxCloseStyle: "light",
                                                                ajaxClosePosition: "tr",
                                                                space: 40,
                                                                pageAnimation: "fade",
                                                                paginationScrollToTop: "off",
                                                                spinner: "spinner0",
                                                                lightBoxMode: "single",
                                                                animSpeed: 1000,
                                                                delayBasic: 1,
                                                                mainhoverdelay: 1,
                                                                filterType: "single",
                                                                showDropFilter: "hover",
                                                                filterGroupClass: "esg-fgc-5",
                                                                googleFonts: ['Open+Sans:300,400,600,700,800', 'Raleway:100,200,300,400,500,600,700,800,900', 'Droid+Serif:400,700'],
                                                                aspectratio: "4:2",
                                                                responsiveEntries: [{
                                                                    width: 1400,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 1170,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 1024,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 960,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 778,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 640,
                                                                    amount: 3,
                                                                    mmheight: 0
                                                                }, {
                                                                    width: 480,
                                                                    amount: 1,
                                                                    mmheight: 0
                                                                }]
                                                            });

                                                        });
                                                    </script>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END page-row -->

                            </div>
                            <!-- END full-section -->
                            <!-- BEGIN page-row ->
                            <div class="page-row ">
                                <div class="container">
                                    <div class="row">
                                        <div class="lm-col-12 lol-page-item" id="boton-mas-info">

                                            <div class="lol-item-column"><a href="https://www.manprec.com/contacto/">MÁS INFORMACIÓN</a></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END page-row -->

                        </div>

                    </article>
                    <!-- END #post -->
                    
<div class="page-row " style="background:#eee;">
	<div class="container">
		<p>&nbsp;</p>
                                <!-- BEGIN page-row -->
                                        <div class="row">
                                            <div class="lm-col-12 lol-page-item">
                                                <div class="lol-item-heading">
                                                    <h2>NUESTRAS MARCAS</h2>
                                                    <p>&nbsp;</p>
                                                </div>
                                            </div>
                                        </div>
                                <!-- END page-row -->
	</div>

<div class="row">
	<div class="lm-col-12 lol-page-item">
		<div class="lol-item-column" style="padding:0 35px;">
<ul class="marcas">
<li><a href="http://www.hollister.com.mx/es-mx/"><img src="http://dgoba.com/temas/dgoba/img/hollister.jpg" style="height:45px;"></a></li><li><a href="http://www.agfa.com/corporate/​"><img src="http://dgoba.com/temas/dgoba/img/agfa.jpg" style="height:45px;"></a></li><li><a href="http://www.linet.es/es/cuidado-sanitario​"><img src="http://dgoba.com/temas/dgoba/img/wlinet.jpg" style="height:45px;"></a></li><li><a href="http://www.welchallyn.com/en.html​"><img src="http://dgoba.com/temas/dgoba/img/welch-allyn.jpg" style="height:45px;"></a></li><li><a href="http://www.smith-nephew.com/latin-america/"><img src="http://dgoba.com/temas/dgoba/img/smith.jpg" style="height:45px;"></a></li><li><a href="http://www.mindray.com/mx/about.html"><img src="http://dgoba.com/temas/dgoba/img/mindray.jpg" style="height:45px;"></a></li><li><a href="http://www.draeger.com/es_mx/Home"><img src="http://dgoba.com/temas/dgoba/img/drager.jpg" style="height:45px;"></a></li><li><a href="http://www.bausch.com.es/inicio/"><img src="http://dgoba.com/temas/dgoba/img/bausch+lomb.jpg" style="height:45px;"></a></li><li><a href="http://coopervisionlatam.com/nuestros-productos"><img src="http://dgoba.com/temas/dgoba/img/coopervision.jpg" style="height:45px;"></a></li><li><a href="http://www.jnjmexico.com/acuvue"><img src="http://dgoba.com/temas/dgoba/img/jhonson-jhonson.jpg" style="height:45px;"></a></li><li><a href="http://dgoba.com/productos/item/121-MEDIMAT"><img src="http://dgoba.com/temas/dgoba/img/medimat.jpg" style="height:45px;"></a></li><li><a href="http://www.samsunghealthcare.com/en"><img src="http://dgoba.com/temas/dgoba/img/samsung.jpg" style="height:45px;"></a></li></ul>
		</div>
	</div>
</div>

</div>                    
                </div>
                <!-- END #content -->

            </div>
            <!-- END #main -->

        </div>
        <!-- END #page -->

<!--/Modulos-->

        <!-- BEGIN #footer -->
        <footer id="footer" class="dark" role="contentinfo">
            <div class="container">
                <div class="footer-widgets">
					<div class="row">
						<img src="http://dgoba.com/temas/dgoba/images/logo2.png" alt="logo 2" width="170" />
						<p></p>
					</div>
                    <!-- BEGIN row -->
                    <div class="row">
                        <!-- BEGIN col-3 -->
                        <div class="lm-col-4">
                            <div class="footer-widget footer-widget-1">
                                <section id="text-2" class="widget widget_text">
                                    <div class="widget-header">
                                        <h3 class="widget-title">Acerca de GOBA</h3></div>
                                    <div class="textwidget">
                                        <p>Distribuidora GOBA se fund&oacute; en el a&ntilde;o de 1992 la empresa se encuentra ubicada en la Cd. De Quer&eacute;taro, M&eacute;xico.</p>
                                        <p>Goba es proveedor del &aacute;rea de la salud proporcionando equipo m&eacute;dico, soporte, software, accesorios, consumibles, capacitaci&oacute;n y asistencia.</p>
                                        <p><a href="#" class="btn-azul">Ver m&aacute;s</a></p>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- END col-3 -->
                        <!-- BEGIN col-3 -->
                        <div class="lm-col-4">
                            <div class="footer-widget footer-widget-3">
                                <section id="nav_menu-2" class="widget widget_nav_menu">
                                    <div class="widget-header">
                                        <h3 class="widget-title">Blog</h3></div>
                                    <div class="textwidget">
                                        <p>Somos una empresa 100% Mexicana. </p>

                                        <p>Importadora, distribuidora e integradora de equipo y mobiliario m&eacute;dico y hospitalario.</p>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- END col-3 -->
                        <!-- BEGIN col-3 -->
                        <div class="lm-col-4">
                            <div class="footer-widget footer-widget-2">
                                <section id="text-3" class="widget widget_text">
                                    <div class="widget-header">
                                        <h3 class="widget-title">Contacto</h3></div>
                                    <div class="textwidget">
                                        <div class="uju">
                                            <!--p>Contáctanos a través de cualquier medio para asesorarte sobre nuestros productos y servicios.</p-->
    <p class="opciones2"><span>T.</span> <a target="_blank" href="tel:(+52 442) 2455656">(+52 442) 2455656</a></p>	<p class="opciones2"><span>@.</span> <a target="_blank" href="/cdn-cgi/l/email-protection#bfdcd0d1cbdedccbd0ffdbd8d0ddde91dcd0d2"><span class="__cf_email__" data-cfemail="2c4f4342584d4f58436c484b434e4d024f4341">[email&#160;protected]</span></a></p>	<!--?php echo $webMail = ($webMail!='') ? '<p class="opciones"><i class="fa fa-envelope" aria-hidden="true"></i> <a target="_blank" href="mailto:'.$webMail.'">'.$webMail.'</a></p>' : '';?-->
                                        </div>
                                        <p>&nbsp;</p>
                                        <p class="opciones">
    <a target="_blank" href="https://www.facebook.com/Distribuidora-Goba-de-Quer?taro-SA-de-CV-104110954536950"><i class="fa fa-facebook" aria-hidden="true"></i></a> 	        <a target="_blank" href="https://ar.linkedin.com/company/distribuidora-goba&#8203;/"><i class="fa fa-linkedin" aria-hidden="true"></i></a>         <a target="_blank" href="https://www.instagram.com/gobadequeretaro/"><i class="fa fa-instagram" aria-hidden="true"></i></a>                                         
                                        </p>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- END col-3 -->

                    </div>
                    <!-- END row -->

                </div>

            </div>
        </footer>
        <!-- END #footer -->

        <!-- BEGIN #footer-bottom -->
        <div id="footer-bottom" class="dark">
            <div class="container">
                <!-- BEGIN row -->
                <div class="row">
                    <div class="lm-col-12">
                        <p class="footer-bottom-copy">&copy; <span>2020 GOBA</span> Todos los Derechos Reservados.</p>
                    </div>
                </div>
                <!-- END row -->
            </div>
        </div>
        <!-- END #footer-bottom -->

    </div>
    <!-- END wrap -->
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/html" id="tmpl-wp-playlist-current-item">
        <# if ( data.image ) { #>
            <img src="{{ data.thumb.src }}" alt="" />
            <# } #>
                <div class="wp-playlist-caption">
                    <span class="wp-playlist-item-meta wp-playlist-item-title">&#8220;{{ data.title }}&#8221;</span>
                    <# if ( data.meta.album ) { #><span class="wp-playlist-item-meta wp-playlist-item-album">{{ data.meta.album }}</span>
                        <# } #>
                            <# if ( data.meta.artist ) { #><span class="wp-playlist-item-meta wp-playlist-item-artist">{{ data.meta.artist }}</span>
                                <# } #>
                </div>
    </script>
    <script type="text/html" id="tmpl-wp-playlist-item">
        <div class="wp-playlist-item">
            <a class="wp-playlist-caption" href="{{ data.src }}">
			{{ data.index ? ( data.index + '. ' ) : '' }}
			<# if ( data.caption ) { #>
				{{ data.caption }}
			<# } else { #>
				<span class="wp-playlist-item-title">&#8220;{{{ data.title }}}&#8221;</span>
				<# if ( data.artists && data.meta.artist ) { #>
				<span class="wp-playlist-item-artist"> &mdash; {{ data.meta.artist }}</span>
				<# } #>
			<# } #>
		</a>
            <# if ( data.meta.length_formatted ) { #>
                <div class="wp-playlist-item-length">{{ data.meta.length_formatted }}</div>
                <# } #>
        </div>
    </script>

    <div id="views-extra-css-ie7" style="display:none;" aria-hidden="true">
        <!--[if IE 7]><style>
.wpv-pagination { *zoom: 1; }
</style><![endif]-->
    </div>
    <script type="text/javascript">
        function revslider_showDoubleJqueryError(sliderID) {
            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
            jQuery(sliderID).show().html(errorMessage);
        }
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpcf7 = {
            "recaptcha": {
                "messages": {
                    "empty": "Please verify that you are not a robot."
                }
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/scripts.js?ver=4.7'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var lolfmk_love_it_vars = {
            "ajaxurl": "https:\/\/www.dgoba.com\/wp-admin\/admin-ajax.php",
            "nonce": "56dc1bec4b",
            "theme_name": "nantes_child"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/love.js?ver=1.31'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var lolfmk_process_form_a_vars = {
            "ajaxurl": "https:\/\/www.dgoba.com\/wp-admin\/admin-ajax.php",
            "nonce": "36919966d5"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/mailchimp.js?ver=1.31'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "Ver Carrito",
            "cart_url": "https:\/\/www.dgoba.com\/carrito\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/add-to-cart.min.js?ver=3.0.4'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.blockUI.min.js?ver=2.70'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/js.cookie.min.js?ver=2.1.4'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var woocommerce_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/woocommerce.min.js?ver=3.0.4'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "fragment_name": "wc_fragments"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/cart-fragments.min.js?ver=3.0.4'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/common.js?ver=4.7.8'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/init.js?ver=4.7.8'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.parallax.js?ver=4.7.8'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/core.min.js?ver=1.11.4'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/datepicker.min.js?ver=1.11.4'></script>
    <script type='text/javascript'>
        jQuery(document).ready(function(jQuery) {
            jQuery.datepicker.setDefaults({
                "closeText": "Cerrar",
                "currentText": "Hoy",
                "monthNames": ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
                "monthNamesShort": ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                "nextText": "Siguiente",
                "prevText": "Previo",
                "dayNames": ["Domingo", "Lunes", "Martes", "Mi\u00e9rcoles", "Jueves", "Viernes", "S\u00e1bado"],
                "dayNamesShort": ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                "dayNamesMin": ["D", "L", "M", "X", "J", "V", "S"],
                "dateFormat": "d MM, yy",
                "firstDay": 1,
                "isRTL": false
            });
        });
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var mejsL10n = {
            "language": "es-MX",
            "strings": {
                "Close": "Cerrar",
                "Fullscreen": "Pantalla completa",
                "Turn off Fullscreen": "Salir de pantalla completa",
                "Go Fullscreen": "Ver en pantalla completa",
                "Download File": "Descargar archivo",
                "Download Video": "Descargar v\u00eddeo",
                "Play": "Reproducir",
                "Pause": "Pausa",
                "Captions\/Subtitles": "Pies de foto \/ Subt\u00edtulos",
                "None": "None",
                "Time Slider": "Control deslizante de tiempo",
                "Skip back %1 seconds": "Retroceder %1 segundos",
                "Video Player": "Reproductor de video",
                "Audio Player": "Reproductor de audio",
                "Volume Slider": "Control deslizante de volumen",
                "Mute Toggle": "Desactivar sonido",
                "Unmute": "Activar sonido",
                "Mute": "Silenciar",
                "Use Up\/Down Arrow keys to increase or decrease volume.": "Use las teclas de flecha arriba\/abajo para incrementar o disminuir el volumen.",
                "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.": "Use las teclas de flecha izquierda\/derecha para avanzar un segundo, las flechas arriba\/abajo para avanzar diez segundos."
            }
        };
        var _wpmejsSettings = {
            "pluginPath": "\/temas\/goba\/js\/"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/mediaelement-and-player.min.js?ver=2.22.0'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/wp-mediaelement.min.js?ver=4.7.8'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/underscore.min.js?ver=1.8.3'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpUtilSettings = {
            "ajax": {
                "url": "\/wp-admin\/admin-ajax.php"
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/wp-util.min.js?ver=4.7.8'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/backbone.min.js?ver=1.2.3'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/wp-playlist.min.js?ver=4.7.8'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpv_pagination_local = {
            "front_ajaxurl": "https:\/\/www.dgoba.com\/wp-admin\/admin-ajax.php",
            "calendar_image": "https:\/\/www.dgoba.com\/wp-content\/plugins\/wp-views\/embedded\/res\/img\/calendar.gif",
            "calendar_text": "Select date",
            "datepicker_min_date": null,
            "datepicker_max_date": null,
            "resize_debounce_tolerance": "100",
            "datepicker_style_url": "https:\/\/www.dgoba.com\/wp-content\/plugins\/wp-views\/vendor\/toolset\/toolset-common\/toolset-forms\/css\/wpt-jquery-ui\/jquery-ui-1.11.4.custom.css"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/wpv-pagination-embedded.js?ver=2.5.1'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/hoverIntent.min.js?ver=1.8.1'></script>
<!--Agregado-->
<script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/position.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pum_vars = {"version":"1.7.20","ajaxurl":"https:\/\/www.dgoba.com\/wp-admin\/admin-ajax.php","restapi":"https:\/\/www.dgoba.com\/wp-json\/pum\/v1","rest_nonce":null,"default_theme":"1126940","debug_mode":"","popups":{"pum-1126946":{"disable_form_reopen":false,"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"theme_id":"1126940","size":"medium","responsive_min_width":"0%","responsive_max_width":"100%","custom_width":"640px","custom_height":"380px","animation_type":"fade","animation_speed":"350","animation_origin":"center top","location":"center top","position_top":"100","position_bottom":"0","position_left":"0","position_right":"0","zindex":"1999999999","close_button_delay":"0","triggers":[],"cookies":[],"id":1126946,"slug":"e-book"}},"disable_tracking":"","home_url":"\/","message_position":"top","core_sub_forms_enabled":"1"};
var ajaxurl = "https:\/\/www.dgoba.com\/wp-admin\/admin-ajax.php";
var pum_debug_vars = {"debug_mode_enabled":"Popup Maker: Debug Mode Enabled","debug_started_at":"Debug started at:","debug_more_info":"For more information on how to use this information visit https:\/\/docs.wppopupmaker.com\/?utm_medium=js-debug-info&utm_campaign=ContextualHelp&utm_source=browser-console&utm_content=more-info","global_info":"Global Information","localized_vars":"Localized variables","popups_initializing":"Popups Initializing","popups_initialized":"Popups Initialized","single_popup_label":"Popup: #","theme_id":"Theme ID: ","label_method_call":"Method Call:","label_method_args":"Method Arguments:","label_popup_settings":"Settings","label_triggers":"Triggers","label_cookies":"Cookies","label_delay":"Delay:","label_conditions":"Conditions","label_cookie":"Cookie:","label_settings":"Settings:","label_selector":"Selector:","label_mobile_disabled":"Mobile Disabled:","label_tablet_disabled":"Tablet Disabled:","label_event":"Event: %s","triggers":{"click_open":"Click Open","auto_open":"Time Delay \/ Auto Open"},"cookies":{"on_popup_close":"On Popup Close","on_popup_open":"On Popup Open","pum_sub_form_success":"Subscription Form: Successful","pum_sub_form_already_subscribed":"Subscription Form: Already Subscribed","manual":"Manual JavaScript","cf7_form_success":"Contact Form 7 Success"}};
var pum_sub_vars = {"ajaxurl":"https:\/\/www.dgoba.com\/wp-admin\/admin-ajax.php","message_position":"top"};
/* ]]> */
</script>
<!--/Agregado--> 
<!--script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.min.js'></script-->   
<script> 
var $ = jQuery.noConflict(); 
$(document).ready(function(){  
        $("#bm1").hover(function () {
            $("#sm1").slideToggle("slow");
        });
        $("#bm2").hover(function () {
            $("#sm2").slideToggle("slow");
        });
        $("#bm3").hover(function () {
            $("#sm3").slideToggle("slow");
        });
        $("#bm4").hover(function () {
            $("#sm4").slideToggle("slow");
        });
        $("#bm5").hover(function () {
            $("#sm5").slideToggle("slow");
        });
        $("#bm6").hover(function () {
            $("#sm6").slideToggle("slow");
        });
});
</script>

    
    <script type='text/javascript'>
        /* <![CDATA[ */
        var megamenu = {
            "timeout": "300",
            "interval": "100"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/maxmegamenu.js?ver=2.3.5'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/wp-embed.min.js?ver=4.7.8'></script>
    <script type='text/javascript' src='http://dgoba.com/temas/dgoba/js/jquery.themepunch.essential.min.js?ver=2.1.0.2'></script>
</body>
</html>