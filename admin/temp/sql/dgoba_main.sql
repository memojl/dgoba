-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-03-2020 a las 03:32:59
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dgoba_main`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_access`
--

CREATE TABLE `goba_access` (
  `ID` int(9) UNSIGNED NOT NULL,
  `user` varchar(50) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `navegador` varchar(20) NOT NULL,
  `os` varchar(10) NOT NULL,
  `code` varchar(6) NOT NULL,
  `fecha` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_access`
--

INSERT INTO `goba_access` (`ID`, `user`, `ip`, `navegador`, `os`, `code`, `fecha`) VALUES
(1, 'admin', '127.0.0.1', 'CHROME', 'WIN', '944950', '2019-06-06 03:35:27'),
(2, 'usuario', '127.0.0.1', 'CHROME', 'WIN', '234567', '2019-06-06 03:35:27'),
(4, 'demo', '127.0.0.1', 'CHROME', 'WIN', '234567', '2019-06-06 03:35:27'),
(5, 'ventas', '127.0.0.1', 'CHROME', 'WIN', '234567', '2019-06-06 03:35:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_api_version`
--

CREATE TABLE `goba_api_version` (
  `ID` int(9) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `vence` varchar(20) NOT NULL,
  `ultimate` varchar(50) NOT NULL,
  `status` varchar(100) NOT NULL,
  `des_ver` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_api_version`
--

INSERT INTO `goba_api_version` (`ID`, `nom`, `vence`, `ultimate`, `status`, `des_ver`) VALUES
(1, 'phponix2017', '31/08/2019', '01.2.3.5', 'obsoleta', ''),
(2, 'AdminLTE', '31/12/2019', '01.2.4.5', 'obsoleta', ''),
(3, 'AdminLTE CSS', '30/11/2019', '01.2.4.6', 'activa', ''),
(4, 'AdminLTE CSS2', '30/11/2021', '01.2.5.1', 'activa', ''),
(5, 'AdminLTE 7Ajax', '29/05/2024', '01.2.6.6', 'activa', ''),
(6, 'AdminLTE PHP7', '01/12/2026', '01.2.7.2', 'activa', ''),
(7, 'AdminLTE SE-X', '30/11/2027', '01.2.7.8', 'activa', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_blog`
--

CREATE TABLE `goba_blog` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `tag` varchar(200) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `fecha` varchar(21) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_blog`
--

INSERT INTO `goba_blog` (`ID`, `cover`, `titulo`, `descripcion`, `contenido`, `tag`, `autor`, `fmod`, `fecha`, `visible`) VALUES
(1, 'blog_FO_petrolera.jpg', 'Mi primer blog', 'Si vives con EPOC, tener una fuente de oxígeno confiable es importante para mantener...', '<p>Si vives con EPOC, tener una fuente de ox&iacute;geno confiable es importante para mantener tu calidad de vida. Sin embargo, existen tantos tipos diferentes de concentradores de ox&iacute;geno en el mercado hoy en d&iacute;a, que puede ser dif&iacute;cil elegir el que mejor se adapte a sus necesidades. A medida que esta tecnolog&iacute;a contin&uacute;a avanzando, aparecen caracter&iacute;sticas m&aacute;s nuevas y opciones m&aacute;s c&oacute;modas, &iexcl;y desea aprovecharlas al m&aacute;ximo!</p>\r\n<p>La buena noticia es que hay m&aacute;s opciones para la terapia de ox&iacute;geno disponibles para ti; a continuaci&oacute;n, hemos recopilado informaci&oacute;n excelente sobre los dos principales concentradores de oxigeno dom&eacute;sticos de Philips Respironics:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td width=\"299\"><strong>EVERFLO</strong>\r\n<p>&nbsp;</p>\r\n<p>El concentrador de ox&iacute;geno EverFlo de 5 litros es una m&aacute;quina silenciosa, liviana y compacta que es menos llamativa que muchas otras.</p>\r\n<p>Los usuarios pueden comprar el modelo est&aacute;ndar, o el que tiene un indicador de porcentaje de ox&iacute;geno (OPI) y usa ultrasonido para medir el flujo de ox&iacute;geno.</p>\r\n<p>Los controles se encuentran en el lado delantero izquierdo de la m&aacute;quina y una perilla de rodillo controla el medidor de flujo de ox&iacute;geno empotrado en el centro. Una botella de humidificador se puede conectar a la parte posterior izquierda de la m&aacute;quina con velcro. &iexcl;El tubo se conecta f&aacute;cilmente a la c&aacute;nula de metal encima del interruptor de encendido, y tambi&eacute;n se pueden almacenar tubos adicionales en el interior.</p>\r\n<p>&iexcl;EverFlo 5L pesa 14 kgs y entrega ox&iacute;geno a .5-5 LPM con una concentraci&oacute;n de ox&iacute;geno de hasta 95% en todas las velocidades de flujo. La m&aacute;quina mide 58 cm de profundidad.</p>\r\n<p>El concentrador EverFlo de 5L viene con una garant&iacute;a est&aacute;ndar de 1 a&ntilde;o.</p>\r\n</td>\r\n<td width=\"299\"><strong>MILLENNIUM</strong>\r\n<p>&nbsp;</p>\r\n<p>El concentrador de ox&iacute;geno Millenium proporciona hasta 10 LPM de ox&iacute;geno, d&aacute;ndole las especificaciones de una unidad de &ldquo;alto flujo&rdquo;.</p>\r\n<p>El concentrador de ox&iacute;geno est&aacute; disponible en dos modelos: el modelo est&aacute;ndar y uno dise&ntilde;ado con un indicador de porcentaje de ox&iacute;geno (OPI), una funci&oacute;n que utiliza tecnolog&iacute;a de ultrasonido para medir el flujo de ox&iacute;geno.</p>\r\n<p>El dise&ntilde;o rectangular blanco es fuerte y resistente, y cuatro ruedas grandes (junto con un asa insertada en la parte superior) lo hacen bastante f&aacute;cil de mover.</p>\r\n<p>Este concentrador tiene una v&aacute;lvula SMC de &ldquo;ciclo seguro&rdquo;, dise&ntilde;ada espec&iacute;ficamente para manejar los mayores flujos de presi&oacute;n necesarios para una m&aacute;quina de 10 LPM. Millenium tambi&eacute;n est&aacute; dise&ntilde;ado con un compresor de doble cabezal equipado para impulsar m&aacute;s aire a trav&eacute;s de los lechos de tamices de la m&aacute;quina para eliminar el nitr&oacute;geno.</p>\r\n<p>Philips Respironics &ldquo;Millennium&rdquo; viene con una garant&iacute;a est&aacute;ndar de un a&ntilde;o.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"599\"><strong>Caracter&iacute;sticas y Beneficios</strong></td>\r\n</tr>\r\n<tr>\r\n<td width=\"299\">Silencioso y f&aacute;cil de usar\r\n<p>&nbsp;</p>\r\n<p>Controles claros y visibles</p>\r\n<p>Dise&ntilde;o ergon&oacute;mico: rueda f&aacute;cilmente</p>\r\n<p>Peso ligero de 14 kg</p>\r\n<p>Medidor de flujo empotrado para proteger contra la rotura</p>\r\n<p>Velcro asegura la botella del humidificador en la m&aacute;quina</p>\r\n<p>Proporciona ox&iacute;geno a .5-5 LPM con 95% de ox&iacute;geno</p>\r\n<p>Alarmas de seguridad por fallas</p>\r\n<p>Garant&iacute;a de producto de tres a&ntilde;os</p>\r\n</td>\r\n<td width=\"299\">F&aacute;cil de usar: los controles son claros y visibles\r\n<p>&nbsp;</p>\r\n<p>Pesa 24 kg</p>\r\n<p>Indicador de Porcentaje de Ox&iacute;geno (OPI) se puede agregar en</p>\r\n<p>Proporciona ox&iacute;geno a 1-10 LPM al 96% de ox&iacute;geno</p>\r\n<p>Alarmas de seguridad por fallas y bajo porcentaje de ox&iacute;geno</p>\r\n<p>Menos partes m&oacute;viles que otros concentradores</p>\r\n<p>Garant&iacute;a est&aacute;ndar de un a&ntilde;o</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"599\"><strong>Pros</strong></td>\r\n</tr>\r\n<tr>\r\n<td width=\"299\">Funcionamiento silencioso y sonido silencioso cuando se inicia (45 db)\r\n<p>&nbsp;</p>\r\n<p>F&aacute;cil de usar</p>\r\n<p>Confiable y ligero</p>\r\n<p>Port&aacute;til y f&aacute;cil de mover</p>\r\n<p>Consumo de energ&iacute;a de 350 w</p>\r\n<p>&nbsp;</p>\r\n</td>\r\n<td width=\"299\">Bien hecho y f&aacute;cil de configurar\r\n<p>&nbsp;</p>\r\n<p>Robusto, confiable y de bajo mantenimiento</p>\r\n<p>Produce hasta 10 LPM de ox&iacute;geno</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"599\"><strong>Contras</strong></td>\r\n</tr>\r\n<tr>\r\n<td width=\"299\">Bip fuerte cuando se inicia\r\n<p>&nbsp;</p>\r\n<p>Baja altitud de trabajo</p>\r\n<p>Produce hasta 5 LPM de ox&iacute;geno</p>\r\n</td>\r\n<td width=\"299\">Demasiado ruidoso para algunos usuarios (50 db)\r\n<p>&nbsp;</p>\r\n<p>Pesa 24 kg</p>\r\n<p>Tiene m&aacute;s potencia de la que muchos usuarios necesitan</p>\r\n<p>Consumo de energ&iacute;a de 600 w</p>\r\n<p><strong>&nbsp;</strong></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>La elecci&oacute;n de los dispositivos de administraci&oacute;n de ox&iacute;geno depende del requerimiento del paciente, la eficacia del dispositivo, la fiabilidad, la facilidad de aplicaci&oacute;n terap&eacute;utica y la aceptaci&oacute;n del paciente. <a href=\"http://samsung-healthcare.mx/contacto\"><span style=\"text-decoration: underline;\">Para m&aacute;s informaci&oacute;n sobre la elecci&oacute;n de su concentrador de ox&iacute;geno no dude en contactarnos.</span></a></p>', 'EPOC, Oxígeno', 'admin', '2018-09-24 22:23:34', '2017-01-18 14:05:23', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_blog_coment`
--

CREATE TABLE `goba_blog_coment` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ip` varchar(18) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `id_b` int(3) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_blog_coment`
--

INSERT INTO `goba_blog_coment` (`ID`, `ip`, `nombre`, `email`, `comentario`, `id_b`, `fecha`, `visible`) VALUES
(1, '127.0.0.1', 'Miguel Hernadez', 'mherco@hotmail.com', 'Mensaje de prueba de un comentario.', 1, '2019-07-06 18:14:37', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_clientes`
--

CREATE TABLE `goba_clientes` (
  `ID_cli` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `empresa` varchar(200) NOT NULL,
  `domicilio` varchar(300) NOT NULL,
  `tel_ofi` varchar(12) NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_clientes`
--

INSERT INTO `goba_clientes` (`ID_cli`, `nombre`, `empresa`, `domicilio`, `tel_ofi`, `email`) VALUES
(1, 'KAREN NAVARRO', 'KEY AGENCIA DIGITAL', 'CELAYA, GTO', '442 788 5025', 'karen.navarro@keyagenciadigital.com'),
(2, 'RAMSES', 'FIBRECEN', 'Guanajuato No. 5-B, Col. San Francisquito', '442 305 7704', 'fibrecen@fibrecen.com.mx');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_comp`
--

CREATE TABLE `goba_comp` (
  `ID` int(1) UNSIGNED NOT NULL,
  `page` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_comp`
--

INSERT INTO `goba_comp` (`ID`, `page`) VALUES
(1, 'usuarios/login.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_config`
--

CREATE TABLE `goba_config` (
  `ID` int(1) UNSIGNED NOT NULL,
  `logo` varchar(100) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `dominio` varchar(100) NOT NULL,
  `path_root` varchar(150) NOT NULL,
  `page_url` varchar(100) NOT NULL,
  `keyword` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `metas` text NOT NULL,
  `g_analytics` text NOT NULL,
  `tel` varchar(20) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `wapp` varchar(20) NOT NULL,
  `webMail` varchar(100) NOT NULL,
  `contactMail` varchar(100) NOT NULL,
  `mode` varchar(50) NOT NULL,
  `chartset` varchar(30) NOT NULL,
  `dboard` varchar(50) NOT NULL,
  `dboard2` varchar(50) NOT NULL,
  `direc` varchar(250) NOT NULL,
  `CoR` varchar(100) NOT NULL,
  `CoE` varchar(100) NOT NULL,
  `BCC` varchar(100) NOT NULL,
  `CoP` varchar(100) NOT NULL,
  `fb` varchar(250) NOT NULL,
  `tw` varchar(100) NOT NULL,
  `gp` varchar(100) NOT NULL,
  `lk` varchar(100) NOT NULL,
  `yt` varchar(100) NOT NULL,
  `ins` varchar(100) NOT NULL,
  `wv` varchar(100) NOT NULL,
  `licencia` varchar(300) NOT NULL,
  `version` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_config`
--

INSERT INTO `goba_config` (`ID`, `logo`, `page_name`, `title`, `dominio`, `path_root`, `page_url`, `keyword`, `description`, `metas`, `g_analytics`, `tel`, `phone`, `wapp`, `webMail`, `contactMail`, `mode`, `chartset`, `dboard`, `dboard2`, `direc`, `CoR`, `CoE`, `BCC`, `CoP`, `fb`, `tw`, `gp`, `lk`, `yt`, `ins`, `wv`, `licencia`, `version`) VALUES
(1, 'logo.min.png', 'Goba', 'DGoba - La m&aacute;s grande red en el baj&iacute;&shy;o', 'http://localhost/', '/MisSitios/dgoba', 'http://localhost/MisSitios/dgoba/', 'samsung-healthcare,salud,equipo medico,rayos-x', 'Venta de equipo de rayos-x.', '<!--Responsive Meta-->\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\r\n<!-- META-TAGS generadas por http://metatags.miarroba.es -->\r\n<META NAME=\"DC.Language\" SCHEME=\"RFC1766\" CONTENT=\"Spanish\">\r\n<META NAME=\"AUTHOR\" CONTENT=\"Guillermo Jimenez\">\r\n<META NAME=\"REPLY-TO\" CONTENT=\"multiportal@outlook.com\">\r\n<LINK REV=\"made\" href=\"mailto:multiportal@outlook.com\">\r\n', '<!--Google Analytics-->', '(+52 442) 2455656', '', '4426002842', 'ventas@dgoba.com', 'contacto@dgoba.com', 'page', 'iso-8859-1', 'dashboard', 'AdminLTE', 'Centro, Santiago de Querétaro, México', 'multiportal@outlook.com', 'phponix@webcindario.com', '', 'memojl08@gmail.com', 'https://www.facebook.com/Distribuidora-Goba-de-Quer?taro-SA-de-CV-104110954536950', '', '', 'https://ar.linkedin.com/company/distribuidora-goba&#8203;/', '', 'https://www.instagram.com/gobadequeretaro/', '', 'cms-px31q2hponix31q2x.admx31q2in458x31q2x.202x31q24.05.x31q212.01x31q2.2.6.x31q26x31q2', '01.2.7.8');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_contacto`
--

CREATE TABLE `goba_contacto` (
  `ID` int(9) UNSIGNED NOT NULL,
  `ip` varchar(25) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `para` varchar(50) NOT NULL,
  `de` varchar(50) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `asunto` varchar(150) NOT NULL,
  `msj` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cat_list` varchar(50) NOT NULL,
  `seccion` varchar(50) NOT NULL,
  `tabla` varchar(50) NOT NULL,
  `adjuntos` text NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `status` int(11) NOT NULL,
  `ID_login` int(11) NOT NULL,
  `ID_user` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_contacto`
--

INSERT INTO `goba_contacto` (`ID`, `ip`, `nombre`, `email`, `para`, `de`, `tel`, `titulo`, `asunto`, `msj`, `fecha`, `cat_list`, `seccion`, `tabla`, `adjuntos`, `visto`, `status`, `ID_login`, `ID_user`, `visible`) VALUES
(1, '127.0.0.1', 'Miguel Hernadez ', 'mherco@hotmail.com', '', '', '4421234567', '', 'Mensaje de Bienvenida- CENTRO DE CONTACTO', 'Hola estimado usuario, bienvenido a su plataforma \"PHPONIX CMS\" aqui se guardara un copia de respaldo de todos sus correos de contacto y registros de su página web.\r\n\r\nCualquier duda o comentario puede ponerse en contacto a través del correo a multiportal@outlook.com o en nuestra página https://phponix.webcindario.com \r\n\r\nATTE.\r\nEl equipo de PHPONIX & MULTIPORTAL ', '2019-11-24 03:19:14', 'inbox', 'contacto', '', '', 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_contacto_forms`
--

CREATE TABLE `goba_contacto_forms` (
  `ID` int(6) UNSIGNED NOT NULL,
  `seccion` varchar(100) NOT NULL,
  `modulo` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `bcc` varchar(200) NOT NULL,
  `CoE` varchar(100) NOT NULL,
  `CoP` varchar(100) NOT NULL,
  `usuario` varchar(300) NOT NULL,
  `url_m` varchar(500) NOT NULL,
  `fecha` varchar(22) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_contacto_forms`
--

INSERT INTO `goba_contacto_forms` (`ID`, `seccion`, `modulo`, `email`, `bcc`, `CoE`, `CoP`, `usuario`, `url_m`, `fecha`, `activo`) VALUES
(1, 'Contacto', 'contacto', 'multiportal@outlook.com', 'memojl08@gmail.com', 'phponix@webcindario.com', 'memojl08@gmail.com', '', 'index.php?mod=contacto', '2018-09-28 18:31:45', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_countries`
--

CREATE TABLE `goba_countries` (
  `id` int(5) NOT NULL,
  `countryCode` char(2) NOT NULL DEFAULT '',
  `countryName` varchar(45) NOT NULL DEFAULT '',
  `currencyCode` char(3) DEFAULT NULL,
  `capital` varchar(30) DEFAULT NULL,
  `continentName` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `goba_countries`
--

INSERT INTO `goba_countries` (`id`, `countryCode`, `countryName`, `currencyCode`, `capital`, `continentName`) VALUES
(1, 'AD', 'Andorra', 'EUR', 'Andorra la Vella', 'Europe'),
(2, 'AE', 'United Arab Emirates', 'AED', 'Abu Dhabi', 'Asia'),
(3, 'AF', 'Afghanistan', 'AFN', 'Kabul', 'Asia'),
(4, 'AG', 'Antigua and Barbuda', 'XCD', 'St. John\'s', 'North America'),
(5, 'AI', 'Anguilla', 'XCD', 'The Valley', 'North America'),
(6, 'AL', 'Albania', 'ALL', 'Tirana', 'Europe'),
(7, 'AM', 'Armenia', 'AMD', 'Yerevan', 'Asia'),
(8, 'AO', 'Angola', 'AOA', 'Luanda', 'Africa'),
(9, 'AQ', 'Antarctica', '', '', 'Antarctica'),
(10, 'AR', 'Argentina', 'ARS', 'Buenos Aires', 'South America'),
(11, 'AS', 'American Samoa', 'USD', 'Pago Pago', 'Oceania'),
(12, 'AT', 'Austria', 'EUR', 'Vienna', 'Europe'),
(13, 'AU', 'Australia', 'AUD', 'Canberra', 'Oceania'),
(14, 'AW', 'Aruba', 'AWG', 'Oranjestad', 'North America'),
(15, 'AX', 'Åland', 'EUR', 'Mariehamn', 'Europe'),
(16, 'AZ', 'Azerbaijan', 'AZN', 'Baku', 'Asia'),
(17, 'BA', 'Bosnia and Herzegovina', 'BAM', 'Sarajevo', 'Europe'),
(18, 'BB', 'Barbados', 'BBD', 'Bridgetown', 'North America'),
(19, 'BD', 'Bangladesh', 'BDT', 'Dhaka', 'Asia'),
(20, 'BE', 'Belgium', 'EUR', 'Brussels', 'Europe'),
(21, 'BF', 'Burkina Faso', 'XOF', 'Ouagadougou', 'Africa'),
(22, 'BG', 'Bulgaria', 'BGN', 'Sofia', 'Europe'),
(23, 'BH', 'Bahrain', 'BHD', 'Manama', 'Asia'),
(24, 'BI', 'Burundi', 'BIF', 'Bujumbura', 'Africa'),
(25, 'BJ', 'Benin', 'XOF', 'Porto-Novo', 'Africa'),
(26, 'BL', 'Saint Barthélemy', 'EUR', 'Gustavia', 'North America'),
(27, 'BM', 'Bermuda', 'BMD', 'Hamilton', 'North America'),
(28, 'BN', 'Brunei', 'BND', 'Bandar Seri Begawan', 'Asia'),
(29, 'BO', 'Bolivia', 'BOB', 'Sucre', 'South America'),
(30, 'BQ', 'Bonaire', 'USD', '', 'North America'),
(31, 'BR', 'Brazil', 'BRL', 'Brasília', 'South America'),
(32, 'BS', 'Bahamas', 'BSD', 'Nassau', 'North America'),
(33, 'BT', 'Bhutan', 'BTN', 'Thimphu', 'Asia'),
(34, 'BV', 'Bouvet Island', 'NOK', '', 'Antarctica'),
(35, 'BW', 'Botswana', 'BWP', 'Gaborone', 'Africa'),
(36, 'BY', 'Belarus', 'BYR', 'Minsk', 'Europe'),
(37, 'BZ', 'Belize', 'BZD', 'Belmopan', 'North America'),
(38, 'CA', 'Canada', 'CAD', 'Ottawa', 'North America'),
(39, 'CC', 'Cocos [Keeling] Islands', 'AUD', 'West Island', 'Asia'),
(40, 'CD', 'Democratic Republic of the Congo', 'CDF', 'Kinshasa', 'Africa'),
(41, 'CF', 'Central African Republic', 'XAF', 'Bangui', 'Africa'),
(42, 'CG', 'Republic of the Congo', 'XAF', 'Brazzaville', 'Africa'),
(43, 'CH', 'Switzerland', 'CHF', 'Berne', 'Europe'),
(44, 'CI', 'Ivory Coast', 'XOF', 'Yamoussoukro', 'Africa'),
(45, 'CK', 'Cook Islands', 'NZD', 'Avarua', 'Oceania'),
(46, 'CL', 'Chile', 'CLP', 'Santiago', 'South America'),
(47, 'CM', 'Cameroon', 'XAF', 'Yaoundé', 'Africa'),
(48, 'CN', 'China', 'CNY', 'Beijing', 'Asia'),
(49, 'CO', 'Colombia', 'COP', 'Bogotá', 'South America'),
(50, 'CR', 'Costa Rica', 'CRC', 'San José', 'North America'),
(51, 'CU', 'Cuba', 'CUP', 'Havana', 'North America'),
(52, 'CV', 'Cape Verde', 'CVE', 'Praia', 'Africa'),
(53, 'CW', 'Curacao', 'ANG', 'Willemstad', 'North America'),
(54, 'CX', 'Christmas Island', 'AUD', 'The Settlement', 'Asia'),
(55, 'CY', 'Cyprus', 'EUR', 'Nicosia', 'Europe'),
(56, 'CZ', 'Czech Republic', 'CZK', 'Prague', 'Europe'),
(57, 'DE', 'Germany', 'EUR', 'Berlin', 'Europe'),
(58, 'DJ', 'Djibouti', 'DJF', 'Djibouti', 'Africa'),
(59, 'DK', 'Denmark', 'DKK', 'Copenhagen', 'Europe'),
(60, 'DM', 'Dominica', 'XCD', 'Roseau', 'North America'),
(61, 'DO', 'Dominican Republic', 'DOP', 'Santo Domingo', 'North America'),
(62, 'DZ', 'Algeria', 'DZD', 'Algiers', 'Africa'),
(63, 'EC', 'Ecuador', 'USD', 'Quito', 'South America'),
(64, 'EE', 'Estonia', 'EUR', 'Tallinn', 'Europe'),
(65, 'EG', 'Egypt', 'EGP', 'Cairo', 'Africa'),
(66, 'EH', 'Western Sahara', 'MAD', 'El Aaiún', 'Africa'),
(67, 'ER', 'Eritrea', 'ERN', 'Asmara', 'Africa'),
(68, 'ES', 'Spain', 'EUR', 'Madrid', 'Europe'),
(69, 'ET', 'Ethiopia', 'ETB', 'Addis Ababa', 'Africa'),
(70, 'FI', 'Finland', 'EUR', 'Helsinki', 'Europe'),
(71, 'FJ', 'Fiji', 'FJD', 'Suva', 'Oceania'),
(72, 'FK', 'Falkland Islands', 'FKP', 'Stanley', 'South America'),
(73, 'FM', 'Micronesia', 'USD', 'Palikir', 'Oceania'),
(74, 'FO', 'Faroe Islands', 'DKK', 'Tórshavn', 'Europe'),
(75, 'FR', 'France', 'EUR', 'Paris', 'Europe'),
(76, 'GA', 'Gabon', 'XAF', 'Libreville', 'Africa'),
(77, 'GB', 'United Kingdom', 'GBP', 'London', 'Europe'),
(78, 'GD', 'Grenada', 'XCD', 'St. George\'s', 'North America'),
(79, 'GE', 'Georgia', 'GEL', 'Tbilisi', 'Asia'),
(80, 'GF', 'French Guiana', 'EUR', 'Cayenne', 'South America'),
(81, 'GG', 'Guernsey', 'GBP', 'St Peter Port', 'Europe'),
(82, 'GH', 'Ghana', 'GHS', 'Accra', 'Africa'),
(83, 'GI', 'Gibraltar', 'GIP', 'Gibraltar', 'Europe'),
(84, 'GL', 'Greenland', 'DKK', 'Nuuk', 'North America'),
(85, 'GM', 'Gambia', 'GMD', 'Banjul', 'Africa'),
(86, 'GN', 'Guinea', 'GNF', 'Conakry', 'Africa'),
(87, 'GP', 'Guadeloupe', 'EUR', 'Basse-Terre', 'North America'),
(88, 'GQ', 'Equatorial Guinea', 'XAF', 'Malabo', 'Africa'),
(89, 'GR', 'Greece', 'EUR', 'Athens', 'Europe'),
(90, 'GS', 'South Georgia and the South Sandwich Islands', 'GBP', 'Grytviken', 'Antarctica'),
(91, 'GT', 'Guatemala', 'GTQ', 'Guatemala City', 'North America'),
(92, 'GU', 'Guam', 'USD', 'Hagåtña', 'Oceania'),
(93, 'GW', 'Guinea-Bissau', 'XOF', 'Bissau', 'Africa'),
(94, 'GY', 'Guyana', 'GYD', 'Georgetown', 'South America'),
(95, 'HK', 'Hong Kong', 'HKD', 'Hong Kong', 'Asia'),
(96, 'HM', 'Heard Island and McDonald Islands', 'AUD', '', 'Antarctica'),
(97, 'HN', 'Honduras', 'HNL', 'Tegucigalpa', 'North America'),
(98, 'HR', 'Croatia', 'HRK', 'Zagreb', 'Europe'),
(99, 'HT', 'Haiti', 'HTG', 'Port-au-Prince', 'North America'),
(100, 'HU', 'Hungary', 'HUF', 'Budapest', 'Europe'),
(101, 'ID', 'Indonesia', 'IDR', 'Jakarta', 'Asia'),
(102, 'IE', 'Ireland', 'EUR', 'Dublin', 'Europe'),
(103, 'IL', 'Israel', 'ILS', '', 'Asia'),
(104, 'IM', 'Isle of Man', 'GBP', 'Douglas', 'Europe'),
(105, 'IN', 'India', 'INR', 'New Delhi', 'Asia'),
(106, 'IO', 'British Indian Ocean Territory', 'USD', '', 'Asia'),
(107, 'IQ', 'Iraq', 'IQD', 'Baghdad', 'Asia'),
(108, 'IR', 'Iran', 'IRR', 'Tehran', 'Asia'),
(109, 'IS', 'Iceland', 'ISK', 'Reykjavik', 'Europe'),
(110, 'IT', 'Italy', 'EUR', 'Rome', 'Europe'),
(111, 'JE', 'Jersey', 'GBP', 'Saint Helier', 'Europe'),
(112, 'JM', 'Jamaica', 'JMD', 'Kingston', 'North America'),
(113, 'JO', 'Jordan', 'JOD', 'Amman', 'Asia'),
(114, 'JP', 'Japan', 'JPY', 'Tokyo', 'Asia'),
(115, 'KE', 'Kenya', 'KES', 'Nairobi', 'Africa'),
(116, 'KG', 'Kyrgyzstan', 'KGS', 'Bishkek', 'Asia'),
(117, 'KH', 'Cambodia', 'KHR', 'Phnom Penh', 'Asia'),
(118, 'KI', 'Kiribati', 'AUD', 'Tarawa', 'Oceania'),
(119, 'KM', 'Comoros', 'KMF', 'Moroni', 'Africa'),
(120, 'KN', 'Saint Kitts and Nevis', 'XCD', 'Basseterre', 'North America'),
(121, 'KP', 'North Korea', 'KPW', 'Pyongyang', 'Asia'),
(122, 'KR', 'South Korea', 'KRW', 'Seoul', 'Asia'),
(123, 'KW', 'Kuwait', 'KWD', 'Kuwait City', 'Asia'),
(124, 'KY', 'Cayman Islands', 'KYD', 'George Town', 'North America'),
(125, 'KZ', 'Kazakhstan', 'KZT', 'Astana', 'Asia'),
(126, 'LA', 'Laos', 'LAK', 'Vientiane', 'Asia'),
(127, 'LB', 'Lebanon', 'LBP', 'Beirut', 'Asia'),
(128, 'LC', 'Saint Lucia', 'XCD', 'Castries', 'North America'),
(129, 'LI', 'Liechtenstein', 'CHF', 'Vaduz', 'Europe'),
(130, 'LK', 'Sri Lanka', 'LKR', 'Colombo', 'Asia'),
(131, 'LR', 'Liberia', 'LRD', 'Monrovia', 'Africa'),
(132, 'LS', 'Lesotho', 'LSL', 'Maseru', 'Africa'),
(133, 'LT', 'Lithuania', 'LTL', 'Vilnius', 'Europe'),
(134, 'LU', 'Luxembourg', 'EUR', 'Luxembourg', 'Europe'),
(135, 'LV', 'Latvia', 'EUR', 'Riga', 'Europe'),
(136, 'LY', 'Libya', 'LYD', 'Tripoli', 'Africa'),
(137, 'MA', 'Morocco', 'MAD', 'Rabat', 'Africa'),
(138, 'MC', 'Monaco', 'EUR', 'Monaco', 'Europe'),
(139, 'MD', 'Moldova', 'MDL', 'Chişinău', 'Europe'),
(140, 'ME', 'Montenegro', 'EUR', 'Podgorica', 'Europe'),
(141, 'MF', 'Saint Martin', 'EUR', 'Marigot', 'North America'),
(142, 'MG', 'Madagascar', 'MGA', 'Antananarivo', 'Africa'),
(143, 'MH', 'Marshall Islands', 'USD', 'Majuro', 'Oceania'),
(144, 'MK', 'Macedonia', 'MKD', 'Skopje', 'Europe'),
(145, 'ML', 'Mali', 'XOF', 'Bamako', 'Africa'),
(146, 'MM', 'Myanmar [Burma]', 'MMK', 'Nay Pyi Taw', 'Asia'),
(147, 'MN', 'Mongolia', 'MNT', 'Ulan Bator', 'Asia'),
(148, 'MO', 'Macao', 'MOP', 'Macao', 'Asia'),
(149, 'MP', 'Northern Mariana Islands', 'USD', 'Saipan', 'Oceania'),
(150, 'MQ', 'Martinique', 'EUR', 'Fort-de-France', 'North America'),
(151, 'MR', 'Mauritania', 'MRO', 'Nouakchott', 'Africa'),
(152, 'MS', 'Montserrat', 'XCD', 'Plymouth', 'North America'),
(153, 'MT', 'Malta', 'EUR', 'Valletta', 'Europe'),
(154, 'MU', 'Mauritius', 'MUR', 'Port Louis', 'Africa'),
(155, 'MV', 'Maldives', 'MVR', 'Malé', 'Asia'),
(156, 'MW', 'Malawi', 'MWK', 'Lilongwe', 'Africa'),
(157, 'MX', 'Mexico', 'MXN', 'Mexico City', 'North America'),
(158, 'MY', 'Malaysia', 'MYR', 'Kuala Lumpur', 'Asia'),
(159, 'MZ', 'Mozambique', 'MZN', 'Maputo', 'Africa'),
(160, 'NA', 'Namibia', 'NAD', 'Windhoek', 'Africa'),
(161, 'NC', 'New Caledonia', 'XPF', 'Noumea', 'Oceania'),
(162, 'NE', 'Niger', 'XOF', 'Niamey', 'Africa'),
(163, 'NF', 'Norfolk Island', 'AUD', 'Kingston', 'Oceania'),
(164, 'NG', 'Nigeria', 'NGN', 'Abuja', 'Africa'),
(165, 'NI', 'Nicaragua', 'NIO', 'Managua', 'North America'),
(166, 'NL', 'Netherlands', 'EUR', 'Amsterdam', 'Europe'),
(167, 'NO', 'Norway', 'NOK', 'Oslo', 'Europe'),
(168, 'NP', 'Nepal', 'NPR', 'Kathmandu', 'Asia'),
(169, 'NR', 'Nauru', 'AUD', '', 'Oceania'),
(170, 'NU', 'Niue', 'NZD', 'Alofi', 'Oceania'),
(171, 'NZ', 'New Zealand', 'NZD', 'Wellington', 'Oceania'),
(172, 'OM', 'Oman', 'OMR', 'Muscat', 'Asia'),
(173, 'PA', 'Panama', 'PAB', 'Panama City', 'North America'),
(174, 'PE', 'Peru', 'PEN', 'Lima', 'South America'),
(175, 'PF', 'French Polynesia', 'XPF', 'Papeete', 'Oceania'),
(176, 'PG', 'Papua New Guinea', 'PGK', 'Port Moresby', 'Oceania'),
(177, 'PH', 'Philippines', 'PHP', 'Manila', 'Asia'),
(178, 'PK', 'Pakistan', 'PKR', 'Islamabad', 'Asia'),
(179, 'PL', 'Poland', 'PLN', 'Warsaw', 'Europe'),
(180, 'PM', 'Saint Pierre and Miquelon', 'EUR', 'Saint-Pierre', 'North America'),
(181, 'PN', 'Pitcairn Islands', 'NZD', 'Adamstown', 'Oceania'),
(182, 'PR', 'Puerto Rico', 'USD', 'San Juan', 'North America'),
(183, 'PS', 'Palestine', 'ILS', '', 'Asia'),
(184, 'PT', 'Portugal', 'EUR', 'Lisbon', 'Europe'),
(185, 'PW', 'Palau', 'USD', 'Melekeok - Palau State Capital', 'Oceania'),
(186, 'PY', 'Paraguay', 'PYG', 'Asunción', 'South America'),
(187, 'QA', 'Qatar', 'QAR', 'Doha', 'Asia'),
(188, 'RE', 'Réunion', 'EUR', 'Saint-Denis', 'Africa'),
(189, 'RO', 'Romania', 'RON', 'Bucharest', 'Europe'),
(190, 'RS', 'Serbia', 'RSD', 'Belgrade', 'Europe'),
(191, 'RU', 'Russia', 'RUB', 'Moscow', 'Europe'),
(192, 'RW', 'Rwanda', 'RWF', 'Kigali', 'Africa'),
(193, 'SA', 'Saudi Arabia', 'SAR', 'Riyadh', 'Asia'),
(194, 'SB', 'Solomon Islands', 'SBD', 'Honiara', 'Oceania'),
(195, 'SC', 'Seychelles', 'SCR', 'Victoria', 'Africa'),
(196, 'SD', 'Sudan', 'SDG', 'Khartoum', 'Africa'),
(197, 'SE', 'Sweden', 'SEK', 'Stockholm', 'Europe'),
(198, 'SG', 'Singapore', 'SGD', 'Singapore', 'Asia'),
(199, 'SH', 'Saint Helena', 'SHP', 'Jamestown', 'Africa'),
(200, 'SI', 'Slovenia', 'EUR', 'Ljubljana', 'Europe'),
(201, 'SJ', 'Svalbard and Jan Mayen', 'NOK', 'Longyearbyen', 'Europe'),
(202, 'SK', 'Slovakia', 'EUR', 'Bratislava', 'Europe'),
(203, 'SL', 'Sierra Leone', 'SLL', 'Freetown', 'Africa'),
(204, 'SM', 'San Marino', 'EUR', 'San Marino', 'Europe'),
(205, 'SN', 'Senegal', 'XOF', 'Dakar', 'Africa'),
(206, 'SO', 'Somalia', 'SOS', 'Mogadishu', 'Africa'),
(207, 'SR', 'Suriname', 'SRD', 'Paramaribo', 'South America'),
(208, 'SS', 'South Sudan', 'SSP', 'Juba', 'Africa'),
(209, 'ST', 'São Tomé and Príncipe', 'STD', 'São Tomé', 'Africa'),
(210, 'SV', 'El Salvador', 'USD', 'San Salvador', 'North America'),
(211, 'SX', 'Sint Maarten', 'ANG', 'Philipsburg', 'North America'),
(212, 'SY', 'Syria', 'SYP', 'Damascus', 'Asia'),
(213, 'SZ', 'Swaziland', 'SZL', 'Mbabane', 'Africa'),
(214, 'TC', 'Turks and Caicos Islands', 'USD', 'Cockburn Town', 'North America'),
(215, 'TD', 'Chad', 'XAF', 'N\'Djamena', 'Africa'),
(216, 'TF', 'French Southern Territories', 'EUR', 'Port-aux-Français', 'Antarctica'),
(217, 'TG', 'Togo', 'XOF', 'Lomé', 'Africa'),
(218, 'TH', 'Thailand', 'THB', 'Bangkok', 'Asia'),
(219, 'TJ', 'Tajikistan', 'TJS', 'Dushanbe', 'Asia'),
(220, 'TK', 'Tokelau', 'NZD', '', 'Oceania'),
(221, 'TL', 'East Timor', 'USD', 'Dili', 'Oceania'),
(222, 'TM', 'Turkmenistan', 'TMT', 'Ashgabat', 'Asia'),
(223, 'TN', 'Tunisia', 'TND', 'Tunis', 'Africa'),
(224, 'TO', 'Tonga', 'TOP', 'Nuku\'alofa', 'Oceania'),
(225, 'TR', 'Turkey', 'TRY', 'Ankara', 'Asia'),
(226, 'TT', 'Trinidad and Tobago', 'TTD', 'Port of Spain', 'North America'),
(227, 'TV', 'Tuvalu', 'AUD', 'Funafuti', 'Oceania'),
(228, 'TW', 'Taiwan', 'TWD', 'Taipei', 'Asia'),
(229, 'TZ', 'Tanzania', 'TZS', 'Dodoma', 'Africa'),
(230, 'UA', 'Ukraine', 'UAH', 'Kyiv', 'Europe'),
(231, 'UG', 'Uganda', 'UGX', 'Kampala', 'Africa'),
(232, 'UM', 'U.S. Minor Outlying Islands', 'USD', '', 'Oceania'),
(233, 'US', 'United States', 'USD', 'Washington', 'North America'),
(234, 'UY', 'Uruguay', 'UYU', 'Montevideo', 'South America'),
(235, 'UZ', 'Uzbekistan', 'UZS', 'Tashkent', 'Asia'),
(236, 'VA', 'Vatican City', 'EUR', 'Vatican', 'Europe'),
(237, 'VC', 'Saint Vincent and the Grenadines', 'XCD', 'Kingstown', 'North America'),
(238, 'VE', 'Venezuela', 'VEF', 'Caracas', 'South America'),
(239, 'VG', 'British Virgin Islands', 'USD', 'Road Town', 'North America'),
(240, 'VI', 'U.S. Virgin Islands', 'USD', 'Charlotte Amalie', 'North America'),
(241, 'VN', 'Vietnam', 'VND', 'Hanoi', 'Asia'),
(242, 'VU', 'Vanuatu', 'VUV', 'Port Vila', 'Oceania'),
(243, 'WF', 'Wallis and Futuna', 'XPF', 'Mata-Utu', 'Oceania'),
(244, 'WS', 'Samoa', 'WST', 'Apia', 'Oceania'),
(245, 'XK', 'Kosovo', 'EUR', 'Pristina', 'Europe'),
(246, 'YE', 'Yemen', 'YER', 'Sanaa', 'Asia'),
(247, 'YT', 'Mayotte', 'EUR', 'Mamoutzou', 'Africa'),
(248, 'ZA', 'South Africa', 'ZAR', 'Pretoria', 'Africa'),
(249, 'ZM', 'Zambia', 'ZMW', 'Lusaka', 'Africa'),
(250, 'ZW', 'Zimbabwe', 'ZWL', 'Harare', 'Africa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_css`
--

CREATE TABLE `goba_css` (
  `ID` int(9) UNSIGNED NOT NULL,
  `tema` varchar(50) NOT NULL,
  `general` varchar(50) NOT NULL,
  `body` varchar(50) NOT NULL,
  `fuente` varchar(100) NOT NULL,
  `size` varchar(10) NOT NULL,
  `color` varchar(20) NOT NULL,
  `fondo` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_css2`
--

CREATE TABLE `goba_css2` (
  `ID` int(11) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `contenido` text NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_cursos`
--

CREATE TABLE `goba_cursos` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `fechas` varchar(100) NOT NULL,
  `lugar` varchar(200) NOT NULL,
  `horario` varchar(100) NOT NULL,
  `video` varchar(300) NOT NULL,
  `tag` varchar(200) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `fecha` varchar(21) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_cursos_coment`
--

CREATE TABLE `goba_cursos_coment` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ip` varchar(18) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `id_b` int(3) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_depa`
--

CREATE TABLE `goba_depa` (
  `ID` int(2) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `list_depa` varchar(100) NOT NULL,
  `puesto` varchar(100) NOT NULL,
  `nivel` int(1) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_depa`
--

INSERT INTO `goba_depa` (`ID`, `nombre`, `list_depa`, `puesto`, `nivel`, `icono`, `visible`) VALUES
(1, 'Administrador', 'Administradores', 'Administrador', 1, '', 0),
(2, 'Edecan', 'Edecanes', 'Edecan', 2, '', 1),
(3, 'Modelo', 'Modelos', 'Modelo', 2, '', 0),
(4, 'Fotografo', 'Fotografos', 'Fotografo', 2, '', 1),
(5, 'Agencia', 'Agencias', 'Agencia', 2, '', 1),
(6, 'Escuela', 'Escuelas', 'Escuela', 2, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_directorio`
--

CREATE TABLE `goba_directorio` (
  `ID` int(6) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `url_link` varchar(300) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `des` varchar(250) NOT NULL,
  `filtro` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `fecha` varchar(22) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_empresa`
--

CREATE TABLE `goba_empresa` (
  `ID` int(11) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `ima` varchar(100) NOT NULL,
  `contenido` text NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_empresa`
--

INSERT INTO `goba_empresa` (`ID`, `nom`, `ima`, `contenido`, `visible`) VALUES
(1, 'VISIÓN', 'vision.jpg', '<p>Como visi&oacute;n buscamos a mediano plazo convertimos en un semillero de grandes ingenieros, expandimos a algunas otras del pa&iacute;s y EU, as&iacute; como iniciar con el desarrollo de productos espec&iacute;ficos y convertinos en OEM en ciertos procesos en los cuales el personal clave de la compa&ntilde;ia cuenta con bastante experiencia. De la misma manera estamos trabajando en consolidar nuestra relaci&oacute;n con algunas marcas adicionales las cuales complentar&iacute;a el servicio que esta el momento hemos podido ofrecer.</p>', 1),
(2, 'MISIÓN', 'vision.jpg', '<p>2(MISI&Oacute;N)Como visi&oacute;n buscamos a mediano plazo convertimos en un semillero de grandes ingenieros, expandimos a algunas otras del pa&iacute;s y EU, as&iacute; como iniciar con el desarrollo de productos espec&iacute;ficos y convertinos en OEM en ciertos procesos en los cuales el personal clave de la compa&ntilde;ia cuenta con bastante experiencia. De la misma manera estamos trabajando en consolidar nuestra relaci&oacute;n con algunas marcas adicionales las cuales complentar&iacute;a el servicio que esta el momento hemos podido ofrecer.</p>', 1),
(3, 'VALORES', 'vision.jpg', '<p>3(VALORES)Como visi&oacute;n buscamos a mediano plazo convertimos en un semillero de grandes ingenieros, expandimos a algunas otras del pa&iacute;s y EU, as&iacute; como iniciar con el desarrollo de productos espec&iacute;ficos y convertinos en OEM en ciertos procesos en los cuales el personal clave de la compa&ntilde;ia cuenta con bastante experiencia. De la misma manera estamos trabajando en consolidar nuestra relaci&oacute;n con algunas marcas adicionales las cuales complentar&iacute;a el servicio que esta el momento hemos podido ofrecer.</p>', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_galeria`
--

CREATE TABLE `goba_galeria` (
  `ID` int(6) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `resena` varchar(500) NOT NULL,
  `url_page` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_histo_backupdb`
--

CREATE TABLE `goba_histo_backupdb` (
  `ID` int(9) UNSIGNED NOT NULL,
  `fecha` varchar(50) NOT NULL,
  `archivo` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_home_config`
--

CREATE TABLE `goba_home_config` (
  `ID` int(1) UNSIGNED NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `selc` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_home_config`
--

INSERT INTO `goba_home_config` (`ID`, `titulo`, `contenido`, `selc`, `activo`) VALUES
(1, 'phponix backup', '<!--Contenido Generado MySql-->\r\n<div id=\"content1\">\r\n<div id=\"intro\">\r\n<div class=\"clear\">&nbsp;</div>\r\n<h2>PHP Onix el mejor CMS para crear y administrar tu sitio web.</h2>\r\n<div class=\"clear\">&nbsp;</div>\r\n<p style=\"font-size: 16px;\">Con <strong>PHPOnix</strong> podras instalar y crear tu sitio web en 5 minutos ademas con herramientas para gestionar, monitoriar y posicionar tu p&aacute;gina web. <strong>PHPOnix</strong> cuenta con multiples funcionalidades desde crear un p&aacute;gina <strong>web standar</strong>, <strong>landingpage</strong>, <strong>intranet</strong>, <strong>blog</strong>, <strong>catalogo</strong>, <strong>portafolio</strong>, incluso una tienda virtual*(<strong>ecommerce</strong>) para tu negocio o servicio tu elijes la funcionalidad.</p>\r\n<div class=\"clear\">&nbsp;</div>\r\n<div class=\"col-md-4 col-sm-6 col-xs-12\" style=\"text-align: center;\">\r\n<h3>Sitio Web</h3>\r\n<img src=\"./modulos/Home/img/web.png\" alt=\"\" width=\"80%\" /></div>\r\n<div class=\"col-md-4 col-sm-6 col-xs-12\" style=\"text-align: center;\">\r\n<h3>LandingPage</h3>\r\n<img src=\"./modulos/Home/img/intuitivo.png\" alt=\"\" width=\"80%\" /></div>\r\n<div class=\"col-md-4 col-sm-6 col-xs-12\" style=\"text-align: center;\">\r\n<h3>Ecommerce</h3>\r\n<img src=\"./modulos/Home/img/ecommerce.png\" alt=\"\" width=\"80%\" /></div>\r\n</div>\r\n</div>\r\n<div id=\"content2\">\r\n<div id=\"beneficios\">\r\n<div class=\"clear\">&nbsp;</div>\r\n<h2>Beneficios</h2>\r\n<div class=\"clear\">&nbsp;</div>\r\n<p>&nbsp;</p>\r\n<div class=\"clear\">&nbsp;</div>\r\n<div class=\"col-md-4 col-sm-6 col-xs-12\" style=\"text-align: center;\">\r\n<h3>Multi-Dispositivos</h3>\r\n<img src=\"./modulos/Home/img/multidispositivo.png\" alt=\"\" width=\"80%\" /></div>\r\n<div class=\"col-md-4 col-sm-6 col-xs-12\" style=\"text-align: center;\">\r\n<h3>Reporte de Estadisticas</h3>\r\n<img src=\"./modulos/Home/img/estadisticas.png\" alt=\"\" width=\"80%\" /></div>\r\n<div class=\"col-md-4 col-sm-6 col-xs-12\" style=\"text-align: center;\">\r\n<h3>Gestion de Contenido</h3>\r\n<img src=\"./modulos/Home/img/busqueda-sistema.png\" alt=\"\" width=\"80%\" /></div>\r\n</div>\r\n</div>', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_iconos`
--

CREATE TABLE `goba_iconos` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `fa_icon` varchar(100) NOT NULL,
  `icon` text NOT NULL,
  `tipo` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_iconos`
--

INSERT INTO `goba_iconos` (`ID`, `nom`, `fa_icon`, `icon`, `tipo`) VALUES
(1, 'Descarga', 'fa-download', '<i class=\"fa fa-download\"></i>', 'awesome'),
(2, 'Menu', 'fa-list', '<i class=\"fa fa-list\"></i>', 'awesome'),
(3, 'Configuracion', 'fa-gear', '<i class=\"fa fa-gear\"></i>', 'awesome'),
(4, 'Configuraciones', 'fa-gears', '<i class=\"fa fa-gear\"></i>', 'awesome'),
(5, 'Modulos', 'fa-cubes', '<i class=\"fa fa-cubes\"></i>', 'awesome'),
(6, 'Home', 'fa-home', '<i class=\"fa fa-home\"></i>', 'awesome'),
(7, 'Portafolio', 'fa-briefcase', '<i class=\"fa fa-briefcase\"></i>', 'awesome'),
(8, 'Blog', 'fa-comments', '<i class=\"fa fa-comments\"></i>', 'awesome'),
(9, 'BlockIP', 'fa-crosshairs', '<i class=\"fa fa-crosshairs\"></i>', 'awesome'),
(10, 'Estadisticas', 'fa-bar-chart', '<i class=\"fa fa-bar-chart\"></i>', 'awesome'),
(11, 'Moneda', 'fa-usd', '<i class=\"fa fa-usd\"></i>', 'awesome'),
(12, 'Dashboard', 'fa-dashboard', '<i class=\"fa fa-dashboard\"></i>', 'awesome'),
(13, 'Usuario', 'fa-user', '<i class=\"fa fa-user\"></i>', 'awesome'),
(14, 'Usuarios', 'fa-users', '<i class=\"fa fa-users\"></i>', 'awesome'),
(15, 'Global', 'fa-globe', '<i class=\"fa fa-globe\"></i>', 'awesome'),
(16, 'Ver', 'fa-eye', '<i class=\"fa fa-eye\"></i>', 'awesome'),
(17, 'Enviar', 'fa-send-o', '<i class=\"fa fa-send-o\"></i>', 'awesome'),
(18, 'Mail', 'fa-envelope', '<i class=\"fa  fa-envelope\"></i>', 'awesome'),
(19, 'Marca de Mapa', 'fa-map-marker', '<i class=\"fa  fa-map-marker\"></i>', 'awesome'),
(20, 'Formularios', 'fa-pencil-square-o', '<i class=\"fa  fa-pencil-square-o\"></i>', 'awesome'),
(21, 'Carrito', 'fa-shopping-cart', '<i class=\"fa fa-shopping-cart\"></i>', 'awesome'),
(22, 'Folder Open Blanco', 'fa-folder-open-o', '<i class=\"fa fa-folder-open-o\"></i>', 'awesome'),
(23, 'Folder Open', 'fa-folder-open', '<i class=\"fa-folder-open\"></i>', 'awesome'),
(24, 'Tesmoniales', 'fa-commenting', '<i class=\"fa fa-commenting\"></i>', 'awesome'),
(25, 'Clientes', 'fa-child', '<i class=\"fa fa-child\"></i>', 'awesome'),
(26, 'Sitemap', 'fa-sitemap', '<i class=\"fa fa-sitemap\"></i>', 'awesome'),
(27, 'Play', 'fa fa-caret-square-o-right', '<i class=\"fa fa-caret-square-o-right\"></i>', 'awesome');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_ipbann`
--

CREATE TABLE `goba_ipbann` (
  `ID` int(11) NOT NULL,
  `ip` varchar(256) NOT NULL DEFAULT '',
  `bloqueo` tinyint(1) NOT NULL,
  `alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_ipbann`
--

INSERT INTO `goba_ipbann` (`ID`, `ip`, `bloqueo`, `alta`) VALUES
(1, '127.0.0.5', 0, '2017-10-17 11:55:47'),
(2, '174.128.181.67', 0, '2019-11-23 10:01:09'),
(3, '174.128.181.68', 0, '2019-11-23 10:01:56'),
(4, '78.0.3904.70', 0, '2019-11-23 10:02:19'),
(5, '189.166.7.220', 0, '2019-11-23 10:02:38'),
(6, '165.227.41.143', 0, '2019-11-23 10:02:54'),
(7, '159.203.34.197', 0, '2019-11-23 10:03:24'),
(8, '167.99.177.203', 0, '2019-11-24 08:55:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_landingpage_seccion`
--

CREATE TABLE `goba_landingpage_seccion` (
  `ID` int(6) UNSIGNED NOT NULL,
  `seccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tit` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `conte` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `visible` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_landingpage_seccion`
--

INSERT INTO `goba_landingpage_seccion` (`ID`, `seccion`, `tit`, `conte`, `visible`) VALUES
(1, 'Nosotros', '', '', '1'),
(2, 'Equipo', '', '', '1'),
(3, 'Servicios', '', '', '1'),
(4, 'Portafolio', '', '', '1'),
(5, 'Clientes', '', '', '1'),
(6, 'Contacto', '', '', '1'),
(7, 'Testimoniales', '', '', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_map_config`
--

CREATE TABLE `goba_map_config` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `lat` varchar(20) NOT NULL,
  `lng` varchar(20) NOT NULL,
  `zoom` varchar(2) NOT NULL,
  `cover` varchar(50) NOT NULL,
  `on_costo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_map_config`
--

INSERT INTO `goba_map_config` (`ID`, `nom`, `lat`, `lng`, `zoom`, `cover`, `on_costo`) VALUES
(1, 'Querétaro', '20.5931297', '-100.3920483', '12', 'g_intelmex.png', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_map_ubicacion`
--

CREATE TABLE `goba_map_ubicacion` (
  `ID` int(9) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `adres` varchar(150) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `info` varchar(250) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `tel` varchar(30) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `nivel` varchar(2) NOT NULL,
  `rol` varchar(2) NOT NULL,
  `lat` varchar(15) NOT NULL,
  `lng` varchar(15) NOT NULL,
  `alta` varchar(20) NOT NULL,
  `fmod` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_map_ubicacion`
--

INSERT INTO `goba_map_ubicacion` (`ID`, `nom`, `adres`, `descripcion`, `info`, `precio`, `tel`, `cover`, `nivel`, `rol`, `lat`, `lng`, `alta`, `fmod`, `visible`, `activo`) VALUES
(1, 'Intelmex', 'Calle 1 303, Jurica, 76130 Santiago de Querétaro, Qro.', '', 'Reparación de telefonos', '0.00', '4421234567', 'nodisponible.jpg', '1', '3', '20.6500317', '-100.4290312', '2018-04-03 13:44:50', '2018-04-03 13:59:06', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_menu_admin`
--

CREATE TABLE `goba_menu_admin` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom_menu` varchar(50) NOT NULL,
  `icono` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `nivel` int(1) NOT NULL,
  `ID_menu_adm` int(2) NOT NULL,
  `ID_mod` int(2) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_menu_admin`
--

INSERT INTO `goba_menu_admin` (`ID`, `nom_menu`, `icono`, `link`, `nivel`, `ID_menu_adm`, `ID_mod`, `visible`) VALUES
(1, 'Config. Sistema', 'fa-gear', 'index.php?mod=sys&ext=admin/index', -1, 0, 9, 1),
(2, 'Modulos', 'fa-cubes', 'index.php?mod=sys&ext=modulos', -1, 0, 0, 1),
(3, 'Logs', 'fa-globe', 'index.php?mod=sys&ext=admin/index&opc=logs', -1, 0, 9, 1),
(4, 'Bloquear IP', 'fa-crosshairs', 'index.php?mod=sys&ext=admin/index&opc=bloquear', -1, 0, 9, 1),
(5, 'Temas', 'fa-sticky-note-o', 'index.php?mod=sys&ext=admin/index&opc=temas', -1, 0, 9, 1),
(6, 'Admin. Usuarios', 'fa-users', 'index.php?mod=usuarios&ext=admin/index', -1, 0, 4, 0),
(7, 'Menu Admin', 'fa-list', 'index.php?mod=sys&ext=menu_admin', -1, 0, 9, 1),
(8, 'Iconos', 'fa-smile-o', 'index.php?mod=sys&ext=admin/index&opc=iconos', -1, 0, 9, 1),
(9, 'Informe de Visitas', 'fa-download', 'index.php?mod=estadisticas&ext=admin/index', 1, 0, 10, 1),
(10, 'Backup DB', 'fa-download', 'index.php?mod=sys&ext=backup', -1, 0, 9, 1),
(11, 'Config. Mailbox', 'fa-gear', 'index.php?mod=mailbox&ext=admin/index', 1, 0, 12, 1),
(12, 'Mensajes', 'fa-envelope', 'index.php?mod=mailbox', 1, 0, 12, 1),
(13, 'Editar', 'fa-home', 'index.php?mod=Home&ext=admin/index', 1, 0, 3, 1),
(14, 'Menu Web', 'fa-list', 'index.php?mod=Home&ext=admin/index&opc=menu_web', 1, 0, 3, 1),
(15, 'Admin productos', 'fa-shopping-cart', 'index.php?mod=productos&ext=admin/index&opc=producto', 1, 0, 15, 1),
(16, 'Categoria de productos', 'fa-folder-open', 'index.php?mod=productos&ext=admin/index&opc=categoria', 1, 0, 15, 1),
(17, 'Subcategoria de productos', 'fa-folder-open-o', 'index.php?mod=productos&ext=admin/index&opc=subcategoria', 1, 0, 15, 1),
(18, 'Config. Contacto', 'fa-gear', 'index.php?mod=contacto&ext=admin/index', 1, 0, 8, 1),
(19, 'Correos de Formulario', 'fa-pencil-square-o', 'index.php?mod=contacto&ext=admin/index&opc=forms', 1, 0, 8, 1),
(20, 'Subcategoria2 De Productos', 'fa-folder-open-o', 'index.php?mod=productos&ext=admin/index&opc=subcategoria2', 1, 0, 15, 1),
(21, 'Generador Sitemap', 'fa-sitemap', 'index.php?mod=sys&ext=admin/index&opc=sitemap', 1, 0, 9, 1),
(22, 'Opciones', 'fa-gears', 'index.php?mod=sys&ext=admin/index&ext=opciones', 1, 0, 9, 1),
(23, 'Licencia', 'fa-eye', 'index.php?mod=sys&ext=licencia', 1, 0, 9, 1),
(24, 'Slider', 'fa fa-caret-square-o-right', 'index.php?mod=Home&ext=admin/index&opc=slider', 1, 0, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_menu_web`
--

CREATE TABLE `goba_menu_web` (
  `ID` int(6) UNSIGNED NOT NULL,
  `menu` varchar(50) NOT NULL,
  `url` varchar(254) NOT NULL,
  `modulo` varchar(100) NOT NULL,
  `ext` varchar(50) NOT NULL,
  `ord` varchar(2) NOT NULL,
  `subm` varchar(3) NOT NULL,
  `ima_top` varchar(100) NOT NULL,
  `tit_sec` varchar(100) NOT NULL,
  `des_sec` text NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_menu_web`
--

INSERT INTO `goba_menu_web` (`ID`, `menu`, `url`, `modulo`, `ext`, `ord`, `subm`, `ima_top`, `tit_sec`, `des_sec`, `visible`) VALUES
(1, 'HOME', 'index.php', 'Home', '', '1', '', '', '', '', 1),
(2, 'PRODUCTOS', 'productos/', 'productos', '', '2', '', '', '', '', 1),
(3, 'NOSOTROS', 'nosotros/', 'nosotros', '', '3', '', '', '', '', 0),
(4, 'BLOG', 'blog/', 'blog', '', '4', '', '', '', '', 0),
(5, 'CONTACTO', 'contacto/', 'contacto', '', '5', '', '', '', '', 1),
(6, 'COLCHONES', '#', '', '', '1', '2', '', '', '', 1),
(7, 'CAMAS', '#', '', '', '2', '2', '', '', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_mode_page`
--

CREATE TABLE `goba_mode_page` (
  `ID` int(2) UNSIGNED NOT NULL,
  `page_mode` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_mode_page`
--

INSERT INTO `goba_mode_page` (`ID`, `page_mode`) VALUES
(1, 'page'),
(2, 'landingpage'),
(3, 'extranet'),
(4, 'ecommerce'),
(5, 'CRM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_modulos`
--

CREATE TABLE `goba_modulos` (
  `ID` int(6) NOT NULL,
  `nombre` varchar(25) NOT NULL DEFAULT '',
  `modulo` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL,
  `dashboard` tinyint(1) NOT NULL,
  `nivel` tinyint(4) NOT NULL DEFAULT '0',
  `home` tinyint(4) NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '0',
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  `sname` varchar(10) NOT NULL DEFAULT '',
  `icono` varchar(50) NOT NULL,
  `link` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_modulos`
--

INSERT INTO `goba_modulos` (`ID`, `nombre`, `modulo`, `description`, `dashboard`, `nivel`, `home`, `visible`, `activo`, `sname`, `icono`, `link`) VALUES
(1, 'admin', 'admin', '', 0, 0, 0, 0, 0, 'false', '', ''),
(2, 'Dashboard', 'dashboard', '', 1, -1, 0, 0, 1, 'false', '', 'index.php?mod=dashboard'),
(3, 'Home', 'Home', 'Administración y gestión del Home.', 0, 0, 1, 1, 1, 'false', 'fa-home', 'index.php?mod=Home'),
(4, 'Usuarios', 'usuarios', 'Administación y gestión de usuarios.', 0, -1, 0, 1, 1, 'false', 'fa-users', 'index.php?mod=usuarios'),
(5, 'Nosotros', 'nosotros', 'Administración del contenido del modulo de nosotros.', 0, 0, 0, 1, 1, 'false', 'fa-users', 'index.php?mod=nosotros'),
(6, 'Portafolio', 'portafolio', 'Administración y gestión del portafolio.', 0, 0, 0, 0, 1, 'false', 'fa-briefcase', 'index.php?mod=portafolio'),
(7, 'Blog', 'blog', 'Administración del contenido del modulo de blog.', 0, 0, 0, 1, 1, 'false', 'fa-comments', 'index.php?mod=blog'),
(8, 'Contacto', 'contacto', 'Consultas del modulo de contacto.', 0, 0, 0, 1, 1, 'false', 'fa-map-marker', 'index.php?mod=contacto'),
(9, 'Sistema', 'sys', 'Configuración y administración del sistema.', 1, -1, 0, 1, 1, 'false', 'fa-gear', 'index.php?mod=sys'),
(10, 'Estadistica', 'estadisticas', 'Estadisticas de trafico. ', 0, -1, 0, 1, 1, 'false', 'fa-bar-chart', 'index.php?mod=estadisticas'),
(11, 'Formularios', 'forms', 'Administracion de Formularios para la web.', 1, -1, 0, 0, 0, 'false', '', ''),
(12, 'Mailbox', 'mailbox', 'Mailbox de formularios', 1, 1, 0, 1, 1, 'false', ' fa-envelope', 'index.php?mod=mailbox'),
(13, 'Ecommerce', 'ecommerce', 'Administración y gestión del modulo ecommerce.', 0, 0, 0, 0, 0, 'false', 'fa-shopping-cart', 'index.php?mod=ecommerce'),
(14, 'Marketing', 'marketing', '', 0, 1, 0, 0, 0, 'false', 'fa-globe', 'index.php?mod=marketing'),
(15, 'Productos', 'productos', 'Administración de productos', 0, 1, 0, 1, 1, 'false', 'fa-shopping-cart', 'index.php?mod=productos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_noticias`
--

CREATE TABLE `goba_noticias` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `contenido` text NOT NULL,
  `tag` varchar(200) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `fecha` varchar(21) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_noticias`
--

INSERT INTO `goba_noticias` (`ID`, `cover`, `titulo`, `descripcion`, `contenido`, `tag`, `autor`, `fmod`, `fecha`, `visible`) VALUES
(1, 'automatizacion.jpg', 'Tendencias en Automatización para el 2019', 'Comenzamos un nuevo año que continuará dándonos importantes avances en la digitalización de la industria. 2019 apunta a que sera un año importante en muchos aspectos pero quizás, sea el avance hacia l', '<div class=\"itemFullText\">\r\n<p>Comenzamos un nuevo a&ntilde;o que continuar&aacute; d&aacute;ndonos importantes avances en la digitalizaci&oacute;n de la industria.&nbsp; 2019 apunta a que sera un a&ntilde;o importante en muchos aspectos pero quiz&aacute;s, sea&nbsp;el avance hacia la posible estandarizaci&oacute;n de las comunicaciones industriales gracias a TSN uno de los aspectos m&aacute;s importantes.</p>\r\n<p>Aqu&iacute; os mostramos 8&nbsp;tendencias pueden marcar el futuro de la automatizaci&oacute;n industrial en 2019</p>\r\n<p>&nbsp;</p>\r\n<h2>1 #&nbsp;Inteligencia Artificial (IA)</h2>\r\n<p><img src=\"/files/imagenes/noticias/2019/106084_01.jpg\" alt=\"\" /></p>\r\n<p>La Inteligencia Articial (IA)&nbsp; siempre a estado asociada a pel&iacute;culas futuristas pero parece que el futuro ya est&aacute; aqu&iacute;. Este a&ntilde;o, la feria Hannover Messe 2018 puso su acento en la IA,&nbsp;\"La <strong>inteligencia artificial</strong>&nbsp;tiene el potencial de <strong>revolucionar las industrias de producci&oacute;n</strong>&nbsp;y energ&iacute;a\", dec&iacute;a el Dr. Jochen K&ouml;ckler, presidente de la Junta de Deutsche Messe.</p>\r\n<p>Durante 2018 varias empresas del mundo de la automatizaci&oacute;n han empezado a presentar soluciones basadas en IA:</p>\r\n<p><strong>Omron</strong>&nbsp;present&oacute; lo que afirma ser el <a href=\"/noticias/item/104315-omron-inteligencia-artificial-machine-automation-controller\" rel=\"noopener noreferrer\" target=\"_blank\">primer&nbsp;controlador de m&aacute;quina equipado con un algoritmo de inteligencia artificial (IA)</a>&nbsp;de aprendizaje autom&aacute;tico (machine-learning).&nbsp;<strong>ABB e IBM</strong>&nbsp;anunciaron su <a href=\"/plus-plus/empresas/item/104268-abb-ibm-asocian-soluciones-inteligencia-artificial\" rel=\"noopener noreferrer\" target=\"_blank\">asociaci&oacute;n en soluciones de inteligencia artificial</a>. <strong>Rockwell Automation</strong> present&oacute; el <a href=\"/noticias/item/105086-rockwell-inteligencia-artificial-ia-project-sherlock\" rel=\"noopener noreferrer\" target=\"_blank\">m&oacute;dulo de Inteligencia Artificial (IA) Project Sherlock</a>&nbsp;que utiliza el aprendizaje autom&aacute;tico para crear anal&iacute;ticas poderosas desde la infraestructura de automatizaci&oacute;n. <strong>Siemens</strong> tambi&eacute;n a incorporado Inteligencia Artificial para los controladores Simatic&nbsp;con la <a href=\"/noticias/item/106008-inteligencia-artificial-automatas-simatic-siemens\" rel=\"noopener noreferrer\" target=\"_blank\">nueva S7-1500 TM NPU</a> (unidad de procesamiento neural).</p>\r\n<h2>&nbsp;</h2>\r\n<h2>2 #&nbsp;Plataformas para OEMs</h2>\r\n<p><img src=\"/files/imagenes/noticias/2019/106084_02.jpg\" alt=\"\" /></p>\r\n<p>Las plataformas de servicios Cloud &amp; Analytics para fabricantes de maquinaria (OEMs) es un sector que empieza a tomar fuerza. En infoPLC hemos abierto un nuevo canal dedicado&nbsp;a contaros las novedades referentes a los&nbsp;<a href=\"/plataformas-servicios-cloud-oem\" rel=\"noopener noreferrer\" target=\"_blank\">servicios de plataformas</a>.</p>\r\n<p>Estas plataformas permite implementar de una manera real la industria 4.0 y sus beneficios a los fabricantes de maquinaria ya que les&nbsp;ofrece la posibilidad monitorizar y analizar su flota de m&aacute;quinas instaladas sin tener que invertir en elevados costos de implementaci&oacute;n.&nbsp;</p>\r\n<p>Las plataformas permiten conectar las m&aacute;quinas a las Nube y recopilar datos de una manera muy sencilla. EL fabricante de maquinaria puede implementar mediante Dashboards interfaces que permiten la monitorizaci&oacute;n y an&aacute;lisis de los datos pudiendo ofrecer nuevos servicios a sus clientes como por ejemplo mantenimiento predictivo.&nbsp;</p>\r\n<p>Empresas del secrtor de la automatizaci&oacute;n han creado estos servicios. <strong>Schneider Electric</strong> present&oacute; recientemente <a href=\"/noticias/item/106026-machine-advisor-datos-fabricantes-maquinaria\" rel=\"noopener noreferrer\" target=\"_blank\">Machine Advisor</a>, <strong>Rockwell</strong> ofrece la soluci&oacute;n <a href=\"/noticias/item/105248-rockwell-factorytalk-analytics-rendimiento-maquina\" rel=\"noopener noreferrer\" target=\"_blank\">FactoryTalk Analytics for Machines cloud para OEMs</a>&nbsp;y<strong> KUKA </strong>ofrece <a href=\"/noticias/item/103816-plataforma-cloud-kuka-connect\" rel=\"noopener noreferrer\" target=\"_blank\">KUKA Connect</a>, una completa&nbsp;plataforma de monitorizaci&oacute;n y an&aacute;lisis para sus robots. la compra de <strong>B&amp;R</strong> por parte de <strong>ABB</strong> empieza a dar sus frutos, una de las pimeras soluciones realizadas en conjunto es <a href=\"/noticias/item/106095-asset-performance-monitor\" rel=\"noopener noreferrer\" target=\"_blank\">Asset Performance Monitor</a>.</p>\r\n<p>Pero no solo empresas del sector cl&aacute;sicas ofrecen estos servicios, nuevas empresas esta&aacute;n aprovechando el auge de la Industria 4.0 para ofrecer nuevas soluciones, por ejemplo <strong>IXON</strong> una plataforma que ofrece conexi&oacute;n remota a m&aacute;quina VPN y <a href=\"https://www.ixon.cloud/\" rel=\"noopener noreferrer\" target=\"_blank\">servicios de&nbsp;Cloud Logging y&nbsp;Cloud Notify</a>.&nbsp;<strong>MachineMetrics</strong> es una <a href=\"https://www.machinemetrics.com\" rel=\"noopener noreferrer\" target=\"_blank\">plataforma de an&aacute;lisis de fabricaci&oacute;n</a> que aumenta la productividad a trav&eacute;s de la visibilidad en tiempo real, el an&aacute;lisis profundo y las notificaciones predictivas. Seguro que nuevas empresas ajenas actualmente al sector industrial empiezan a ofrecer soluciones y plataformas enfocadas a los OEMS y clientes finales con el fin de sacar partido a los datos de sus m&aacute;quinas.</p>\r\n<h2>&nbsp;</h2>\r\n<h2>3 #&nbsp;Robotica&nbsp;sigue creciendo</h2>\r\n<p><img src=\"/files/imagenes/noticias/2019/106084_03.jpg\" alt=\"\" /></p>\r\n<p>El&nbsp;desarrollo constante de las tecnolog&iacute;as rob&oacute;ticas sin duda ha ampliado las aplicaciones potenciales para robots industriales inteligentes. De este modo, hoy en d&iacute;a, los robots impulsados por software de vanguardia y sistemas de visi&oacute;n pueden programarse para realizar una serie de tareas, que se ajustan perfectamente a la demanda de fabricaci&oacute;n flexible.</p>\r\n<p>Durante 2018 hemos visto importantes movimientos en el sector, como uno de los pioneros de la rob&oacute;tica colaborativa <a href=\"/noticias/item/105831-rethink-robotics-cierra-empresa\" rel=\"noopener noreferrer\" target=\"_blank\">Rethink Robotics fue a la quiebra</a>, Omron continua su apuesta por la rob&oacute;tica, tras la compra de Adept se ha aliado con <a href=\"/noticias/item/106033-omron-robots-colaborativos-serie-tm-cobot\" rel=\"noopener noreferrer\" target=\"_blank\">TM para ofrecer rob&oacute;tica colaborativa</a>. A nivel nacional destaca la llegada de los robots chinos <a href=\"/plus-plus/entrevistas/item/105866-estun-robotica-al-alcance-de-todos\" rel=\"noopener noreferrer\" target=\"_blank\"><strong>ESTUN</strong> a Espa&ntilde;a</a> de la mano de Mec&aacute;nico Moderna y el anuncio de <strong>FANUC</strong> de su <a href=\"/plus-plus/empresas/item/105845-fanuc-nueva-sede-sant-cugat-barcelona\" rel=\"noopener noreferrer\" target=\"_blank\">nueva sede en&nbsp;Sant Cugat</a>.</p>\r\n<p>En 2019 seguir&aacute; creciendo las soluciones OPEN que permite controlar la mec&aacute;nica de los robots utilizando controladores independientes al robot. Ademas&nbsp;<strong>Microsoft</strong> ha anunciado un lanzamiento experimental del sistema operativo de c&oacute;digo abierto Robot (ROS) para Windows y&nbsp;<strong>Google</strong> planea lanzar una plataforma rob&oacute;tica en la nube</p>\r\n<p>Aqu&iacute; os indicamos <a href=\"/noticias/item/106085-6-tendencias-futuras-robotica-industrial\" rel=\"noopener noreferrer\" target=\"_blank\">6 tendencias futuras en la rob&oacute;tica industrial</a>.</p>\r\n<h2>&nbsp;</h2>\r\n<h2>4 #&nbsp;TSN (Time-Sensitive Networking)</h2>\r\n<p><img src=\"/files/imagenes/noticias/2019/106084_04.jpg\" alt=\"\" /></p>\r\n<p>Lo pudimos comprobar en la ultima edici&oacute;n de la feria SPS IPC Drives 2018, <strong>TSN ha sido sin duda uno de los grandes protagonistas, una red abierta unificadora</strong>. Parece que por primera vez en la historia de la automatizaci&oacute;n puede verse un horizonte de estandarizaci&oacute;n en los protocolos de comunicaci&oacute;n.</p>\r\n<p>Existe una gran cantidad de discusiones&nbsp;en las que se creen que TSN tiene el potencial de ser una &uacute;nica red determinista unificadora compartida por todas las aplicaciones en la industria .</p>\r\n<p>Como TSN es una arquitectura de red compartida totalmente administrada, todo el tr&aacute;fico de red, incluidos todos los protocolos industriales&nbsp;de la planta, deber&aacute;n cumplir con el conjunto de est&aacute;ndares TSN para lograr comunicaciones deterministas y confiables.</p>\r\n<p>Este a&ntilde;o hemos conocido la noticia de que<a href=\"/noticias/item/105966-opc-foundation-extiende-opc-ua-incluyendo-tsn-nivel-campo\" rel=\"noopener noreferrer\" target=\"_blank\"> OPC Foundation extiende OPC UA incluyendo TSN</a> hasta el nivel de campo, esta iniciativoa ha sido secundada por&nbsp;los <a href=\"/plus-plus/tecnologia/item/106018-industria-automatizacion-une-opc-ua-incluido-tsn\" rel=\"noopener noreferrer\" target=\"_blank\">principales Players de la industria de la automatizaci&oacute;n se unen a OPC UA con TSN</a>.</p>\r\n<h2>&nbsp;</h2>\r\n<h2>5 #&nbsp;Gemelo Digital</h2>\r\n<p><img src=\"/files/imagenes/noticias/2019/106084_05.jpg\" alt=\"\" /></p>\r\n<p>El concepto de \"gemelos digitales\" permite la <strong>creaci&oacute;n de una copia virtual de una m&aacute;quina o sistema</strong>. Esto se est&aacute; convirtiendo en un requisito previo en el panorama del desarrollo de productos. Adem&aacute;s, la digitalizaci&oacute;n de plantas y maquinaria garantiza una puesta en servicio eficiente, un dise&ntilde;o optimizado de la m&aacute;quina, operaciones sin problemas y un corto tiempo de cambio. Este proceso reduce la dependencia de prototipos costosos a la vez que acelera el tiempo de comercializaci&oacute;n.</p>\r\n<p>Adem&aacute;s, los gemelos digitales ahora est&aacute;n activos en los pisos de f&aacute;brica, analizando las eficiencias de producci&oacute;n e impulsando el mantenimiento predictivo. En el futuro, los fabricantes conocer&aacute;n todos los componentes instalados en sus productos. De este modo, les permite brindar una respuesta espec&iacute;fica a los problemas y optimizar los procesos.</p>\r\n<p>EMpresas del sector empiezan a ofrecer soluciones enfocadas a al \"Digital Twin\"&nbsp;<strong>Lenze</strong> usa cada vez m&aacute;s la realidad virtual y el <a href=\"/noticias/item/105864-lenze-presenta-ingenieria-gemelo-virtual\" rel=\"noopener noreferrer\" target=\"_blank\">gemelo digital</a> como una herramienta efectiva de ingenier&iacute;a y formaci&oacute;n. <a href=\"/noticias/item/105391-tia-portal-v15-1\" rel=\"noopener noreferrer\" target=\"_blank\">TIA Portal V15.1</a>&nbsp;de <strong>Siemens</strong> se centra en nuevas&nbsp;opciones de simulaci&oacute;n y puesta en marcha virtual ofreciendo un&nbsp;gemelo digital del controlador Simatic S7-1500 as&iacute; como la reciente <a href=\"/plus-plus/empresas/item/106065-siemens-aker-solutions-alianza\" rel=\"noopener noreferrer\" target=\"_blank\">alianza con Aker Solutions</a>&nbsp;que tiene el digital twin en el centro</p>\r\n<p>&nbsp;</p>\r\n<h2>6 #&nbsp;Realidad Virtual</h2>\r\n<p><img src=\"/files/imagenes/noticias/2019/106084_06.jpg\" alt=\"\" width=\"651\" height=\"360\" /></p>\r\n<p>Hoy en d&iacute;a, la realidad aumentada (AR) y la realidad virtual (VR) se utilizan en varios contextos, desde las aplicaciones del consumidor hasta la fabricaci&oacute;n. Sin embargo, es en este &uacute;ltimo que AR ofrece un inmenso valor en innumerables formas, en combinaci&oacute;n con otras tecnolog&iacute;as. De hecho, las tecnolog&iacute;as VR y AR est&aacute;n revolucionando los procesos de producci&oacute;n complejos y los desarrollos de productos.</p>\r\n<p>En el contexto de la automatizaci&oacute;n industrial y de fabricaci&oacute;n, la VR puede ayudar a los fabricantes a simular un producto o entorno digitalmente. De este modo, permiti&eacute;ndoles interactuar y sumergirse en &eacute;l. AR ayuda a los usuarios industriales a proyectar productos digitales o informaci&oacute;n en un entorno del mundo real. Esto es m&aacute;s productivo que proyectar en un entorno simulado digitalmente como en la realidad virtual.</p>\r\n<h2>&nbsp;</h2>\r\n<h2>7 #&nbsp;Ciberseguridad</h2>\r\n<p><img src=\"/files/imagenes/noticias/2019/106084_07.jpg\" alt=\"\" /></p>\r\n<p>A nuestro entender es el gran reto para el despliegue de la Industria 4.0.&nbsp;Ya existen est&aacute;ndares para la ciberseguridad industrial y en la mayor&iacute;a de las industrias, estos son voluntarios. Dicho esto, existe una tendencia mundial para la regulaci&oacute;n gubernamental. Ya hemos visto esto en las regulaciones de los sistemas de seguridad industrial. Desafortunadamente, en lugar de un esfuerzo estandarizado, existen m&uacute;ltiples iniciativas en el mundo con diferentes objetivos y, en &uacute;ltima instancia, diferentes est&aacute;ndares.</p>\r\n<p>El <a href=\"/plus-plus/tecnologia/item/105937-nuevo-estandar-isa-reducir-vulnerabilidades\" rel=\"noopener noreferrer\" target=\"_blank\">nuevo&nbsp;est&aacute;ndar ISA/IEC 62443-4-2-2018</a>&nbsp;de la serie SA/IEC 62443&nbsp;persigue blindar los procesos de adquisici&oacute;n e integraci&oacute;n de ordenadores, aplicaciones, equipos de red y dispositivos de control que constituyen un sistema de control.</p>\r\n<p>Las soluciones avanzadas de ciberseguridad industrial disponibles en la actualidad tienen un enfoque h&iacute;brido muy efectivo. Adem&aacute;s, esto incluye tanto la detecci&oacute;n de anomal&iacute;as basada en el comportamiento que ayuda a identificar posibles amenazas cibern&eacute;ticas que utilizan enfoques de ciberseguridad convencionales, como el an&aacute;lisis basado en reglas que permite a los fabricantes aprovechar una inspecci&oacute;n profunda para descubrir ciberataques de malware en la red.</p>\r\n<p>Pero como hemos visto en numerosas ocasiones \"los malos siempre van por delante\" y la industria deber de estar preparada e invertir en Ciberseguridad. Las empresas industriales este a&ntilde;o han seguido dando muestras de tomarse en serio la Ciberseguridad,&nbsp;<strong>Moxa y Trend Micro</strong> crean una joint venture,&nbsp; <a href=\"/plus-plus/empresas/item/105972-moxa-y-trend-micro-crean-una-joint-venture\" rel=\"noopener noreferrer\" target=\"_blank\">TXOne Networks</a> se centrar&aacute; en la construcci&oacute;n de pasarelas de seguridad.&nbsp;<strong>ForeScout y Belden</strong> se unen para <a href=\"/plus-plus/empresas/item/105951-forescout-belden-alianza-entornos-industriales\" rel=\"noopener noreferrer\" target=\"_blank\">proteger los entornos industriales</a>.&nbsp;<strong>Honeywell</strong> present&oacute; nuevos servicios de Ciberseguridad Industrial.</p>\r\n<p>Puedes estar al d&iacute;a de todas las noticias sobre <a href=\"/ciberseguridad-industrial\" rel=\"noopener noreferrer\" target=\"_blank\">Ciberseguridad Industrial en nuestro portal</a></p>\r\n<h2>&nbsp;</h2>\r\n<h2>8 #&nbsp;Open Source Code</h2>\r\n<p><img src=\"/files/imagenes/noticias/2019/106084_08.jpg\" alt=\"\" /></p>\r\n<p>Los ecosistemas abiertos permiten la interoperabilidad de m&uacute;ltiples proveedores en todos los niveles de la arquitectura del sistema, al tiempo que simplifican la integraci&oacute;n. Algunos ejemplos de <strong>Open Source en la Automatizaci&oacute;n Industrial</strong>.<br /><br />OPC UA es una plataforma de c&oacute;digo abierto, una arquitectura independiente orientada a servicios. <strong>OPC UA</strong> integra toda la funcionalidad de las especificaciones de OPC Classic y un n&uacute;mero creciente de otros modelos de datos de c&oacute;digo abierto, como MTConnect y FDT, en un marco extensible.</p>\r\n<p>El software de c&oacute;digo abierto <strong>Node-RED</strong> es un entorno de programaci&oacute;n para crear y ejecutar aplicaciones visualmente, esta tecnolog&iacute;a est&aacute; siendo utilizada por una cantidad de proveedores. Estamos viendo como mucho IoT Box o IoT Gateway lo est&aacute;n utilizando&nbsp;para subir datos de m&aacute;quinas a la nube.</p>\r\n<p>Algunos proveedores incluyen nombres como, por ejemplo, Opto 22, Hilscher, Harting, NEXCOM, Siemens,&nbsp;veremos como&nbsp;<strong>Node-RED cada vez est&aacute; m&aacute;s integrado en la automatizaci&oacute;n industrial.</strong></p>\r\n</div>', 'automatizacion, tendencias', 'admin', '2019-08-22 20:57:28', '2017-01-18 14:05:23', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_noticias_coment`
--

CREATE TABLE `goba_noticias_coment` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ip` varchar(18) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `id_b` int(3) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_noticias_coment`
--

INSERT INTO `goba_noticias_coment` (`ID`, `ip`, `nombre`, `email`, `comentario`, `id_b`, `fecha`, `visible`) VALUES
(1, '127.0.0.1', 'Guillermo Jim&eacute;nez L&oacute;pez', 'mherco@hotmail.com', 'Mensaje de prueba para noticias', 1, '2018-12-21 22:14:55', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_notificacion`
--

CREATE TABLE `goba_notificacion` (
  `ID` int(11) UNSIGNED NOT NULL,
  `ID_user` int(11) NOT NULL,
  `ID_user2` int(11) NOT NULL,
  `nombre_envio` varchar(255) NOT NULL,
  `mensaje` text NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `fecha` varchar(22) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_notificacion`
--

INSERT INTO `goba_notificacion` (`ID`, `ID_user`, `ID_user2`, `nombre_envio`, `mensaje`, `visto`, `activo`, `fecha`) VALUES
(1, 1, 1, 'admin', 'Su sistema PHPONIX ha sido actualizado.', 1, 1, '2019-06-27 23:09:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_opciones`
--

CREATE TABLE `goba_opciones` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `valor` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_opciones`
--

INSERT INTO `goba_opciones` (`ID`, `nom`, `descripcion`, `valor`) VALUES
(1, 'google_analytics', '', '0'),
(2, 'form_registro', '', '0'),
(3, 'geo_loc_visitas', '', '0'),
(4, 'slide_active', '', '1'),
(5, 'API_facebook', '', '0'),
(6, 'API_google_maps', '', '0'),
(7, 'api_noti_chrome', '', '0'),
(8, 'link_var', '', '0'),
(9, 'link_productos', '', '0'),
(10, 'tiny_text_des', '', '0'),
(11, 'email_test', '', '0'),
(12, 'skin_AdminLTE', '', 'blue'),
(13, 'mini_bar_AdminLTE', '', '0'),
(14, 'wordpress', '', '0'),
(15, 'bar_login', '', '0'),
(16, 'bar_productos', '', '0'),
(17, 'toogle_nombre', '', '0'),
(18, 'mostrar_precio', '', '0'),
(19, 'mostrar_nombre', '', '0'),
(20, 'mostrar_des_corta', '', '0'),
(21, 'mostrar_des', '', '0'),
(22, 'mostrar_galeria', '', '0'),
(23, 'b_vista_rapida', '', '0'),
(24, 'b_ver_pro', '', '0'),
(25, 'b_cotizar', '', '0'),
(26, 'b_cart', '', '0'),
(27, 'b_paypal', '', '0'),
(28, 'blog_coment', '', '1'),
(29, 'noticias_coment', '', '0'),
(30, 'cursos_coment', '', '0'),
(31, 'productos_coment', '', '0'),
(32, 'all_productos', '', '0'),
(33, 'e_rates', '', '0'),
(34, 'footer_dir', '', '0'),
(35, 'validacion_json', '', '0'),
(36, 'url_var_json', '', '0'),
(37, 'VUE2', '', '0'),
(38, 'api_social_chat', 'Chat de redes sociales', '0'),
(39, 'AJAX', '', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_pages`
--

CREATE TABLE `goba_pages` (
  `ID` int(6) UNSIGNED NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `contenido` text NOT NULL,
  `modulo` varchar(50) NOT NULL,
  `ext` varchar(50) NOT NULL,
  `url` varchar(250) NOT NULL,
  `fmod` varchar(20) NOT NULL,
  `alta` varchar(20) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_pages`
--

INSERT INTO `goba_pages` (`ID`, `titulo`, `contenido`, `modulo`, `ext`, `url`, `fmod`, `alta`, `visible`, `activo`) VALUES
(1, 'Nosotros 1', '<p style=\"text-align: center;\"><span style=\"font-size: 16px;\"><strong><br /></strong></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 16px;\"><strong>Nosotros</strong></span></p>', 'nosotros', '', '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_portafolio`
--

CREATE TABLE `goba_portafolio` (
  `ID` int(6) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `resena` varchar(500) NOT NULL,
  `url_page` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_portafolio`
--

INSERT INTO `goba_portafolio` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `precio`, `cate`, `resena`, `url_page`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `visible`, `alta`, `fmod`, `user`) VALUES
(1, '', 'Betrec', '1-compressor.jpg', '', 'Descripcion', '0.00', 'Categoria', 'RESEÑA', '', 'be1.jpg', '', '', '', '', 1, '2018-01-07 21:10:52', '', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_productos`
--

CREATE TABLE `goba_productos` (
  `ID` int(9) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `marca` varchar(150) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `moneda` varchar(10) NOT NULL,
  `unidad` varchar(10) NOT NULL,
  `stock` int(6) NOT NULL,
  `ID_cate` int(6) NOT NULL,
  `ID_sub_cate` int(6) NOT NULL,
  `ID_sub_cate2` int(6) NOT NULL,
  `ID_marca` int(6) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `url_name` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `resena` text NOT NULL,
  `land` tinyint(1) NOT NULL,
  `file` varchar(100) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_productos`
--

INSERT INTO `goba_productos` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `marca`, `tipo`, `precio`, `moneda`, `unidad`, `stock`, `ID_cate`, `ID_sub_cate`, `ID_sub_cate2`, `ID_marca`, `visible`, `url_name`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `cate`, `resena`, `land`, `file`, `alta`, `fmod`, `user`) VALUES
(1, '', 'MB1-2', 'nodisponible.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 0, '', 'nodisponible.jpg', 'nodisponible.jpg', '', '', '', '', '<p>Rese&ntilde;a</p>', 0, '', '2018-08-29 13:02:27', '', 'admin'),
(2, '', 'MB3-4', 'nodisponible.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 0, '', 'nodisponible.jpg', '', '', '', '', '', '', 0, '', '2019-01-06 12:52:17', '', 'admin'),
(3, '', 'FABRICADOS A MEDIDA', 'nodisponible1.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 1, 2, 0, 0, 1, '', 'nodisponible.jpg', '', '', '', '', '', '', 0, '', '2019-01-06 13:22:17', '', 'admin'),
(4, '', 'AIR2 CARE', 'Air2Care--1_paramenu.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 1, 3, 0, 0, 1, '', 'Air2Care--1_paradescripcion.jpg', 'Air2Care--2_paradescripcion.jpg', 'Air2Care--4_paradescripcion.jpg', 'Air2Care--3_paradescripcion.jpg', '', '', '<p>El sistema de presi&oacute;n alterna Air2Care ha sido dise&ntilde;ado para la prevenci&oacute;n y el tratamiento de las &uacute;lceras por presi&oacute;n las 24 horas, lo cual es f&aacute;cil de usar y confiable, en aquellos con riesgo bajo a moderado. Air2Care es adecuado para su uso en diversos entornos de atenci&oacute;n m&eacute;dica, incluida la atenci&oacute;n a largo plazo. Para mayor comodidad, el sistema est&aacute; disponible como un reemplazo de colch&oacute;n o como una capa superpuesta que brinda la flexibilidad de elegir una soluci&oacute;n adecuada para los requisitos individuales del paciente y del cuidador.</p>', 0, '', '2019-01-06 13:24:28', '', 'admin'),
(5, '', 'VIRTUOSO', 'VIRTUOSO-01_paramenu.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 1, 3, 0, 0, 1, '', 'VIRTUOSO-01_paradescripcion.jpg', 'VIRTUOSO-2_paradescripcion.jpg', 'VIRTUOSO-4_paradescripcion.jpg', 'VIRTUOSO-3_paradescripcion.jpg', '', '', '<p>Una opci&oacute;n para pacientes con &uacute;lceras por presi&oacute;n de bajo riesgo. Se trata de una combinaci&oacute;n de una capa de aire alternante con una capa de aire est&aacute;tica. El sistema de control inteligente (SCU) Virtuoso ha sido dise&ntilde;ado teniendo en mente la ergonom&iacute;a y la facilidad de manipulaci&oacute;n. Es f&aacute;cil de transportar y montar sobre la cama. Los sencillos controles del panel frontal con s&iacute;mbolos gr&aacute;ficos claros hacen que la operaci&oacute;n sea f&aacute;cil de aprender y recordar. Pueden conectarse 6 colchones diferentes y un coj&iacute;n de asiento alternante a un &uacute;nico SCU.</p>', 0, '', '2019-01-06 13:25:30', '', 'admin'),
(6, '', 'POSICIONADORES', 'nodisponible.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 1, 4, 0, 0, 1, '', 'nodisponible.jpg', '', '', '', '', '1', '', 0, '', '2019-01-06 13:36:20', '', 'admin'),
(7, '', 'EMPAPADORES', 'nodisponible.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 1, 4, 0, 0, 1, '', '', '', '', '', '', '1', '', 0, '', '2019-01-06 13:37:21', '', 'admin'),
(8, '', 'PRACTIKA 1', 'PRAKTIKA1_paramenu.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 2, 5, 1, 0, 1, '', 'PRAKTIKA1_paradescripcion.jpg', '2.PRACTIKA1.jpg', '3.PRACTIKA1.jpg', '4.PRACTIKA1.jpg', '5.PRACTIKA1.jpg', '', '<p>Cama mec&aacute;nica, contiene ciertas funciones como posicionamiento de cama variable con ergoframe&reg;, la operaci&oacute;n mec&aacute;nica f&aacute;cil por mecanismo de manivela permite el ajuste de respaldo y muslo, el reposapiernas se puede ajustar a 5 posiciones usando la manija, la prevenci&oacute;n efectiva de las &uacute;lceras por presi&oacute;n est&aacute; garantizada por el exclusivo sistema ergoframe&reg; que disminuye la presi&oacute;n en partes del paciente cuerpo que son de alto riesgo.</p>', 0, '', '2019-01-06 20:20:43', '', 'admin'),
(9, '', 'ELEGANZA 1', 'menu_Eleganza1.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 2, 5, 2, 0, 1, '', 'ELEGANZA1-2_paradescripcion.jpg', 'ELEGANZA1-1_paradescripcion.jpg', 'ELEGANZA1-3_paradescripcion.jpg', 'ELEGANZA1-4_paradescripcion.jpg', '', '', '<p>Est&aacute; dise&ntilde;ada para las estancias est&aacute;ndar de hospital y para unidades de cuidados de larga estancia. Como resultado, esta cama est&aacute; por encima de las prestaciones est&aacute;ndar y tiene sofisticadas funciones disponibles para una gran variedad de consumidores.</p>', 0, '', '2019-01-13 17:05:30', '', 'admin'),
(10, '', 'ELEGANZA 2', 'menu_Eleganza2.jpg', '', 'Descripcion', '', '', '0.00', 'MNX', 'PZ', 0, 2, 5, 2, 0, 1, '', 'Eleganza2.jpg', 'ELEGANZA2-1_paradescripcion.jpg', 'ELEGANZA2-3_paradescripcion.jpg', 'ELEGANZA2-4_paradescripcion.jpg', '', '', '<p>Es la primera cama de su clase, y genuinamente define la cama moderna.&nbsp; Contiene innovadoras funciones, incluidas propiedades \"inteligentes\" y puede ser usada de forma flexible en todas las estancias de hospital.&nbsp; La Eleganza 2 Hace m&aacute;s f&aacute;cil las exigencias de la rutina diaria del cuidado de pacientes y gracias a su moderno dise&ntilde;o eleva a otro nivel la percepci&oacute;n de los cuidados proporcionados.</p>', 0, '', '2019-01-13 17:06:41', '', 'admin'),
(11, '', 'ELEGANZA 3XC', 'ELEGANZA3XC_paramenu.jpg', '', '', '', '', '0.00', 'MNX', 'PZ', 0, 2, 5, 3, 0, 1, '', 'ELEGANZA3XC_paradescripcion.jpg', 'ELEGANZA-3XC-1_paradescripcion.jpg', 'ELEGANZA-3XC-3_paradescripcion.jpg', 'ELEGANZA-3XC-4_paradescripcion.jpg', 'ELEGANZA 3XC-5.jpg', '', '<p>El dise&ntilde;o y la funcionabilidad de la cama Eleganza 3XC est&aacute; customizada para cumplir con las exigentes demandas de los cuidados intensivos.&nbsp; Esta moderna cama con una construcci&oacute;n de columna ofrece un programa integrado que ayuda al personal cuidador a tratar y cuidar a los pacientes cr&iacute;ticamente enfermos.</p>', 0, '', '2019-04-11 21:01:19', '', 'admin'),
(12, '', 'ELEGANZA 5', 'ELEGANZA5_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 5, 3, 0, 1, 'url_name', 'ELEGANZA5_paradescripcion.jpg', 'ELEGANZA-5-1_paradescripcion.jpg', 'ELEGANZA-5-2_paradescripcion.jpg', 'ELEGANZA-5-3_paradescripcion.jpg', 'ELEGANZA-5-4_paradescripcion.jpg', '', '<p>Cama de posici&oacute;n adaptable para cuidados intensivos. Su prop&oacute;sito es proporcionar apoyo al paciente y facilitar el tratamiento y manipulaci&oacute;n del paciente por el personal de enfermer&iacute;a. Eleganza 5 es compatible con el sistema de sustituci&oacute;n de colch&oacute;n integrado de OptiCare. El sistema de colch&oacute;n OptiCare est&aacute; pensado para asistir en la prevenci&oacute;n y el tratamiento de las &uacute;lceras de dec&uacute;bito.</p>', 0, '', '2019-05-12 14:58:09', '', 'admin'),
(13, '', 'MULTICARE', 'MULTICARE_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 5, 3, 0, 1, 'url_name', 'MULTICARE_paradescripcion.jpg', 'MULTICARE-1_paradescripcion.jpg', 'MULTICARE-2_paradescripcion.jpg', 'MULTICARE-3_paradescripcion.jpg', 'MULTICARE-4_paradescripcion.jpg', '', '<p>La cama Multicare para unidades de cuidados card&iacute;acos (CCU) es la soluci&oacute;n ideal para el exigente ambiente de los cuidados intensivos. Ayuda a mantener las constantes vitales y permite al paciente ser tratado de manera segura con el m&iacute;nimo esfuerzo para el personal de cuidados.</p>', 0, '', '2019-05-13 09:36:13', '', 'admin'),
(14, '', 'SPRINT 100', 'SPRINT100_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 5, 4, 0, 1, 'url_name', 'Camilla2.jpg', 'SPRINT-100--3_paradescripcion.jpg', 'SPRINT-100--5_paradescripcion.jpg', 'SPRINT-100--1_paradescripcion.jpg', '', '', '<p>Es un dispositivo altamente fiable dise&ntilde;ado para el cuidado completo de pacientes en unidades de admisi&oacute;n de emergencia. Tiene una amplia gama de caracter&iacute;sticas progresivas que facilitan un transporte r&aacute;pido y seguro mientras se mantienen las constantes vitales.</p>', 0, '', '2019-05-13 09:37:36', '', 'admin'),
(15, '', 'GRACIE DE REVISI&Oacute;N GINECOLOG&Iacute;CA', 'GRACIE_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 6, 0, 0, 1, 'url_name', 'GRACIE_paradescripcion.jpg', 'GRACIE-1_paradescripcion.jpg', 'GRACIE-2_paradescripcion.jpg', 'GRACIE-3_paradescripcion.jpg', 'GRACIE-4_paradescripcion.jpg', '', '<p>La silla de exploraci&oacute;n ginecol&oacute;gica combina un dise&ntilde;o moderno con una gran funcionalidad, GRACIE crea un atractivo entorno tanto para el m&eacute;dico como para la paciente, centr&aacute;ndose al mismo tiempo en la ergonom&iacute;a y la eficiencia, la silla puede convertirse de forma instant&aacute;nea en una cama.</p>', 0, '', '2019-05-13 11:16:22', '', 'admin'),
(16, '', 'AVE 2 DE EXPULSI&Oacute;N', 'AVE2_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 6, 0, 0, 1, 'url_name', 'AVE2_paradescripcion.jpg', 'AVE-1_paradescripcion.jpg', 'AVE-2_paradescripcion.jpg', 'AVE-3_paradescripcion.jpg', 'AVE-4_paradescripcion.jpg', '', '<p>La cama de parto AVE2 aporta la m&aacute;xima seguridad y confort a las futuras mam&aacute;s. Gracias al AVE2 el parto podr&aacute; ser natural conforme a sus expectativas. Para el personal sanitario, la cama es un instrumento fiable y pr&aacute;ctico que puede mejorar y facilitar el cuidado de la madre en todas las fases del parto, les da una sensaci&oacute;n de estar en un entorno agradable y seguro.</p>', 0, '', '2019-05-13 11:17:18', '', 'admin'),
(17, '', 'TOM 2', 'TOM2_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 7, 0, 0, 1, 'url_name', 'TOM2_paradescripcion.jpg', 'TOM-1_paradescripcion.jpg', 'TOM-2_paradescripcion.jpg', 'TOM-3_paradescripcion.jpg', 'TOM-4_paradescripcion.jpg', '', '<p>Una cama de &uacute;ltima tecnolog&iacute;a para pacientes en edad pre-escolar es segura y capaz de proporcionar al personal de enfermer&iacute;a un excelente acceso al paciente. Los ajustes el&eacute;ctricos de posici&oacute;n de la cama el&eacute;ctrica infantil, con su original concepto de barandillas telesc&oacute;picas, su mecanismo de construcci&oacute;n de columna y sus altos par&aacute;metros de seguridad, cumplen con todas estas exigencias, adem&aacute;s de con otras.</p>', 0, '', '2019-05-13 11:18:55', '', 'admin'),
(18, '', 'SMART JUNIOR', 'SMARTJUNIOR_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 7, 0, 0, 1, 'url_name', 'SMARTJUNIOR_paradescripcion.jpg', 'SMART-1_paradescripcion.jpg', 'SMART-2_paradescripcion.jpg', 'SMART-3_paradescripcion.jpg', 'SMART-4_paradescripcion.jpg', '', '<p>Es la soluci&oacute;n ideal para departamentos pedi&aacute;tricos y para adultos de baja estatura.&nbsp; La cama ofrece una construcci&oacute;n en columna &uacute;nica que permite una carga de trabajo segura, gran estabilidad y la capacidad de soportar un uso vigoroso.</p>', 0, '', '2019-05-13 11:19:59', '', 'admin'),
(19, '', 'MIMI', 'MIMI_paramenu_copia.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 7, 0, 0, 1, 'url_name', 'MIMI_paradescripcion_copia.jpg', 'MIMI-01_paradescripcion.jpg', 'MIMI-02_paradescripcion.jpg', 'MIMI-03_paradescripcion.jpg', 'MIMI-04_paradescripcion.jpg', '', '<p>Cuna para los reci&eacute;n nacidos, proporciona a los ni&ntilde;os con total seguridad y garantiza una atenci&oacute;n eficaz. Es muy pr&aacute;ctico y hace m&aacute;s f&aacute;cil para el personal m&eacute;dico y las madres para el cuidado de los reci&eacute;n nacidos.</p>', 0, '', '2019-05-13 11:20:45', '', 'admin'),
(20, '', 'MOVITA', 'MOVITA_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 23, 0, 0, 1, 'url_name', 'MOVITA_paradescripcion.jpg', 'MOVITA-2_paradescripcion.jpg', 'MOVITA-3_paradescripcion.jpg', 'MOVITA-4_paradescripcion.jpg', 'MOVITA-5_paradescripcion.jpg', '', '<p>Las ventajas de movita: Concepto de protecci&oacute;n lateral patentado SafeFree, Protecci&oacute;n lateral dividida, ajustable a 4 alturas, respectivamente, Sin elementos verticales en la zona central ni peligrosos huecos, Mando de dise&ntilde;o, Funcionalidad mejorada y ampliada, 3 niveles de seguridad ajustables: Modo de cuidados, modo de pacientesy modo de seguridad, Funci&oacute;n de movilizaci&oacute;n, Control de la iluminaci&oacute;n nocturna</p>\r\n<p>Desplazamiento por separado del reposapiernas, Bloqueo del teclado.</p>', 0, '', '2019-05-13 12:53:41', '', 'admin'),
(21, '', 'SENTIDA 5', 'SENTIDA5_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 23, 0, 0, 1, 'url_name', 'SENTIDA5_paradescripcion.jpg', 'SENTIDA-2_paradescripcion.jpg', 'SENTIDA-3_paradescripcion.jpg', 'SENTIDA-4_paradescripcion.jpg', 'SENTIDA-5_paradescripcion.jpg', '', '<p>Concepto de protecci&oacute;n lateral patentado SafeFree&reg;, la aportaci&oacute;n a una reducci&oacute;n de las fjaciones seg&uacute;n la pr&aacute;ctica &laquo;Werdenfelser Weg&raquo; y del proyecto Redufx, Protecci&oacute;n lateral dividida, ajustable siempre a 4 alturas, Sin elementos verticales en la zona central ni peligrosos huecos, Funci&oacute;n de luz nocturna, Modo acogedor, con cualquier altura de la cama, Ruedas dobles de 50 mm, completamente recubiertas gracias a un chasis acogedor, Freno de pedal por ejes con sistema de freno centralizado de las 2 ruedas.</p>', 0, '', '2019-05-13 12:57:40', '', 'admin'),
(22, '', 'DX-D 100', 'DX-D100_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 8, 0, 0, 1, 'url_name', 'DX-D100--1_paradescripcion.jpg', 'DX-D100--2_paradescripcion.jpg', 'DX-D100--3_paradescripcion.jpg', 'DX-D100--4_paradescripcion.jpg', 'DX-D100--5_paradescripcion.jpg', '', '<p>De f&aacute;cil manejo y calidad de imagen excelente, el dx-d100 m&oacute;vil con detector inal&aacute;mbrico ofrece im&aacute;genes r&aacute;pidas y de gran calidad validables inmediatamente, f&aacute;cil manejo y posicionamiento preciso y seguro, control motorizado y detector DR port&aacute;til inal&aacute;mbrico. MUSICA ofrece una calidad de imagen constante, independiente del tipo de examen y con m&aacute;s detalle y contraste, M&aacute;s conectividad a PACS, HIS/RIS. Generador de gran potencia: tiempos de exposici&oacute;n m&aacute;s cortos e im&aacute;genes m&aacute;s n&iacute;tidas.</p>', 0, '', '2019-05-14 16:37:26', '', 'admin'),
(23, '', 'DXD-300', 'DX-D-300_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 8, 0, 0, 1, 'url_name', 'DX-D-300-2_paradescripcion.jpg', 'DX-D-300-3_paradescripcion.jpg', 'DX-D-300-4_paradescripcion.jpg', 'DX-D-300-6_paradescripcion.jpg', 'DX-D-300-7_paradescripcion.jpg', '', '<p>El DX-D 300 es la forma m&aacute;s r&aacute;pida y f&aacute;cil de evolucionar a la radiograf&iacute;a digital directa. Con &eacute;l, disfrutar&aacute;, constantemente de la excelente calidad de imagen del procesamiento de im&aacute;genes MUSICA. La versatilidad para recoger una amplia variedad de estudios. Mayor productividad a trav&eacute;s de la excelencia en cuanto a calidad de imagen.</p>', 0, '', '2019-05-14 16:38:44', '', 'admin'),
(24, '', 'DR600', 'DR600_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 8, 0, 0, 1, 'url_name', 'DR-600-1_paradescripcion.jpg', 'DR-600-2_paradescripcion.jpg', 'DR-600-3_-paradescripcion.jpg', 'DR-600-4_paradescripcion.jpg', 'DR-600-5_paradescripcion.jpg', '', '<p>Gracias a las funciones innovadoras de alta productividad ya la tecnolog&iacute;a ZeroForce que ofrece alta velocidad, precisi&oacute;n y comodidad, el DR 600 totalmente autom&aacute;tico agiliza el flujo de trabajo, aumenta el rendimiento y mejora la experiencia de los pacientes como de los operadores, incluso en El entorno de im&aacute;genes m&aacute;s activo.</p>', 0, '', '2019-05-14 16:39:54', '', 'admin'),
(25, '', 'DR400', 'DR-400_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 8, 0, 0, 1, 'url_name', 'DR-400-1_paradescripcion.jpg', 'DR-400-2_paradescripcion.jpg', 'DR-400-3_paradescripcion.jpg', 'DR-400-4_paradescripcion.jpg', 'DR-400-5_paradescripcion.jpg', '', '<p>El sistema DR 400 es una soluci&oacute;n flexible y accesible, montado en el piso y de f&aacute;cil instalaci&oacute;n, necesita poco espacio y no requiere costosos soportes de techo, al mismo tiempo, sus configuraciones adaptables permiten que se adapte a las necesidades espec&iacute;ficas de la mayor&iacute;a de los clientes. Nuestro DR 400 ofrece una soluci&oacute;n de DR completa y escalable que puede crecer y desarrollarse con su centro.</p>', 0, '', '2019-05-14 16:42:19', '', 'admin'),
(26, '', 'DR RETROFIT', 'DR_DETROFIT_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 8, 0, 0, 1, 'url_name', 'DR-1_paradescripcion.jpg', 'DR-2_paradescripcion.jpg', 'DR-5_paradescripcion.jpg', 'DR-6_paradescripcion.jpg', 'DR-7_paradescripcion.jpg', '', '<p>Con las soluciones DR Retrofit neutral, no vinculadas exclusivamente a Agfa HealthCare, puede actualizar f&aacute;cilmente su modalidad actual de rayos X a los sistemas de radiograf&iacute;a directa (DR) a la vez que protege su inversi&oacute;n actual. Tanto en salas de rayos X como en unidades m&oacute;viles anal&oacute;gicas, solo toma un momento pasar al sistema de DR.</p>', 0, '', '2019-05-14 16:43:18', '', 'admin'),
(27, '', 'CR10-X', 'CR-10X_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 9, 0, 0, 1, 'url_name', 'CR-10X--1_paradescripcion.jpg', 'CR-10X--2_paradescripcion.jpg', 'CR-10X--4_paradescripcion.jpg', 'CR-10X--4_paradescripcion.jpg', 'CR-10X--5_paradescripcion.jpg', '', '<p>La versatilidad y la integraci&oacute;n del CR 10-X proporcionan un flujo de trabajo incrementado, que maximiza el retorno de su inversi&oacute;n, crea una oportunidad accesible para ingresar al entorno de la CR, del sistema anal&oacute;gico al digital, abordando las necesidades de las operaciones de menor volumen, hospitales rurales, consultorios privados.</p>', 0, '', '2019-05-14 17:07:44', '', 'admin'),
(28, '', 'CR15-X', 'CR-15X_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 9, 0, 0, 0, 'url_name', 'CR-15X-1_paradescripcion.jpg', 'CR-15X-2_paradescripcion.jpg', 'CR-15X-3_paradescripcion.jpg', 'CR-15X-4_paradescripcion.jpg', 'CR-15X-5_paradescripcion.jpg', '', '<p>Le permite definir su propio flujo de trabajo y adaptarlo cuando lo desee, de manera r&aacute;pida y f&aacute;cil. As&iacute; es el CR 15-X, una soluci&oacute;n vers&aacute;til y flexible que ofrece una alta calidad de imagen, y medidas para seguir siendo liviana y accesible, siendo compacta, r&aacute;pida, que permite una gama completa de aplicaciones, con placas de f&oacute;sforo est&aacute;ndar, el CR 15-X ofrece una excelente calidad de imagen.</p>', 0, '', '2019-05-14 17:08:49', '', 'admin'),
(29, '', 'CR30-XM', 'CR-30-XM_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 9, 0, 0, 0, 'url_name', 'CR-30-XM-1_paradescripcion.jpg', 'CR-30-XM-2_paradescripcion.jpg', 'CR-30-XM-3_paradescripcion.jpg', 'CR-30-XM-4_paradescripcion.jpg', 'CR-30-XM-5_paradescripcion.jpg', '', '<p>Digitalizador de mesa compacta, de una sola ranura, que controla tanto aplicaciones de mamograf&iacute;a digital como de radiograf&iacute;a general. Un digitalizador que cumple con la calidad de alto nivel que exige la mamograf&iacute;a digital y que ofrece una soluci&oacute;n que facilita el cambio del sistema anal&oacute;gico al digital de manera r&aacute;pida y rentable. (Nota: El CR 30-Xm no est&aacute; disponible en EE. UU. Ni Canad&aacute;).</p>', 0, '', '2019-05-14 17:10:15', '', 'admin'),
(31, '', 'DRYSTAR AXYS', 'AXYS_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 10, 0, 0, 1, 'url_name', 'AXYS-1_paradescripcion.jpg', 'AXYS-2_paradescripcion.jpg', 'AXYS-3_paradescripcion.jpg', '', '', '', '<p>El dispositivo de imagen con plena flexibilidad, es el nuevo eje para variaciones de trabajo con im&aacute;genes de radiograf&iacute;a digital, incluida la mamograf&iacute;a. Su peque&ntilde;o tama&ntilde;o cubre un enorme potencial. Ya sea que necesite una impresora centralizada o requerida, DRYSTAR AXYS se adapta perfectamente. Incluye dos bandejas de impresi&oacute;n en l&iacute;nea que pueden manipular todos los tipos de medios en todos los tama&ntilde;os disponibles, mientras que el manejo de los medios de luz diurna facilita la carga.</p>', 0, '', '2019-05-14 17:29:00', '', 'admin'),
(32, '', 'DRYSTAR 5302', '5302_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 10, 0, 0, 1, 'url_name', 'DRYSTAR-5302-1_paradescripcion.jpg', 'DRYSTAR-5302-3_paradescripcion.jpg', 'DRYSTAR-5302-4_paradescripcion.jpg', 'DRYSTAR-5302-5_paradescripcion.jpg', 'DRYSTAR-5302-7_paradescripcion.jpg', '', '<p>Ofrece impresi&oacute;n de alta calidad en un tama&ntilde;o de mesa pr&aacute;ctico, lo que permite la impresi&oacute;n pr&oacute;xima a la aplicaci&oacute;n, incluso en los espacios m&aacute;s peque&ntilde;os.&nbsp;Esta libertad de ubicaci&oacute;n, combinada con los bajos costos de inversi&oacute;n y ejecuci&oacute;n, hace que la DRYSTAR 5302 sea el complemento ideal para muchas modalidades, y la respuesta correcta a sus necesidades de impresi&oacute;n.</p>', 0, '', '2019-05-14 17:31:18', '', 'admin'),
(33, '', 'PELICULA', 'PELICULAS_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 11, 0, 0, 1, 'url_name', 'PELICULA-1_paradescripcion.jpg', 'PELICULA-3_paradescripcion.jpg', 'PELICULA-2_paradescripcion.jpg', '', '', '', '<p>Pel&iacute;cula de rayos X basada en haluro de plata para radiolog&iacute;a general y mamograf&iacute;a que ofrece una calidad de imagen n&iacute;tida. Desarrolladores y fijadores universales y respetuosos con el medio ambiente para todo tipo de procesadores y pel&iacute;culas.</p>', 0, '', '2019-05-14 17:32:47', '', 'admin'),
(34, '', 'QUIMICO PARA REVELADO', 'REVELADO_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 11, 0, 0, 1, 'url_name', 'REVELADO-2_paradescripcion.jpg', 'REVELADO-3_paradescripcion.jpg', 'REVELADO-4_paradescripcion.jpg', 'REVELADO-5_paradescripcion.jpg', 'REVELADO-6_paradescripcion.jpg', '', '', 0, '', '2019-05-14 17:33:50', '', 'admin'),
(35, '', 'DRAGER FABIUS PLUS', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 12, 0, 0, 0, 'url_name', 'nodisponible.jpg', '', '', '', '', '', '', 0, '', '2019-05-14 17:35:47', '', 'admin'),
(36, '', 'OXILOG 3000 PLUS', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 13, 0, 0, 0, 'url_name', 'nodisponible.jpg', '', '', '', '', '', '', 0, '', '2019-05-14 17:38:30', '', 'admin'),
(37, '', 'PULMOVISTA 500', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 13, 0, 0, 0, 'url_name', 'nodisponible.jpg', '', '', '', '', '', '', 0, '', '2019-05-14 17:39:31', '', 'admin'),
(38, '', 'BABYLEO TN500', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 14, 0, 0, 0, 'url_name', 'nodisponible.jpg', '', '', '', '', '', '', 0, '', '2019-05-14 17:40:26', '', 'admin'),
(39, '', 'JM 105', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 0, 0, 0, 0, 'url_name', 'nodisponible.jpg', '', '', '', '', '', '', 0, '', '2019-05-14 17:41:33', '', 'admin'),
(40, '', 'VISTA 120', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 15, 0, 0, 0, 'url_name', 'nodisponible.jpg', '', '', '', '', '', '', 0, '', '2019-05-14 17:42:28', '', 'admin'),
(41, '', 'POLARIS 100/200', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 16, 0, 0, 0, 'url_name', 'nodisponible.jpg', '', '', '', '', '', '', 0, '', '2019-05-14 17:44:49', '', 'admin'),
(42, '', 'SOFLENS TORIC', 'SOFLENS TORIC.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'SOFLENS-TORIC_paradescripcion.jpg', '', '', '', '', '', '<p>Lente de hidrogel con dise&ntilde;o de estabilizaci&oacute;n Lo-Torque&trade; para proporcionar buena agudeza visual, comodidad, y facilidad de adaptaci&oacute;n.</p>', 0, '', '2019-05-14 17:47:29', '', 'admin'),
(44, '', 'PRACTIKA 2', 'Praktica2.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 2, 5, 1, 0, 1, 'url_name', 'PRAKTIKA2_paraDescripcion.jpg', '2.PRACTIKA2.jpg', '3.PRACTIKA2.jpg', '4.PRACTIKA2.jpg', '5.PRACTIKA2.jpg', '', '<p>Practika 2 tiene ajustable altura de la plataforma del colch&oacute;n, opcionalmente funci&oacute;n TR / ATR y puede ser frenado centralmente, la soluci&oacute;n completa de rieles laterales ofrece configuraciones individuales y garantiza m&aacute;xima seguridad del paciente, Protector para la parte del pie de cama se puede usar con rieles laterales simples y tambi&eacute;n divididos</p>', 0, '', '2019-05-27 18:23:14', '', 'admin'),
(45, '', 'CR-12X', 'CR12X_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 9, 0, 0, 1, 'url_name', 'CR12X-1_paradescripcion.jpg', 'CR12X-2_paradescripcion.jpg', 'CR12X-3_paradescripcion.jpg', 'CR12X-4_paradescripcion.jpg', 'CR12X-5_paradescripcion.jpg', '', '<p>Una entrada accesible a la radiograf&iacute;a digital que le permite tener todo el control, conveniente para la radiolog&iacute;a general, la ortopedia, la quiropr&aacute;ctica y las aplicaciones de pierna o columna completas, el amplio rango de capacidades del CR&nbsp;12-X lo hace altamente vers&aacute;til y rentable que le permite adaptar y ajustar su elecci&oacute;n en base al estudio y aprovecha la capacidad del software de procesamiento de im&aacute;genes MUSICA.</p>', 0, '', '2019-08-26 21:52:07', '', 'admin'),
(46, '', 'DRYSTAR 5503', '5503_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 10, 0, 0, 1, 'url_name', '5503-1_paradescripcion.jpg', '5503-2_paradescripcion.jpg', '5503-3_paradescripcion.jpg', '5503-4_paradescripcion.jpg', '', '', '<p>Es todo en uno, que admite todas las aplicaciones y m&uacute;ltiples modalidades, al tiempo que ofrece un alto rendimiento y una alta resoluci&oacute;n, aborda todos sus requisitos de im&aacute;genes centralizadas. Obtiene las caracter&iacute;sticas de alto valor que marcan la diferencia, incluida la galardonada tecnolog&iacute;a Direct Digital Imaging de Agfa A # Sharpenhanced; la flexibilidad de tres bandejas de medios multiformato; y f&aacute;cil integraci&oacute;n DICOMnative. La opci&oacute;n de impresi&oacute;n de mamograf&iacute;a aumenta las posibilidades de uso.</p>', 0, '', '2019-08-26 21:55:54', '', 'admin'),
(47, '', 'CHASIS', 'CHASIS_OK.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 11, 0, 0, 1, 'url_name', 'PELICULA-3_paradescripcion.jpg', 'PELICULA-5_paradescripcion.jpg', 'PELICULA-6_paradescripcion.jpg', '', '', '', '', 0, '', '2019-08-26 22:03:41', '', 'admin'),
(48, '', 'CLINICARE 20', 'CliniCare-20_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 1, 'url_name', 'CliniCare-20_paradescripcion.jpg', 'CliniCare-20-2_paradescripcion.jpg', 'CliniCare-20-3_paradescripcion.jpg', 'CliniCare-4_paradescripcion.jpg', '', '', '<p>Es un colch&oacute;n de una capa de poliuretano totalmente cubierta por una capa de espuma termoel&aacute;stica. Esta composici&oacute;n del n&uacute;cleo, combinado con el perfil, garantiza una mejor distribuci&oacute;n de la presi&oacute;n creada debajo del paciente durante la colocaci&oacute;n de la cama.</p>\r\n<p><strong>Espuma termoel&aacute;stica de siete zonas: </strong>para m&aacute;xima comodidad del paciente, la espuma termoel&aacute;stica soporta todo el cuerpo del paciente y forma toda la capa superior del colch&oacute;n.</p>\r\n<p><strong>Asas de transporte:</strong>&nbsp;las asas de transporte del lateral del colch&oacute;n pueden utilizarse para una mejor manipulaci&oacute;n y una reubicaci&oacute;n m&aacute;s sencilla del colch&oacute;n.</p>', 0, '', '2019-08-26 22:45:43', '', 'admin'),
(49, '', 'EFFECTACARE 10', 'EffectaCare-10_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 1, 'url_name', 'EffectaCare-10_paradescripcion.jpg', 'EffectaCare-10-3_paradescripcion.jpg', 'EffectaCare-10-2_paradescripcion.jpg', 'EffectaCare-10-4_paradescripcion.jpg', '', '', '<p>El colch&oacute;n consiste en un bloque s&oacute;lido con perfil contorneado en ambos lados del colch&oacute;n. Este perfil ayuda a distribuir mejor la presi&oacute;n, y debido a su dise&ntilde;o totalmente sim&eacute;trico, se puede dar la vuelta al colch&oacute;n y rotarse (alternando la cabeza y los pies) para alargar su vida &uacute;til. La cobertura del colch&oacute;n es de un material permeable al vapor: Ayuda a reducir la transpiraci&oacute;n de un paciente y tambi&eacute;n la humedad de la piel.</p>', 0, '', '2019-08-26 22:47:13', '', 'admin'),
(50, '', 'EFFECTACARE 20', 'EffectaCare-20-1_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 1, 'url_name', 'EffectaCare-20-1_paradescripcion.jpg', 'EffectaCare-20-2_paradescripcion.jpg', 'EffectaCare-20-3_paradescripcion.jpg', 'EffectaCare-20-4_paradescripcion.jpg', '', '', '<p>El colch&oacute;n est&aacute; formado por un n&uacute;cleo de espuma monol&iacute;tico. Gracias al perfil de la parte superior del colch&oacute;n en el &aacute;rea de la cadera del paciente, se produce una mejor circulaci&oacute;n del aire y una mejor distribuci&oacute;n de la presi&oacute;n entre la piel del paciente y la base, el colch&oacute;n presenta recortes en la parte interior gracias a los cuales se optimiza la presi&oacute;n en el paciente durante su colocaci&oacute;n en la cama.</p>', 0, '', '2019-08-26 22:50:46', '', 'admin'),
(51, '', 'MEDIMAT A1', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 0, 'url_name', '', '', '', '', '', '', '', 0, '', '2019-08-26 22:51:31', '', 'admin'),
(52, '', 'MEDIMAT V1', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 0, 'url_name', '', '', '', '', '', '', '', 0, '', '2019-08-26 22:52:23', '', 'admin'),
(53, '', 'PRIMACARE 10', 'PrimaCare-10-1_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 1, 'url_name', 'PrimaCare-10-1_paradescripcion.jpg', 'PrimaCare-10-2_paradescripcion.jpg', 'PrimaCare-10-3_paradescripcion.jpg', 'PrimaCare-10-4_paradescripcion.jpg', '', '', '<p>Cuenta con un n&uacute;cleo de espuma monol&iacute;tico. La superficie superior del colch&oacute;n presenta un perfil transversal y longitudinal distribuido de modo uniforme sobre toda su superficie. Gracias a este perfil, se produce una mejor circulaci&oacute;n del aire entre la piel del paciente y la base. Tambi&eacute;n ayuda a distribuir la presi&oacute;n de un modo &oacute;ptimo, el colch&oacute;n garantiza un mayor confort y una mejor distribuci&oacute;n de la presi&oacute;n entre el paciente y el colch&oacute;n, ayuda a reducir la transpiraci&oacute;n de un paciente y tambi&eacute;n la humedad de la piel.</p>', 0, '', '2019-08-26 22:52:56', '', 'admin'),
(54, '', 'PRIMACARE 20', 'PrimaCare-20-1_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 1, 'url_name', 'PrimaCare-20-1_paradescripcion.jpg', 'PrimaCare-20-2_paradescripcion.jpg', 'PrimaCare-20-3_paradescripcion.jpg', 'PrimaCare-20-4_paradescripcion.jpg', '', '', '<p>El cuerpo del colch&oacute;n est&aacute; formado por una combinaci&oacute;n de dos espumas de poliuretano en dos capas. La superficie superior del colch&oacute;n presenta un perfil en aquellos lugares donde se ejerce mayor presi&oacute;n en la piel del paciente: la cabeza, los costados y los pies. Gracias a este perfil, se produce una mejor circulaci&oacute;n del aire entre la piel del paciente y la base. Tambi&eacute;n ayuda a distribuir la presi&oacute;n de modo &oacute;ptimo.</p>', 0, '', '2019-08-26 22:53:35', '', 'admin'),
(55, '', 'PRECIOSO', 'PRECIOSO-1_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 3, 0, 0, 1, 'url_name', 'PRECIOSO-1_paradescripcion.jpg', 'PRECIOSO-2_paradescripcion.jpg', 'PRECIOSO-3_paradescripcion.jpg', 'PRECIOSO-4_paradescripcion.jpg', 'PRECIOSO-5_paradescripcion.jpg', '', '<p>El Precioso es un sistema 24 horas para la gesti&oacute;n del cuidado de heridas.&nbsp; El sistema consiste en un sobre-colch&oacute;n y puede ser usado con un coj&iacute;n especial.&nbsp; El Precioso contribuye significativamente al cuidado y prevenci&oacute;n de &uacute;lceras de dec&uacute;bito de todos los grados.&nbsp; Es adecuado para el uso en departamentos hospitalarios generales y residencias. La Precioso est&aacute; equipada con 4 sensores de presi&oacute;n y un microprocesador que controla la presi&oacute;n concreta de las celdas.&nbsp; Para la comodidad del paciente es posible ajustar la presi&oacute;n, increment&aacute;ndola o reduci&eacute;ndola, desde la membrana del panel.</p>', 0, '', '2019-08-26 22:59:35', '', 'admin'),
(56, '', 'SYMBIOSO', 'symbioso_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 3, 0, 0, 1, 'url_name', 'symbioso-1_paradescripcion.jpg', 'symbioso-2_paradescripcion.jpg', 'symbioso-3_paradescripcion.jpg', 'symbioso-4_paradescripcion.jpg', '', '', '<p>Es la nueva generaci&oacute;n de sistema Integrado de Baja P&eacute;rdida de Aire, Symbioso tiene un modo de doble terapia, Gesti&oacute;n de MicroClima (MCM) y Baja Presi&oacute;n Constante (CLP).&nbsp; Es la soluci&oacute;n perfecta para unidades de cuidados intensivos porque su superficie beneficia y se integra con una de las m&aacute;s avanzadas del mercado de camas de cuidados intensivos, contiene 5 niveles de soporte, basados en el peso del paciente, permiten a los consumidores utilizar su criterio cl&iacute;nico para determinar los par&aacute;metros correctos para el paciente.</p>', 0, '', '2019-08-26 23:00:26', '', 'admin'),
(57, '', 'DR 100E', 'DR100E_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 8, 0, 0, 1, 'url_name', 'DX-D100--1_paradescripcion.jpg', 'DX-D100--2_paradescripcion.jpg', 'DX-D100--3_paradescripcion.jpg', 'DX-D100--4_paradescripcion.jpg', 'DX-D100--5_paradescripcion.jpg', '', '<p>Con un potente generador, de tama&ntilde;o compacto y manejo flexible, el DR 100e ofrece una soluci&oacute;n de rayos X rentable, de alta calidad en cl&iacute;nicas y hospitales, maximizando el desempe&ntilde;o y la comodidad del paciente. El DR 100e puede gestionar una amplia gama de estudios generales de radiolog&iacute;a, mediante el uso de detectores inal&aacute;mbricos.</p>', 0, '', '2019-10-01 18:41:55', '', 'admin'),
(59, '', 'ACUVUE2', 'ACUVUE 2.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 19, 0, 0, 1, 'url_name', 'ACUVUE-2_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>Un lente de contacto ver&#347;atil para una visi&oacute;n clara y c&oacute;moda, disponible para correcci&oacute;n de miop&iacute;a e hipermetrop&iacute;a</strong></p>\r\n<p><strong></strong>Alta protecci&oacute;n UV</p>', 0, '', '2019-10-08 17:43:51', '', 'admin'),
(60, '', 'ACUVUE OASYS MIOPIA O HIPERMETROPIA', 'ACUVUE OASYS.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 19, 0, 0, 1, 'url_name', 'ACUVUE-OASYS_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>Para quienes poseen un estilo de vida que exige m&aacute;s de parte de los lentes de contacto, como trabajar largas horas frente a la computadora, en ambientes secos o con aire acondicionado.</strong></p>\r\n<ul>\r\n<li>Material de Hidrogel de Silicona que permite el paso del 98% del Ox&iacute;geno a la C&oacute;rnea:Salud Ocular.</li>\r\n<li>Tecnolog&iacute;a Exclusiva Hydraclear Plus, mantiene el lente hidratado durante todo el tiempo de uso.</li>\r\n<li>Alta protecci&oacute;n UV.</li>\r\n</ul>', 0, '', '2019-10-08 19:58:48', '', 'admin'),
(61, '', 'ACUVUE OASYS ASTIGMATISMO', '1-DAY ACUVUE MOIST PARA ASTIGMATISMO.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 19, 0, 0, 1, 'url_name', 'ACUVUE-OASYS-ASTIGMATISMO_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>Para usuarios existentes o nuevos de lentes de contacto para astigmatismo, que buscan calidad de visi&oacute;n con comodidad.</strong></p>\r\n<ul>\r\n<li>Material de Hidrogel de Silicona que permite el paso del 98% del Ox&iacute;geno a la C&oacute;rnea:Salud Ocular.</li>\r\n<li>Tecnolog&iacute;a Exclusiva Hydraclear Plus, mantiene el lente hidratado durante todo el tiempo de uso.</li>\r\n<li>Calidad Visual y estabilidad constante en las actividades diarias del usuario.</li>\r\n<li><strong>Alta protecci&oacute;n UV.</strong></li>\r\n</ul>', 0, '', '2019-10-08 20:01:17', '', 'admin'),
(62, '', '1-DAY ACUVUE MOIST MIOPIA O HIPERMETROPIA', '1-DAY ACUVUE MOIST.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 19, 0, 0, 1, 'url_name', '1-DAY-ACUVUE-MOIST_paradescripcion.jpg', '', '', '', '', '', '<ul>\r\n<li><strong>Un lente nuevo y limpio cada d&iacute;a: Salud Ocular.</strong></li>\r\n<li><strong>Tecnolog&iacute;a exclusiva Lacreon, que retiene la humedad en el lente y lo mantiene hidratado por m&aacute;s tiempo: Comodidad.</strong></li>\r\n<li><strong>No acumula Dep&oacute;sitos, no requiere estuches, soluciones o cuidados de mantenimiento: Comodidad y Practicidad.</strong></li>\r\n</ul>', 0, '', '2019-10-08 20:21:21', '', 'admin'),
(63, '', '1-DAY ACVUE MOIST ASTIGMATISMO', '1-DAY ACUVUE MOIST PARA ASTIGMATISMO.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 19, 0, 0, 1, 'url_name', '1-DAY-ACUVUE-MOIST-PARA-ASTIGMATISMO_paradescripcion.jpg', '', '', '', '', '', '<ul>\r\n<li><strong>Un lente nuevo y limpio cada d&iacute;a: Salud Ocular.</strong></li>\r\n<li><strong>Tecnolog&iacute;a exclusiva Lacreon, que retiene la humedad en el lente y lo mantiene hidratado por m&aacute;s tiempo: Comodidad.</strong><strong></strong></li>\r\n<li><strong>No acumula Dep&oacute;sitos, no requiere estuches, soluciones o cuidados de mantenimiento: Comodidad y Practicidad.</strong></li>\r\n</ul>', 0, '', '2019-10-08 21:44:46', '', 'admin'),
(64, '', 'BIOFINITY', 'BIOFINITY.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'BIOFINITY_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>Lentes de contacto blandas de calidad superior.&nbsp; Confort de lujo para uso prolongado</strong>.</p>\r\n<ul>\r\n<li><strong>Alta transmisibilidad de ox&iacute;geno.</strong></li>\r\n<li><strong>Humectaci&oacute;n uniforme y natural.</strong></li>\r\n<li><strong>Superficie resistente a dep&oacute;sitos.</strong></li>\r\n<li><strong>Modulo de elasticidad &oacute;ptimo.</strong></li>\r\n</ul>', 0, '', '2019-10-08 21:47:23', '', 'admin'),
(65, '', 'BIOFINITY XR', 'BIOFINITY XR.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'BIOFINITY-XR_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>Ahora puedes adaptar pr&aacute;cticamente a todos tus pacientes.</strong></p>\r\n<ul>\r\n<li><strong>Alta transmisibilidad de ox&iacute;geno.</strong></li>\r\n<li><strong>Humectaci&oacute;n uniforme y natural.</strong></li>\r\n<li><strong>Superficie resistente a dep&oacute;sitos.</strong></li>\r\n<li><strong>Modulo de elasticidad &oacute;ptimo.</strong></li>\r\n</ul>\r\n<p>&nbsp;</p>', 0, '', '2019-10-08 21:51:05', '', 'admin'),
(66, '', 'BIOMEDICS 55', 'BIOMEDICS 55.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'BIOMEDICS-55_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>Proporciona un comportamiento estable en los ojos de su paciente en el que podr&aacute; confiar. Dispone de un dise&ntilde;o asf&eacute;rico que ayudar&aacute; a sus pacientes a tener una visi&oacute;n m&aacute;s n&iacute;tida y clara. Tambi&eacute;n incorpora filtro UV*.</strong></p>', 0, '', '2019-10-08 22:12:10', '', 'admin'),
(67, '', 'CLARITI 1 DAY', 'CLARITI 1DAY.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'CLARITI-1DAY_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>Proporcionan hasta 3 veces m&aacute;s transmisibilidad de ox&iacute;geno que los lentes desechables de HEMA</strong></p>\r\n<ul>\r\n<li>Hidrogel de silicona de reemplazo diario</li>\r\n<li>Comodidad y practicidad de uso saludable.</li>\r\n</ul>\r\n<p>Tecnolog&iacute;a Wetloc, mayor comodidad durante todo el d&iacute;a por su alto contenido de agua.</p>', 0, '', '2019-10-08 22:13:34', '', 'admin'),
(68, '', 'BIOFINITY TORIC', 'BIOMEDICS TORIC.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'BIOMEDICS-TORIC_paradescripcion.jpg', '', '', '', '', '', '<ul>\r\n<li><strong>Alta transmisibilidad de ox&iacute;geno.</strong></li>\r\n<li><strong>Humectaci&oacute;n uniforme y natural.</strong></li>\r\n<li><strong>Superficie resistente a dep&oacute;sitos.</strong></li>\r\n<li><strong>Modulo de elasticidad &oacute;ptimo.</strong></li>\r\n</ul>', 0, '', '2019-10-08 22:26:15', '', 'admin'),
(69, '', 'BIOFINITY XR TORIC', 'BIOFINITY XR TORIC.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'BIOFINITY-TORIC_paradescripcion.jpg', '', '', '', '', '', '<ul>\r\n<li><strong>Alta transmisibilidad de ox&iacute;geno.</strong></li>\r\n<li><strong>Humectaci&oacute;n uniforme y natural.</strong></li>\r\n<li><strong>Superficie resistente a dep&oacute;sitos.</strong></li>\r\n</ul>\r\n<p><strong>Modulo de elasticidad &oacute;ptimo.</strong></p>', 0, '', '2019-10-08 22:28:53', '', 'admin'),
(70, '', 'BIOMEDICS TORIC', 'BIOMEDICS TORIC.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'BIOMEDICS-TORIC_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>Son los lentes de hidrogel de referencia para los pacientes que necesitan corregir su astigmatismo y que prefieren un programa de reemplazo mensual.</strong></p>\r\n<p class=\"Standard\"><strong>Los lentes t&oacute;ricos Biomedics ofrecen visi&oacute;n n&iacute;tida y confort duradero.</strong></p>', 0, '', '2019-10-08 22:48:47', '', 'admin'),
(71, '', 'CLARITI 1 DAY TORIC', 'CLARITI 1DAY TORIC.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'CLARITI-1DAY-TORIC_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\"><strong>Permite a sus pacientes con astigmatismo disfrutar de la comodidad de una lente desechable diaria y de las ventajas para la salud del hidrogel de silicona.</strong><strong></strong></p>\r\n<ul>\r\n<li>Correcci&oacute;n de astigmatismo</li>\r\n<li>Material de hidrogel de silicona</li>\r\n<li>Permite una excelente comodidad durante todo el d&iacute;a</li>\r\n<li>Protecci&oacute;n contra rayos UVA y UVB(1)</li>\r\n<li>Lentes de reemplazo diario</li>\r\n</ul>', 0, '', '2019-10-08 22:50:25', '', 'admin'),
(72, '', 'BIOFINITY MULTIFOCAL', 'BIOFINITY MULTIFOCAL.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'BIOFINITY-MULTIFOCAL_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>Claridad a cualquier distancia para pacientes con presbicia</strong></p>\r\n<ul>\r\n<li><strong>Permite una adaptaci&oacute;n personalizada para cada usuario y cada ojo.</strong></li>\r\n<li><strong>Gracias al principio de visi&oacute;n simult&aacute;nea la visi&oacute;n binocular se mantiene.</strong></li>\r\n<li><strong>Alta transmisibilidad de ox&iacute;geno.</strong></li>\r\n<li><strong>Humectaci&oacute;n uniforme y natural.</strong></li>\r\n<li><strong>Superficie resistente a dep&oacute;sitos.</strong></li>\r\n<li><strong>Modulo de elasticidad &oacute;ptimo.</strong></li>\r\n</ul>', 0, '', '2019-10-08 22:51:43', '', 'admin'),
(73, '', 'CLARITI 1 DAY MULTIFOCAL', 'CLARITI 1DAY MULTIFOCAL.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'CLARITI-1DAY-MULTIFOCAL_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\"><strong>El &uacute;nico hidrogel de silicona de reemplazo diario para presbicia en M&eacute;xico.</strong></p>\r\n<ul>\r\n<li>Correcci&oacute;n de la presbicia</li>\r\n<li>Permite una excelente comodidad durante todo el d&iacute;a</li>\r\n</ul>\r\n<p>Protecci&oacute;n contra rayos UVA y UVB(1)</p>', 0, '', '2019-10-08 22:53:51', '', 'admin'),
(74, '', 'EXPRESSIONS COLORS', 'EXPRESSIONS COLORS.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 20, 0, 0, 1, 'url_name', 'EXPRESSIONS-COLORS_paradescripcion.jpg', '', '', '', '', '', '<ul>\r\n<li><strong>Apariencia natural</strong></li>\r\n<li><strong>Visi&oacute;n n&iacute;tida</strong></li>\r\n</ul>', 0, '', '2019-10-08 22:55:23', '', 'admin'),
(75, '', 'FRESH LOOK COLOR BLENDS', 'FRESH LOOK COLOR BLENDS.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'FRESH-LOOK-COLOR-BLENDS_paradescripcion.jpg', '', '', '', '', '', '<p>Est&aacute;n dise&ntilde;ados para ojos claros y oscuros, creando cambios de color completos obteniendo una apariencia muy natural gracias a su tecnolog&iacute;a tres colores en 1</p>', 0, '', '2019-10-09 18:05:41', '', 'admin'),
(76, '', 'AIR OPTIX ASTIGMATISMO', 'AIR OPTIX ASTIGMATISMO.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'AIR-OPTIX-ASTIGMATISMO_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\">Lentes de contacto de Hidrogel de Silicona para astigmatismo transmiten 5 veces m&aacute;s ox&iacute;geno que los lentes de contacto t&oacute;ricos comunes.</p>\r\n<p>Proporciona m&aacute;xima comodidad gracias a su dise&ntilde;o innovador, excepcional visi&oacute;n a lo largo del d&iacute;a adem&aacute;s de libertad para usarlos todas las horas que quieras, incluso dormir con ellos.</p>', 0, '', '2019-10-09 18:28:16', '', 'admin'),
(77, '', 'AOSEPT PLUS', 'AOSEPT PLUS.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'AOSEPT-PLUS_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\">Es una soluci&oacute;n desinfectante basada en per&oacute;xido de hidr&oacute;geno que es parte de un sistema ideal en el cuidado de los lentes y&nbsp; para las personas&nbsp; que tienen alguna&nbsp; sensibilidad a los preservantes encontrados en las soluciones multiprop&oacute;sito.</p>\r\n<p>Los usuarios de lentes de contacto simplemente tienen que quitarse sus lentes, colocarlos en el estuche especial que viene en la caja de AOSEPT&reg; PLUS, enjuagar los lentes con la soluci&oacute;n a trav&eacute;s del estuche, llenar el estuche con soluci&oacute;n&nbsp; y dejar los lentes&nbsp; por lo menos 6 horas.</p>', 0, '', '2019-10-09 18:30:00', '', 'admin'),
(78, '', 'OPTIFREE PURE MOIST', 'OPTIFREE PURE MOIST.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'OPTIFREE-PURE-MOIST_paradescripcion.jpg', '', '', '', '', '', '<p>Es una soluci&oacute;n que contribuye al uso satisfactorio de lentes de contacto, ya que mantiene las lentes c&oacute;modas desde que te las pones hasta el final del d&iacute;a.</p>', 0, '', '2019-10-09 20:10:51', '', 'admin'),
(79, '', 'AVIZOR EVER CLEAN', 'AVIZOR EVER CLEAN.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'AVIZOR-EVER-CLEAN_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\">Un sistema revolucionario para la limpieza y desinfecci&oacute;n de todo tipo de lentes de contacto.</p>\r\n<ul>\r\n<li>Sin conservantes.</li>\r\n<li>En s&oacute;lo 2 horas lentes como nuevas.</li>\r\n<li>Sin frotar.</li>\r\n<li>Sin necesidad de usar productos adicionales.</li>\r\n</ul>', 0, '', '2019-10-09 20:29:07', '', 'admin'),
(80, '', 'AVIZOR GP CLEANER', 'AVIZOR GP CLEANER.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'AVIZOR-GP-CLEANER_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\">El limpiador GP CLEANER AVIZOR es un detergente no i&oacute;nico muy efectivo, que contiene part&iacute;culas que eliminan los dep&oacute;sitos de l&iacute;pidos de las lentes r&iacute;gidas y gas-permeables.</p>\r\n<p class=\"Textbody\">Para completar la desinfecci&oacute;n de las lentes, este producto debe combinarse con el AVIZOR GP CONDITIONER que completa el sistema.</p>', 0, '', '2019-10-09 20:46:35', '', 'admin'),
(81, '', 'AVIZOR GP CONDITIONER', 'AVIZOR GP CONDITIONER.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'AVIZOR GP CONDITIONER.jpg', '', '', '', '', '', '<p class=\"Textbody\">Una vez que se han limpiado las lentes con AVIZOR GP CLEANER, &eacute;stas necesitan ser aclaradas y humectadas.</p>\r\n<p>El humectante AVIZOR GP CONDITIONER cubre las lentes con una pel&iacute;cula lubricante para asegurar el m&aacute;ximo confort. Su PH y su presi&oacute;n osm&oacute;tica hacen la soluci&oacute;n ideal para guardar y conservar las lentes r&iacute;gidas y permeables al gas.</p>', 0, '', '2019-10-09 21:15:27', '', 'admin'),
(82, '', 'A5', 'A5.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 12, 0, 0, 1, 'url_name', 'A5_paradescripcion.jpg', '', '', '', '', '', '<p>La interfaz de usuario del A5 y su dise&ntilde;o ergon&oacute;mico simplifica el flujo de trabajo. Gracias a la pantalla t&aacute;ctil de 15&rdquo; el personal cl&iacute;nico puede seleccionar el ajuste de la ventilaci&oacute;n f&aacute;cilmente, lo que permite invertir menos tiempo en el manejo y m&aacute;s en el cuidado al paciente. El A5 proporciona una gama de modos de ventilaci&oacute;n que garantizan un cuidado efectivo para todos los pacientes sea cual sea su gravedad, se ajusta al perfil del dominio Patient Care (PCD) de IHE (Integrando las empresas sanitarias). A5 puede producir datos en el protocolo de industria HL7. El A5 dispone de una amplia superficie de trabajo, un c&oacute;modo reposapi&eacute;s, un freno central y un organizador de cables que aumentan la movilidad del dispositivo.</p>', 0, '', '2019-10-09 23:27:27', '', 'admin');
INSERT INTO `goba_productos` (`ID`, `clave`, `nombre`, `cover`, `foto`, `descripcion`, `marca`, `tipo`, `precio`, `moneda`, `unidad`, `stock`, `ID_cate`, `ID_sub_cate`, `ID_sub_cate2`, `ID_marca`, `visible`, `url_name`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `cate`, `resena`, `land`, `file`, `alta`, `fmod`, `user`) VALUES
(83, '', 'A7', 'A7.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 12, 0, 0, 1, 'url_name', 'A7_paradescripcion.jpg', '', '', '', '', '', '<p>Una gama completa de modos de ventilaci&oacute;n grado ICU cumple con los requerimientos a trav&eacute;s de todas las etapas de la anestesia, PSV + Apnea, PVC, PVC-VG, CPAP/PS, SIMV-VG, SIMV-PC, SIMV-VC,&nbsp; Mezclador de gas digital con flujo bajo de seguridad de Optimizador, Monitoreo preciso, Anestesia visible, Predicci&oacute;n de AA, Predicci&oacute;n de O2, Reclutamiento pulmonar, Monitoreo NMT para un tiempo de intubaci&oacute;n &oacute;ptimo, Autoprueba visual y alarma inteligente, Experiencia de usuario sin parang&oacute;n, Touch &amp; Beyong completo, Ajuste f&aacute;cil de FGF.</p>', 0, '', '2019-10-10 12:16:04', '', 'admin'),
(84, '', 'WATO EX-35', 'WATO EX-35.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 12, 0, 0, 1, 'url_name', 'WATO-EX-35_paradescripcion.jpg', '', '', '', '', '', '<p>El nuevo WATO EX-35 permite al usuario hacer ajustes al modo ventilador y a la configuraci&oacute;n del monitor m&aacute;s f&aacute;cilmente que nunca en un producto de este nivel, El nuevo sistema de alarma inteligente de respuesta t&aacute;ctil proporciona informaci&oacute;n detallada en tiempo real que alerta de manera r&aacute;pida y clara al m&eacute;dico cl&iacute;nico de situaciones adversas, lo que resalta la necesidad de una interacci&oacute;n y resoluci&oacute;n temprana. WATO EX-35 admite una extensa gama de caracter&iacute;sticas y funcionalidades, lo que asegura que todos los procedimientos de anestesia se realicen de manera segura, eficiente y efectiva. Las caracter&iacute;sticas mejoradas incluyen, pero no est&aacute;n limitadas a: M&oacute;dulo AG integrado &ldquo;Plug-and-play&rdquo; (auto ID de 5 agentes y N2O), Ventilador multimodo integrado que ofrece modos controlados y compatibles de ventilaci&oacute;n.</p>', 0, '', '2019-10-10 12:17:41', '', 'admin'),
(85, '', 'WATO EX-65PRO', 'WATO EX-65PRO.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 12, 0, 0, 1, 'url_name', 'WATO-EX-65PRO_paradescripcion.jpg', '', '', '', '', '', '<p>El nuevo WATO EX-65 Pro le permite controlar de manera precisa el sistema, as&iacute; como controlar diferentes tipos de pacientes f&aacute;cilmente. Visualizaci&oacute;n del procedimiento de autoprueba del sistema y de la gesti&oacute;n de alarma inteligente con gr&aacute;ficos y tablas para simplificar pasos complicados de una operaci&oacute;n. Con una pantalla de alta resoluci&oacute;n de 15,1 pulgadas y una interfaz de usuario t&aacute;ctil intuitiva. WATO EX-65 Pro hace m&aacute;s visible el proceso de la anestesia. Siendo una estaci&oacute;n de trabajo de anestesia multifuncional, WATO EX-65 Pro est&aacute; dise&ntilde;ado pensando en los costos, Se hace posible un flujo m&aacute;s bajo con el Optimizador, el cual ayuda a reducir el uso de agentes anest&eacute;sicos. Los sensores de flujo con una vida &uacute;til mucho m&aacute;s larga hacen que el mantenimiento sea mucho m&aacute;s f&aacute;cil.</p>', 0, '', '2019-10-10 12:18:57', '', 'admin'),
(86, '', 'HYLED 7', 'HYLED7_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 16, 0, 0, 1, 'url_name', '1.HYLED7_paradescripcion.jpg', '2.HYLED7_paradescripcion.jpg', '3.HYLED7_paradescripcion.jpg', '4.HYLED7_paradescripcion.jpg', '5.HYLED7_paradescripcion.jpg', '', '<p>Con la tecnolog&iacute;a LED de vanguardia, la serie HyLED 7 ofrece una excelente eficiencia luminosa, una fuente de luz sin calor y una incre&iacute;ble vida &uacute;til prolongada, que le brinda soporte total en quir&oacute;fano, con tiempo de servicio prolongado de hasta 60,000 horas, Iluminaci&oacute;n central 160,000 lux / 130,000 lux, Di&aacute;metro ajustable del campo de luz de 195-300 mm, Perfectamente integrado en el flujo laminar (Certificado seg&uacute;n DIN-1946 Parte 4),Panel de pantalla t&aacute;ctil para un control m&aacute;s f&aacute;cil, C&aacute;mara HD integrada giratoria de 330 &deg;.</p>', 0, '', '2019-10-10 12:23:28', '', 'admin'),
(87, '', 'HYLED 8', 'HYLED8_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 16, 0, 0, 1, 'url_name', '1.HYLED8_paradescripcion.jpg', '2.HYLED8_paradescripcion.jpg', '3.HYLED8_paradescripcion.jpg', '4.HYLED8_paradescripcion.jpg', '5.HYLED8_paradescripcion.jpg', '', '<p>Con tecnolog&iacute;a LED de vanguardia, la serie HyLED 8 ofrece una excelente eficiencia luminosa, una fuente de luz sin calor y una incre&iacute;ble vida &uacute;til prolongada, lo que le brinda un soporte total en quir&oacute;fano, Incre&iacute;ble vida &uacute;til prolongada: hasta 60,000 horas, Forma ergon&oacute;mica \"Trigon\" para una mejor compatibilidad con sistemas de flujo laminar, Di&aacute;metro de campo de luz ajustable de 180-280 mm. Temperatura de color ajustable como configuraci&oacute;n opcional (3000-5000K) para una mejor diferenciaci&oacute;n de tejidos, Brazo portador de 2 megap&iacute;xeles HD y sistema de c&aacute;mara HD integrado.</p>', 0, '', '2019-10-10 12:27:31', '', 'admin'),
(88, '', 'HYLED 9', 'HYLED9_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 16, 0, 0, 1, 'url_name', '1.HYLED9_paradescripcion.jpg', '2.HYLED9_paradescripcion.jpg', '3.HYLED9_paradescripcion.jpg', '4.HYLED9_paradescripcion.jpg', '5.HYLED9_paradescripcion.jpg', '', '<p>Con tecnolog&iacute;a LED de vanguardia, la serie HyLED 9 proporciona una iluminaci&oacute;n excelente, un dise&ntilde;o ergon&oacute;mico y compacto y una larga vida &uacute;til, lo que le brinda un soporte total en OR , Iluminaci&oacute;n central premium hasta 160,000lux, Ajuste del campo de luz el&eacute;ctrica para luz homog&eacute;nea en el &aacute;rea de cirug&iacute;a, Temperatura de color variable para la percepci&oacute;n de verdaderos colores de tejido (opcional), Dise&ntilde;o en forma de cruz para una mejor compatibilidad de flujo laminar, Panel de pantalla t&aacute;ctil ergon&oacute;mico (opcional), Sistema de c&aacute;mara HD de 2 megap&iacute;xeles, Larga vida &uacute;til de hasta 60,000 horas.</p>', 0, '', '2019-10-10 12:43:46', '', 'admin'),
(89, '', 'HYBASE 3000', '2.menu..jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 14, 0, 0, 1, 'url_name', '1HYBASE3000_paradescripcion.jpg', '2HYBASE3000._paradescripcion.jpg', '3HYBASE3000_paradescripcion.jpg', '4HYBASE3000_paradescripcion.jpg', '', '', '<p>Dise&ntilde;ado teniendo en cuenta las necesidades de los cirujanos y las enfermeras, Mindray ha respondido a las demandas de una soluci&oacute;n completa de mesa de operaciones. El HyBase 3000 rentable cumple con los requisitos de diferentes cirujanos, Colch&oacute;n de descompresi&oacute;n con dise&ntilde;o antiest&aacute;tico, impermeable y sin costuras, Desplazamiento longitudinal el&eacute;ctrico con 300 mm., Capacidad de peso de 185 Kg en posici&oacute;n normal, Energ&iacute;a de la bater&iacute;a para 50-80 operaciones, Regrese a la posici&oacute;n original con un bot&oacute;n, Placa de mesa radiotransparente para acceso gratuito al brazo en C, Tres paneles de control que incluyen panel remoto, control de respaldo e interruptor de pie.</p>', 0, '', '2019-10-10 12:54:54', '', 'admin'),
(90, '', 'IMEC SERIES', 'IMECSERIES_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 15, 0, 0, 1, 'url_name', '1.._paradescripcion.jpg', '1.IMEC_SERIES_paradescripcion.jpg', '2.IMEC_SERIES_paradescripcion.jpg', '4.IMEC_SERIES_paradescripcion.jpg', '5.IMEC_SERIES_paradescripcion.jpg', '', '<p>Gracias al dise&ntilde;o optimizado del hardware, el monitor iMEC consume un 50% menos que los monitores de paciente convencionales. De este modo, la vida &uacute;til de la bater&iacute;a aumenta y se puede prescindir del ventilador, por lo que su funcionamiento es m&aacute;s limpio y menos ruidoso, su estructura es ligera y delgada muy resistente, por lo que su transporte resulta muy sencillo. El dise&ntilde;o sin ventilador ayuda a reducir la contaminaci&oacute;n ac&uacute;stica, De 2,6 a 3,6 kg de peso, con asa de transporte, id&oacute;neo para desplazamientos, Pantalla t&aacute;ctil con una resoluci&oacute;n de 800x600 y un m&aacute;ximo de 8 trazas, la bater&iacute;a de i&oacute;n-litio permite un funcionamiento continuo m&aacute;ximo de 4 horas.</p>', 0, '', '2019-10-10 13:23:37', '', 'admin'),
(91, '', 'PM-60', 'PM-60_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 15, 0, 0, 1, 'url_name', '1.PM60_paradescripcion.jpg', '2.PM60_paradescripcion.jpg', '3.PM60_paradescripcion.jpg', '5.PM60_paradescripcion.jpg', '6PM60_paradescripcion.jpg', '', '<p>Es un dispositivo en miniatura y liviano capaz de verificar y monitorear continuamente la SpO2 y la frecuencia del pulso. Para verificaciones simples de signos vitales, el modo de verificaci&oacute;n puntual ofrece funciones b&aacute;sicas que incluyen la asignaci&oacute;n autom&aacute;tica de identificaciones de pacientes, el estado de supresi&oacute;n de alarma y las funciones de espera autom&aacute;tica y apagado, adecuado para adultos, pediatr&iacute;a y pacientes neonatales, realiza la medici&oacute;n de SpO2 y frecuencia del pulso en modo spot-check o continuo, la pantalla LCD en color de 2.4 \"muestra de forma destacada las lecturas de SpO2 y frecuencia del pulso,alarmas visuales y audibles ajustables: puede elegir si mostrar los l&iacute;mites de alarma de SpO2 y frecuencia del pulso en la pantalla LCD.</p>', 0, '', '2019-10-10 13:43:18', '', 'admin'),
(92, '', 'DC70', 'DC70_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 27, 0, 0, 1, 'url_name', '1_paradescripcion.jpg', '2_paradescripcion.jpg', '3_paradescripcion.jpg', '4_paradescripcion.jpg', '5_paradescripcion.jpg', '', '', 0, '', '2019-10-10 13:56:20', '', 'admin'),
(93, '', 'DC-N6', 'DCN6_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 27, 0, 0, 1, 'url_name', '1_paradescripcion.jpg', '2_paradescripcion.jpg', '3_paradescripcion.jpg', '4_paradescripcion.jpg', '5_paradescripcion.jpg', '', '', 0, '', '2019-10-10 15:15:10', '', 'admin'),
(94, '', 'M7', 'M_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 27, 0, 0, 1, 'url_name', '1_paradescripcion.jpg', '2_paradescripcion.jpg', '3_paradescripcion.jpg', '4_paradescripcion.jpg', '5_paradescripcion.jpg', '', '', 0, '', '2019-10-10 15:19:46', '', 'admin'),
(95, '', 'SV 300', 'SV300_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 13, 0, 0, 1, 'url_name', 'SV-300-1_paradescripcion.jpg', 'SV-300-2_paradescripcion.jpg', 'SV-300-3_paradescripcion.jpg', 'SV-300-4_paradescripcion.jpg', 'SV-300-5_paradescripcion.jpg', '', '<p>El SV300 es un ventilador de vanguardia con una configuraci&oacute;n simple, f&aacute;cil de manejar y de uso vers&aacute;til. Trata a los pacientes pedi&aacute;tricos y adultos con todos los niveles de agilidad en cuidados intensivos e intermedios. Equipado con funciones que normalmente se encuentran en ventiladores de cuidado intensivo, el SV300 incorpora modos de ventilaci&oacute;n extensivos, y las funciones &uacute;nicas lo convierten en un ventilador de alto nivel, Medici&oacute;n de CO2 volum&eacute;trica, indicadores de destete, Terapia O2, Intelli Cycel.</p>', 0, '', '2019-10-10 15:50:13', '', 'admin'),
(96, '', 'SV 600-800', 'SV600-800_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 13, 0, 0, 1, 'url_name', 'SV-600-1_paradescripcion.jpg', 'SV-600-2_paradescripcion.jpg', 'SV-600-3_paradescripcion.jpg', 'SV-600-4_paradescripcion.jpg', 'SV-600-5_paradescripcion.jpg', '', '<p>Un ventilador de alta gama con pantalla panor&aacute;mica HD 1080P combina una interfaz de usuario personalizada e intuitiva con potentes herramientas y m&oacute;dulos de asistencia. Los modos de ventilaci&oacute;n y las herramientas de soporte de decisiones como el Asistente inteligente se desarrollan sobre la base de las necesidades cl&iacute;nicas y las pautas profesionales para ayudar al personal m&eacute;dico a tomar decisiones cl&iacute;nicas con calma. Una amplia gama de modos de ventilaci&oacute;n. Soluci&oacute;n de ventilaci&oacute;n inteligente: AMV TM + IntelliCycle TM, Soluci&oacute;n de emergencia: CPRVTM, R&eacute;gimen de tratamiento secuencial: ventilaci&oacute;n no invasiva y oxigenoterapia de alto flujo, Mediciones de presi&oacute;n auxiliar de doble canal, Kit de protecci&oacute;n pulmonar</p>', 0, '', '2019-10-10 22:55:30', '', 'admin'),
(97, '', 'BENEHEART D1', 'BeneheartD!_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 17, 0, 0, 1, 'url_name', '1.BENEHEART D1_paradescripcion.jpg', '2.BENEHEART D1_paradescripcion.jpg', '3.BENEHEART D1_paradescripcion.jpg', '4.BENEHEART D1_paradescripcion.jpg', '5.BENEHEART D1_paradescripcion.jpg', '', '<p>Es un potente desfibrilador externo autom&aacute;tico (DEA). Adem&aacute;s de ayudarle a lo largo de todo el proceso, BeneHeart D1 es ligero, compacto, resistente y muy f&aacute;cil de utilizar ofreci&eacute;ndole todas las funciones DEA est&aacute;ndar. Las funciones inteligentes y la interfaz de usuario intuitiva hacen del D1 una herramienta eficaz y flexible para los profesionales sanitarios permiti&eacute;ndoles salvar vidas, Almohadillas preconectadas que permiten ahorrar tiempo, Cambio muy sencillo entre pacientes infantiles y adultos con solo pulsar un bot&oacute;n, Identificaci&oacute;n autom&aacute;tica del tipo de paciente a trav&eacute;s del conector de la almohadilla. De esa forma, se suministra una cantidad de energ&iacute;a menor a los pacientes infantiles, Modo manual real con selecci&oacute;n de nivel de energ&iacute;a, que permite completar el proceso de tres pasos para transmitir al paciente descargas el&eacute;ctricas de alta calidad, Supervisi&oacute;n ECG de 3 derivaciones independiente mediante diferentes conectores, lo que le permite visualizar m&aacute;s derivaciones de ECG o arritmias mientras se usan las almohadillas, El peque&ntilde;o y compacto D1 tambi&eacute;n ofrece cardioversi&oacute;n sincronizada.</p>', 0, '', '2019-10-10 23:00:10', '', 'admin'),
(98, '', 'BENEHEART D3', 'BenehearthD3_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 17, 0, 0, 1, 'url_name', '1BENEHEART D3_paradescripcion.jpg', '2BENEHEART D3_paradescripcion.jpg', '3BENEHEART D3_paradescripcion.jpg', '4BENEHEART D3_paradescripcion.jpg', '5BENEHEART D3_paradescripcion.jpg', '', '<p>BeneHeart D3 es un desfibrilador compacto, duradero y ligero que integra monitorizaci&oacute;n, desfibrilaci&oacute;n manual, DEA y marcapasos. Se trata de un desfibrilador-monitor bif&aacute;sico y profesional apropiado para su uso en hospitales y cl&iacute;nicas de todo el mundo, Dise&ntilde;o compacto 4 en 1 que integra: monitorizaci&oacute;n, desfibrilaci&oacute;n manual, DEA y marcapasos, Pantalla grande y n&iacute;tida con 3 formas de onda que garantiza una &oacute;ptima y f&aacute;cil visualizaci&oacute;n de ECG y SPO2, Desfibrilaci&oacute;n, cardioversi&oacute;n sincronizada y DEA con tecnolog&iacute;a bif&aacute;sica, Hasta 360 J de dosis de energ&iacute;a para maximizar el &eacute;xito de la desfibrilaci&oacute;n, Gran capacidad de alimentaci&oacute;n con bater&iacute;a que permite la monitorizaci&oacute;n continua y de larga duraci&oacute;n y descargas durante el transporte sin una fuente de alimentaci&oacute;n externa, Dise&ntilde;o compacto y ligero especial para aplicaciones tanto en hospitales como en cl&iacute;nicas.</p>', 0, '', '2019-10-10 23:48:51', '', 'admin'),
(99, '', 'BENEHEART D6', 'BENEHEARTD6_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 17, 0, 0, 1, 'url_name', '1BENEHEART D6_paradescripcion.jpg', '2BENEHEART D6_paradescripcion.jpg', '3BENEHEART D6_paradescripcion.jpg', '4BENEHEART D6_paradescripcion.jpg', '5BENEHEART D6_paradescripcion.jpg', '', '<p>BeneHeart D6, un desfibrilador-monitor bif&aacute;sico profesional, responde a los requisitos de los profesionales m&eacute;dicos de los hospitales y las cl&iacute;nicas de todo el mundo, Gracias a sus diferentes modos de funcionamiento (desfibrilaci&oacute;n manual, DEA, marcapasos y modo de monitorizaci&oacute;n), tendr&aacute; el control de cualquier situaci&oacute;n, Dise&ntilde;o 4 en 1 que incluye monitorizaci&oacute;n, desfibrilaci&oacute;n manual, DEA y marcapasos, Pantalla grande y de colores vivos con 4 formas de onda que garantiza la visualizaci&oacute;n de ECG y las constantes vitales, Dise&ntilde;o compacto: f&aacute;cil de transportar y usar, Desfibrilaci&oacute;n, cardioversi&oacute;n sincronizada y DEA con tecnolog&iacute;a bif&aacute;sica, Hasta 360 J de dosis de energ&iacute;a para maximizar el &eacute;xito de la desfibrilaci&oacute;n.</p>', 0, '', '2019-10-10 23:52:47', '', 'admin'),
(100, '', 'ENDOSCOPIA HD3', 'ENDOSCOPIS_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 28, 0, 0, 1, 'url_name', '1ENDOSCOPIA HD3_paradescripcion.jpg', '2ENDOSCOPIA HD3_paradescripcion.jpg', '3ENDOSCOPIA HD3_paradescripcion.jpg', '4ENDOSCOPIA HD3_paradescripcion.jpg', '', '', '<p>El sistema de c&aacute;mara full HD de 3 chips ofrece alta definici&oacute;n y una calidad de imagen excepcional que respalda su funcionamiento con la funci&oacute;n de almacenamiento USB, con una lista de funciones de Unidad de control de c&aacute;mara Full HD, 3CMOS Cabezal de C&aacute;mara, Laparoscopios HD sin distorsi&oacute;n, HB 300X, HB 200L.</p>', 0, '', '2019-10-10 23:57:24', '', 'admin'),
(101, '', 'SOFLENS 59', 'SOFLENS 59.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'SOFLENS-59_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\">Lente de contacto de hidrogel con 59% de agua y resistencia al acumulo de restos lip&iacute;dicos en la superficie de la lente.</p>', 0, '', '2019-10-11 13:58:52', '', 'admin'),
(102, '', 'PUREVISION 2', 'PUREVISION 2.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'PUREVISION-2_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\">Lente de contacto de hidrogel de silicona con &oacute;ptica High Definition&reg;para ofrecer una visi&oacute;n clara y n&iacute;tida incluso en condiciones de baja iluminaci&oacute;n.</p>\r\n<p class=\"Textbody\">Aprobada para uso continuado durante 30 d&iacute;as</p>', 0, '', '2019-10-11 17:59:44', '', 'admin'),
(103, '', 'PUREVISION 2 ASTIGMATISMO', 'PUREVISION 2 ASTIGMATISMO.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'PUREVISION-2-ASTIGMATISMO_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\">Lente de hidrogel de silicona con &oacute;ptica High Definition&reg; para una visi&oacute;n clara y n&iacute;tida de forma constante.</p>', 0, '', '2019-10-11 18:01:48', '', 'admin'),
(104, '', 'OPTIMA 38', 'OPTIMA 38.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'OPTIMA-38_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\">Lente de contacto blando esf&eacute;rico de uso diario y remplazo anual para corregir miop&iacute;a.</p>\r\n<p>Ideal para pacientes que buscan un lente anual con excelente calidad.</p>', 0, '', '2019-10-11 18:04:58', '', 'admin'),
(105, '', 'STAR COLORS', 'STAR COLORS.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'STAR-COLORS_paradescripcion.jpg', '', '', '', '', '', '<p>Lentes de contacto blandos de color, sin graduaci&oacute;n, con gran variedad de tonalidades que brindan un cambio y definici&oacute;n natural de color para tus ojos.</p>', 0, '', '2019-10-11 18:08:33', '', 'admin'),
(106, '', 'LUNARE', 'LUNARE.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'LUNARE_paradescripcion.jpg', '', '', '', '', '', '<p>Los lentes de contacto blandos para Miop&iacute;a y uso cosm&eacute;tico Lunare Graduados, est&aacute;n fabricados con la tecnolog&iacute;a TKI (Tri Color Interior) con tres capas de color que ofrece un aspecto m&aacute;s natural en tus ojos y mayor comodidad.</p>', 0, '', '2019-10-11 18:14:55', '', 'admin'),
(107, '', 'RENUFRESH', 'RENU FRESH.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'RENU-FRESH_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\"><strong>Ayuda a mantener la comodidad de los lentes de contacto y cuida la salud de sus ojos. Limpia, desinfecta enjuaga, humecta, conserva y remueve prote&iacute;nas. &iexcl;Todo en una sola botella!</strong></p>\r\n<p class=\"Textbody\">Fue la primera soluci&oacute;n multiprop&oacute;sito en M&eacute;xico y es la de mayor venta en el mercado.</p>\r\n<p>Tambi&eacute;n es compatible con lentes de hidrogel de silic&oacute;n (lentes con los que puedes dormir).</p>', 0, '', '2019-10-11 18:16:50', '', 'admin'),
(108, '', 'BIOTRUE', 'BIOTRUE.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'BIOTRUE_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\">Es una soluci&oacute;n &uacute;nica inspirada en la biolog&iacute;a de los ojos.<strong></strong></p>\r\n<ul>\r\n<li>Permite un uso confortable de las lentes durante todo el d&iacute;a, ya que aporta hasta 20 horas de hidrataci&oacute;n y una eficacia en desinfecci&oacute;n excepcional.</li>\r\n<li>Soluci&oacute;n para lentes de contacto blandos incluyendo lentes de hidrogel de silic&oacute;n.</li>\r\n</ul>', 0, '', '2019-10-11 18:19:27', '', 'admin'),
(109, '', 'SIMPLUS', 'SIMPLUS.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'SIMPLUS_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\">Es una soluci&oacute;n de acci&oacute;n m&uacute;ltiple que remueve prote&iacute;nas, limpia, desinfecta, acondiciona y enjuaga, haciendo m&aacute;s c&oacute;modo el uso de lentes de contacto r&iacute;gidos.</p>\r\n<p>Es la soluci&oacute;n l&iacute;der para limpieza de lentes de contacto r&iacute;gidos.</p>', 0, '', '2019-10-11 18:20:05', '', 'admin'),
(110, '', 'GOTAS LUBRICANTES', 'GOTAS LUBRICANTES.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 18, 0, 0, 1, 'url_name', 'GOTAS-LUBRICANTES_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Textbody\"><strong>Recomendadas para la correcta lubricaci&oacute;n y humectaci&oacute;n de todo tipo de lentes de contacto blandos, incluyendo los de hidrogel de silic&oacute;n.</strong></p>\r\n<p>Ayudan a minimizar la resequedad de los lentes mediante su lubricaci&oacute;n y humecta sus ojos mientras tiene puestos los lentes de contacto.</p>', 0, '', '2019-10-11 18:23:15', '', 'admin'),
(111, '', 'E5', 'E5_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 13, 0, 0, 1, 'url_name', '1E5_paradescripcion.jpg', '2.E5_paradescripcion.jpg', '3.E5_paradescripcion.jpg', '4.E5_paradescripcion.jpg', '5.E5_paradescripcion.jpg', '', '<p>El SynoVent E5 es el ventilador que necesita con la interfaz que desea. No solo incluye la funcionalidad avanzada del ventilador para pacientes que van desde beb&eacute;s hasta adultos, sino tambi&eacute;n una interfaz moderna y f&aacute;cil de usar. La pantalla se puede configurar para satisfacer los gustos y necesidades de cada m&eacute;dico, y proporciona un control r&aacute;pido y f&aacute;cil sobre los par&aacute;metros y configuraciones del ventilador. La pantalla de 12.1 \' Un total de 6 conexiones externas aseguran que los datos se puedan almacenar o compartir de varias maneras diferentes. Con la serie de monitores de pacientes Beneview, la serie E de SynoVent puede conectarse sin problemas a los sistemas HIS y CIS con facilidad.</p>', 0, '', '2019-10-14 20:13:40', '', 'admin'),
(112, '', 'DR 10E', 'menu_10e.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 29, 0, 0, 1, 'url_name', '1.10e.jpg', '2.10e.jpg', '3.10e.jpg', '', '', '', '<p>Detector digital DR 10e ofrece a las instalaciones de radiograf&iacute;a generales todas las ventajas de Direct Digital y maximiza el uso de sus inversiones existentes en rayos X, incluido el equipo m&oacute;vil de im&aacute;genes. El marco liviano de aleaci&oacute;n de magnesio Rib y la tecnolog&iacute;a inal&aacute;mbrica mejoran la comodidad del operador y la flexibilidad del examen, con 10 &times; 12 pulgadas, el DR 10e cabe en bandejas de incubadora, lo que lo hace conveniente para im&aacute;genes neonatales y pedi&aacute;tricas, as&iacute; como para extremidades y ex&aacute;menes especiales.</p>', 0, '', '2019-10-14 21:18:42', '', 'admin'),
(113, '', 'DR 14E', 'MENU_14e.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 29, 0, 0, 1, 'url_name', '1.14e.jpg', 'dr14e-2.jpg', 'dr14e-3.jpg', '', '', '', '<p>Es una parte integral de una soluci&oacute;n Agfa HealthCare Instant DR, que incluye el software de adquisici&oacute;n de im&aacute;genes NX con procesamiento y detector MUSICA. Detector digital DR 14e ofrece a las instalaciones de radiograf&iacute;a generales todas las ventajas de Direct Digital, a la vez que maximiza el uso de su equipo existente, tanto para sistemas de rayos X digitales convencionales como m&oacute;viles.</p>', 0, '', '2019-10-14 22:44:20', '', 'admin'),
(114, '', 'DR 17E', 'MENU_dr17e.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 29, 0, 0, 1, 'url_name', '1.dr17e.jpg', '2.dr17e.jpg', '3dr17e.jpg', '', '', '', '<p>Es una parte integral de una soluci&oacute;n Agfa HealthCare Instant DR, que incluye el software de adquisici&oacute;n de im&aacute;genes NX con procesamiento MUSICA y el detector. Con su conveniente tama&ntilde;o de 17 &times; 17 pulgadas y su dise&ntilde;o robusto y est&eacute;ticamente agradable, el DR 17e es ideal para todos los ex&aacute;menes Genrad, las im&aacute;genes se pueden enviar m&aacute;s r&aacute;pidamente a un PACS o generador de im&aacute;genes en formato DICOM.</p>', 0, '', '2019-10-14 22:46:50', '', 'admin'),
(115, '', 'DR 18M', 'MENU_dr18m.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 29, 0, 0, 1, 'url_name', '1.dr18m.jpg', '2.dr18m.jpg', '3.dr18m.jpg', '', '', '', '<p>El detector de rayos X digital de panel plano DR 18M est&aacute; dise&ntilde;ado para mamograf&iacute;as de radiograf&iacute;a digital (DR). Admite una actualizaci&oacute;n f&aacute;cil y neutral para el vendedor de sus modalidades de mamograf&iacute;a de rayos X anal&oacute;gicas a radiograf&iacute;a digital (DR).&nbsp;El detector de tama&ntilde;o de cassette est&aacute; dise&ntilde;ado para ajustarse a la mamograf&iacute;a anal&oacute;gica est&aacute;ndar de 18 &times; 24 buckys. Parte de la soluci&oacute;n de mamograf&iacute;a DR de Agfa, incluye la estaci&oacute;n de trabajo de adquisici&oacute;n de im&aacute;genes NX con procesamiento de im&aacute;genes MUSICA.</p>', 0, '', '2019-10-14 23:15:00', '', 'admin'),
(116, '', 'DR 24M', 'MENU_dr24m.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 29, 0, 0, 1, 'url_name', '1.dr24m.jpg', '2.dr24m.jpg', '3.dr42m.jpg', '', '', '', '<p>El detector de rayos X digital de panel plano DR 24M est&aacute; dise&ntilde;ado para mamograf&iacute;as de radiograf&iacute;a digital (DR). El detector DR 24M admite una actualizaci&oacute;n f&aacute;cil y neutral para el vendedor de sus modalidades de mamograf&iacute;a de rayos X anal&oacute;gicas a radiograf&iacute;a digital (DR). El detector de tama&ntilde;o de cassette est&aacute; dise&ntilde;ado para adaptarse a la mamograf&iacute;a anal&oacute;gica est&aacute;ndar de 24 &times; 30 buckys., ofrece todas las ventajas de Direct Digital, al tiempo que le permite maximizar el uso de su equipo existente.&nbsp;Parte de la soluci&oacute;n de mamograf&iacute;a DR de Agfa, incluye la estaci&oacute;n de trabajo de adquisici&oacute;n de im&aacute;genes NX con procesamiento de im&aacute;genes MUSICA.</p>', 0, '', '2019-10-14 23:25:06', '', 'admin'),
(117, '', 'HYBASE 6100', 'HYBASE600_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 14, 0, 0, 1, 'url_name', '1HYBASE6100_paradescripcion.jpg', '2HYBASE6100_paradescripcion.jpg', '3HYBASE6100_paradescripcion.jpg', '4HYBASE6100_paradescripcion.jpg', '5HYBASE6100_paradescripcion.jpg', '', '<p>Dise&ntilde;ado para casos de cirug&iacute;as cada vez m&aacute;s complejas, HyBase 6100 exige un tiempo y un esfuerzo m&iacute;nimo para posicionar al paciente de forma eficiente, segura y confortable. Mindray ha dado respuesta a las demandas de una soluci&oacute;n para mesas de operaciones, ya que ofrece: Colch&oacute;n de descompresi&oacute;n para mayor comodidad de los pacientes para eliminar la incidencia de la &uacute;lcera de dec&uacute;bito, con un dise&ntilde;o impermeable, antiest&aacute;tico y sin costuras ,Alto nivel de capacidad de peso en posici&oacute;n normal y reversa, La bater&iacute;a interna mantiene la alimentaci&oacute;n de energ&iacute;a durante 50-80 operaciones, La posici&oacute;n m&aacute;s baja de la mesa es de 600 mm luego de regular la posici&oacute;n del paciente, lo que hace que resulte adecuada para neurocirug&iacute;a y cirug&iacute;a m&iacute;nimamente invasiva, Sistema de freno el&eacute;ctrico para lograr una &oacute;ptima estabilidad, Regreso a la posici&oacute;n original con solo oprimir un bot&oacute;n, Opci&oacute;n con mesa de fibra de carbono.</p>', 0, '', '2019-10-14 23:50:17', '', 'admin'),
(118, '', 'HYBASE 8300-8500', 'HYBASE8300-500_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 14, 0, 0, 1, 'url_name', '1HYBASE8300-8500_paradescripcion.jpg', '2HYBASE8300-8500_paradescripcion.jpg', '3HYBASE8300-8500_paradescripcion.jpg', '4HYBASE8300-8500_paradescripcion.jpg', '5HYBASE8300-8500_paradescripcion.jpg', '', '<p>La mesa electro-hidr&aacute;ulica es una soluci&oacute;n completa para disciplinas quir&uacute;rgicas, reemplazando los costos de la compra de una mesa adicional para el hospital con la disponibilidad de m&oacute;dulos espec&iacute;ficos, con un sistema de reconocimiento de m&oacute;dulo integrado., ofrece, soporte para piernas de doble anclaje, soporte para piernas corto el&eacute;ctrico- hidr&aacute;ulico para ajuste individual o sincr&oacute;nico, Deslizamiento longitudinal motorizado para brazo-C, Soporte bajo para espalda con corte ginecol&oacute;gico, 1 bandeja para extensi&oacute;n para m&oacute;dulos adicionales, Soporte alto para m&oacute;dulos adicionales , Soporte alto para espalda para varias necesidades de disciplinas quir&uacute;rgicas, Soporte de la cabeza de doble anclaje.</p>', 0, '', '2019-10-15 00:09:35', '', 'admin'),
(119, '', 'BENEVIEW T5, T6, T8, T9', 'BENEVIEW_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 4, 15, 0, 0, 1, 'url_name', '1.BENEVIEW_paradescripcion.jpg', '2.BENEVIEW_paradescripcion.jpg', '3.BENEVIEW_paradescripcion.jpg', '4.BENEVIEW_paradescripcion.jpg', '5.BENEVIEW_paradescripcion.jpg', '', '<p>La serie BeneView se ha dise&ntilde;ado para que lleve a cabo una completa monitorizaci&oacute;n del paciente al tiempo que integra y muestra informaci&oacute;n de la red hospitalaria y de otros dispositivos de cabecero. De este modo, BeneView funciona como un sofisticado gestor de informaci&oacute;n en &aacute;reas de cuidados intensivos. Gracias a su potente plataforma de monitorizaci&oacute;n y a su dise&ntilde;o totalmente modular, el monitor BeneView puede medir y gestionar de forma simult&aacute;nea un gran n&uacute;mero de par&aacute;metros de monitorizaci&oacute;n, incluso modernas tecnolog&iacute;as como PiCCO2, ScvO2 y BISx/x4, as&iacute; como herramientas cl&iacute;nicas como diagramas de ara&ntilde;a y matriz espectral de densidad (DSA).</p>', 0, '', '2019-10-15 21:43:08', '', 'admin'),
(120, '', 'MEDIOS DE CONTRASTE', 'medios0.png', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 11, 0, 0, 1, 'url_name', 'medios1.jpg', 'medios2.jpg', '', '', '', '', '', 0, '', '2019-10-21 21:13:18', '', 'admin'),
(121, '', 'MEDIMAT', 'medimat_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 1, 1, 0, 0, 1, 'url_name', '1_paradescripcion.jpg', '2_paradescripcion.jpg', '3_paradescripcion.jpg', '4_paradescripcion.jpg', '5_paradescripcion.jpg', '', '<p>El colch&oacute;n, esta espec&iacute;ficamente dise&ntilde;ado para proteger zona de alto riesgo cuenta con el dise&ntilde;o anti dec&uacute;bito, dise&ntilde;ado especial para amortiguar presi&oacute;n en talones, especial para posici&oacute;n de cadera o coxis para reducir riesgo de ulcera por presi&oacute;n, para mayor comodidad en cabeza.</p>', 0, '', '2019-10-22 18:18:04', '', 'admin'),
(122, '', 'AIR OPTIX COLORS', 'AIR OPTIX COLORS.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'AIR-OPTIX-COLORS_paradescripcion.jpg', '', '', '', '', '', '<p>Con los lentes de contacto Air Optix Colors comodos, oxiginables y un rango completo de 9 colores con o sin graduaci&oacute;n visual.</p>', 0, '', '2019-10-29 15:42:18', '', 'admin'),
(123, '', 'AIR OPTIX AQUA', 'AIR OPTIX AQUA.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'AIR-OPTIX-AQUA_paradescripcion.jpg', '', '', '', '', '', '<p class=\"Standard\">Capa de humectaci&oacute;n protectora en la superficie que es resistente a los dep&oacute;sitos. Comodidad consistente desde el 1 hasta el d&iacute;a 30.</p>\r\n<p>Hasta 5 veces m&aacute;s ox&iacute;geno que los lentes de contacto tradicionales.</p>', 0, '', '2019-10-29 15:43:47', '', 'admin'),
(124, '', 'AIR OPTIX AQUA MULTIFOCAL', 'PRESBICIA AIR OPTIX AQUA MULTIFOCAL.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 5, 21, 0, 0, 1, 'url_name', 'PRESBICIA-AIR-OPTIX-AQUA-MULTIFOCAL_paradescripcion.jpg', '', '', '', '', '', '<p>Permite que un amplio rango de poderes se mezclen en todo el lente. Trabaja en conjunto con la funci&oacute;n natural de sus ojos para ofrecerle una visi&oacute;n clara ya sea cerca o lejos</p>', 0, '', '2019-10-29 15:46:14', '', 'admin'),
(125, '', 'CR15-X', 'CR-15X_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 9, 0, 0, 1, 'url_name', 'CR-15X-1_paradescripcion.jpg', 'CR-15X-2_paradescripcion.jpg', 'CR-15X-3_paradescripcion.jpg', 'CR-15X-4_paradescripcion.jpg', 'CR-15X-5_paradescripcion.jpg', '', '<p>Le permite definir su propio flujo de trabajo y adaptarlo cuando lo desee, de manera r&aacute;pida y f&aacute;cil. As&iacute; es el CR 15-X, una soluci&oacute;n vers&aacute;til y flexible que ofrece una alta calidad de imagen, y medidas para seguir siendo liviana y accesible, siendo compacta, r&aacute;pida, que permite una gama completa de aplicaciones, con placas de f&oacute;sforo est&aacute;ndar, el CR 15-X ofrece una excelente calidad de imagen.</p>', 0, '', '2019-11-12 19:12:00', '', 'admin'),
(126, '', 'CR30-XM', 'CR-30-XM_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 9, 0, 0, 1, 'url_name', 'CR-30-XM-1_paradescripcion.jpg', 'CR-30-XM-2_paradescripcion.jpg', 'CR-30-XM-3_paradescripcion.jpg', 'CR-30-XM-4_paradescripcion.jpg', 'CR-30-XM-5_paradescripcion.jpg', '', '<p>Digitalizador de mesa compacta, de una sola ranura, que controla tanto aplicaciones de mamograf&iacute;a digital como de radiograf&iacute;a general. Un digitalizador que cumple con la calidad de alto nivel que exige la mamograf&iacute;a digital y que ofrece una soluci&oacute;n que facilita el cambio del sistema anal&oacute;gico al digital de manera r&aacute;pida y rentable. (Nota: El CR 30-Xm no est&aacute; disponible en EE. UU. Ni Canad&aacute;).</p>', 0, '', '2019-11-12 20:27:53', '', 'admin'),
(127, '', 'DX-M', 'DX-M_paramenu.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 3, 9, 0, 0, 1, 'url_name', 'DX-M-2_paradescripcion.jpg', 'DX-M-3_paradescripcion.jpg', 'DX-M-4_paradescripcion.jpg', 'DX-M-5_paradescripcion.jpg', 'DX-M-6_paradescripcion.jpg', '', '<p>Una soluci&oacute;n de CR con detectores de aguja para alcanzar la alta calidad que exige la mamograf&iacute;a, tiene el potencial de reducir la dosis en todos los estudios, lo que incluye mamograf&iacute;a, neonatolog&iacute;a y pediatr&iacute;a, combina una excelente calidad de imagen con un alto rendimiento, con un &uacute;nico dispensador de cinco casetes y una vista previa muy r&aacute;pida, todos alojados en un sistema compacto.</p>', 0, '', '2019-11-12 20:32:56', '', 'admin'),
(128, '', 'Anaconda - Anaesthetic Conserving Device', 'nodisponible.jpg', 'foto', '', 'marca', 'tipo', '0.00', 'MNX', 'PZ', 0, 6, 0, 0, 0, 1, 'url_name', 'nodisponible1.jpg', '', '', '', '', '', '<p>Anaconda, es un peque&ntilde;o dispostivo m&eacute;dico que se inserta en el circuito de respiraci&oacute;n entre el TET y la pieza Y.</p>', 0, '', '2020-03-13 03:05:55', '', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_productos_cate`
--

CREATE TABLE `goba_productos_cate` (
  `ID_cate` int(6) UNSIGNED NOT NULL,
  `categoria` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `resena` text NOT NULL,
  `ord` int(2) NOT NULL,
  `cover` varchar(150) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_productos_cate`
--

INSERT INTO `goba_productos_cate` (`ID_cate`, `categoria`, `descripcion`, `resena`, `ord`, `cover`, `visible`) VALUES
(1, 'COLCHONES', '<p><strong>Hospitalarios, para Camilla y de Aire</strong></p>', '<p>Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</p>', 1, 'colchones.jpg', 1),
(4, 'QUIROFANO Y SOPORTE DE VIDA', '<p><strong>Hospitalarios, para Camilla y de Aire</strong></p>', '<p>Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</p>', 4, 'QUIROFANO_MENU.jpg', 1),
(2, 'CAMAS', '<p><strong>Hospitalarias, Ginecol&oacute;gicas y Pedri&aacute;ticas</strong></p>', '<p>Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</p>', 2, 'camas.jpg', 1),
(3, 'RAYOS X', '<p><strong>Radiograf&iacute;a Directa y Computarizada</strong></p>', '<p>Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</p>', 3, 'menu-home-rayosx.jpg', 1),
(5, 'OPTICA', '<p><strong>Lentes de Contacto y Cuidado</strong></p>', '<p>Maneja y reduce las dosis de radiaci&oacute;n requerida con varias funciones avanzadas. Proporcina estimaciones de dosis a nivel del paciente y exposiciones m&aacute;ximas recomendadas.</p>', 5, 'menu-home-optica.jpg', 1),
(6, 'SEDANTES', '<p><strong><span class=\"TextRun  BCX0 SCXP1746169\" lang=\"ES-ES\" xml:lang=\"ES-ES\" data-scheme-color=\"@595959,1,18:65000,19:35000\" data-usefontface=\"true\" data-contrast=\"none\"><span class=\"NormalTextRun  BCX0 SCXP1746169\">Innovaci&oacute;n dise&ntilde;ada para facilitar la&nbsp;</span></span><span class=\"TextRun  BCX0 SCXP1746169\" lang=\"ES-ES\" xml:lang=\"ES-ES\" data-scheme-color=\"@595959,1,18:65000,19:35000\" data-usefontface=\"true\" data-contrast=\"none\"><span class=\"NormalTextRun  BCX0 SCXP1746169\">administraci&oacute;n de anest&eacute;sicos inhalados</span></span></strong></p>', '<p><span class=\"TextRun SCXP267490768 BCX0\" lang=\"ES-ES\" xml:lang=\"ES-ES\" data-scheme-color=\"@7F7F7F,0,18:50000\" data-usefontface=\"true\" data-contrast=\"none\"><span class=\"NormalTextRun SCXP267490768 BCX0\">El dispositivo AnaConDa&reg; ha sido desarrollado para&nbsp;</span></span><span class=\"TextRun SCXP267490768 BCX0\" lang=\"ES-ES\" xml:lang=\"ES-ES\" data-scheme-color=\"@7F7F7F,0,18:50000\" data-usefontface=\"true\" data-contrast=\"none\"><span class=\"NormalTextRun SCXP267490768 BCX0\">reemplazar las costosas y complejas m&aacute;quinas de&nbsp;</span></span><span class=\"TextRun SCXP267490768 BCX0\" lang=\"ES-ES\" xml:lang=\"ES-ES\" data-scheme-color=\"@7F7F7F,0,18:50000\" data-usefontface=\"true\" data-contrast=\"none\"><span class=\"NormalTextRun SCXP267490768 BCX0\">anestesia. Su dise&ntilde;o &uacute;nico permite la&nbsp;</span></span><span class=\"TextRun SCXP267490768 BCX0\" lang=\"ES-ES\" xml:lang=\"ES-ES\" data-scheme-color=\"@7F7F7F,0,18:50000\" data-usefontface=\"true\" data-contrast=\"none\"><span class=\"NormalTextRun SCXP267490768 BCX0\">administraci&oacute;n sencilla de Isoflurano y Sevoflurano.</span></span></p>', 6, 'nodisponible.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_productos_coti`
--

CREATE TABLE `goba_productos_coti` (
  `ID_coti` int(11) UNSIGNED NOT NULL,
  `ID_reg` int(11) NOT NULL,
  `ID_cli` int(11) NOT NULL,
  `observaciones` text NOT NULL,
  `Total` decimal(11,2) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `fmod` varchar(20) NOT NULL,
  `user` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_productos_coti_r`
--

CREATE TABLE `goba_productos_coti_r` (
  `ID_reg` int(11) UNSIGNED NOT NULL,
  `articulo` varchar(300) NOT NULL,
  `cant` int(6) NOT NULL,
  `precio` decimal(11,2) NOT NULL,
  `tot` decimal(11,2) NOT NULL,
  `ID_coti` int(11) NOT NULL,
  `ID_cli` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_productos_files`
--

CREATE TABLE `goba_productos_files` (
  `ID_pfile` int(9) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `ID_p` int(9) NOT NULL,
  `ID_f` int(9) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_productos_marcas`
--

CREATE TABLE `goba_productos_marcas` (
  `ID_marca` int(6) UNSIGNED NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_productos_marcas`
--

INSERT INTO `goba_productos_marcas` (`ID_marca`, `nombre`, `visible`) VALUES
(1, 'Samsung', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_productos_sub_cate`
--

CREATE TABLE `goba_productos_sub_cate` (
  `ID_sub_cate` int(6) UNSIGNED NOT NULL,
  `subcategoria` varchar(100) NOT NULL,
  `ord` int(2) NOT NULL,
  `ID_cate` int(6) NOT NULL,
  `cover` varchar(150) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_productos_sub_cate`
--

INSERT INTO `goba_productos_sub_cate` (`ID_sub_cate`, `subcategoria`, `ord`, `ID_cate`, `cover`, `descripcion`, `visible`) VALUES
(1, 'HOSPITALARIOS', 1, 1, 'EffectaCare-20-4_paramenu.jpg', '', 1),
(2, 'PARA CAMILLAS', 2, 1, 'Camilla2.jpg', '', 0),
(3, 'DE AIRE ANTIDECUBITO', 3, 1, 'colchones.jpg', '', 1),
(4, 'ACCESORIOS', 4, 1, 'camas.jpg', '', 0),
(5, 'HOSPITALARIAS', 1, 2, 'Hospitalaria.jpg', '', 1),
(6, 'GINECOL&Oacute;GICAS', 2, 2, 'Ginecologica.jpg', '', 1),
(7, 'PEDI&Aacute;TRICAS', 3, 2, 'SMARTJUNIOR_paramenu.jpg', '', 1),
(8, 'RADIOGRAFIA DIRECTA', 1, 3, 'DR-400_paramenu.jpg', '', 1),
(9, 'RADIOGRAFIA COMPUTARIZADA', 2, 3, 'CR-10X_paramenu.jpg', '', 1),
(10, 'IMPRESORAS', 3, 3, 'AXYS_paramenu.jpg', '', 1),
(11, 'CONSUMIBLES', 4, 3, 'CONSUMIBLES.jpg', '', 1),
(12, 'MAQUINA DE ANESTESIA', 1, 4, 'A5.jpg', '', 1),
(13, 'VENTILADORES', 2, 4, 'ventiladores m.jpg', '', 1),
(14, 'MESAS DE QUIROFANO', 3, 4, 'MENU OK.jpg', '', 1),
(15, 'MONITOR DE SIGNOS VITALES', 4, 4, '1.IMEC.jpg', '', 1),
(16, 'LAMPARAS DE QUIROFANO', 5, 4, 'MENU..jpg', '', 1),
(17, 'DESFIBRILADORES', 6, 4, 'MENU.DESFRIBILADOR.jpg', '', 1),
(18, 'BAUSH+LOMB', 1, 5, 'BAUSH+LOMB-MENU_paradescripcion.jpg', '', 1),
(19, 'JOHNSON &amp; JOHNSON', 2, 5, 'J&J_paramenu.jpg', '', 1),
(20, 'COOPERVISION', 3, 5, 'COOPERVISION_paramenu.jpg', '', 1),
(21, 'ALCON', 4, 5, 'ALCON_paramenu.jpg', '', 1),
(23, 'GERIATRICAS', 4, 2, 'GERIATRICA_paramenu.jpg', '', 1),
(27, 'ULTRASONIDO', 7, 4, 'nodisponible.jpg', '', 0),
(28, 'ENDOSCOPIA', 8, 4, 'MENU.ENDOSCOPIA.jpg', '', 1),
(29, 'DETECTORES', 5, 3, 'DR 2.jpg', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_productos_sub_cate2`
--

CREATE TABLE `goba_productos_sub_cate2` (
  `ID_sub_cate2` int(6) UNSIGNED NOT NULL,
  `subcategoria2` varchar(100) NOT NULL,
  `ord` int(2) NOT NULL,
  `ID_cate` int(6) NOT NULL,
  `ID_sub_cate` int(6) NOT NULL,
  `cover` varchar(150) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_productos_sub_cate2`
--

INSERT INTO `goba_productos_sub_cate2` (`ID_sub_cate2`, `subcategoria2`, `ord`, `ID_cate`, `ID_sub_cate`, `cover`, `descripcion`, `visible`) VALUES
(1, 'MANUALES', 1, 2, 5, 'Manual.jpg', '', 1),
(2, 'ELECTRICAS', 2, 2, 5, 'menu_Electrica.jpg', '', 1),
(3, 'TERAPIA INTENSIVA', 3, 2, 5, 'TerapiaIntensiva.jpg', '', 1),
(4, 'CAMILLAS', 4, 2, 5, 'Camilla2.jpg', '', 1),
(5, 'LENTES DE HIDROGEL TRADICIONAL', 5, 5, 18, 'nodisponible.jpg', '', 0),
(6, 'LENTES DE HIDROGEL DE SILICONA', 6, 5, 18, 'nodisponible.jpg', '', 0),
(7, 'REMPLAZO ANUAL - LENTES DE HIDROGEL TRADICIONAL', 7, 5, 18, 'nodisponible.jpg', '', 0),
(8, 'LENTES COSMETICOS', 8, 5, 18, 'nodisponible.jpg', '', 0),
(9, 'SOLUCIONES PARA LIMPIEZA', 9, 5, 18, 'nodisponible.jpg', '', 0),
(10, 'MIOPIA O HIPERMETROPIA', 10, 5, 19, 'nodisponible.jpg', '', 0),
(11, 'LENTES DE CONTACTO REEMPLAZO PROGRAMADO - HIDROGEL DE SILICONA', 11, 5, 19, 'nodisponible.jpg', '', 0),
(12, 'LENTES DESECHABLES DIARIOS HIDROGEL', 12, 5, 19, 'nodisponible.jpg', '', 0),
(13, 'MIOPIA O HIPERMETROPIA', 13, 5, 20, 'nodisponible.jpg', '', 0),
(14, 'ASTIGMATISMO', 14, 5, 20, 'nodisponible.jpg', '', 0),
(15, 'PRESBICIA', 15, 5, 20, 'nodisponible.jpg', '', 0),
(16, 'EXPRESSIONS COLORS', 16, 5, 20, 'nodisponible.jpg', '', 0),
(17, 'COSMETICOS', 17, 5, 21, 'nodisponible.jpg', '', 0),
(18, 'SOLUCIONES PARA LIMPIEZA Y CUIDADO', 18, 5, 21, 'nodisponible.jpg', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_promociones`
--

CREATE TABLE `goba_promociones` (
  `ID` int(6) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `cate` varchar(100) NOT NULL,
  `alta` varchar(25) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_promociones`
--

INSERT INTO `goba_promociones` (`ID`, `nom`, `cate`, `alta`, `visible`) VALUES
(1, 'p1.png', 'Promociones Bimestrales', '', 1),
(2, 'p2.png', 'Promociones Bimestrales', '', 1),
(3, 'p3.png', 'Nuevos Productos', '', 1),
(4, 'p4.png', 'Nuevos Productos', '', 1),
(5, 'p5.png', 'Nuevos Productos', '', 1),
(6, 'p6.png', 'Nuevos Productos', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_registros`
--

CREATE TABLE `goba_registros` (
  `ID` int(9) UNSIGNED NOT NULL,
  `ip` varchar(25) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `para` varchar(50) NOT NULL,
  `de` varchar(50) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `asunto` varchar(150) NOT NULL,
  `msj` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cat_list` varchar(50) NOT NULL,
  `seccion` varchar(50) NOT NULL,
  `tabla` varchar(50) NOT NULL,
  `adjuntos` text NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `status` int(11) NOT NULL,
  `ID_login` int(11) NOT NULL,
  `ID_user` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_servicios`
--

CREATE TABLE `goba_servicios` (
  `ID` int(6) UNSIGNED NOT NULL,
  `clave` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `cate` varchar(50) NOT NULL,
  `url_page` varchar(150) NOT NULL,
  `imagen1` varchar(100) NOT NULL,
  `imagen2` varchar(100) NOT NULL,
  `imagen3` varchar(100) NOT NULL,
  `imagen4` varchar(100) NOT NULL,
  `imagen5` varchar(100) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `alta` varchar(21) NOT NULL,
  `fmod` varchar(21) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_signup`
--

CREATE TABLE `goba_signup` (
  `ID` int(9) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` varchar(2) NOT NULL,
  `lastlogin` datetime NOT NULL,
  `tema` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apaterno` varchar(100) NOT NULL,
  `amaterno` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `tel` varchar(100) NOT NULL,
  `ext` int(4) NOT NULL,
  `fnac` date NOT NULL,
  `fb` varchar(100) NOT NULL,
  `tw` varchar(100) NOT NULL,
  `puesto` varchar(100) NOT NULL,
  `ndepa` int(1) NOT NULL,
  `depa` varchar(100) NOT NULL,
  `empresa` varchar(100) NOT NULL,
  `adress` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `mpio` varchar(100) NOT NULL,
  `edo` varchar(100) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `genero` varchar(20) NOT NULL,
  `exp` varchar(1000) NOT NULL,
  `likes` int(6) NOT NULL,
  `filtro` varchar(50) NOT NULL,
  `zona` varchar(50) NOT NULL,
  `alta` varchar(20) NOT NULL,
  `actualizacion` varchar(100) NOT NULL,
  `page` varchar(250) NOT NULL,
  `nivel_oper` int(2) NOT NULL,
  `rol` int(2) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `intentos` varchar(2) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_signup`
--

INSERT INTO `goba_signup` (`ID`, `username`, `password`, `email`, `level`, `lastlogin`, `tema`, `nombre`, `apaterno`, `amaterno`, `foto`, `cover`, `tel`, `ext`, `fnac`, `fb`, `tw`, `puesto`, `ndepa`, `depa`, `empresa`, `adress`, `direccion`, `mpio`, `edo`, `pais`, `genero`, `exp`, `likes`, `filtro`, `zona`, `alta`, `actualizacion`, `page`, `nivel_oper`, `rol`, `codigo`, `intentos`, `activo`) VALUES
(1, 'admin', 'c64f923f7f476f0b78716079452e7bdec4b2c016', 'multiportal@outlook.com', '-1', '2020-03-13 03:28:29', 'default', 'Guillermo', 'Jimenez', 'Lopez', 'sinfoto.png', '', '4421944950', 1, '0000-00-00', '', '', 'Programador', 0, '', 'Multiportal', '', '', '', '', '', 'M', '', 0, '0', '', '', 'admin2019xadmin79', '', 0, 0, '944950', '0', 1),
(2, 'demo', '71cc541bd1ccb6670de3f8d40f425ffb7315fe7f', 'demo@gmail.com', '-1', '2019-12-01 23:29:44', 'default', 'Demo', 'Apaterno', 'Amaterno', 'sinfoto.png', 'sincover.jpg', '4421234567', 0, '0000-00-00', '', '', 'Director', 0, '', 'PHPONIX', '', '', '', '', '', 'M', '', 0, '0', '', '', 'demo2019xdemo2017', '', 0, 0, '234567', '0', 1),
(3, 'usuario', '3c6e6ac5382f4e804e824c0d785b275252ddacb0', 'multiportal@outlook.com', '1', '2019-12-02 00:14:38', 'default', 'Usuario', 'Apaterno', 'Amaterno', 'sinfoto.png', '', '4421234567', 0, '0000-00-00', '', '', 'Usuario', 0, '', 'PHPONIX', '', '', '', '', '', 'M', '', 0, '0', '', '', 'usuario2019xuser79x', '', 0, 0, '234567', '0', 1),
(4, 'ventas', '1d415500d481e0c1c238189c22ea057da663c1e7', 'ventas@gmail.com', '2', '0000-00-00 00:00:00', 'default', 'Ventas', 'Apaterno', 'Amaterno', 'sinfoto.png', 'sincover.jpg', '4421234567', 0, '0000-00-00', '', '', 'Gerente', 0, '', 'PHPONIX', '', '', '', '', '', 'M', '', 0, '0', '', '', 'ventas2019xventas', '', 0, 0, '234567', '0', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_slider`
--

CREATE TABLE `goba_slider` (
  `ID` int(6) UNSIGNED NOT NULL,
  `ima` varchar(100) NOT NULL,
  `tit1` varchar(200) NOT NULL,
  `tit2` varchar(200) NOT NULL,
  `des_slide` text NOT NULL,
  `btn_nom` varchar(50) NOT NULL,
  `url` varchar(300) NOT NULL,
  `tema_slider` varchar(50) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_slider`
--

INSERT INTO `goba_slider` (`ID`, `ima`, `tit1`, `tit2`, `des_slide`, `btn_nom`, `url`, `tema_slider`, `visible`) VALUES
(1, 'home3.jpg', 'fibrecen', 'Fibra de vidrio y carbono', '', 'Boton', 'https://www.youtube.com/embed/fc3JqR68e-c', 'fibrecen', 1),
(2, 'home4.jpg', 'fibrecen', 'Resinas ', '', 'boton2', 'https://www.youtube.com/embed/fc3JqR68e-c', 'fibrecen', 1),
(3, 'home7.jpg', 'fibra', 'Cursos', '', 'video', 'https://www.youtube.com/embed/fc3JqR68e-c', 'fibrecen', 1),
(4, 'BANNER1.png', 'La Sala de Rayos X Digital', 'AUTOMATIZADA', '', 'Ver m&aacute;s', 'http://dgoba.com/productos/categoria/3-RAYOS-X', 'dgoba', 1),
(5, 'ANACONDA_BANNER-b.png', '', '', '', 'Ver m&aacute;s', 'http://dgoba.com/productos/categoria/6-SEDANTES', 'dgoba', 1),
(6, 'MEDIMAT_BANNER-b.png', '', '', '', 'Ver m&aacute;s', 'http://dgoba.com/productos/item/121-MEDIMAT', 'dgoba', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_tareas`
--

CREATE TABLE `goba_tareas` (
  `ID` int(11) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_temas`
--

CREATE TABLE `goba_temas` (
  `ID` int(3) UNSIGNED NOT NULL,
  `tema` varchar(100) NOT NULL,
  `subtema` varchar(100) NOT NULL,
  `selec` tinyint(1) NOT NULL,
  `nivel` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_temas`
--

INSERT INTO `goba_temas` (`ID`, `tema`, `subtema`, `selec`, `nivel`) VALUES
(1, 'default', '', 0, '0'),
(4, 'goba', '', 0, '0'),
(5, 'dgoba', '', 1, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_testimonios`
--

CREATE TABLE `goba_testimonios` (
  `ID` int(9) UNSIGNED NOT NULL,
  `cover` varchar(100) NOT NULL,
  `pro` varchar(100) NOT NULL,
  `comentario` text NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `goba_testimonios`
--

INSERT INTO `goba_testimonios` (`ID`, `cover`, `pro`, `comentario`, `visible`) VALUES
(1, 'testimonial_person2.jpg', 'Ingeniera Civil', 'Super recomendado, la atenci&oacute;n es buenisima y te ayudan con cualquier duda', 1),
(2, 'testimonial_person1.jpg', 'Emprendedor', 'Su curso se me hizo f&aacute;cil y muy creativo, impartidos por excelentes maestros.', 1),
(3, 'testimonial_person3.jpg', 'Ingeniera Industrial', 'Super recomendado, la atenci&oacute;n es buenisima y te ayudan con cualquier duda.', 1),
(4, 'TESTIMONIO01.png', 'Emprendedor', 'Excelente curso introducci&oacute;n a los materiales compuestos, muchas gracias.', 1),
(5, 'testimonio02.png', 'Emprendedor', 'Excelente curso de Mesas Ep&oacute;xicas en Parota y Cristal Templado.  &iexcl;No dejen pasar la oportunidad de tomar este curso!', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goba_visitas`
--

CREATE TABLE `goba_visitas` (
  `ID` int(9) UNSIGNED NOT NULL,
  `IPv4` bigint(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `info_nave` varchar(100) NOT NULL,
  `navegador` varchar(50) NOT NULL,
  `version` varchar(100) NOT NULL,
  `os` varchar(50) NOT NULL,
  `pais` varchar(100) NOT NULL,
  `user` varchar(50) NOT NULL,
  `page` varchar(500) NOT NULL,
  `refer` varchar(500) NOT NULL,
  `vhref` varchar(500) NOT NULL,
  `modulo` varchar(50) NOT NULL,
  `ext` varchar(50) NOT NULL,
  `idp` varchar(50) NOT NULL,
  `salida_pag` datetime NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `goba_access`
--
ALTER TABLE `goba_access`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_api_version`
--
ALTER TABLE `goba_api_version`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_blog`
--
ALTER TABLE `goba_blog`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_blog_coment`
--
ALTER TABLE `goba_blog_coment`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_clientes`
--
ALTER TABLE `goba_clientes`
  ADD PRIMARY KEY (`ID_cli`);

--
-- Indices de la tabla `goba_comp`
--
ALTER TABLE `goba_comp`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_config`
--
ALTER TABLE `goba_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_contacto`
--
ALTER TABLE `goba_contacto`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_contacto_forms`
--
ALTER TABLE `goba_contacto_forms`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_countries`
--
ALTER TABLE `goba_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `goba_css`
--
ALTER TABLE `goba_css`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_css2`
--
ALTER TABLE `goba_css2`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_cursos`
--
ALTER TABLE `goba_cursos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_cursos_coment`
--
ALTER TABLE `goba_cursos_coment`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_depa`
--
ALTER TABLE `goba_depa`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_directorio`
--
ALTER TABLE `goba_directorio`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_empresa`
--
ALTER TABLE `goba_empresa`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_galeria`
--
ALTER TABLE `goba_galeria`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_histo_backupdb`
--
ALTER TABLE `goba_histo_backupdb`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_home_config`
--
ALTER TABLE `goba_home_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_iconos`
--
ALTER TABLE `goba_iconos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_ipbann`
--
ALTER TABLE `goba_ipbann`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_landingpage_seccion`
--
ALTER TABLE `goba_landingpage_seccion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_map_config`
--
ALTER TABLE `goba_map_config`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_map_ubicacion`
--
ALTER TABLE `goba_map_ubicacion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_menu_admin`
--
ALTER TABLE `goba_menu_admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_menu_web`
--
ALTER TABLE `goba_menu_web`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_mode_page`
--
ALTER TABLE `goba_mode_page`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_modulos`
--
ALTER TABLE `goba_modulos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_noticias`
--
ALTER TABLE `goba_noticias`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_noticias_coment`
--
ALTER TABLE `goba_noticias_coment`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_notificacion`
--
ALTER TABLE `goba_notificacion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_opciones`
--
ALTER TABLE `goba_opciones`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_pages`
--
ALTER TABLE `goba_pages`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_portafolio`
--
ALTER TABLE `goba_portafolio`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_productos`
--
ALTER TABLE `goba_productos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_productos_cate`
--
ALTER TABLE `goba_productos_cate`
  ADD PRIMARY KEY (`ID_cate`);

--
-- Indices de la tabla `goba_productos_coti`
--
ALTER TABLE `goba_productos_coti`
  ADD PRIMARY KEY (`ID_coti`);

--
-- Indices de la tabla `goba_productos_coti_r`
--
ALTER TABLE `goba_productos_coti_r`
  ADD PRIMARY KEY (`ID_reg`);

--
-- Indices de la tabla `goba_productos_files`
--
ALTER TABLE `goba_productos_files`
  ADD PRIMARY KEY (`ID_pfile`);

--
-- Indices de la tabla `goba_productos_marcas`
--
ALTER TABLE `goba_productos_marcas`
  ADD PRIMARY KEY (`ID_marca`);

--
-- Indices de la tabla `goba_productos_sub_cate`
--
ALTER TABLE `goba_productos_sub_cate`
  ADD PRIMARY KEY (`ID_sub_cate`);

--
-- Indices de la tabla `goba_productos_sub_cate2`
--
ALTER TABLE `goba_productos_sub_cate2`
  ADD PRIMARY KEY (`ID_sub_cate2`);

--
-- Indices de la tabla `goba_promociones`
--
ALTER TABLE `goba_promociones`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_registros`
--
ALTER TABLE `goba_registros`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_servicios`
--
ALTER TABLE `goba_servicios`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_signup`
--
ALTER TABLE `goba_signup`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_slider`
--
ALTER TABLE `goba_slider`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_tareas`
--
ALTER TABLE `goba_tareas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_temas`
--
ALTER TABLE `goba_temas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_testimonios`
--
ALTER TABLE `goba_testimonios`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `goba_visitas`
--
ALTER TABLE `goba_visitas`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `goba_access`
--
ALTER TABLE `goba_access`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `goba_api_version`
--
ALTER TABLE `goba_api_version`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `goba_blog`
--
ALTER TABLE `goba_blog`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_blog_coment`
--
ALTER TABLE `goba_blog_coment`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_clientes`
--
ALTER TABLE `goba_clientes`
  MODIFY `ID_cli` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `goba_comp`
--
ALTER TABLE `goba_comp`
  MODIFY `ID` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_config`
--
ALTER TABLE `goba_config`
  MODIFY `ID` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_contacto`
--
ALTER TABLE `goba_contacto`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_contacto_forms`
--
ALTER TABLE `goba_contacto_forms`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_countries`
--
ALTER TABLE `goba_countries`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT de la tabla `goba_css`
--
ALTER TABLE `goba_css`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_css2`
--
ALTER TABLE `goba_css2`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_cursos`
--
ALTER TABLE `goba_cursos`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_cursos_coment`
--
ALTER TABLE `goba_cursos_coment`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_depa`
--
ALTER TABLE `goba_depa`
  MODIFY `ID` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `goba_directorio`
--
ALTER TABLE `goba_directorio`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_empresa`
--
ALTER TABLE `goba_empresa`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `goba_galeria`
--
ALTER TABLE `goba_galeria`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_histo_backupdb`
--
ALTER TABLE `goba_histo_backupdb`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_home_config`
--
ALTER TABLE `goba_home_config`
  MODIFY `ID` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_iconos`
--
ALTER TABLE `goba_iconos`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `goba_ipbann`
--
ALTER TABLE `goba_ipbann`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `goba_landingpage_seccion`
--
ALTER TABLE `goba_landingpage_seccion`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `goba_map_config`
--
ALTER TABLE `goba_map_config`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `goba_map_ubicacion`
--
ALTER TABLE `goba_map_ubicacion`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_menu_admin`
--
ALTER TABLE `goba_menu_admin`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `goba_menu_web`
--
ALTER TABLE `goba_menu_web`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `goba_mode_page`
--
ALTER TABLE `goba_mode_page`
  MODIFY `ID` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `goba_modulos`
--
ALTER TABLE `goba_modulos`
  MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `goba_noticias`
--
ALTER TABLE `goba_noticias`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_noticias_coment`
--
ALTER TABLE `goba_noticias_coment`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_notificacion`
--
ALTER TABLE `goba_notificacion`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_opciones`
--
ALTER TABLE `goba_opciones`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `goba_pages`
--
ALTER TABLE `goba_pages`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_portafolio`
--
ALTER TABLE `goba_portafolio`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_productos`
--
ALTER TABLE `goba_productos`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT de la tabla `goba_productos_cate`
--
ALTER TABLE `goba_productos_cate`
  MODIFY `ID_cate` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `goba_productos_coti`
--
ALTER TABLE `goba_productos_coti`
  MODIFY `ID_coti` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_productos_coti_r`
--
ALTER TABLE `goba_productos_coti_r`
  MODIFY `ID_reg` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_productos_files`
--
ALTER TABLE `goba_productos_files`
  MODIFY `ID_pfile` int(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_productos_marcas`
--
ALTER TABLE `goba_productos_marcas`
  MODIFY `ID_marca` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `goba_productos_sub_cate`
--
ALTER TABLE `goba_productos_sub_cate`
  MODIFY `ID_sub_cate` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `goba_productos_sub_cate2`
--
ALTER TABLE `goba_productos_sub_cate2`
  MODIFY `ID_sub_cate2` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `goba_promociones`
--
ALTER TABLE `goba_promociones`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `goba_registros`
--
ALTER TABLE `goba_registros`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_servicios`
--
ALTER TABLE `goba_servicios`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_signup`
--
ALTER TABLE `goba_signup`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `goba_slider`
--
ALTER TABLE `goba_slider`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `goba_tareas`
--
ALTER TABLE `goba_tareas`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goba_temas`
--
ALTER TABLE `goba_temas`
  MODIFY `ID` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `goba_testimonios`
--
ALTER TABLE `goba_testimonios`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `goba_visitas`
--
ALTER TABLE `goba_visitas`
  MODIFY `ID` int(9) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
