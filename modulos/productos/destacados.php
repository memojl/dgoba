<?php 
if($mod!='productos'){
	include 'admin/functions.php';
	echo'<link href="'.$page_url.'modulos/productos/css/menup.css" rel="stylesheet">
';
}
?>

<div class="related products">
   <div class="divider">
      <h3>Related Products</h3>
   </div>
   <!-- BEGIN row -->
   <div class="row">
      <div class="products">
         <div class="product-item lm-col-3">
            <div class="product-wrap">
               <a class="product-mask" href="https://www.manprec.com/producto/ultra-360/">
                  <div class="product-meta">
                     <div class="product-categories">
                        <span>Stimuplex</span>			
                     </div>
                     <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>11,617.63</span></span>
                  </div>
                  <img width="720" height="964" src="//www.manprec.com/wp-content/uploads/2018/08/AGUJAS_1-720x964.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="">
               </a>
            </div>
            <h3><a href="https://www.manprec.com/producto/ultra-360/">Ultra 360</a></h3>
            <a href="https://www.manprec.com/producto/ultra-360/" data-quantity="1" class="button product_type_variable add_to_cart_button" data-product_id="1127020" data-product_sku="" aria-label="Selecciona las opciones para “Ultra 360”" rel="nofollow">Ver opciones</a>
         </div>
         <div class="product-item lm-col-3">
            <div class="product-wrap">
               <a class="product-mask" href="https://www.manprec.com/producto/paquete-2/">
                  <div class="product-meta">
                     <div class="product-categories">
                        <span>Philips</span>			
                     </div>
                     <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span>
                  </div>
                  <img width="720" height="270" src="//www.manprec.com/wp-content/uploads/2018/08/2.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="//www.manprec.com/wp-content/uploads/2018/08/2.png 1600w, //www.manprec.com/wp-content/uploads/2018/08/2-300x113.png 300w, //www.manprec.com/wp-content/uploads/2018/08/2-768x288.png 768w, //www.manprec.com/wp-content/uploads/2018/08/2-1024x384.png 1024w, //www.manprec.com/wp-content/uploads/2018/08/2-720x270.png 720w" sizes="(max-width: 720px) 100vw, 720px">
               </a>
            </div>
            <h3><a href="https://www.manprec.com/producto/paquete-2/">PAQUETE 2 Philips Lumify</a></h3>
            <a href="/producto/paquete-1-stimuplex-philips/?add-to-cart=1127029" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="1127029" data-product_sku="paquete-2-philips-lumify" aria-label="Agrega “PAQUETE 2 Philips Lumify” a tu carrito" rel="nofollow" data-pys-event-id="5b86eaa83d604">Añadir al carrito</a>
         </div>
         <div class="product-item lm-col-3">
            <div class="product-wrap">
               <a class="product-mask" href="https://www.manprec.com/producto/ultrasonido-portatil-lumify/">
                  <div class="product-meta">
                     <div class="product-categories">
                        <span>Philips</span><span>Productos Promo</span>			
                     </div>
                     <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>211,000.00</span></span>
                  </div>
                  <img width="535" height="363" src="//www.manprec.com/wp-content/uploads/2017/11/Capture1.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="Philips-Lumify" srcset="//www.manprec.com/wp-content/uploads/2017/11/Capture1.png 535w, //www.manprec.com/wp-content/uploads/2017/11/Capture1-300x204.png 300w" sizes="(max-width: 535px) 100vw, 535px">
               </a>
            </div>
            <h3><a href="https://www.manprec.com/producto/ultrasonido-portatil-lumify/">Philips – Lumify – Ultrasonido portátil</a></h3>
            <a href="/producto/paquete-1-stimuplex-philips/?add-to-cart=1126674" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="1126674" data-product_sku="" aria-label="Agrega “Philips - Lumify - Ultrasonido portátil” a tu carrito" rel="nofollow" data-pys-event-id="5b86eaa84050b">Añadir al carrito</a>
         </div>
         <div class="product-item lm-col-3">
            <div class="product-wrap">
               <a class="product-mask" href="https://www.manprec.com/producto/neuroestimulador-hsn12/">
                  <div class="product-meta">
                     <div class="product-categories">
                        <span>Stimuplex</span>			
                     </div>
                     <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>36,134.44</span></span>
                  </div>
                  <img width="500" height="500" src="//www.manprec.com/wp-content/uploads/2018/08/NEURO_1.jpeg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="//www.manprec.com/wp-content/uploads/2018/08/NEURO_1.jpeg 500w, //www.manprec.com/wp-content/uploads/2018/08/NEURO_1-150x150.jpeg 150w, //www.manprec.com/wp-content/uploads/2018/08/NEURO_1-300x300.jpeg 300w, //www.manprec.com/wp-content/uploads/2018/08/NEURO_1-100x100.jpeg 100w" sizes="(max-width: 500px) 100vw, 500px">
               </a>
            </div>
            <h3><a href="https://www.manprec.com/producto/neuroestimulador-hsn12/">Neuroestimulador HSN12</a></h3>
            <a href="https://www.manprec.com/producto/neuroestimulador-hsn12/" data-quantity="1" class="button product_type_variable add_to_cart_button" data-product_id="1127014" data-product_sku="" aria-label="Selecciona las opciones para “Neuroestimulador HSN12”" rel="nofollow">Ver opciones</a>
         </div>
         
         
		<!--?php productos_destacados();?-->

      </div>
      <!-- END row -->
   </div>
</div>
