<?php 
/*//FUNCIONES ADMIN-LTE//*****************************************************************************/
function cadena_replace(&$replace1,&$replace2){
	$replace1=array(' ','.',',','(',')','/','"',"’",'+','á','é','í','ó','ú','&aacute;','&eacute;','&iacute;','&oacute;','&uacute;','Á','É','Í','Ó','Ú','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;','ñ','Ñ','&ntilde;','&Ntilde;','&','amp;');
	$replace2=array('-','-','-','-','-','-','-',"",'-','a','e','i','o','u','a','e','i','o','u','A','E','I','O','U','A','E','I','O','U','n','N','n','N','','');
}

function html_iso(&$nombre,&$cate){
global $chartset;
 if($chartset=='iso-8859-1'){
	$nombre=htmlentities($nombre, ENT_COMPAT,'ISO-8859-1', true);
	//$des = htmlentities($des, ENT_COMPAT,'ISO-8859-1', true);
	//$resena = htmlentities($resena, ENT_COMPAT,'ISO-8859-1', true);	
	$cate=htmlentities($cate, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function html_iso_productos(&$nombre,&$titulo){
global $chartset;
 if($chartset=='iso-8859-1'){
	$nombre=htmlentities($nombre, ENT_COMPAT,'ISO-8859-1', true);
	$titulo=htmlentities($titulo, ENT_COMPAT,'ISO-8859-1', true);	
	$modelo=htmlentities($modelo, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function html_iso_cate(&$cate){
global $chartset;
 if($chartset=='iso-8859-1'){
	$cate=htmlentities($cate, ENT_COMPAT,'ISO-8859-1', true);
	//$des=htmlentities($des, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function html_iso_subcate(&$subcate,&$cate){
global $chartset;
 if($chartset=='iso-8859-1'){
	$subcate=htmlentities($subcate, ENT_COMPAT,'ISO-8859-1', true);
	$cate=htmlentities($cate, ENT_COMPAT,'ISO-8859-1', true);
	//$des=htmlentities($des, ENT_COMPAT,'ISO-8859-1', true);
 }
}

function jQuery_select_cate_subcate(){
echo '
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script language="javascript">
$(document).ready(function(){
    $("#ID_cate").on(\'change\', function () {
        $("#ID_cate option:selected").each(function () {
            cate=$(this).val();
            $.post("modulos/productos/admin/_query_subcategorias.php", { cate: cate }, function(data){
                $("#ID_sub_cate").html(data);
            });			
        });
   });
});

$(document).ready(function(){
    $("#ID_sub_cate").on(\'change\', function () {
        $("#ID_sub_cate option:selected").each(function () {
            subcate=$(this).val();
            $.post("modulos/productos/admin/_query_subcategorias2.php", { subcate: subcate }, function(data){
                $("#ID_sub_cate2").html(data);
            });			
        });
   });
});

</script>
';
}

function logos_marcas($id){
global $mysqli,$DBprefix,$url,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo,$path_jsonDB;
$tabla='productos_marcas';
$api_url=$page_url.'bloques/ws/t/?t='.$tabla;
$urlexists = url_exists($api_url);
$path_JSON=($urlexists!=1)?$path_jsonDB.$tabla.'.json':$api_url;

if($path_JSON){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
usort($Data, function($a, $b){return strnatcmp($a['ord'], $b['ord']);});//Orden del menu
$i=0;
if($_SESSION['level']!=-1){echo '<!-- '.$tabla.'.json -->'."\n\r";}else{echo '<!-- '.$tabla.'.json URL:('.$path_JSON.')-->'."\n\r";}
	foreach ($Data as $rowm){$i++;
		$ID_marca=$rowm['ID_marca'];
		$ID_cate=$rowm['ID_cate'];
		$nombre=$rowm['nombre'];
		$cover=$rowm['cover'];
		$visible=$rowm['visible'];
		
		if($ID_cate==$id && $visible==1){
			$logos.='<img src="'.$page_url.$path_tema.'img/'.$cover.'" class="logo-marcas">';
		}
	}
	echo $logos;
}
}


function select_cate($cate){
global $mysqli,$DBprefix;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate ORDER BY ID_cate ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="ID_cate" name="ID_cate">
<option value="0">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){$ID_cate=$row['ID_cate'];$clave=$row['ord'];$categoria=$row['categoria'];
		$seleccion=($ID_cate==$cate)?'selected' : '';
		echo '<option value="'.$ID_cate.'" '.$seleccion.'>'.$clave.' '.$categoria.'</option>';
	}
echo '</select>';	
}

function select_sub_cate($cate,$subcate){
global $mysqli,$DBprefix;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_cate='{$cate}' ORDER BY ID_sub_cate ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="ID_sub_cate" name="ID_sub_cate">
<option value="0">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){$ID_sub_cate=$row['ID_sub_cate'];$clave=$row['ord'];$subcategoria=$row['subcategoria'];
		$seleccion=($ID_sub_cate==$subcate) ? 'selected' : '';
		echo '<option value="'.$ID_sub_cate.'" '.$seleccion.'>'.$clave.' '.$subcategoria.'</option>';
	}
echo '</select>';	
}

function select_sub_cate2($cate,$subcate,$subcate2){
global $mysqli,$DBprefix;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate2 WHERE ID_sub_cate='{$subcate}' ORDER BY ID_sub_cate2 ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="ID_sub_cate2" name="ID_sub_cate2">
<option value="0">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){$ID_sub_cate2=$row['ID_sub_cate2'];$clave=$row['ord'];$subcategoria2=$row['subcategoria2'];
		$seleccion=($ID_sub_cate2==$subcate2) ? 'selected' : '';
		echo '<option value="'.$ID_sub_cate2.'" '.$seleccion.'>'.$subcategoria2.'</option>';
	}
echo '</select>';	
}

function compact_ajax_mod($fun,$tag_id,$url_ajax,$seg,$jqs){
echo '<script>
var $jq = jQuery.noConflict();
//$(document).ready(function() {	
	function '.$fun.'(){
		$jq.ajax({
			type: \'POST\',
			url: \''.$url_ajax.'\',
			success: function(respuesta) {			
				$(\'#'.$tag_id.'\').html(respuesta);
	   		}
		});
	}
	setInterval('.$fun.', '.$seg.'000);//setInterval(function(){'.$fun.'();},'.$seg.'000)//Actualizamos cada '.$seg.' segundo    	
	window.onload='.$fun.';
//});
</script>';
if($tag_id!=''){echo '<div id="'.$tag_id.'" class="row w3ls_banner_bottom_grids"></div>';}
}

function crear_ajax_productos(){
global $mysqli,$DBprefix,$page_url,$path_tema,$mod,$ext,$opc,$action,$URL;
$cond_opc=($opc!='')?'&opc='.$opc:'';
	
//ajax_crud($campos,$salidadebusaqueda,1=tinyMCE);
$campos='
			cover: $("#cover").val(),
			clave: $("#clave").val(),
			titulo: $("#titulo").val(),
			nombre: $("#nom").val(),
			des: $("#des").val(),
			precio: $("#precio").val(),
			cate: $("#cate").val(),
			visible: $("#visible").val(),
      		id: $("#id").val()';
$template='
	<div class="col-md-3 col-xs-12">
		<div class="box box-primary">
			<div class="box-header with-border">
       			<h3 class="box-title">C&oacute;digo: <b>${task.clave}</b></h3>
				<span class="controles">${sel}
					<a href="'.$page_url.'index.php?mod='.$mod.'&ext=admin/index'.$cond_opc.'&form=1&action=edit&id=${task.ID}" title="Editar"><i class="fa fa-edit"></i></a> | <a href="#" taskid="${task.ID}" class="task-delete" title="Borrar"><i class="fa fa-trash"></i></a>
				</span>
			</div>
			<div class="box-body">
				<div class="ima-size">
					<img src="'.$page_url.'modulos/'.$mod.'/fotos/${task.cover}" class="ima-size img-responsive">
				</div>
				<div id="title"><strong>${task.nombre}</strong></div>	
			</div><!-- /.box-body -->
		</div>
	</div>';
ajax_crud($campos,$template,1);
}

/*
function select_cate($cate){
global $mysqli,$DBprefix;	
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate ORDER BY ID_cate ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="cate" name="cate">
<option value="">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){
		$seleccion=($row['ID_cate']==$cate) ? 'selected' : '';
		echo '<option value="'.$row['ID_cate'].'" '.$seleccion.'>'.$row['categoria'].'</option>';
	}
echo '</select>';	
}

function select_sub_cate($cate,$subcate){
global $mysqli,$DBprefix,$opc;
$cond=($opc=='edit')?"WHERE ID_cate='{$cate}' ":'';
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate ".$condi."ORDER BY ID_sub_cate ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="subcate" name="subcate">
<option value="">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){
		/*Consultar Nombre de Categoria*
		$sql1=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='".$row['ID_cate']."' ORDER BY ID_cate ASC;") or print mysqli_error($mysqli);
		while($row1=mysqli_fetch_array($sql1)){$categoria=$row1['categoria'];}
		/**
		$seleccion=($row['ID_sub_cate']==$subcate) ? 'selected' : '';
		echo '<option value="'.$row['ID_sub_cate'].'" '.$seleccion.'>'.$row['subcategoria'].'-'.$categoria.'</option>';
	}
echo '</select>';	
}

function select_sub_cate2($subcate2){
global $mysqli,$DBprefix,$opc;
$cond=($opc=='edit')?"WHERE ID_sub_cate2='{$subcate2}' ":'';
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate2 ".$condi."ORDER BY ID_sub_cate2 ASC;") or print mysqli_error($mysqli);
echo '<select class="form-control" id="subcate2" name="subcate2">
<option value="">Seleccionar</option>';
	while($row=mysqli_fetch_array($sql)){
		/*Consultar Nombre de Categoria*
		$sql1=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='".$row['ID_cate']."' ORDER BY ID_cate ASC;") or print mysqli_error($mysqli);
		while($row1=mysqli_fetch_array($sql1)){$categoria=$row1['categoria'];}
		/*Consultar Nombre de SubCategoria*
		$sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_sub_cate='".$row['ID_sub_cate']."' ORDER BY ID_sub_cate ASC;") or print mysqli_error($mysqli);
		while($row2=mysqli_fetch_array($sql2)){$subcategoria=$row2['subcategoria'];}
		/**
		
		$seleccion=($row['ID_sub_cate2']==$subcate2) ? 'selected' : '';		
		echo '<option value="'.$row['ID_sub_cate2'].'" '.$seleccion.'>'.$row['subcategoria2'].'-'.$subcategoria.'-'.$categoria.'</option>';
	}
echo '</select>';
}
*/
function nombre_cate_subcate($id,&$data_cate_subcate){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
$fjson='productos';
$path_JSON='bloques/webservices/rest/json/'.$fjson.'.json';
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/?t='.$fjson;}

 if(file_exists($path_JSON) && filesize($path_JSON)!=0){
  $objData=file_get_contents($path_JSON);
  $Data=json_decode($objData,true);
  if($Data!='' && $Data!=NULL){
  $i=0;
  //echo '<!-- productos.json -->';
	foreach ($Data as $reg){$i++;
		$ID=$reg['ID'];
		$ID_cate_p=$reg['ID_cate'];
		$ID_sub_cate_p=$reg['ID_sub_cate'];
		$visible=$reg['visible'];
		if($visible==1 && $ID==$id){
			
			//echo '<!-- categorias.json -->';
			$path_JSON="modulos/productos/categorias.json";		
			if(file_exists($path_JSON) && filesize($path_JSON)!=0){
				$objData2=file_get_contents($path_JSON);
				$Data2=json_decode($objData2,true);
				foreach ($Data2 as $row){
					$ID_cate=$row['ID_cate'];
					if($ID_cate_p==$ID_cate){
						$categoria=$row['categoria'];
					}
				}
			}else{
	    		$sql1=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='{$ID_cate_p}';") or print mysqli_error($mysqli);
				while($row1=mysqli_fetch_array($sql1)){
					$categoria=$row1['categoria'];
				}
			}
			//echo '<!-- /categorias.json -->';

			//echo '<!-- subcategorias.json -->';		
			$path_JSON="modulos/productos/categorias.json";		
			if(file_exists($path_JSON) && filesize($path_JSON)!=0){
				$objData3=file_get_contents("modulos/productos/subcategorias.json");
				$Data3=json_decode($objData3,true);
				foreach ($Data3 as $row){
					$ID_sub_cate=$row['ID_sub_cate'];
					if($ID_sub_cate_p==$ID_sub_cate){
						$subcategoria=$row['subcategoria'];
					}
				}
			}else{
				$sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_sub_cate='{$ID_sub_cate_p}';") or print mysqli_error($mysqli);
				while($row2=mysqli_fetch_array($sql2)){
					$subcategoria=$row2['subcategoria'];
				}
			}
			//echo '<!-- /subcategorias.json -->';
			$data_cate_subcate=array($ID,$ID_cate_p,$categoria,$ID_sub_cate_p,$subcategoria);						
		}
	}
  }
  //echo '<!-- /productos.json -->';
 }else{
  $sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE ID='{$id}';") or print mysqli_error($mysqli);
  while($row=mysqli_fetch_array($sql)){
		$ID=$row['ID'];
		$ID_cate_p=$row['ID_cate'];
		$ID_sub_cate_p=$row['ID_sub_cate'];
	    $sql1=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate='{$ID_cate_p}';") or print mysqli_error($mysqli);
		while($row1=mysqli_fetch_array($sql1)){
			$categoria=$row1['categoria'];
		}
		$sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE ID_sub_cate='{$ID_sub_cate_p}';") or print mysqli_error($mysqli);
		while($row2=mysqli_fetch_array($sql2)){
			$subcategoria=$row2['subcategoria'];
		}
  }
  $data_cate_subcate=array($ID,$ID_cate_p,$categoria,$ID_sub_cate_p,$subcategoria);						
 } 
}

function menu_categoria(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc,$id,$idp;	
echo '
<link href="'.$page_url.'modulos/productos/css/menup.css" rel="stylesheet">
<script src="'.$page_url.'modulos/productos/js/ddaccordion.js" type="text/javascript"></script>
<script type="text/javascript">
//Initialize first demo:
ddaccordion.init({
    headerclass: "mypets", //Shared CSS class name of headers group-compartido CSS nombre de la clase de grupo encabezados
    contentclass: "thepet", //Shared CSS class name of contents group-compartido CSS nombre de la clase de grupo de contenidos
    revealtype: "clickgo", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"-Reveal contenido cuando el usuario hace clic o onmouseover la cabecera ? Valor válido: " click", " clickgo " , o " mouseover "
    mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover-si revealtype = " mouseover " , establecer el retardo en milisegundos antes de cabecera expande onMouseover
    collapseprev: true, //Collapse previous content (so only one open at any time)? true/false-//Contraer contenido anterior ( lo único abierto en cualquier momento) ? verdadero / falso 
    defaultexpanded: [], //index of content(s) open by default [index1, index2, etc]. [] denotes no content.-//índice de contenido ( s ) abierta por defecto [ index1 , index2 , etc] . [] Denota ningún contenido .
    onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
    animatedefault: true, //Should contents open by default be animated into view?-En caso de contenidos abiertos por defecto será animado a la vista ?
    persiststate: true, //persist state of opened contents within browser session?-//persistir estado de contenidos abiertos en la sesión del navegador ?
    toggleclass: ["", "openpet"], //Two CSS classes to be applied to the header when it\'s collapsed and expanded, respectively ["class1", "class2"]-// Dos clases CSS que se aplicarán a la cabecera cuando se derrumbó y se expandió , respectivamente [" class1 " , " clase 2 " ]
    togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it\'s collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)-//HTML adicional añade a la cabecera cuando se derrumbó y se expandió , respectivamente [" posición" , " html1 " , " HTML2 " ] ( ver docs )
    animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"-//velocidad de la animación : entero en milisegundos ( es decir : 200 ), o palabras clave "rápida", "normal " , o "lento"
    oninit:function(headers, expandedindices){ //custom code to run when headers have initalized-//código personalizado para ejecutarse cuando cabeceras han initalized
        //do nothing
    },
    onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
        //do nothing
    }
})
</script>
';
//Menu-Categoria
if($mod=='productos' && $ext=='categoria'){
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE visible=1 AND ID_cate='{$idp}' ORDER BY ord") or print mysqli_error($mysqli);
	if($row=mysqli_fetch_array($sql)){$ID_cate=$row['ID_cate'];}
	if($ID_cate==$idp){
		echo '<style>.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	}
}
//Menu-Subcategoria
if($mod=='productos' && $ext=='subcategoria'){
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE visible=1 AND ID_sub_cate='{$idp}' ORDER BY ord") or print mysqli_error($mysqli);
	if($row=mysqli_fetch_array($sql)){$ID_sub_cate=$row['ID_sub_cate'];$ID_cate=$row['ID_cate'];}
	if($ID_sub_cate==$idp){
		echo '<style>.subcat_'.$ID_sub_cate.'{border-bottom:2px solid #c00;}.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	}
}
//Menu-Subcategoria
if($mod=='productos' && $ext=='subcategoria2'){
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate2 WHERE visible=1 AND ID_sub_cate2='{$idp}' ORDER BY ord") or print mysqli_error($mysqli);
	if($row=mysqli_fetch_array($sql)){$ID_subcate=$row['ID_sub_cate'];}
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE visible=1 AND ID_sub_cate='{$ID_subcate}' ORDER BY ord") or print mysqli_error($mysqli);
	if($row=mysqli_fetch_array($sql)){$ID_sub_cate=$row['ID_sub_cate'];$ID_cate=$row['ID_cate'];}
	if($ID_sub_cate==$ID_subcate){
		echo '<style>.subcat_'.$ID_sub_cate.'{border-bottom:2px solid #c00;}.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	}
}
//Menu--Item
if($mod=='productos' && $ext=='item'){
	nombre_cate_subcate($idp,$data_cate_subcate);
	$ID_cate=$data_cate_subcate[1];
	$ID_sub_cate=$data_cate_subcate[3];
	//if($ID_sub_cate==$idp){
		echo '<style>.subcat_'.$ID_sub_cate.'{border-bottom:2px solid #c00;}.cate_'.$ID_cate.'{border-bottom:2px solid #c00;}</style>';
	//}
}
//MENU
	$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE visible=1 ORDER BY ord") or print mysqli_error($mysqli);
	while($row=mysqli_fetch_array($sql)){$icon=$row['icon'];
	cadena_replace($replace1,$replace2);
	$categoria=str_replace($replace1,$replace2,$row['categoria']);
		$link1=($_GET['tema_previo']!='')?$page_url.'index.php?mod=productos&ext=categoria&id='.$row['ID_cate'].'&tema_previo='.$_GET['tema_previo']:$page_url.'productos/categoria/'.$row['ID_cate'].'-'.$categoria;
		$icono=($icon=='' && $icon==NULL)?'<div id="circulo"></div>&nbsp;&nbsp;':'<i class="fa '.$icon.'"></i>&nbsp;&nbsp;';
		echo '<div class="mypets">'.$icono.'<a class="cate_'.$row['ID_cate'].'" href="'.$link1.'">'.$row['categoria'].'</a></div>'."\n";
		echo '<div class="thepet">'."\n";
        $sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE visible=1 AND ID_cate={$row[ID_cate]} ORDER BY ID_sub_cate") or print mysqli_error($mysqli);
        while($row2=mysqli_fetch_array($sql2)){$sub_cate=ucfirst($row2["subcategoria"]);
		cadena_replace($replace1,$replace2);
		$subcategoria=str_replace($replace1,$replace2,$row2['subcategoria']);
			$link2=($_GET['tema_previo']!='')?$page_url.'index.php?mod=productos&ext=subcategoria&id='.$row2['ID_sub_cate'].'&tema_previo='.$_GET['tema_previo']:$page_url.'productos/subcategoria/'.$row2['ID_sub_cate'].'-'.$subcategoria;
            echo '<i class="fa fa-plus"></i>&nbsp;&nbsp;<a class="subcat_'.$row2['ID_sub_cate'].'" href="'.$link2.'">'.wordwrap($sub_cate, 75, '<br>').'</a><div id="espacio"></div>';				       
        }
        echo '</div>'."\n";
	}
}

function item_productos($id){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
sql_opciones('link_productos',$valor);
$fjson='productos';
$path_JSON='bloques/webservices/rest/json'.$fjson.'.json';
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/?t='.$fjson;}
 if($path_JSON){
 $objData=file_get_contents($path_JSON);
 $Data=json_decode($objData,true);
  if($Data!='' && $Data!=NULL){
  $i=0;
  echo '<!-- productos.json -->';
	foreach ($Data as $reg){$i++;
		$ID=$reg['ID'];
		$cover=$reg['cover'];
		cadena_replace($replace1,$replace2);
		$producto=str_replace($replace1,$replace2,$reg['nombre']);
		$nom_producto=$reg['nombre'];
		$descripcion=$reg['descripcion'];
		$ID_cate=$reg['ID_cate'];
		$ID_sub_cate=$reg['ID_sub_cate'];
		$ID_sub_cate2=$reg['ID_sub_cate2'];
		$visible=$reg['visible'];
		
		if($visible==1 && $ID_cate==$id && $ID_sub_cate==0){
		$objData2=file_get_contents("modulos/productos/categorias.json");
		$Data2=json_decode($objData2,true);
			foreach ($Data2 as $row){
				$ID_cate2=$row['ID_cate'];
				if($ID_cate==$ID_cate2){
					$categoria=$row['categoria'];
				}
			}
			
		$tema_p=$_GET['tema_previo'];
		$link_zp=($tema_p!='')?$page_url.'index.php?mod='.$mod.'&ext=item&id='.$ID.'&tema_previo='.$tema_p:$page_url.'productos/item/'.$ID.'-'.$producto;
		echo '<!--['.$i.'] -'.$ID.'-->
               <div class="product-item lm-col-4">
                  <div class="product-wrap">
                     <a class="product-mask" href="'.$link_zp.'">
                        <div class="product-meta">
                           <div class="product-categories">
                              <span>Ver</span><span>M&aacute;s</span>			
                           </div>
                           <!--span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span-->
                        </div>
                        <img width="720" height="270" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 1600w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 768w, '.$page_url.'modulos/productos/fotos/'.$cover.' 1024w, '.$page_url.'modulos/productos/fotos/'.$cover.' 720w" sizes="(max-width: 720px) 100vw, 720px">
                     </a>
                  </div>
                  <h3><a href="'.$link_zp.'">'.$nom_producto.'</a></h3>
                  <a href="'.$link_zp.'" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">'.$categoria.'</a>
               </div>
			';
		}
	}
	if($i==0){
				echo '<div class="col-lg-12 col-xs-12">
					<div>Por el momento no hay productos disponibles.</div>
				</div>
  				';
	}
  }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
  }
  echo '<!-- /productos.json -->';
 }
}

function item_subproductos($id,$idp){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
sql_opciones('link_productos',$valor);
$fjson='productos';
$path_JSON='bloques/webservices/rest/json'.$fjson.'.json';
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/?t='.$fjson;}
 echo $rut_origen=($_SESSION['level']!=-1)?'<!-- '.$fjson.' -->'."\n\r":'<!-- '.$fjson.' URL:('.$path_JSON.')-->'."\n\r";
 if($path_JSON){
  $objData=file_get_contents($path_JSON);
  $Data=json_decode($objData,true);
  if($Data!='' && $Data!=NULL){
  $i=0;
  echo '<!-- productos.json -->';
	foreach ($Data as $reg){$i++;
		$ID=$reg['ID'];
		$cover=$reg['cover'];
		cadena_replace($replace1,$replace2);
		$producto=str_replace($replace1,$replace2,$reg['nombre']);
		$nom_producto=$reg['nombre'];
		$descripcion=$reg['descripcion'];
		$ID_cate=$reg['ID_cate'];
		$ID_sub_cate=$reg['ID_sub_cate'];
		$ID_sub_cate2=$reg['ID_sub_cate2'];
		$visible=$reg['visible'];

		if($visible==1 && $ID_sub_cate==$id && $ID_sub_cate2==0){
		$objData2=file_get_contents("modulos/productos/categorias.json");
		$Data2=json_decode($objData2,true);
			foreach ($Data2 as $row){
				$ID_cate2=$row['ID_cate'];
				if($ID_cate==$ID_cate2){
					$categoria=$row['categoria'];
				}
			}

		$tema_p=$_GET['tema_previo'];
		$link_zp=($tema_p!='')?$page_url.'index.php?mod='.$mod.'&ext=item&id='.$ID.'&tema_previo='.$tema_p:$page_url.'productos/item/'.$ID.'-'.$producto;
		echo '<!--['.$i.'] -'.$ID.'-->
               <div class="product-item lm-col-4">
                  <div class="product-wrap">
                     <a class="product-mask" href="'.$link_zp.'">
                        <div class="product-meta">
                           <div class="product-categories">
                              <span>Ver</span><span>M&aacute;s</span>			
                           </div>
                           <!--span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span-->
                        </div>
                        <img width="720" height="270" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 1600w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 768w, '.$page_url.'modulos/productos/fotos/'.$cover.' 1024w, '.$page_url.'modulos/productos/fotos/'.$cover.' 720w" sizes="(max-width: 720px) 100vw, 720px">
                     </a>
                  </div>
                  <!--h3><a href="'.$link_zp.'">'.$nom_producto.'</a></h3-->
				  <div class="mlink">
                  	<a href="'.$link_zp.'" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">'.$nom_producto.'</a>
				  </div>
               </div>
			';

		}
	}
	if($i==0){
				echo '<div class="col-lg-12 col-xs-12">
					<div>Por el momento no hay productos disponibles.</div>
				</div>
  				';
	}
  }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
  }
  echo '<!-- /productos.json -->';
 }
}

function item_subproductos2($id,$idp){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;
sql_opciones('link_productos',$valor);
$fjson='productos';
$path_JSON='bloques/webservices/rest/json'.$fjson.'.json';
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/?t='.$fjson;}
 if($path_JSON){
  $objData=file_get_contents($path_JSON);
  $Data=json_decode($objData,true);
  if($Data!='' && $Data!=NULL){
  $i=0;
  echo '<!-- productos.json -->';
	foreach ($Data as $reg){$i++;
		$ID=$reg['ID'];
		$cover=$reg['cover'];
		cadena_replace($replace1,$replace2);		
		$producto=str_replace($replace1,$replace2,$reg['nombre']);
		$nom_producto=$reg['nombre'];
		$descripcion=$reg['descripcion'];
		$ID_cate=$reg['ID_cate'];
		$ID_sub_cate=$reg['ID_sub_cate'];
		$ID_sub_cate2=$reg['ID_sub_cate2'];
		$visible=$reg['visible'];

		if($visible==1 && $ID_sub_cate2==$id && $ID_sub_cate2!=0){

		$objData2=file_get_contents("modulos/productos/categorias.json");
		$Data2=json_decode($objData2,true);
			foreach ($Data2 as $row){
				$ID_cate2=$row['ID_cate'];
				if($ID_cate==$ID_cate2){
					$categoria=$row['categoria'];
				}
			}
		$objData3=file_get_contents("modulos/productos/subcategorias.json");
		$Data3=json_decode($objData3,true);
			foreach ($Data3 as $row){
				$ID_cate3=$row['ID_sub_cate'];
				if($ID_cate==$ID_cate3){
					$subcategoria=$row['subcategoria'];
				}
			}

		$tema_p=$_GET['tema_previo'];
		$link_zp=($tema_p!='')?$page_url.'index.php?mod='.$mod.'&ext=item&id='.$ID.'&tema_previo='.$tema_p:$page_url.'productos/item/'.$ID.'-'.$producto;
		echo '<!--['.$i.'] -'.$ID.'-->
               <div class="product-item lm-col-4">
                  <div class="product-wrap">
                     <a class="product-mask" href="'.$link_zp.'">
                        <div class="product-meta">
                           <div class="product-categories">
                              <span>Ver</span><span>M&aacute;s</span>			
                           </div>
                           <!--span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span-->
                        </div>
                        <img width="720" height="270" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 1600w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 768w, '.$page_url.'modulos/productos/fotos/'.$cover.' 1024w, '.$page_url.'modulos/productos/fotos/'.$cover.' 720w" sizes="(max-width: 720px) 100vw, 720px">
                     </a>
                  </div>
                  <!--h3><a href="'.$link_zp.'">'.$nom_producto.'</a></h3-->
				  <div class="mlink">
                  <a href="'.$link_zp.'" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">'.$nom_producto.'</a>
				  </div>
               </div>
			';
		}
	}
	if($i==0){
				echo '<div class="col-lg-12 col-xs-12">
					<div>Por el momento no hay productos disponibles.</div>
				</div>
  				';
	}
   }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
   }
  echo '<!-- /productos.json -->';
 }
}

function item_cate(){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;

$menu_json='categorias.json';
$path_JSON='modulos/productos/'.$menu_json;

if(file_exists($path_JSON) && filesize($path_JSON)!=0){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
if($Data!='' && $Data!=NULL){
$i=0;
echo '<!-- categorias.json -->';
	foreach ($Data as $reg){$i++;
		$ID_cate=$reg['ID_cate'];
		cadena_replace($replace1,$replace2);
		$categoria=str_replace($replace1,$replace2,$reg['categoria']);
		$nom_categoria=$reg['categoria'];
		$cover=$reg['cover'];
		$visible=$reg['visible'];

		if($visible==1){
$tema_p=$_GET['tema_previo'];
$link_zp=($tema_p!='')?$page_url.'index.php?mod='.$mod.'&ext=categoria&id='.$ID_cate.'&tema_previo='.$tema_p:$page_url.'productos/categoria/'.$ID_cate.'-'.$categoria;
		echo '<!--['.$i.'] -'.$ID_cate.'-->
               <div class="product-item lm-col-4">
                  <div class="product-wrap">
                     <a class="product-mask" href="'.$link_zp.'">
                        <div class="product-meta">
                           <div class="product-categories">
                              <span>Ver</span><span>M&aacute;s</span>			
                           </div>
                           <!--span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span-->
                        </div>
                        <img width="720" height="270" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 1600w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 768w, '.$page_url.'modulos/productos/fotos/'.$cover.' 1024w, '.$page_url.'modulos/productos/fotos/'.$cover.' 720w" sizes="(max-width: 720px) 100vw, 720px">
                     </a>
                  </div>
                  <!--h3><a href="'.$link_zp.'">'.$nom_categoria.'</a></h3-->
                  <a href="'.$link_zp.'" class="button product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">'.$nom_categoria.'</a>
               </div>
			';
		}
	}
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
 }
 echo '<!-- /categorias.json -->';
}else{
echo '<!-- mysql -->';
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE visible=1 ORDER BY ID_cate ASC;") or print mysqli_error($mysqli); 
$num_rows = mysqli_num_rows($sql);
  if($num_rows!=0){
	while($reg=mysqli_fetch_array($sql)){
		$ID_cate=$reg['ID_cate'];
		cadena_replace($replace1,$replace2);
		$categoria=str_replace($replace1,$replace2,$reg['categoria']);
		$nom_categoria=$reg['categoria'];
		$cover=$reg['cover'];
		//$visible=$reg['visible'];
$tema_p=$_GET['tema_previo'];
$link_zp=($tema_p!='')?$page_url.'index.php?mod='.$mod.'&ext=categoria&id='.$ID_cate.'&tema_previo='.$tema_p:$page_url.'productos/categoria/'.$ID_cate.'-'.$categoria;
		echo '<!--['.$i.'] -'.$ID_cate.'-->
               <div class="product-item lm-col-4">
                  <div class="product-wrap">
                     <a class="product-mask" href="'.$link_zp.'">
                        <div class="product-meta">
                           <div class="product-categories">
                              <span>Ver</span><span>M&aacute;s</span>			
                           </div>
                           <!--span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span-->
                        </div>
                        <img width="720" height="270" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 1600w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 768w, '.$page_url.'modulos/productos/fotos/'.$cover.' 1024w, '.$page_url.'modulos/productos/fotos/'.$cover.' 720w" sizes="(max-width: 720px) 100vw, 720px">
                     </a>
                  </div>
                  <!--h3><a href="'.$link_zp.'">'.$nom_categoria.'</a></h3-->
                  <a href="'.$link_zp.'" class="button product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">'.$nom_categoria.'</a>
               </div>
			';
	}

  }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
  }
  echo '<!--/ mysql -->';
 }
}

function item_sub_cate($id){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;

$menu_json='subcategorias.json';
$path_JSON='modulos/productos/'.$menu_json;

if(file_exists($path_JSON) && filesize($path_JSON)!=0){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
if($Data!='' && $Data!=NULL){
$i=0;
echo '<!-- subcategoria.json -->';
	foreach ($Data as $reg){$i++;
		$ID_sub_cate=$reg['ID_sub_cate'];
		$ID_cate=$reg['ID_cate'];
		cadena_replace($replace1,$replace2);
		$subcategoria=str_replace($replace1,$replace2,$reg['subcategoria']);
		$nom_subcategoria=$reg['subcategoria'];
		$cover=$reg['cover'];
		$visible=$reg['visible'];		
		if($visible==1 && $ID_cate==$id){
$tema_p=$_GET['tema_previo'];
$link_zp=($tema_p!='')?$page_url.'index.php?mod='.$mod.'&ext=subcategoria&id='.$ID_sub_cate.'&tema_previo='.$tema_p:$page_url.'productos/subcategoria/'.$ID_sub_cate.'-'.$subcategoria;
		echo '<!--['.$i.'] -'.$ID_sub_cate.'-->
               <div class="product-item lm-col-4">
                  <div class="product-wrap">
                     <a class="product-mask" href="'.$link_zp.'">
                        <div class="product-meta">
                           <div class="product-categories">
                              <span>Ver</span><span>M&aacute;s</span>			
                           </div>
                           <!--span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span-->
                        </div>
                        <img width="720" height="270" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 1600w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 768w, '.$page_url.'modulos/productos/fotos/'.$cover.' 1024w, '.$page_url.'modulos/productos/fotos/'.$cover.' 720w" sizes="(max-width: 720px) 100vw, 720px">
                     </a>
                  </div>
                  <!--h3><a href="'.$link_zp.'">'.$nom_subcategoria.'</a></h3-->
                  <a href="'.$link_zp.'" class="button product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">'.$nom_subcategoria.'</a>
               </div>
			';
		}
	}
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
 }
	echo '<!-- /subcategorias.json -->';
}else{
echo '<!-- mysql -->';
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate WHERE visible=1 AND ID_cate=".$id." ORDER BY ID_sub_cate ASC;") or print mysqli_error($mysqli); 
$num_rows = mysqli_num_rows($sql);
  if($num_rows!=0){
	while($reg=mysqli_fetch_array($sql)){
		$ID_sub_cate=$reg['ID_sub_cate'];
		$ID_cate=$reg['ID_cate'];
		cadena_replace($replace1,$replace2);
		$subcategoria=str_replace($replace1,$replace2,$reg['subcategoria']);
		$nom_subcategoria=$reg['subcategoria'];
		$cover=$reg['cover'];
		//$visible=$reg['visible'];
$tema_p=$_GET['tema_previo'];
$link_zp=($tema_p!='')?$page_url.'index.php?mod='.$mod.'&ext=subcategoria&id='.$ID_sub_cate.'&tema_previo='.$tema_p:$page_url.'productos/subcategoria/'.$ID_sub_cate.'-'.$subcategoria;
		echo '<!--['.$i.'] -'.$ID_sub_cate.'-->
               <div class="product-item lm-col-4">
                  <div class="product-wrap">
                     <a class="product-mask" href="'.$link_zp.'">
                        <div class="product-meta">
                           <div class="product-categories">
                              <span>Ver</span><span>M&aacute;s</span>			
                           </div>
                           <!--span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span-->
                        </div>
                        <img width="720" height="270" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 1600w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 768w, '.$page_url.'modulos/productos/fotos/'.$cover.' 1024w, '.$page_url.'modulos/productos/fotos/'.$cover.' 720w" sizes="(max-width: 720px) 100vw, 720px">
                     </a>
                  </div>
                  <!--h3><a href="'.$link_zp.'">'.$nom_subcategoria.'</a></h3-->
                  <a href="'.$link_zp.'" class="button product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">'.$nom_subcategoria.'</a>
               </div>
			';
	}
  }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
  }
	echo '<!--/ mysql -->';
 }
}

function item_sub_cate2($id){
global $mysqli,$DBprefix,$page_url,$mod,$ext,$opc;

$menu_json='subcategorias2.json';
$path_JSON='modulos/productos/'.$menu_json;

if(file_exists($path_JSON) && filesize($path_JSON)!=0){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
if($Data!='' && $Data!=NULL){
$i=0;
echo '<!-- subcategoria2.json -->';
	foreach ($Data as $reg){$i++;
		$ID_sub_cate2=$reg['ID_sub_cate2'];
		cadena_replace($replace1,$replace2);
		$subcategoria2=str_replace($replace1,$replace2,$reg['subcategoria2']);
		$nom_subcategoria2=$reg['subcategoria2'];
		$ID_cate=$reg['ID_cate'];
		$ID_sub_cate=$reg['ID_sub_cate'];
		$cover=$reg['cover'];
		$visible=$reg['visible'];		
		if($visible==1 && $ID_sub_cate==$id){
$tema_p=$_GET['tema_previo'];
$link_zp=($tema_p!='')?$page_url.'index.php?mod='.$mod.'&ext=subcategoria2&id='.$ID_sub_cate2.'&tema_previo='.$tema_p:$page_url.'productos/subcategoria2/'.$ID_sub_cate2.'-'.$subcategoria2;
		echo '<!--['.$i.'] -'.$ID_sub_cate.'-->
               <div class="product-item lm-col-4">
                  <div class="product-wrap">
                     <a class="product-mask" href="'.$link_zp.'">
                        <div class="product-meta">
                           <div class="product-categories">
                              <span>Ver</span><span>M&aacute;s</span>			
                           </div>
                           <!--span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span-->
                        </div>
                        <img width="720" height="270" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 1600w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 768w, '.$page_url.'modulos/productos/fotos/'.$cover.' 1024w, '.$page_url.'modulos/productos/fotos/'.$cover.' 720w" sizes="(max-width: 720px) 100vw, 720px">
                     </a>
                  </div>
                  <!--h3><a href="'.$link_zp.'">'.$nom_subcategoria2.'</a></h3-->
                  <a href="'.$link_zp.'" class="button product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">'.$nom_subcategoria2.'</a>
               </div>
			';
		}
	}
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
 }
	echo '<!-- /subcategorias.json -->';
}else{
echo '<!-- mysql -->';
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_sub_cate2 WHERE visible=1 AND ID_cate=".$id." ORDER BY ID_sub_cate ASC;") or print mysqli_error($mysqli); 
$num_rows = mysqli_num_rows($sql);
  if($num_rows!=0){
	while($reg=mysqli_fetch_array($sql)){
		$ID_sub_cate=$reg['ID_sub_cate'];
		$ID_cate=$reg['ID_cate'];
		cadena_replace($replace1,$replace2);		
		$subcategoria=str_replace($replace1,$replace2,$reg['subcategoria']);
		$nom_subcategoria=$reg['subcategoria'];
		$cover=$reg['cover'];
		//$visible=$reg['visible'];
$tema_p=$_GET['tema_previo'];
$link_zp=($tema_p!='')?$page_url.'index.php?mod='.$mod.'&ext=subcategoria&id='.$ID_sub_cate.'&tema_previo='.$tema_p:$page_url.'productos/subcategoria/'.$ID_sub_cate.'-'.$subcategoria;
		echo '<!--['.$i.'] -'.$ID_sub_cate.'-->
               <div class="product-item lm-col-4">
                  <div class="product-wrap">
                     <a class="product-mask" href="'.$link_zp.'">
                        <div class="product-meta">
                           <div class="product-categories">
                              <span>Ver</span><span>M&aacute;s</span>			
                           </div>
                           <!--span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></span-->
                        </div>
                        <img width="720" height="270" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 1600w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 768w, '.$page_url.'modulos/productos/fotos/'.$cover.' 1024w, '.$page_url.'modulos/productos/fotos/'.$cover.' 720w" sizes="(max-width: 720px) 100vw, 720px">
                     </a>
                  </div>
                  <!--h3><a href="'.$link_zp.'">'.$nom_subcategoria2.'</a></h3-->
                  <a href="'.$link_zp.'" class="button product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">'.$nom_subcategoria2.'</a>
               </div>
			';
	}
  }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
  }
	echo '<!--/ mysql -->';
 }
}

function one_producto($id){
global $mysqli,$DBprefix,$url,$page_url,$mod,$ext,$opc,$path_tema,$tema_previo,$path_jsonDB;
$tabla='productos';
$api_url=$page_url.'bloques/ws/t/?t='.$tabla;
$urlexists = url_exists($api_url);
$path_JSON=($urlexists!=1)?$path_jsonDB.$tabla.'.json':$api_url;	
if($path_JSON){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);
usort($Data, function($a, $b){return strnatcmp($a['ord'], $b['ord']);});//Orden del menu
if($Data!='' && $Data!=NULL){
$i=0;
if($_SESSION['level']!=-1){echo '<!-- '.$tabla.'.json -->'."\n\r";}else{echo '<!-- '.$tabla.'.json URL:('.$path_JSON.')-->'."\n\r";}
	foreach ($Data as $reg){$i++;cadena_replace($replace1,$replace2);
		$ID=$reg['ID'];
		$cover=$reg['cover'];
		cadena_replace($replace1,$replace2);		
		$producto=str_replace($replace1,$replace2,$reg['nombre']);
		$nom_producto=$reg['nombre'];
		$descripcion=$reg['descripcion'];
		$resena=$reg['resena'];
		$ID_cate=$reg['ID_cate'];
		$ID_sub_cate=$reg['ID_sub_cate'];
		$imagen1=$reg['imagen1'];
		$imagen2=$reg['imagen2'];
		$imagen3=$reg['imagen3'];
		$imagen4=$reg['imagen4'];
		$imagen5=$reg['imagen5'];
		$vid=$pdf=$reg['file'];
		$url_s=str_replace('watch?v=','embed/',$vid);
		$tipo=$reg['tipo'];
		$visible=$reg['visible'];
		nombre_cate_subcate($id,$data_cate_subcate);
		if($visible==1 && $ID==$id){

		$pdf_img=($pdf!='')?'<img src="'.$page_url.'modulos/productos/fotos/pdf_icon.png" width="25"> <a target="_blank" href="'.$page_url.'modulos/productos/pdf/'.$pdf.'" class="al">DESCARGA FICHAS TECNICAS</a>':'&nbsp;';
		echo '<!--['.$i.'] -'.$ID.'-->
      <!-- BEGIN row -->
      <div class="row">
         <!-- BEGIN col-5 -->
         <div class="lm-col-5">
            <div class="product-single-image">
               <div class="images">
                  <div class="flexslider flex-product">
                     <ul class="slides">
                        <span class="preloader" style="display: none;"></span>';
/*
if($cover!=''){
	echo '<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide"><a href="'.$page_url.'modulos/productos/fotos/'.$cover.'" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]"><img width="500" height="500" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-shop_single size-shop_single" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 500w, '.$page_url.'modulos/productos/fotos/'.$cover.' 150w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 100w" sizes="(max-width: 500px) 100vw, 500px" draggable="true"></a></li>';
}*/						
if($imagen1!=''){
	echo '<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide"><a href="'.$page_url.'modulos/productos/fotos/'.$imagen1.'" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]"><img width="500" height="500" src="'.$page_url.'modulos/productos/fotos/'.$imagen1.'" class="attachment-shop_single size-shop_single" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen1.' 500w, '.$page_url.'modulos/productos/fotos/'.$imagen1.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen1.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen1.' 100w" sizes="(max-width: 500px) 100vw, 500px" draggable="true"></a></li>';
}						
if($imagen2!=''){
	echo '<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide"><a href="'.$page_url.'modulos/productos/fotos/'.$imagen2.'" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]"><img width="500" height="500" src="'.$page_url.'modulos/productos/fotos/'.$imagen2.'" class="attachment-shop_single size-shop_single" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen2.' 500w, '.$page_url.'modulos/productos/fotos/'.$imagen2.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen2.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen2.' 100w" sizes="(max-width: 500px) 100vw, 500px" draggable="true"></a></li>';
}						
if($imagen3!=''){
	echo '<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide"><a href="'.$page_url.'modulos/productos/fotos/'.$imagen3.'" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]"><img width="500" height="500" src="'.$page_url.'modulos/productos/fotos/'.$imagen3.'" class="attachment-shop_single size-shop_single" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen3.' 500w, '.$page_url.'modulos/productos/fotos/'.$imagen3.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen3.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen3.' 100w" sizes="(max-width: 500px) 100vw, 500px" draggable="true"></a></li>';
}						
if($imagen4!=''){
	echo '<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide"><a href="'.$page_url.'modulos/productos/fotos/'.$imagen4.'" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]"><img width="500" height="500" src="'.$page_url.'modulos/productos/fotos/'.$imagen4.'" class="attachment-shop_single size-shop_single" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen4.' 500w, '.$page_url.'modulos/productos/fotos/'.$imagen4.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen4.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen4.' 100w" sizes="(max-width: 500px) 100vw, 500px" draggable="true"></a></li>';
}						
if($imagen5!=''){
	echo '<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide"><a href="'.$page_url.'modulos/productos/fotos/'.$imagen5.'" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]"><img width="500" height="500" src="'.$page_url.'modulos/productos/fotos/'.$imagen5.'" class="attachment-shop_single size-shop_single" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen5.' 500w, '.$page_url.'modulos/productos/fotos/'.$imagen5.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen5.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen5.' 100w" sizes="(max-width: 500px) 100vw, 500px" draggable="true"></a></li>';
}

echo'               </ul>
                     <ul class="flex-direction-nav">
                        <li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li>
                        <li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li>
                     </ul>
                  </div>
                  <div class="thumbnails row">
                     <nav class="thumbnails-nav">
                        <ul>
                           <div class="thumbnails">';
/*
if($cover!=''){
	echo '<li><a href="'.$page_url.'modulos/productos/fotos/'.$cover.'" title="" data-rel="thumbnails"><img width="100" height="100" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$cover.' 100w, '.$page_url.'modulos/productos/fotos/'.$cover.' 150w, '.$page_url.'modulos/productos/fotos/'.$cover.' 300w, '.$page_url.'modulos/productos/fotos/'.$cover.' 500w" sizes="(max-width: 100px) 100vw, 100px"></a></li>';
}*/						   
if($imagen1!=''){
	echo '<li><a href="'.$page_url.'modulos/productos/fotos/'.$imagen1.'" title="" data-rel="thumbnails"><img width="100" height="100" src="'.$page_url.'modulos/productos/fotos/'.$imagen1.'" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen1.' 100w, '.$page_url.'modulos/productos/fotos/'.$imagen1.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen1.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen1.' 500w" sizes="(max-width: 100px) 100vw, 100px"></a></li>';
}						   
if($imagen2!=''){
	echo '<li><a href="'.$page_url.'modulos/productos/fotos/'.$imagen2.'" title="" data-rel="thumbnails"><img width="100" height="100" src="'.$page_url.'modulos/productos/fotos/'.$imagen2.'" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen2.' 100w, '.$page_url.'modulos/productos/fotos/'.$imagen2.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen2.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen2.' 500w" sizes="(max-width: 100px) 100vw, 100px"></a></li>';
}						   
if($imagen3!=''){
	echo '<li><a href="'.$page_url.'modulos/productos/fotos/'.$imagen3.'" title="" data-rel="thumbnails"><img width="100" height="100" src="'.$page_url.'modulos/productos/fotos/'.$imagen3.'" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen3.' 100w, '.$page_url.'modulos/productos/fotos/'.$imagen2.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen3.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen3.' 500w" sizes="(max-width: 100px) 100vw, 100px"></a></li>';
}						   
if($imagen4!=''){
	echo '<li><a href="'.$page_url.'modulos/productos/fotos/'.$imagen4.'" title="" data-rel="thumbnails"><img width="100" height="100" src="'.$page_url.'modulos/productos/fotos/'.$imagen4.'" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen4.' 100w, '.$page_url.'modulos/productos/fotos/'.$imagen3.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen4.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen4.' 500w" sizes="(max-width: 100px) 100vw, 100px"></a></li>';
}						   
if($imagen5!=''){
	echo '<li><a href="'.$page_url.'modulos/productos/fotos/'.$imagen5.'" title="" data-rel="thumbnails"><img width="100" height="100" src="'.$page_url.'modulos/productos/fotos/'.$imagen5.'" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="'.$page_url.'modulos/productos/fotos/'.$imagen5.' 100w, '.$page_url.'modulos/productos/fotos/'.$imagen5.' 150w, '.$page_url.'modulos/productos/fotos/'.$imagen5.' 300w, '.$page_url.'modulos/productos/fotos/'.$imagen5.' 500w" sizes="(max-width: 100px) 100vw, 100px"></a></li>';
}						   
echo '
                           </div>
                        </ul>
                     </nav>
                  </div>';
if($vid!=''){
	echo '
	<div class="vid1">Consulta el video:</div>
	<div class="vid2">&iquest;C&oacute;mo se utiliza AnaConDa en ventilaci&oacute;n mec&aacute;nica?</div>
	<div>
		<iframe width="374" height="210" src="'.$url_s.'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>';
}
echo '
               </div>
            </div>
         </div>
         <!-- END col-5 -->
         <!-- BEGIN col-7 -->
         <div class="lm-col-7">
            <div class="summary entry-summary">
               <h1 class="product_title entry-title">'.$nom_producto.'</h1>
               <!--div class="rating-price">
                  <p class="price"><span class="woocommerce-Price-amount amount">Precio: <span class="woocommerce-Price-currencySymbol">$</span>190,298.00</span></p>
               </div-->
               <!--form class="cart" action="#" method="post" enctype="multipart/form-data">
                  <div class="quantity">
                     <label class="screen-reader-text" for="quantity_5b86eaa827631">Cantidad</label>
                     <input type="number" id="quantity_5b86eaa827631" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Cantidad" size="4" pattern="[0-9]*" inputmode="numeric" aria-labelledby="">
                  </div>
                  <button type="submit" name="add-to-cart" value="1127027" class="single_add_to_cart_button button alt">Añadir al carrito</button>
               </form-->
               <div class="product_meta has-meta">
                  <span class="posted_in">Descripci&oacute;n: </span>
                  <p class="site-description">'.$descripcion.'</p>
                  <p>'.$resena.'</p>
                  <!--span class="sku_wrapper">Categor&iacute;as: '.$data_cate_subcate[2].' - '.$data_cate_subcate[4].'</span-->
                  <p></p>
               </div>
               <!--div class="social-meta-wrap">
                  <div class="divider">
                     <h3>Compartir</h3>
                  </div>
                  <ul class="social-meta">
                     <li><a class="facebook-share" href="https://www.facebook.com/sharer/sharer.php?u=https://www.manprec.com/producto/paquete-1-stimuplex-philips/" onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;"><i class="fa fa-facebook"></i></a></li>
                     <li><a class="twitter-share" href="https://twitter.com/share?text=PAQUETE 1   Stimuplex y Philips&amp;url=https://www.manprec.com/producto/paquete-1-stimuplex-philips/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                     <li><a class="google-share" href="https://plus.google.com/share?url=https://www.manprec.com/producto/paquete-1-stimuplex-philips/" onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;"><i class="fa fa-google-plus"></i></a></li>
                     <li><a class="pinterest-share" href="//pinterest.com/pin/create/button/?url=https://www.manprec.com/producto/paquete-1-stimuplex-philips/&amp;media=https://www.manprec.com/wp-content/uploads/2018/08/1.png&amp;description=PAQUETE 1   Stimuplex y Philips"><i class="fa fa-pinterest"></i></a></li>
                  </ul>
               </div-->
            </div>
            <!-- .summary -->
         </div>
         <!-- END col-7 -->
      </div>
      <!-- END row -->
		';
			
			
		}
	}
 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento el producto no esta disponible o no existe.</div>
		</div>
  ';
 }
 echo '<!-- /'.$tabla.'.json -->';
}
}


function productos_destacados(){
global $mysqli,$DBprefix,$page_url,$path_tema,$mod,$ext,$opc;
sql_opciones('link_productos',$valor);
$fjson='productos';
$path_JSON='bloques/webservices/rest/json'.$fjson.'.json';
if(!file_exists($path_JSON)){$path_JSON=$page_url.'bloques/ws/t/?t='.$fjson;}

if(file_exists($path_JSON) && filesize($path_JSON)!=0){
$objData=file_get_contents($path_JSON);
$Data=json_decode($objData,true);

if($Data!='' && $Data!=NULL){
$i=0;
echo '<!-- productos.json -->';
	foreach ($Data as $reg){$i++;
		$ID=$reg['ID'];
		$cover=$reg['cover'];
		$replace=array(' ','.',',','(',')');
		$producto=str_replace($replace,"-",$reg['nombre']);
		$nom_producto=$reg['nombre'];
		$descripcion=$reg['descripcion'];
		$ID_cate=$reg['ID_cate'];
		$ID_sub_cate=$reg['ID_sub_cate'];
		$destacado=$reg['land'];
		$visible=$reg['visible'];
		
		if($visible==1 && $destacado==1){
		$objData2=file_get_contents("modulos/productos/categorias.json");
		$Data2=json_decode($objData2,true);
			foreach ($Data2 as $row){
				$ID_cate2=$row['ID_cate'];
				if($ID_cate==$ID_cate2){
					$categoria=$row['categoria'];
				}
			}
			
		$tema_p=$_GET['tema_previo'];
		$link_zp=($tema_p!='')?'<a href="'.$page_url.'index.php?mod=productos&ext=item&id='.$ID.'&tema_previo='.$tema_p.'">':'<a href="'.$page_url.'productos/item/'.$ID.'-'.$producto.'">';
		echo '<!--['.$i.'] -'.$reg['ID'].'-->
                                        <div class="column mcb-column one-fourth column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style=" padding:0 3% 0 0;">
                                                <hr class="no_line" style="margin: 0 auto 20px;" />
                                                <div style="padding: 20px 6%; background: url('.$page_url.$path_tema.'img/home_agro_sep2.png) repeat-y right top">
												'.$link_zp.'
                                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt="home_agro_product1s" width="380" height="282" /></div>
                                                </div>
                                                    <p style="font-weight:700; text-transform:uppercase;" class="themecolor">'.$nom_producto.'</p>
                                                    <p style="color:#444; font-weight:700;">'.$categoria.'</p>
                                                    <!--p style="margin: 0px;"><i class="icon-doc-text themecolor"></i> <a href="#">Phasellus fermen</a></p-->
                                                </div>
												</a>
                                            </div>
                                        </div>
			';
		}
	}
	if($i==0){
				echo '<div class="col-lg-12 col-xs-12">
					<div>Por el momento no hay productos disponibles.</div>
				</div>
  				';
	}

 }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
 }
 echo '<!-- /productos.json -->';
}else{
echo '<!-- mysql -->';
$sql=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos WHERE visible=1 AND land=1 ORDER BY ID ASC;") or print mysqli_error($mysqli); 
$num_rows = mysqli_num_rows($sql);
  if($num_rows!=0){
	while($reg=mysqli_fetch_array($sql)){
		$ID=$reg['ID'];
		$cover=$reg['cover'];
		$replace=array(' ','.',',','(',')');
		$producto=str_replace($replace,"-",$reg['nombre']);
		$nom_producto=$reg['nombre'];
		$descripcion=$reg['descripcion'];
		$ID_cate=$reg['ID_cate'];
		$ID_sub_cate=$reg['ID_sub_cate'];
		//$visible=$reg['visible'];

		$sql2=mysqli_query($mysqli,"SELECT * FROM ".$DBprefix."productos_cate WHERE ID_cate=".$ID_cate.";") or print mysqli_error($mysqli); 
		while($row=mysqli_fetch_array($sql2)){$categoria=$row['categoria'];}

		$tema_p=$_GET['tema_previo'];
		$link_zp=($tema_p!='')?'<a href="'.$page_url.'index.php?mod=productos&ext=item&id='.$ID.'&tema_previo='.$tema_p.'">':'<a href="'.$page_url.'productos/item/'.$ID.'-'.$producto.'">';
		echo '<!--'.$reg['ID'].'-->
                                        <div class="column mcb-column one-fourth column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style=" padding:0 3% 0 0;">
                                                <hr class="no_line" style="margin: 0 auto 20px;" />
                                                <div style="padding: 20px 6%; background: url('.$page_url.$path_tema.'img/home_agro_sep2.png) repeat-y right top">
												'.$link_zp.'
                                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="'.$page_url.'modulos/productos/fotos/'.$cover.'" alt="home_agro_product1s" width="380" height="282" /></div>
                                                </div>
                                                    <p style="font-weight:700; text-transform:uppercase;" class="themecolor">'.$nom_producto.'</p>
                                                    <p style="color:#444; font-weight:700;">'.$categoria.'</p>
                                                    <!--p style="margin: 0px;"><i class="icon-doc-text themecolor"></i> <a href="#">Phasellus fermen</a></p-->
                                                </div>
												</a>
                                            </div>
                                        </div>
			';
	}
  }else{
	echo '<div class="col-lg-12 col-xs-12">
			<div>Por el momento no hay productos disponibles.</div>
		</div>
  ';
  }
 echo '<!--/ mysql -->';
 }
}

function item_curso($id){
	if($id==6){
	   echo '
<div class="product-item lm-col-6">
	<div class="cur1">Dispositivo AnaConDa</div>

	<div class="cur2">Una introducci&oacute;n y gu&iacute;a de recursos</div>

	<div class="cur3">Este <b>e-learning</b> es una herramienta para que los usuarios se familiaricen con AnaConDa. Con esta capacitaci&oacute;n, aprender&aacute; sobre los aspectos pr&aacute;cticos del uso de AnaConDa y los medicamentos que ofrece. Los nuevos usuarios pueden aprender del programa de entrenamiento completo. Los usuarios experimentados pueden actualizar sus conocimientos y aprender m&aacute;s. Adem&aacute;s de la informaci&oacute;n educativa, hay una secci&oacute;n de Preguntas frecuentes, preguntas del cuestionario y una lista de referencias.</div>

	<div class="cur4">Completa el entrenamiento y domina el AnaConDa.</div>

	<a target="_blank" href="https://anaconda.learnways.com/" class="clink">https://anaconda.learnways.com/</a>
</div>	
	';
	}
    elseif($id==5){
        echo '<div class="product-item lm-col-6 text-center">Somos distribuidores de establecimientos de &oacute;ptica<br>No venta al p&uacute;blico en general</div>';
    }
}

?>