<?php 
include 'admin/functions.php'; 
$id=$_GET['id'];
//$idp=$_GET['idp'];
?>
<style>div#heads{padding-bottom:0;}.mlink{height:56px;}</style>
<!--PRODUCTOS-->
<!--
<div id="page-title-wrap" style="background-image:url(<?php echo $page_url.'modulos/productos/img/banners/GM85_banner.png';?>); background-repeat:no-repeat; background-position:center top; background-color:#f6f6f6; height:450px;" class="header-light has-thumb">
	<div class="page-title">
		<h1>PRODUCTOS</h1>
	</div>
</div>-->
        <!-- BEGIN #page -->
        <div id="page" class="hfeed ">
            <!-- BEGIN #main -->
            <div id="main" class="container sidebar-left">

                <!-- BEGIN #content -->
                <div id="content" class="" role="main">
                    <!-- BEGIN #post -->
                    <article id="post-6" class="post-6 page type-page status-publish hentry">
                        <div class="entry-page-items">
                            <!-- BEGIN full-section -->
                            <div class="page-row full-img parallax-yes margin-yes padding-top-yes padding-bottom-yes dark repeat-no" style="background-color:#ffffff;" id="heads">

                                <!-- BEGIN page-row -->
                                <div class="page-row ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="lm-col-12 lol-page-item">
                                                <div class="lol-item-heading">
                                                    <h2>PRODUCTOS</h2>
                                                    <p></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END page-row -->

                            </div>    
						</div>
					</article>
				</div><!-- /BEGIN #content -->


<div class="row">
   <!-- BEGIN col-9 -->
   <div class="cont lm-col-9">
      <!-- BEGIN #content -->
      <div id="content" role="main">
         <div class="products-filter">
            <div class="box-logos">
              <!--?php logos_marcas($id);?-->
            </div>
            <p class="woocommerce-result-count">
            </p><!--
            <form class="woocommerce-ordering" method="get">
               <select name="orderby" class="orderby">
                  <option value="menu_order" selected="selected">Orden predeterminado</option>
                  <option value="popularity">Lo más popular</option>
                  <option value="rating">Lo mejor calificado</option>
                  <option value="date">Lo más nuevo</option>
                  <option value="price">Lo más económico</option>
                  <option value="price-desc">Lo más costoso</option>
               </select>
               <input type="hidden" name="paged" value="1">
            </form>-->
         </div>
         <div class="products">
            <div class="row">
				<?php item_subproductos($id,$idp);?>
               	<?php item_sub_cate2($id);?>
            </div>
         </div>
      </div>
      <!-- END #content -->
   </div>
   <!-- END col-9 -->
   <!-- BEGIN col-3 -->
   <div class="side lm-col-3">
      <!-- BEGIN #sidebar -->
      <div id="sidebar" role="complementary">
         <aside id="woocommerce_product_categories-2" class="widget woocommerce widget_product_categories">
            <div class="widget-header">
               <h3 class="widget-title">CATEGOR&Iacute;AS</h3>
            </div>
			<?php menu_categoria();?>
         </aside>
      </div>
      <!-- END #sidebar -->
      <!-- END col-3 -->
   </div>
</div>

			</div><!-- /BEGIN #main -->
		</div><!-- /BEGIN #page -->
<!--/PRODUCTOS-->