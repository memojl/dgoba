<?php
//include 'admin/functions.php';
$id=$_GET['id'];
if($id!=''){
	$sql=mysqli_query($mysqli,"UPDATE ".$DBprefix."opciones SET valor={$id} WHERE nom='email_test'");
	$sql1=mysqli_query($mysqli,"SELECT url FROM ".$DBprefix."menu_web WHERE modulo='{$mod}'");	
	if($row=mysqli_fetch_array($sql1)){$url1=$row['url'];}
	sql_opciones('link_var',$valor);
	$URL=($valor==1)?$page_url.'index.php?mod=contacto':$page_url.$url1;	
	recargar(1,$URL,$target);		
}

sql_opciones('email_test',$valor);
$act_prueba=($valor==1)?'<a href="'.$page_url.'index.php?mod=contacto&id=0" style="color:#f00;">Desactivar Prueba</a>':'<a href="'.$page_url.'index.php?mod=contacto&id=1" style="color:#0ff;">Activar Prueba</a>';

if($nivel_login==-1 && $username=='admin'){
	email_forms($CoR1,$CoE1,$BCC1);	
	$aviso.= '<div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-info"></i> Info</h4>
				['.$act_prueba.'] [<a href="index.php?mod='.$mod.'&ext=admin/index"><i class="fa fa-gear"></i> Config. Contacto</a>] [<a href="'.$page_url.'index.php?mod='.$mod.'&ext=admin/index&opc=forms"><i class="fa fa-pencil-square-o"></i> Correos de Formulario</a>]<br>
                Email Recepci&oacute;n: '.$CoR1.' | '.$BCC1.' <br>
				Email Envio: '.$CoE1.'				
          </div>';
}
?>
<div><?php echo $aviso;?></div>
<form class="contact__form" method="post" action="<?php echo $page_url;?>modulos/contacto/form/mail.php">
<div class="lm-col-12 xs-col-12">
	<div class="lm-col-6 xs-col-12">
    	<div class="form-group">
        	<label for="nom">Nombre*</label>
            <input type="text" class="form-control" id="nom" name="nom" required autocomplete="off">
        </div>
	</div>
   	<div class="lm-col-6 xs-col-12">        
    	<div class="form-group">
        	<label for="ape">Apellido*</label>
            <input type="text" class="form-control" id="ape" name="ape" required autocomplete="off">
        </div>
	</div>
   	<div class="lm-col-6 xs-col-12">
    	<div class="form-group">
        	<label for="email">Correo Electr&oacute;nico*</label>
            <input type="email" class="form-control" id="email" name="email" required autocomplete="off">
        </div>
	</div>
   	<div class="lm-col-6 xs-col-12">
    	<div class="form-group">
        	<label for="tel">Tel&eacute;fono*</label>
            <input type="tel" class="form-control" id="tel" name="tel" required autocomplete="off">
        </div>
	</div>
    <div class="form-group">
        	<label for="msj">Mensaje*</label>
            <textarea class="form-control" id="msj" name="msj" cols="20" rows="10" required></textarea>
    </div>
</div>
<div class="lm-col-12 xs-col-12" style="text-align:right;">
    <input type="hidden" class="form-control" id="asunto" name="asunto" required autocomplete="off">
	<input type="submit" id="enviar" name="enviar" value="ENVIAR">
</div>
<!-- form message -->
<div class="row">
	<div class="col-12">
    	<div class="alert alert-success contact__msg" style="display:none;" role="alert">
        	Tu mensaje fue enviado exitosamente.
		</div>
	</div>
</div>
<!-- end message -->
</form>
<script src="<?php echo $page_url.'modulos/contacto/form/form.js';?>"></script>