<?php include 'admin/functions.php';?>
<div id="page-title-wrap" style="background-image:url(<?php echo $page_url.'modulos/'.$mod.'/img/banner.jpg';?>); background-repeat:no-repeat; background-position:center top;background-color:#f6f6f6; height:450px;" class="header-light has-thumb">
	<div class="page-title">
		<h1></h1>
	</div>
</div>
        <!-- BEGIN #page -->
        <div id="page" class="hfeed ">
            <!-- BEGIN #main -->
            <div id="main">
                <!-- BEGIN #content -->
                <div id="content" class="" role="main">
                    <!-- BEGIN #post -->
                    <article id="post-6" class="post-6 page type-page status-publish hentry">
                        <div class="entry-page-items">
                            <!-- BEGIN full-section -->
                            <div class="page-row full-img parallax-yes margin-yes padding-top-yes padding-bottom-yes dark repeat-no" style="background-color:#ffffff;" id="heads">

                                <!-- BEGIN page-row -->
                                <div class="page-row ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="lm-col-12 lol-page-item">
                                                <div class="lol-item-heading">
                                                    <h2>PONTE EN CONTACTO CON NOSOTROS</h2>
                                                    <p></p>
                                                </div>
                                                <div>
                                                	<div class="lm-col-12 xs-col-12" style="padding-bottom:50px;">
														<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7470.395794394436!2d-100.39257146790614!3d20.57997438624327!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d344d0dbb1822f%3A0xaa0326a183bf01ef!2sPinal+de+Amoles+201%2C+zona+dos+extendida%2C+Estrella%2C+76030+Santiago+de+Quer%C3%A9taro%2C+Qro.!5e0!3m2!1ses-419!2smx!4v1517554551743" width="100%" height="280" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                    </div>                                                   
													<div class="lm-col-8 xs-col-12">
														<?php include 'contacto.php';?>
                                                    </div>
													<div class="lm-col-4 xs-col-12">
                                                    	<img src="<?php echo $page_url.$path_tema.'images/'.$logo;?>">
                                                        <div style="font-weight:bold; padding-top:20px">
                                                        Pinal de Amoles #201 C.P. 76030<br>
                                                        Quer&eacute;taro, Qro., M&eacute;xico<br>
                                                        Tel. +52(442)2455656
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END page-row -->

                                <!-- BEGIN page-row -->
                                <div class="page-row ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="lm-col-12 lol-page-item">
                                                <div class="lol-item-heading">
                                                    <!--h2>PONTE EN CONTACTO CON NOSOTROS</h2>
                                                    <p></p-->
                                                </div>
                                                <div id="G">
													<div class="lm-col-8 xs-col-12"></div>
													<div class="lm-col-4 xs-col-12"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END page-row -->


                            </div>    
						</div>
					</article>
				</div><!-- /BEGIN #content -->
			</div><!-- /BEGIN #main -->
		</div><!-- /BEGIN #page -->
